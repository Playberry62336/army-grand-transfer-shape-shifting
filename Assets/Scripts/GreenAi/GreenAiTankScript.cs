﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenAiTankScript : MonoBehaviour
{
    [HideInInspector] public float tankSpeed;
    private GameObject spawnerObject;

    private static GreenAiTankScript instance;
    public static GreenAiTankScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<GreenAiTankScript>();
            }
            return instance;
        }
    }

    private void Start()
    {
        tankSpeed = GreenAiControllerScript.Instance.tankSpeed;
        spawnerObject = GameObject.FindGameObjectWithTag("GreenAi");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * tankSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, spawnerObject.transform.position.y, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Box.T")
        {
            Vector3 newPos = collision.gameObject.transform.position;
            Destroy(collision.gameObject);
            Instantiate(Resources.Load("Prefabs/Crate", typeof(GameObject)) as GameObject,
                newPos,
                this.transform.rotation);
        }
        else if (collision.gameObject.CompareTag("FinishLine"))
        {
            tankSpeed = GreenAiControllerScript.Instance.decreasedSpeed;
        }
        else
        {
            GreenAiControllerScript.Instance.DetectType("Tank", collision.gameObject);
        }
    }
}
