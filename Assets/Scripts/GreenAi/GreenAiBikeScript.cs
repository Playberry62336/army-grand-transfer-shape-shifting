﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenAiBikeScript : MonoBehaviour
{
    [HideInInspector] public float bikeSpeed;
    private GameObject spawnerObject;

    private static GreenAiBikeScript instance;
    public static GreenAiBikeScript Instance
    {
        get
        {
            if(instance == null)
            {
                instance = GameObject.FindObjectOfType<GreenAiBikeScript>();
            }
            return instance;
        }
    }

    private void Start()
    {
        bikeSpeed = GreenAiControllerScript.Instance.bikeSpeed;
        spawnerObject = GameObject.FindGameObjectWithTag("GreenAi");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * bikeSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, spawnerObject.transform.position.y, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("FinishLine"))
        {
            bikeSpeed = GreenAiControllerScript.Instance.decreasedSpeed;
        }
        else
        GreenAiControllerScript.Instance.DetectType("Bike", collision.gameObject);

    }

}
