﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenAiBoatScript : MonoBehaviour
{
    [SerializeField] private float boatSpeed;
    private GameObject spawnerObject;

    private void Start()
    {
        spawnerObject = GameObject.FindGameObjectWithTag("GreenAi");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * boatSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, spawnerObject.transform.position.y, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Water")
        {
            boatSpeed = GreenAiControllerScript.Instance.boatSpeed;
        }
        else if (collision.gameObject.tag != "Water")
        {
            boatSpeed = GreenAiControllerScript.Instance.decreasedSpeed;
            GreenAiControllerScript.Instance.DetectType("Boat", collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("FinishLine"))
        {
            boatSpeed = GreenAiControllerScript.Instance.decreasedSpeed;
        }
    }
}
