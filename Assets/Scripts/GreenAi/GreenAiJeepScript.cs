﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenAiJeepScript : MonoBehaviour
{
    public float jeepSpeed;
    private GameObject spawnerObject;

    private static GreenAiJeepScript instance;
    public static GreenAiJeepScript Instance
    {
        get
        {
            if(instance==null)
            {
                instance = GameObject.FindObjectOfType<GreenAiJeepScript>();
            }
            return instance;
        }
    }

    private void Start()
    {
        jeepSpeed = GreenAiControllerScript.Instance.jeepSpeed;
        spawnerObject = GameObject.FindGameObjectWithTag("GreenAi");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * jeepSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, this.transform.position.y + 1.25f, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Water")
        {
            jeepSpeed = GreenAiControllerScript.Instance.decreasedSpeed;
            GreenAiControllerScript.Instance.DetectType("Jeep", collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("FinishLine"))
        {
            jeepSpeed = GreenAiControllerScript.Instance.decreasedSpeed;
        }
        else
            GreenAiControllerScript.Instance.DetectType("Jeep", collision.gameObject);
    }
}
