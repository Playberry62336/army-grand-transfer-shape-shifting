﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class UnityAdsScript : MonoBehaviour, IUnityAdsListener
{
    private string unityID;
    private string bannerID;
    private string interstitialID;
    private string rewardedID;


    private static UnityAdsScript instance;
    public static UnityAdsScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<UnityAdsScript>();
            }
            return instance;
        }
    }


    void Start()
    {
        unityID = AdsScript.Instance.unityID;
        bannerID = AdsScript.Instance.unityBannerID;
        interstitialID = AdsScript.Instance.unityInterstitialID;
        rewardedID = AdsScript.Instance.unityRewardedID;

        Advertisement.AddListener(this);
        Advertisement.Initialize(unityID, AdsScript.Instance.testMode);
    }

    public void RequestBanner()
    {
        /*while (!Advertisement.isInitialized)
        {
            yield return new WaitForSeconds(0.5f);
        }*/
        Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
        Advertisement.Banner.Show(bannerID);
    }

    public void ShowBanner()
    {
        Advertisement.Banner.Show();
    }

    public void HideBanner()
    {
        Advertisement.Banner.Hide();
    }

    public void RequestInterstitialAd()
    {
        if (Advertisement.IsReady(interstitialID))
            Advertisement.Show(interstitialID);
    }

    public void RequestRewardedAd()
    {
        if (Advertisement.IsReady(rewardedID))
            Advertisement.Show(rewardedID);
    }

    public bool RequestBannerForScript(int position)
    {
        if(!Advertisement.isInitialized)
        {
            return false;
        }
        if(position == 0)
            Advertisement.Banner.SetPosition(BannerPosition.TOP_LEFT);
        else if(position == 1)
            Advertisement.Banner.SetPosition(BannerPosition.TOP_CENTER);
        else if(position == 2)
            Advertisement.Banner.SetPosition(BannerPosition.TOP_RIGHT);
        else if(position == 3)
            Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_LEFT);
        else if(position == 4)
            Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
        else if(position == 5)
            Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_RIGHT);

        Advertisement.Banner.Show(bannerID);
        return true;
    }

    public bool RequestInterstitialForScript()
    {
        // Check if UnityAds ready before calling Show method:
        if (Advertisement.IsReady())
        {
            Advertisement.Show(interstitialID);
            return true;
        }
        else
        {
            Debug.Log("Interstitial ad not ready at the moment! Please try again later!");
            return false;
        }
    }

    public bool RequestRewardedForScript()
    {
        if(Advertisement.IsReady(rewardedID))
        {
            Advertisement.Show(rewardedID);
            return true;
        }
        else
        {
            return false;
        }  
    }


    #region AdListeners

    // Implement IUnityAdsListener interface methods:
    public void OnUnityAdsDidFinish(string surfacingId, ShowResult showResult)
    {
        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished)
        {
            OnAdsViewed(surfacingId);
        }
        else if (showResult == ShowResult.Skipped)
        {
            OnAdsViewed(surfacingId);
        }
        else if (showResult == ShowResult.Failed)
        {
            Debug.LogWarning("The ad did not finish due to an error.");
        }
    }

    public void OnAdsViewed(string currentAdID)
    {
        if (currentAdID == interstitialID)
        {
            //AdsScript.Instance.OnAdsViewedOrNotLoaded();
        }
        else if (currentAdID == rewardedID)
        {
            if (PlayerPrefs.GetInt("FreeCurrency", 0) == 1)
            {
                GameManager.Instance.WatchFor2X2();
                PlayerPrefs.SetInt("FreeCurrency", 0);
            }
        }
    }

    public void OnUnityAdsReady(string surfacingId)
    {
        // If the ready Ad Unit or legacy Placement is rewarded, show the ad:
        /*if (surfacingId == mySurfacingId)
        {
            // Optional actions to take when theAd Unit or legacy Placement becomes ready (for example, enable the rewarded ads button)
        }*/
    }

    public void OnUnityAdsDidError(string message)
    {
        // Log the error.
    }

    public void OnUnityAdsDidStart(string surfacingId)
    {
        // Optional actions to take when the end-users triggers an ad.
    }

    // When the object that subscribes to ad events is destroyed, remove the listener:
    public void OnDestroy()
    {
        Advertisement.RemoveListener(this);
    }

    #endregion

}
