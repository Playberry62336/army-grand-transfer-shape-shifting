﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPScript : MonoBehaviour
{
    public void OnPurchaseComplete(Product product)
    {
        if(product.definition.id == "transform_removeads")
        {
            PlayerPrefs.SetInt("AdsPurchased", 1);
            UIManager.Instance.UpdateUI();
        }
    }

    public void OnpurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(product.definition.id + "failed because " + failureReason);
    }
}