﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class AdsScript : MonoBehaviour
{
    [Header("RemoteKey for Analytics")]
    public string remoteKey;

    [Header("GOOGLE ADMOB")]
    public string bannerID;
    public string interstitialID;
    public string rewardedID;

    [Header("UNITY")]
    public bool testMode;
    public string unityID;
    public string unityBannerID;
    public string unityInterstitialID;
    public string unityRewardedID;

    public enum BannerPosition
    {
        topLeft,  
        topCentre, 
        topRight,        
        bottomLeft,       
        bottomCentre,        
        bottomRight
    } [Header("Position for Banner")] public BannerPosition bannerPosition;

    private static AdsScript instance;
    public static AdsScript Instance
    {
        get
        {
            if(instance==null)
            {
                instance = GameObject.FindObjectOfType<AdsScript>();
            }
            return instance;
        }
    }

    private void Start()
    {
        Invoke("ShowBanner", 5f);
    }

    public void ShowBanner()
    {
        if (PlayerPrefs.GetInt("AdsPurchased", 0) == 0)
        {
            if(!UnityAdsScript.Instance.RequestBannerForScript((int)bannerPosition))
            {
                GoogleAdsScript.Instance.RequestBannerForScript((int)bannerPosition);
            }
        }
    }

    public void ShowInterstitial()
    {
        //PlayerPrefs.SetInt(currentState, 1);
        if(RemoteSettings.GetBool(remoteKey) == true)
        {
            if (PlayerPrefs.GetInt("AdsPurchased", 0) == 0)
            {
                if (!GoogleAdsScript.Instance.RequestInterstitialForScript())
                {
                    if (!UnityAdsScript.Instance.RequestInterstitialForScript())
                    {
                        //OnAdsViewedOrNotLoaded();
                    }
                }
            }
            else if (PlayerPrefs.GetInt("AdsPurchased", 0) == 1)
            {
                //OnAdsViewedOrNotLoaded();
            }
        }
        else if(RemoteSettings.GetBool(remoteKey) == false)
        {
            //OnAdsViewedOrNotLoaded();
        }
    }

    public void ShowRewarded(string currentState)
    {
        PlayerPrefs.SetInt(currentState, 1);
        if (RemoteSettings.GetBool(remoteKey) == true)
        {
            if (PlayerPrefs.GetInt("AdsPurchased", 0) == 0)
            {
                if (!GoogleAdsScript.Instance.RequestRewardedForScript())
                {
                    if (!UnityAdsScript.Instance.RequestRewardedForScript())
                    {
                        //OnAdsViewedOrNotLoaded();
                    }
                }
            }
            else if (PlayerPrefs.GetInt("AdsPurchased", 0) == 1)
            {
                //OnAdsViewedOrNotLoaded();
            }
        }
        else if (RemoteSettings.GetBool(remoteKey) == false)
        {
            //OnAdsViewedOrNotLoaded();
        }
    }
    
    public void FreeCurrencyFunction()
    {
        PlayerPrefs.SetInt("FreeCurrency", 1);
        if (!GoogleAdsScript.Instance.RequestRewardedForScript())
        {
            if (!UnityAdsScript.Instance.RequestRewardedForScript())
            {
                Debug.Log("No rewarded available!"); //or make a toast
            }
        }
    }

    public void NextScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    /*public void OnAdsViewedOrNotLoaded()
    {
        if(PlayerPrefs.GetInt("AdOnNext", 0) == 1)
        {
            PlayerPrefs.SetInt("AdOnNext", 0);
            //Your function
            return;
        }
        else if(PlayerPrefs.GetInt("AdOnNextLevel", 0) == 1)
        {
            PlayerPrefs.SetInt("AdOnNextLevel", 0);
            GameManager.Instance.LoadNextLevel2();
            return;
        }
        else if(PlayerPrefs.GetInt("AdOnMainMenu", 0) == 1)
        {
            PlayerPrefs.SetInt("AdOnMainMenu", 0);
            //Your function
            return;
        }
        else if (PlayerPrefs.GetInt("AdOnRestart", 0) == 1)
        {
            PlayerPrefs.SetInt("AdOnRestart", 0);
            //Your function
            return;
        }
    }*/
}
