﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System;
using System.Linq;

public class GameManager : MonoBehaviour
{
    [HideInInspector]public string[] shapes =
    {
        "Character",
        "Bike",
        "Jeep",
        "Boat",
        "Tank",
        "Helicopter",
        "Glider"
    };

    public GameObject splash;

    [HideInInspector]public int[] buttons = new int[3];

    public GameObject[] playerAndAi;

    [SerializeField]private Vector3[] vector3s = new Vector3[4];

    private int randomNumber = 0;

    public bool isGameStarted = false;

    public int randomCoins = 0;

    public int totalplayers = 0;

    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType<GameManager>();
            return instance;
        }
    }

    private void Awake()
    {
        PlayerPrefs.SetInt("FCF", 0);
        if (PlayerPrefs.GetInt("FirstTime", 0) == 0)
        {
            for (int i = 0; i < shapes.Length; i++)
            {
                PlayerPrefs.SetInt(shapes[i], 1);
            }
            PlayerPrefs.SetInt("FirstTime", 1);
        }
        if (PlayerPrefs.GetInt("HideSplash", 0) == 1)
        {
            splash.SetActive(false);
            PlayerPrefs.SetInt("HideSplash", 0);
        }
        if (PlayerPrefs.GetInt("Level", 0) == 0)
        {
            PlayerPrefs.SetInt("Level", 1);
        }
        //DontDestroyOnLoad(this);
        InitializeLevel();

        //AdsScript.Instance.ShowBanner();
    }

    private void Start()
    {
        int x = 0;
        foreach(GameObject obj in playerAndAi)
        {
            vector3s[x] = obj.GetComponent<Transform>().position;
        }
    }

    public void StartGame()
    {
        isGameStarted = true;
        UIManager.Instance.isGameStarted();
        SoundScript.Instance.PlaySound(0);
    }

    public void InitializeShapeTypes()
    {
        System.Random random = new System.Random();
        if(PlayerPrefs.GetInt("Level") <= 3)
        {
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i] = i;
                Debug.Log(buttons[i]);
            }
        }
        else
        {
            for (int i = 0; i < buttons.Length; i++)
            {
                bool check = true;
                int x = random.Next(1, 8);
                while (check)
                {
                    if (buttons.Contains(x))
                    {
                        x = random.Next(1, 8);
                    }
                    else
                    {
                        buttons[i] = x;
                        check = false;
                    }
                }
            }
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i] -= 1;
            }
        }

        if (buttons[0] == 0 || buttons[0] == 3 || buttons[0] == 4 || buttons[0] == 5 || buttons[0] == 6)
        {
            if (buttons[1] == 0 || buttons[1] == 3 || buttons[1] == 4 || buttons[1] == 5 || buttons[1] == 6)
            {
                if (buttons[2] == 0 || buttons[2] == 3 || buttons[2] == 4 || buttons[2] == 5 || buttons[2] == 6)
                {
                    buttons[UnityEngine.Random.Range(0, 3)] = 2;
                }
            }
        }
        for (int i = 0; i < buttons.Length; i++)
        {
            Debug.Log(buttons[i]);
        }
    }

    public void InitializeLevel()
    {
        InitializeShapeTypes();
        //PlayerPrefs.SetInt("Level", 15);
        //PlayerPrefs.SetInt(shapes[4], 2);
        Instantiate(Resources.Load("Patches/First", typeof(GameObject)) as GameObject, new Vector3(0, 0, 0), this.transform.rotation);
        Transform temp = GameObject.Find("First(Clone)").transform.GetChild(1).GetComponent<Transform>().transform;

        for (int i = 0; i < PlayerPrefs.GetInt("Level"); i++)
        {
            string shape;
            randomNumber = UnityEngine.Random.Range(0, buttons.Length);
            shape = shapes[buttons[randomNumber]];
            Debug.Log("The shape to be spawned is of : " + shape);
            string shapeFolder = Application.dataPath + "/Resources/Patches/" + shape;
            DirectoryInfo directoryInfo = new DirectoryInfo(shapeFolder);
            FileInfo[] infos = directoryInfo.GetFiles("*.prefab");
            int noOfShapes = infos.Length;
            int randomShape = UnityEngine.Random.Range(0, noOfShapes);
            GameObject patch = Instantiate(Resources.Load("Patches/" + shape + "/" + (randomShape + 1), typeof(GameObject)) as GameObject, temp.position, this.transform.rotation);
            temp = patch.transform.GetChild(1).GetComponent<Transform>().transform;
        }
            Instantiate(Resources.Load("Patches/Last", typeof(GameObject)) as GameObject, temp.position, this.transform.rotation);


    }


    public void InstantiateCharacters()
    {
        if(isGameStarted)
        {
            Instantiate(Resources.Load("Prefabs/Blue", typeof(GameObject)) as GameObject,
                new Vector3(-2.5f, 2.5f, 1f),
                this.transform.rotation);
        }
    }

    public void ToggleVibration(bool state)
    {
        PlayerPrefs.SetInt("Vibration", state == true ? 1 : 0);
        Debug.Log(PlayerPrefs.GetInt("Vibration"));
    }

    public void LevelCompleted()
    {
        StartCoroutine(isGameCompleted());
    }

    public void LevelFailed()
    {
        StartCoroutine(IsLevelFailed());
    }

    private IEnumerator IsLevelFailed()
    {
        randomCoins = UnityEngine.Random.Range(0,100);
        UIManager.Instance.ShowGameFailedCoins(randomCoins);
        yield return new WaitForSeconds(2f);
        isGameStarted = false;
        UIManager.Instance.LevelFailed();
    }

    public void RestartLevel()
    {
        StartCoroutine(RestartLevell());
    }

    public IEnumerator RestartLevell()
    {
       
       
        yield return new WaitForSecondsRealtime(0f);
        CoinAnimationScript.Instance.AddCoinss();
        Invoke("DelayInAnimation1",2f);

    }
    public void DelayInAnimation1()
    {
        SceneManager.LoadScene(0);
        if (PlayerPrefs.GetInt("FCF") == 0)
        {
            AdsScript.Instance.ShowInterstitial();
        }
        PlayerPrefs.SetInt("HideSplash", 1);
    }

    private IEnumerator isGameCompleted()
    {
        PlayerPrefs.SetInt("Level", PlayerPrefs.GetInt("Level") + 1);
        randomCoins = UnityEngine.Random.Range(100, 500);
        
        //PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") + randomCoins);
        
        
        UIManager.Instance.ShowCoins(randomCoins);
        
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraFollowScript>().enabled = false;
        yield return new WaitForSeconds(2f);
        isGameStarted = false;
        UIManager.Instance.isGameCompleted();
    }

    public void LoadNextLevel()
    {
        //AdsScript.Instance.ShowInterstitial("AdOnNextLevel");
        StartCoroutine(StartNextLevel());
    }



    public IEnumerator StartNextLevel()
    {
        yield return new WaitForSecondsRealtime(0f);
        CoinAnimationScript.Instance.AddCoins();
        Invoke("DelayInAnimation", 2f);
        
       
    }
    public void DelayInAnimation()
    {
        SceneManager.LoadScene(0);
      
        if (PlayerPrefs.GetInt("FCF") == 0)
        {
            AdsScript.Instance.ShowInterstitial();
        }
        PlayerPrefs.SetInt("HideSplash", 1);
    }

    public void WatchFor2X()
    {
        if (PlayerPrefs.GetInt("FCF") == 0)
        {
            PlayerPrefs.SetInt("FCF", 1);
            AdsScript.Instance.FreeCurrencyFunction();
        }
        
    }
    public void WatchFor2X2()
    {
        PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") + randomCoins);
        UIManager.Instance.ShowCoins(randomCoins * 2);
    }

    public void UpgradeShape(int index)
    {
        PlayerPrefs.SetInt(shapes[buttons[index]], PlayerPrefs.GetInt(shapes[buttons[index]]) + 1);
        PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") - 1500);
    }
}
