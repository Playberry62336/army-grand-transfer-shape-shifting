﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLineScript : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        GameManager.Instance.totalplayers += 1;
        Debug.Log(GameManager.Instance.totalplayers);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PlayerShape")
        {
            if(GameManager.Instance.totalplayers >= 3)
            {
                GameManager.Instance.LevelFailed();
                GameObject.FindGameObjectWithTag("Player").GetComponent<SoundScript>().StopSound();
                SoundScript.Instance.PlaySound(8);
            }
            else
            {
                GameManager.Instance.LevelCompleted();
                GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>().Stop();
                SoundScript.Instance.PlaySound(7);
            }
        }
    }
}
