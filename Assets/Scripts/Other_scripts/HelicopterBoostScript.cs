﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterBoostScript : MonoBehaviour
{
    public int boostValue;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("PlayerShape"))
        {
            PlayerHelicopterScript.Instance.BoostHelicopter(boostValue);
        }
        else if(other.gameObject.CompareTag("GreenAiShape"))
        {
            GreenAiHelicopterScript.Instance.BoostHelicopter(boostValue);
        }
        else if(other.gameObject.CompareTag("BrownAiShape"))
        {
            BrownAiHelicopterScript.Instance.BoostHelicopter(boostValue);
        }
        else if(other.gameObject.CompareTag("WhiteAiShape"))
        {
            WhiteAiHelicopterScript.Instance.BoostHelicopter(boostValue);
        }
    }
}
