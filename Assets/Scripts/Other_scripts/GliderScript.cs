﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GliderScript : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(this.gameObject.name == "GreenAi")
        {
            //GreenAiControllerScript.Instance.Transform(GreenAiControllerScript.Instance.gliderIndex);
            GreenAiControllerScript.Instance.ChangeShape(GreenAiControllerScript.Instance.gliderIndex);
        }
        else if (this.gameObject.name == "BrownAi")
        {
            BrownAiControllerScript.Instance.ChangeShape(BrownAiControllerScript.Instance.gliderIndex);
        }
        else if (this.gameObject.name == "WhiteAi")
        {
            WhiteAiControllerScript.Instance.ChangeShape(WhiteAiControllerScript.Instance.gliderIndex);
        }
    }
}
