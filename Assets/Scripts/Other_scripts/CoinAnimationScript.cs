﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class CoinAnimationScript : MonoBehaviour
{
	//References
	[Header("UI references")]
	[SerializeField] GameObject animatedCoinPrefab;
	[SerializeField] Transform target;
	[SerializeField] Transform source;

	[Space]
	[Header("Available coins : (coins to pool)")]
	private int maxCoins;
	Queue<GameObject> coinsQueue = new Queue<GameObject>();


	[Space]
	[Header("Animation settings")]
	[SerializeField] [Range(0.5f, 0.9f)] float minAnimDuration;
	[SerializeField] [Range(0.9f, 2f)] float maxAnimDuration;

	[SerializeField] Ease easeType;
	[SerializeField] float spread;

	Vector3 targetPosition;

	public static CoinAnimationScript Instance;

	private int _c = 0;

	public int Coins
	{
		get { return _c; }
		set
		{
			_c = value;
			//update UI text whenever "Coins" variable is changed
			//coinUIText.text = Coins.ToString();
		}
	}

	void Awake()
	{
		maxCoins = GameManager.Instance.randomCoins;

		Instance = this;
		targetPosition = target.position;

		//prepare pool
		PrepareCoins();
	}

    /*private void OnEnable()
    {
		Invoke("AddCoins", 5f);
	}*/

    void PrepareCoins()
	{
		GameObject coin;
		for (int i = 0; i < maxCoins; i++)
		{
			coin = Instantiate(animatedCoinPrefab);
			coin.transform.parent = transform;
			coin.SetActive(false);
			coinsQueue.Enqueue(coin);
		}
	}

	void Animate(Vector3 collectedCoinPosition, int amount)
	{
		for (int i = 0; i < amount; i++)
		{
			//check if there's coins in the pool
			if (coinsQueue.Count > 0)
			{
				//extract a coin from the pool
				GameObject coin = coinsQueue.Dequeue();
				coin.SetActive(true);

				//move coin to the collected coin pos
				coin.transform.position = collectedCoinPosition + new Vector3(Random.Range(-spread, spread), 0f, 0f);

				//animate coin to target position
				float duration = Random.Range(minAnimDuration, maxAnimDuration);
				coin.transform.DOMove(targetPosition, duration)
				.SetEase(easeType)
				.OnComplete(() => {
					//executes whenever coin reach target position
					coin.SetActive(false);
					coinsQueue.Enqueue(coin);
					UIManager.Instance
					.totalGameCoins++;
					Coins++;
				});
			}
		}
	}

	public void AddCoins()
	{
		Animate(source.transform.position, maxCoins);
	}
	
	void Animatee(Vector3 collectedCoinPosition, int amount)
	{
		for (int i = 0; i < amount; i++)
		{
			//check if there's coins in the pool
			if (coinsQueue.Count > 0)
			{
				//extract a coin from the pool
				GameObject coin = coinsQueue.Dequeue();
				coin.SetActive(true);

				//move coin to the collected coin pos
				coin.transform.position = collectedCoinPosition + new Vector3(Random.Range(-spread, spread), 0f, 0f);

				//animate coin to target position
				float duration = Random.Range(minAnimDuration, maxAnimDuration);
				coin.transform.DOMove(targetPosition, duration)
				.SetEase(easeType)
				.OnComplete(() => {
					//executes whenever coin reach target position
					coin.SetActive(false);
					coinsQueue.Enqueue(coin);
					UIManager.Instance
					.totalGameFailedCoins++;
					Coins++;
				});
			}
		}
	}

	public void AddCoinss()
	{
		Animatee(source.transform.position, maxCoins);
	}
}
