﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectStopperScript : MonoBehaviour
{
    private int index;
    private void Update()
    {
        index = PlayerControllerScript.Instance.ReturnIndex();
        if((index== 0 || index == 1 || index == 5 || index == 6) && this.transform.name == "JeepTankBoat")
        {
            this.gameObject.GetComponent<BoxCollider>().isTrigger = true;
        }
        else if(index == 5 && this.transform.name == "OnlyHelicopter")
        {
            this.gameObject.GetComponent<BoxCollider>().isTrigger = true;
        }
        else
        {
            this.gameObject.GetComponent<BoxCollider>().isTrigger = false;
        }
    }
}
