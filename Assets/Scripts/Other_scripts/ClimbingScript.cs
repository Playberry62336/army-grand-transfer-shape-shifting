﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimbingScript : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        string temp = this.transform.name;
        string[] currentObject = temp.Split('.');
        switch (currentObject[0])
        {
            case "Player":
                Debug.Log("Player is about to climb");
                PlayerCharacterScript.Instance.Climb();
                break;
            case "GreenAi":
                GreenAiCharacterScript.Instance.Climb();
                break;
            case "BrownAi":
                BrownAiCharacterScript.Instance.Climb();
                break;
            case "WhiteAi":
                WhiteAiCharacterScript.Instance.Climb();
                break;
        }
    }
}
