﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundScript : MonoBehaviour
{
    public AudioSource[] audioSources;
    private static int _index = 0;

    private static SoundScript instance;
    public static SoundScript Instance
    {
        get
        {
            if(instance==null)
            {
                instance = GameObject.FindObjectOfType<SoundScript>();
            }
            return instance;
        }
    }
    private void Awake()
    {
        audioSources = GetComponents<AudioSource>();
    }

    public void PlaySound(int index)
    {
        if(audioSources[_index].isPlaying)
        {
            audioSources[_index].Stop();
        }
        if(!audioSources[index].isPlaying)
        {
            audioSources[index].Play();
        }
        _index = index;
    }

    public void StopSound()
    {
        if (audioSources[_index].isPlaying)
        {
            audioSources[_index].Stop();
        }
    }
    
    public void PlaySound()
    {
        if (!audioSources[_index].isPlaying)
        {
            audioSources[_index].Play();
        }
    }
}
