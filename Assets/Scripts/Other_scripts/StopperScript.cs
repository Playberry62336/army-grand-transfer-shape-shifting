﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopperScript : MonoBehaviour
{
    private int index;
    private void Update()
    {
        string temp = this.transform.name;
        string[] currentObject = temp.Split('.');
        //Debug.Log(currentObject[0]);
        switch (currentObject[0])
        {
            case "Player":
                index = PlayerControllerScript.Instance.ReturnIndex();
                break;
            case "GreenAi":
                index = GreenAiControllerScript.Instance.ReturnIndex();
                break;
            case "BrownAi":
                index = BrownAiControllerScript.Instance.ReturnIndex();
                break;
            case "WhiteAi":
                index = WhiteAiControllerScript.Instance.ReturnIndex();
                break;
        }  
        if ((index == 0 || index == 1 || index == 5 || index == 6) && this.transform.tag == "Stopper" && currentObject[1] == "E")
        {
            this.gameObject.GetComponent<BoxCollider>().isTrigger = true;
        }
        else if (index == 5 && currentObject[1] == "H")
        {
            this.gameObject.GetComponent<BoxCollider>().isTrigger = true;
        }
        else if (index == 0 && this.transform.tag == "Stopper" && currentObject[1] == "C")
        {
            this.gameObject.GetComponent<BoxCollider>().isTrigger = true;
        }
        else
        {
            this.gameObject.GetComponent<BoxCollider>().isTrigger = false;
        }
    }
}
