﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Toast")]
    public GameObject toast;

    [Header("Level Number Text")]
    public Text levelNumberText;

    [Header("Coins Text")]
    public Text coinText;

    [Header("UI Panels")]
    public GameObject mainMenuPanel;
    public GameObject gameplayPanel;
    public GameObject gameoverPanel;
    public GameObject gameFailedPanel;

    [Header("Upgrade Buttons")]
    public GameObject one;
    public GameObject two;
    public GameObject three;

    [Header("GameButtons")]
    public GameObject[] gameButtons;

    public Button removeAdsButton;

    public Slider slider;

    public Text gameOverCoins;
    public Text totalCoins;

    public Text gameFailedCoins;

    public Text[] upgradeTexts;

    private static UIManager instance;
    public static UIManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<UIManager>();
            }
            return instance;
        }
    }

    private int gameovercoinss;
    private int gameFailedCoinss;

    private int totalgamecoins = 0;
    public int totalGameCoins
    {
        get
        {
            return totalgamecoins;
        }
        set
        {
            totalgamecoins = value;
            Debug.Log(totalgamecoins);
            PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") + 1);
            //totalCoins.text = (PlayerPrefs.GetInt("coins",0) + totalgamecoins).ToString();
            totalCoins.text = PlayerPrefs.GetInt("coins",0).ToString();
            gameOverCoins.text = gameovercoinss.ToString();
            gameovercoinss -= 1;
            if (gameovercoinss == 1)
                gameovercoinss = 0;
        }
    }


    private int totalgamefailedcoins = 0;
    public int totalGameFailedCoins
    {
        get
        {
            return gameFailedCoinss;
        }
        set
        {
            gameFailedCoinss = value;
            Debug.Log(gameFailedCoinss);
            PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") + 1);
            //totalCoins.text = (PlayerPrefs.GetInt("coins",0) + totalgamecoins).ToString();
            totalCoins.text = PlayerPrefs.GetInt("coins",0).ToString();
            gameFailedCoins.text = totalgamefailedcoins.ToString();
            totalgamefailedcoins -= 1;
            if (totalgamefailedcoins == 1)
                totalgamefailedcoins = 0;
        }
    }

    

    public void UpdateUI()
    {
        removeAdsButton.interactable = (PlayerPrefs.GetInt("AdsPurchased", 0) == 1 ? false : true);

        for(int i=0;i<upgradeTexts.Length;i++)
        {
            upgradeTexts[i].text = "LEVEL " + PlayerPrefs.GetInt(GameManager.Instance.shapes[GameManager.Instance.buttons[i]]).ToString();
        }
    }

    private void Awake()
    {
        levelNumberText.text = "LEVEL " + PlayerPrefs.GetInt("Level").ToString();
        coinText.text = PlayerPrefs.GetInt("coins").ToString();

        one.gameObject.GetComponent<Button>().onClick.AddListener(() => UpgradeFunction(int.Parse(one.transform.name)));
        two.gameObject.GetComponent<Button>().onClick.AddListener(() => UpgradeFunction(int.Parse(two.transform.name)));
        three.gameObject.GetComponent<Button>().onClick.AddListener(() => UpgradeFunction(int.Parse(three.transform.name)));

        slider.value = PlayerPrefs.GetFloat("Volume", 1);

        totalCoins.text = PlayerPrefs.GetInt("coins", 0).ToString();
    }

    public void OnSliderValueChanged()
    {
        PlayerPrefs.SetFloat("Volume", slider.value);
        Debug.Log(PlayerPrefs.GetFloat("Volume", 1));
    }

    private void Start()
    {
        DisplayButtonRenders();
        UpdateUI();
    }

    public void isGameStarted()
    {
        mainMenuPanel.SetActive(!mainMenuPanel.activeSelf);
        gameplayPanel.SetActive(!gameplayPanel.activeSelf);
        //gameoverPanel.SetActive(!gameoverPanel.activeSelf);
    }

    public void isGameCompleted()
    {
        gameplayPanel.SetActive(false);

        DisplayUpgradeRenders();
        gameoverPanel.SetActive(true);
    }

    public void LevelFailed()
    {
        gameplayPanel.SetActive(false);
        gameFailedPanel.SetActive(true);
    }

    public void TogglePanel(GameObject panel)
    {
        panel.SetActive(!panel.activeInHierarchy);
        if (panel.name == "SettingsPanel" && panel.activeInHierarchy)
        {
            Time.timeScale = 0;
            SoundScript.Instance.StopSound();
        }
        else
        {
            Time.timeScale = 1;
            if(GameManager.Instance.isGameStarted)
            {
                SoundScript.Instance.PlaySound();
            }
        }
    }

    public void VibrationFunction(bool state)
    {
        GameObject.Find("Vibration").gameObject.transform.GetChild(1).gameObject.SetActive(!state);
        Debug
            .Log(state);
        if(state==true)
        {
            Handheld.Vibrate();
        }
        GameManager.Instance.ToggleVibration(state);
    }

    public void isGameOver()
    {
        gameplayPanel.SetActive(false);
        gameoverPanel.SetActive(true);
    }

    bool toastCheck = false;

    public void UpgradeFunction(int index)
    {
        
        if (PlayerPrefs.GetInt("coins") >= 1500)
        {
            GameManager.Instance.UpgradeShape(index);
            UpdateUI();
        }
        else
        {
            if(!toastCheck)
            {
                StartCoroutine(PlayToast());
            }
        }
        DisplayUpgradeRenders();
    }

    public void ShowCoins(int x)
    {
        gameOverCoins.text = x.ToString();
        //totalCoins.text = PlayerPrefs.GetInt("coins", 0).ToString();
        gameovercoinss = x;
    }

    public void ShowGameFailedCoins(int y)
    {
        gameFailedCoins.text = y.ToString();
        totalgamefailedcoins = y;
    }

    IEnumerator PlayToast()
    {
        toastCheck = true;
        toast.SetActive(true);
        yield return new WaitForSeconds(3f);
        toast.SetActive(false);
        toastCheck = false;
    }

    public void DisplayButtonRenders()
    {
        for(int i=0;i<3;i++)
        {
            Sprite sprite = Resources.Load("Renders/" + GameManager.Instance.shapes[GameManager.Instance.buttons[i]] + "/" + PlayerPrefs.GetInt(GameManager.Instance.shapes[GameManager.Instance.buttons[i]]), typeof(Sprite)) as Sprite;
            gameButtons[i].transform.GetChild(1).gameObject.GetComponent<Image>().sprite = sprite;
            gameButtons[i].transform.GetChild(1).gameObject.GetComponent<Image>().SetNativeSize();
        }
    }

    public void DisplayUpgradeRenders()
    {
        Debug.Log(GameManager.Instance.shapes[GameManager.Instance.buttons[0]]);
        Debug.Log(GameManager.Instance.shapes[GameManager.Instance.buttons[1]]);
        Debug.Log(GameManager.Instance.shapes[GameManager.Instance.buttons[2]]);
        Sprite sprite = Resources.Load("Renders/" + GameManager.Instance.shapes[GameManager.Instance.buttons[0]] + "/" + PlayerPrefs.GetInt(GameManager.Instance.shapes[GameManager.Instance.buttons[0]]), typeof(Sprite)) as Sprite;
        one.transform.GetChild(1).gameObject.GetComponent<Image>().sprite = sprite;
        one.transform.GetChild(1).gameObject.GetComponent<Image>().SetNativeSize();
        one.transform.GetChild(1).gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 25f);
        sprite = Resources.Load("Renders/" + GameManager.Instance.shapes[GameManager.Instance.buttons[1]] + "/" + PlayerPrefs.GetInt(GameManager.Instance.shapes[GameManager.Instance.buttons[1]]), typeof(Sprite)) as Sprite;
        two.transform.GetChild(1).gameObject.GetComponent<Image>().sprite = sprite;
        two.transform.GetChild(1).gameObject.GetComponent<Image>().SetNativeSize();
        two.transform.GetChild(1).gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 25f);
        sprite = Resources.Load("Renders/" + GameManager.Instance.shapes[GameManager.Instance.buttons[2]] + "/" + PlayerPrefs.GetInt(GameManager.Instance.shapes[GameManager.Instance.buttons[2]]), typeof(Sprite)) as Sprite;
        three.transform.GetChild(1).gameObject.GetComponent<Image>().sprite = sprite;
        three.transform.GetChild(1).gameObject.GetComponent<Image>().SetNativeSize();
        three.transform.GetChild(1).gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 25f);
    }


    [HideInInspector] public int prevIndex = 10;
    public void SetState(int index)
    {
        if(prevIndex == 10)
        {
            prevIndex = index;
            gameButtons[index].transform.GetChild(0).gameObject.SetActive(true);
            gameButtons[index].GetComponent<Button>().interactable = false;
            
        }
        else
        {
            gameButtons[prevIndex].transform.GetChild(0).gameObject.SetActive(false);
            gameButtons[prevIndex].GetComponent<Button>().interactable = true;
            gameButtons[index].transform.GetChild(0).gameObject.SetActive(true);
            gameButtons[index].GetComponent<Button>().interactable = false;
            prevIndex = index;

        }
        
    }

    public void OpenLink(string link)
    {
        Application.OpenURL(link);
    }
}
