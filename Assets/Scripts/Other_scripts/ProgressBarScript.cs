﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarScript : MonoBehaviour
{
    private Slider playerSlider;
    private Slider greenAiSlider;
    private Slider BrownAiSlider;
    private Slider WhiteAiSlider;

    private float maxDistance;
    private float currentDistance;

    private void Awake()
    {
        playerSlider = this.transform.GetChild(0).GetComponent<Slider>();
        greenAiSlider = this.transform.GetChild(0).GetChild(2).GetComponent<Slider>();
        BrownAiSlider = this.transform.GetChild(0).GetChild(3).GetComponent<Slider>();
        WhiteAiSlider = this.transform.GetChild(0).GetChild(4).GetComponent<Slider>();
        
        CalculateDistance();
    }

    private void FixedUpdate()
    {
        currentDistance = ReturnDistance("PlayerShape");
        playerSlider.value = maxDistance - currentDistance;

        currentDistance = ReturnDistance("GreenAiShape");
        greenAiSlider.value = maxDistance - currentDistance;

        currentDistance = ReturnDistance("BrownAiShape");
        BrownAiSlider.value = maxDistance - currentDistance;

        currentDistance = ReturnDistance("WhiteAiShape");
        WhiteAiSlider.value = maxDistance - currentDistance;
    }

    public void CalculateDistance()
    {
        maxDistance = ReturnDistance("PlayerShape");
        playerSlider.maxValue = maxDistance;
        greenAiSlider.maxValue = maxDistance;
        BrownAiSlider.maxValue = maxDistance;
        WhiteAiSlider.maxValue = maxDistance;
        Debug.Log(maxDistance);
    }

    private float ReturnDistance(string shape)
    {
        return Vector3.Distance(GameObject.FindGameObjectWithTag(shape).transform.position,
            GameObject.FindGameObjectWithTag("FinishLine").transform.position);
    }
}
