﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrownAiTankScript : MonoBehaviour
{
    [HideInInspector] public float tankSpeed;
    private GameObject spawnerObject;

    private static BrownAiTankScript instance;
    public static BrownAiTankScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<BrownAiTankScript>();
            }
            return instance;
        }
    }

    private void Start()
    {
        tankSpeed = BrownAiControllerScript.Instance.tankSpeed;
        spawnerObject = GameObject.FindGameObjectWithTag("BrownAi");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * tankSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, spawnerObject.transform.position.y, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Box.T")
        {
            Vector3 newPos = collision.gameObject.transform.position;
            Destroy(collision.gameObject);
            Instantiate(Resources.Load("Prefabs/Crate", typeof(GameObject)) as GameObject,
                newPos,
                this.transform.rotation);
        }
        else if(collision.gameObject.CompareTag("FinishLine"))
        {
            tankSpeed = BrownAiControllerScript.Instance.decreasedSpeed;
        }
        else
        {
            BrownAiControllerScript.Instance.DetectType("Tank", collision.gameObject);
        }
    }
}
