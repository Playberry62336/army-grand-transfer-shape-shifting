﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BrownAiHelicopterScript : MonoBehaviour
{
    [SerializeField] private float helicopterSpeed;
    private GameObject spawnerObject;

    private bool helicopterBoost = false;

    private bool pushHelicopter = false;

    private static BrownAiHelicopterScript instance;
    public static BrownAiHelicopterScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<BrownAiHelicopterScript>();
            }
            return instance;
        }
    }

    private void OnEnable()
    {
        StartCoroutine(wait());
    }

    IEnumerator wait()
    {
        yield return new WaitForSecondsRealtime(1f);
        pushHelicopter = true;
    }

    private void Start()
    {
        helicopterSpeed = BrownAiControllerScript.Instance.helicopterSpeed;
        spawnerObject = GameObject.FindGameObjectWithTag("BrownAi");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted && helicopterBoost == false && pushHelicopter)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * helicopterSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, spawnerObject.transform.position.y, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    public void BoostHelicopter(int boostValue)
    {
        helicopterBoost = true;
        StartCoroutine(BoostHelicopterNow(boostValue));
    }

    IEnumerator BoostHelicopterNow(int boostValue)
    {
        yield return new WaitForSecondsRealtime(1f);
        float startTime = Time.time;
        while (Time.time < startTime + 2f)
        {
            this.transform.Translate(Vector3.up * Time.deltaTime * boostValue, Space.World);
            Vector3 newPos = this.transform.position;
            spawnerObject.transform.position = newPos;
            yield return null;
        }
        helicopterBoost = false;

        yield return new WaitForSecondsRealtime(BrownAiControllerScript.Instance.AiWaitTime + Random.Range(0f, BrownAiControllerScript.Instance.AiWaitTime));
        BrownAiControllerScript.Instance.ChangeShape(BrownAiControllerScript.Instance.jeepIndex);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("FinishLine"))
        {
            helicopterSpeed = BrownAiControllerScript.Instance.decreasedSpeed;
        }
        else
        BrownAiControllerScript.Instance.DetectType("Helicopter", collision.gameObject);

    }
}
