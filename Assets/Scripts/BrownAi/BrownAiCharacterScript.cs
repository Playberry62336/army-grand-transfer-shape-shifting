﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrownAiCharacterScript : MonoBehaviour
{
    private CharacterController characterController;

    private Animator animator;

    private float speed;

    private bool collided;
    private bool isClimbing = false;

    private GameObject spawnerObject;

    private static BrownAiCharacterScript instance;
    public static BrownAiCharacterScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<BrownAiCharacterScript>();
            }
            return instance;
        }
    }


    private void Start()
    {
        speed = BrownAiControllerScript.Instance.characterSpeed;
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        spawnerObject = GameObject.FindGameObjectWithTag("BrownAi");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted && isClimbing == false)
        {
            characterController.SimpleMove(new Vector3(0, 0, speed));
            Vector3 newPos = new Vector3(this.transform.position.x, this.transform.position.y + 2.5f, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }


    private IEnumerator OnTriggerEnter(Collider other)
    {
        if (collided && other.gameObject.name == "ConvertToTank")
        {
            Debug.Log("jsldlksadjksad");
            BrownAiControllerScript.Instance.ChangeShape(BrownAiControllerScript.Instance.tankIndex);
            //yield return;
        }
        yield return new WaitForSeconds(2);
        collided = true;
        if (collided && other.gameObject.tag == "Ground")
        {
            BrownAiControllerScript.Instance.DetectType("Character", other.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "BrownAi.H")
        {
            BrownAiControllerScript.Instance.ChangeShape(BrownAiControllerScript.Instance.helicopterIndex);
        }
        else if (collision.gameObject.CompareTag("FinishLine"))
        {
            speed = BrownAiControllerScript.Instance.decreasedSpeed;
        }
    }

    public void Climb()
    {
        StartCoroutine(IsClimbing());
    }

    IEnumerator IsClimbing()
    {
        yield return new WaitForSeconds(1f);
        Debug.Log("Climbing successful");
        isClimbing = true;
        animator.SetBool("isClimbing", true);
        float startTime = Time.time;
        while (Time.time < startTime + 7f)
        {
            this.transform.Translate(Vector3.up * Time.deltaTime * 2, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, this.transform.position.y + 2.5f, this.transform.position.z);
            spawnerObject.transform.position = newPos;
            yield return null;
            Debug.Log(startTime + 7f);
            if (Time.time >= startTime + 6f)
                animator.SetBool("isClimbing", false);
        }
        isClimbing = false;
    }
}
