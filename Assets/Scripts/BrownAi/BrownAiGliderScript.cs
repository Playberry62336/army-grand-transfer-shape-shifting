﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrownAiGliderScript : MonoBehaviour
{
    private float gliderSpeed;
    private GameObject spawnerObject;

    private bool check = false;

    private void Start()
    {
        spawnerObject = GameObject.FindGameObjectWithTag("BrownAi");
        gliderSpeed = BrownAiControllerScript.Instance.gliderSpeed;
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * gliderSpeed, Space.World);
            if (!check)
            {
                this.transform.Translate(-Vector3.up * Time.deltaTime * (gliderSpeed / 3f), Space.World);
            }
            Vector3 newPos = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ground")
        {
            Debug.Log("Glider is on the ground");
            check = true;
            gliderSpeed = BrownAiControllerScript.Instance.decreasedSpeed;

            BrownAiControllerScript.Instance.DetectType("Glider", other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "GliderGround")
        {
            Debug.Log("Glider isn't on the ground");
            check = false;
            gliderSpeed = BrownAiControllerScript.Instance.gliderSpeed;
        }
    }
}
