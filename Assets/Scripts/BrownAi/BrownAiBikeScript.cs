﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrownAiBikeScript : MonoBehaviour
{
    [HideInInspector] public float bikeSpeed;
    private GameObject spawnerObject;

    private static BrownAiBikeScript instance;
    public static BrownAiBikeScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<BrownAiBikeScript>();
            }
            return instance;
        }
    }

    private void Start()
    {
        bikeSpeed = BrownAiControllerScript.Instance.bikeSpeed;
        spawnerObject = GameObject.FindGameObjectWithTag("BrownAi");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * bikeSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, spawnerObject.transform.position.y, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.CompareTag("FinishLine"))
        {
            bikeSpeed = BrownAiControllerScript.Instance.decreasedSpeed;
        }
        BrownAiControllerScript.Instance.DetectType("Bike", collision.gameObject);
    }
}
