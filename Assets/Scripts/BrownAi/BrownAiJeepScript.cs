﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrownAiJeepScript : MonoBehaviour
{
    public float jeepSpeed;
    private GameObject spawnerObject;

    private static BrownAiJeepScript instance;
    public static BrownAiJeepScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<BrownAiJeepScript>();
            }
            return instance;
        }
    }

    private void Start()
    {
        jeepSpeed = BrownAiControllerScript.Instance.jeepSpeed;
        spawnerObject = GameObject.FindGameObjectWithTag("BrownAi");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * jeepSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, this.transform.position.y + 1.25f, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Water")
        {
            jeepSpeed = BrownAiControllerScript.Instance.decreasedSpeed;
            BrownAiControllerScript.Instance.DetectType("Jeep", collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("FinishLine"))
        {
            jeepSpeed = BrownAiControllerScript.Instance.decreasedSpeed;
        }
        else
            BrownAiControllerScript.Instance.DetectType("Jeep", collision.gameObject);
    }
}
