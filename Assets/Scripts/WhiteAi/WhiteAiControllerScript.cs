﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
public class WhiteAiControllerScript : MonoBehaviour
{
    /*[SerializeField] private AudioSource audioSource;

    [SerializeField] private AudioClip[] audioClips;*/
    private string[] shapes =
    {
        "Character",
        "Bike",
        "Jeep",
        "Tank",
        "Boat",
        "Helicopter",
        "Glider"
    };

    public float AiWaitTime;

    [HideInInspector] public int characterIndex = 0;
    [HideInInspector] public int bikeIndex = 1;
    [HideInInspector] public int jeepIndex = 2;
    [HideInInspector] public int boatIndex = 3;
    [HideInInspector] public int tankIndex = 4;
    [HideInInspector] public int helicopterIndex = 5;
    [HideInInspector] public int gliderIndex = 6;

    private GameObject[] rotatableObjects;
    private GameObject mainRotor;
    private GameObject currentShape;
    private GameObject sphere;
    private static int index = 0;
    private bool pause = false;
    private GameObject[] shapeObjects = new GameObject[7];
    private string aiName;

    [Header("Speeds")]
    public int rotationSpeed;
    public float characterSpeed;
    public float bikeSpeed;
    public float jeepSpeed;
    public float boatSpeed;
    public float tankSpeed;
    public float helicopterSpeed;
    public float gliderSpeed;

    public int decreasedSpeed;

    private static WhiteAiControllerScript instance;
    public static WhiteAiControllerScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<WhiteAiControllerScript>();
            }
            return instance;
        }
    }


    private void Awake()
    {
        //audioSource = GetComponent<AudioSource>();
        aiName = this.transform.name;
        for (int i = 0; i < shapes.Length; i++)
        {
            string modelNo = PlayerPrefs.GetInt(shapes[i]).ToString();
            shapeObjects[i] = Resources.Load(shapes[i] + "/" + modelNo + "/" + aiName, typeof(GameObject)) as GameObject;
        }
        Instantiate(shapeObjects[0],
            this.transform.position,
            transform.rotation);
        sphere = Instantiate(Resources.Load("ShapeChanger/" + aiName, typeof(GameObject)) as GameObject, this.transform.GetChild(7));
    }

    void Start()
    {
        string soundPath = Application.dataPath + "/Resources/Sounds/";
        DirectoryInfo directoryInfo = new DirectoryInfo(soundPath);
        FileInfo[] fileInfos = directoryInfo.GetFiles("*.mp3");
        int noOfFiles = fileInfos.Length;
        Debug.Log("no of sound files : " + noOfFiles);

        /*audioClips = new AudioClip[noOfFiles];

        for (int i = 0; i < noOfFiles; i++)
        {
            audioClips[i] = Resources.Load("Sounds/" + i, typeof(AudioClip)) as AudioClip;
        }*/
        GetCurrentObject();
        GetRotationalObjects();
    }

    void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            if (pause == false)
            {
                if (rotatableObjects != null)
                    foreach (GameObject T in rotatableObjects)
                        T.transform.RotateAround(T.transform.position, Vector3.right, rotationSpeed * Time.deltaTime);

                if (mainRotor != null)
                    mainRotor.transform.RotateAround(mainRotor.transform.position, Vector3.up, rotationSpeed * Time.deltaTime);
            }
            /*if (!audioSource.isPlaying)
            {
                audioSource.PlayOneShot(audioClips[index]);
            }*/
        }
    }

    public void GetCurrentObject()
    {
        currentShape = null;
        if (currentShape == null)
            currentShape = GameObject.FindGameObjectWithTag(aiName + "AiShape");
    }

    public void GetRotationalObjects()
    {
        rotatableObjects = null; mainRotor = null;
        if (rotatableObjects == null)
            rotatableObjects = GameObject.FindGameObjectsWithTag(aiName + "AiRotatable");

        if (mainRotor == null)
            mainRotor = GameObject.FindGameObjectWithTag(aiName + "AiPropeller");
    }

    public void ChangeShape(int shapeIndex)
    {
        if (GameManager.Instance.isGameStarted)
        {
            StartCoroutine(MoveObject(currentShape.transform.localScale, new Vector3(0.25f, 0.25f, 0.25f), 0.25f, shapeIndex));
        }
    }

    IEnumerator MoveObject(Vector3 source, Vector3 target, float overTime, int currentObject)
    {
        pause = true;
        float startTime = Time.time;
        while (Time.time < startTime + overTime)
        {
            currentShape.transform.localScale = Vector3.Lerp(source, target, (Time.time - startTime) / overTime);
            sphere.transform.localScale = Vector3.Lerp(sphere.transform.localScale, new Vector3(5f, 5f, 5f), (Time.time - startTime) / overTime);
            yield return null;
        }
        sphere.transform.localScale = new Vector3(5f, 5f, 5f);
        currentShape.transform.localScale = target;
        Vector3 newInstantiationPos = currentShape.transform.position;
        newInstantiationPos.y = this.transform.position.y;
        Destroy(currentShape.gameObject);
        index = currentObject;
        GameObject t = Instantiate(shapeObjects[index],
            newInstantiationPos,
            transform.rotation);
        t.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
        yield return null;
        GetCurrentObject();
        GetRotationalObjects();
        startTime = Time.time;
        while (Time.time < startTime + overTime)
        {
            currentShape.transform.localScale = Vector3.Lerp(currentShape.transform.localScale, new Vector3(1, 1, 1), (Time.time - startTime) / overTime);
            sphere.transform.localScale = Vector3.Lerp(sphere.transform.localScale, new Vector3(0f, 0f, 0f), (Time.time - startTime) / overTime);
            yield return null;
        }
        currentShape.transform.localScale = new Vector3(1, 1, 1);
        sphere.transform.localScale = new Vector3(0f, 0f, 0f);
        pause = false;
    }

    public int ReturnIndex()
    {
        return index;
    }


    public void DetectType(string Shape, GameObject obj)
    {
        string name = obj.gameObject.name;
        string[] check = name.Split('.');
        if (obj.gameObject.tag == "Stopper")
        {
            if (Shape == "Jeep")
            {
                WhiteAiJeepScript.Instance.jeepSpeed = 0;
            }
            if (Shape == "Bike")
            {
                WhiteAiBikeScript.Instance.bikeSpeed = 0;
            }
            if (Shape == "Tank")
            {
                WhiteAiTankScript.Instance.tankSpeed = 0;
            }
            if (Shape == "Character")
            {
                if (check[0] == "Box" && check[1] == "T")
                {
                    StartCoroutine(Transform(tankIndex));
                    return;
                }
            }
            if (check[1] == "H")
            {
                StartCoroutine(Transform(helicopterIndex));
                return;
            }
            else if (check[1] == "E")
            {
                StartCoroutine(TransformToRandom());
                return;
            }
            else if (check[1] == "C")
            {
                StartCoroutine(Transform(characterIndex));
                return;
            }
            else if (check[1] == "T")
            {
                StartCoroutine(Transform(tankIndex));
                return;
            }
        }
        if (Shape == "Character" || Shape == "Bike" || Shape == "Boat" || Shape == "Glider")
        {
            if (obj.gameObject.tag == "Ground")
            {
                StartCoroutine(Transform(jeepIndex));
                return;
            }
        }

        if ((Shape == "Character" || Shape == "Bike" || Shape == "Jeep" || Shape == "Tank") && obj.gameObject.tag == "Water")
        {
            StartCoroutine(Transform(boatIndex));
            return;
        }
    }

    public IEnumerator Transform(int index)
    {
        yield return new WaitForSecondsRealtime(AiWaitTime + Random.Range(0f, AiWaitTime));
        
        foreach (int x in GameManager.Instance.buttons)
        {
            if (index == x)
            {
                WhiteAiControllerScript.Instance.ChangeShape(index);
            }
        }
    }
    IEnumerator TransformToRandom()
    {
        yield return new WaitForSecondsRealtime(AiWaitTime + Random.Range(0f, AiWaitTime));
        int[] array = { 0, 1, 5 };
        WhiteAiControllerScript.Instance.ChangeShape(array[Random.Range(0, 3)]);
    }
}