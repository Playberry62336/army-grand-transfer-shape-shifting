﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WhiteAiJeepScript : MonoBehaviour
{
    public float jeepSpeed;
    private GameObject spawnerObject;

    private static WhiteAiJeepScript instance;
    public static WhiteAiJeepScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<WhiteAiJeepScript>();
            }
            return instance;
        }
    }

    private void Start()
    {
        jeepSpeed = WhiteAiControllerScript.Instance.jeepSpeed;
        spawnerObject = GameObject.FindGameObjectWithTag("WhiteAi");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * jeepSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, this.transform.position.y + 1.25f, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Water")
        {
            jeepSpeed = WhiteAiControllerScript.Instance.decreasedSpeed;
            WhiteAiControllerScript.Instance.DetectType("Jeep", collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("FinishLine"))
        {
            jeepSpeed = WhiteAiControllerScript.Instance.decreasedSpeed;
        }
        else
            WhiteAiControllerScript.Instance.DetectType("Jeep", collision.gameObject);
    }
}
