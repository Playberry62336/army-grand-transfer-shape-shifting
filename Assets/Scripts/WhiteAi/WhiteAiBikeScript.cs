﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteAiBikeScript : MonoBehaviour
{
    [HideInInspector] public float bikeSpeed;
    private GameObject spawnerObject;

    private static WhiteAiBikeScript instance;
    public static WhiteAiBikeScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<WhiteAiBikeScript>();
            }
            return instance;
        }
    }

    private void Start()
    {
        bikeSpeed = WhiteAiControllerScript.Instance.bikeSpeed;
        spawnerObject = GameObject.FindGameObjectWithTag("WhiteAi");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * bikeSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, spawnerObject.transform.position.y, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("FinishLine"))
        {
            bikeSpeed = WhiteAiControllerScript.Instance.decreasedSpeed;
        }
        else
            WhiteAiControllerScript.Instance.DetectType("Bike", collision.gameObject);
    }

}
