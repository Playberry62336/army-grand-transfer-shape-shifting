﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteAiHelicopterScript : MonoBehaviour
{
    [SerializeField] private float helicopterSpeed;
    private GameObject spawnerObject;

    private bool helicopterBoost = false;

    private bool pushHelicopter = false;

    private static WhiteAiHelicopterScript instance;
    public static WhiteAiHelicopterScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<WhiteAiHelicopterScript>();
            }
            return instance;
        }
    }

    private void OnEnable()
    {
        StartCoroutine(wait());
    }

    IEnumerator wait()
    {
        yield return new WaitForSecondsRealtime(1f);
        pushHelicopter = true;
    }

    private void Start()
    {
        helicopterSpeed = WhiteAiControllerScript.Instance.helicopterSpeed;
        spawnerObject = GameObject.FindGameObjectWithTag("WhiteAi");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted && helicopterBoost == false && pushHelicopter)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * helicopterSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, spawnerObject.transform.position.y, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    public void BoostHelicopter(int boostValue)
    {
        helicopterBoost = true;
        StartCoroutine(BoostHelicopterNow(boostValue));
    }

    IEnumerator BoostHelicopterNow(int boostValue)
    {
        yield return new WaitForSecondsRealtime(1f);
        float startTime = Time.time;
        while (Time.time < startTime + 2f)
        {
            this.transform.Translate(Vector3.up * Time.deltaTime * boostValue, Space.World);
            Vector3 newPos = this.transform.position;
            spawnerObject.transform.position = newPos;
            yield return null;
        }
        helicopterBoost = false;

        yield return new WaitForSecondsRealtime(WhiteAiControllerScript.Instance.AiWaitTime + Random.Range(0f, WhiteAiControllerScript.Instance.AiWaitTime));
        WhiteAiControllerScript.Instance.ChangeShape(WhiteAiControllerScript.Instance.jeepIndex);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("FinishLine"))
        {
            helicopterSpeed = WhiteAiControllerScript.Instance.decreasedSpeed;
        }
        else
        WhiteAiControllerScript.Instance.DetectType("Helicopter", collision.gameObject);

    }
}