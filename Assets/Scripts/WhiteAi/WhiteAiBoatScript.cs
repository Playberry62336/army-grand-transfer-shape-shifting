﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteAiBoatScript : MonoBehaviour
{
    [SerializeField] private float boatSpeed;
    private GameObject spawnerObject;

    private void Start()
    {
        spawnerObject = GameObject.FindGameObjectWithTag("WhiteAi");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * boatSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, spawnerObject.transform.position.y, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Water")
        {
            boatSpeed = WhiteAiControllerScript.Instance.boatSpeed;
        }
        else if (collision.gameObject.tag != "Water")
        {
            boatSpeed = WhiteAiControllerScript.Instance.decreasedSpeed;
            WhiteAiControllerScript.Instance.DetectType("Boat", collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("FinishLine"))
        {
            boatSpeed = WhiteAiControllerScript.Instance.decreasedSpeed;
        }
    }
}
