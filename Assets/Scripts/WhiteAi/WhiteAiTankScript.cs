﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WhiteAiTankScript : MonoBehaviour
{
    [HideInInspector] public float tankSpeed;
    private GameObject spawnerObject;

    private static WhiteAiTankScript instance;
    public static WhiteAiTankScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<WhiteAiTankScript>();
            }
            return instance;
        }
    }

    private void Start()
    {
        tankSpeed = WhiteAiControllerScript.Instance.tankSpeed;
        spawnerObject = GameObject.FindGameObjectWithTag("WhiteAi");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * tankSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, spawnerObject.transform.position.y, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Box.T")
        {
            Vector3 newPos = collision.gameObject.transform.position;
            Destroy(collision.gameObject);
            Instantiate(Resources.Load("Prefabs/Crate", typeof(GameObject)) as GameObject,
                newPos,
                this.transform.rotation);
        }
        else if (collision.gameObject.CompareTag("FinishLine"))
        {
            tankSpeed = WhiteAiControllerScript.Instance.decreasedSpeed;
        }
        else
        {
            WhiteAiControllerScript.Instance.DetectType("Tank", collision.gameObject);
        }
    }
}
