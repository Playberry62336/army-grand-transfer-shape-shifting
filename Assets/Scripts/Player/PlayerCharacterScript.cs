﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacterScript : MonoBehaviour
{
    private CharacterController characterController;

    private Animator animator;

    private float speed;

    private bool collided;
    [HideInInspector]public bool isClimbing = false;

    private GameObject spawnerObject;

    private static PlayerCharacterScript instance;
    public static PlayerCharacterScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<PlayerCharacterScript>();
            }
            return instance;
        }
    }


    private void Start()
    {
        speed = PlayerControllerScript.Instance.characterSpeed;
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        spawnerObject = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted && isClimbing == false)
        {
            characterController.SimpleMove(new Vector3(0, 0, speed));
            Vector3 newPos = new Vector3(this.transform.position.x, this.transform.position.y + 2.5f, this.transform.position.z);// this
            spawnerObject.transform.position = newPos;

            //SoundScript.Instance.PlaySound(0);
        }
    }


    private IEnumerator OnTriggerEnter(Collider other)
    {
        yield return new WaitForSeconds(1); //// i am changing this one  am afzal 2f-1f
        /*if (collided && other.gameObject.name == "ConvertToTank")
        {
            Debug.Log("jsldlksadjksad");
            PlayerControllerScript.Instance.ChangeShape(PlayerControllerScript.Instance.tankIndex);
            //yield return;
        }
        yield return new WaitForSeconds(2);
        collided = true;
        if (collided && other.gameObject.tag == "Ground")
        {
            PlayerControllerScript.Instance.DetectType("Character", other.gameObject);
        }*/
    }

    public void Climb()
    {
        StartCoroutine(IsClimbing());
    }

    IEnumerator IsClimbing()
    {
        yield return new WaitForSeconds(1f);

        SoundScript.Instance.StopSound();

        Debug.Log("Climbing successful");
        isClimbing = true;
        animator.SetBool("isClimbing", true);
        //GameObject.FindGameObjectWithTag("Player").GetComponent<SoundScript>().StopSound();
        
        float startTime = Time.time;
        while (Time.time < startTime + 7f)
        {
            this.transform.Translate(Vector3.up * Time.deltaTime * 2, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, this.transform.position.y + 2.5f, this.transform.position.z);
            spawnerObject.transform.position = newPos;
            yield return null;
            Debug.Log(startTime + 7f);
            if (Time.time >= startTime + 6f)
                animator.SetBool("isClimbing", false);
        }
        //GameObject.FindGameObjectWithTag("Player").GetComponent<SoundScript>().PlaySound();
        SoundScript.Instance.PlaySound(PlayerControllerScript.Instance.characterIndex);
        isClimbing = false;
    }

}
