﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBoatScript : MonoBehaviour
{
    [SerializeField] private float boatSpeed;
    private GameObject spawnerObject;

    private void Start()
    {
        spawnerObject = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * boatSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, spawnerObject.transform.position.y, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Water")
        {
            boatSpeed = PlayerControllerScript.Instance.boatSpeed;
        }
        else if (collision.gameObject.tag != "Water" || collision.gameObject.tag == "NonDrivable")
        {
            boatSpeed = PlayerControllerScript.Instance.decreasedSpeed;
        }
    }
}
