﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTankScript : MonoBehaviour
{
    [HideInInspector] public float tankSpeed;
    private GameObject spawnerObject;

    private static PlayerTankScript instance;
    public static PlayerTankScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<PlayerTankScript>();
            }
            return instance;
        }
    }

    private void Start()
    {
        tankSpeed = PlayerControllerScript.Instance.tankSpeed;
        spawnerObject = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * tankSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, spawnerObject.transform.position.y, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Box.T")
        {
            Vector3 newPos = collision.gameObject.transform.position;
            Destroy(collision.gameObject);
            Instantiate(Resources.Load("Prefabs/Crate", typeof(GameObject)) as GameObject,
                newPos,
                this.transform.rotation);
        }
        if (collision.gameObject.tag == "Water" || collision.gameObject.tag == "NonDrivable")
        {
            tankSpeed = PlayerControllerScript.Instance.decreasedSpeed;
        }
    }
}
