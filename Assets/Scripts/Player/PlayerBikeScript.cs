﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBikeScript : MonoBehaviour
{
    [HideInInspector] public float bikeSpeed;
    private GameObject spawnerObject;

    private static PlayerBikeScript instance;
    public static PlayerBikeScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<PlayerBikeScript>();
            }
            return instance;
        }
    }

    private void Start()
    {
        spawnerObject = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * bikeSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, this.transform.position.y + 2.5f, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground" || collision.gameObject.tag == "NonDrivable")
        {
            bikeSpeed = PlayerControllerScript.Instance.bikeSpeed;
        }
        else if (collision.gameObject.tag == "Water")
        {
            bikeSpeed = PlayerControllerScript.Instance.decreasedSpeed;
        }
        else
            PlayerControllerScript.Instance.DetectType("Bike", collision.gameObject);
    }

}
