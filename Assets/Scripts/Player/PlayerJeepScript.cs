﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJeepScript : MonoBehaviour
{
    public float jeepSpeed;
    private GameObject spawnerObject;

    private static PlayerJeepScript instance;
    public static PlayerJeepScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<PlayerJeepScript>();
            }
            return instance;
        }
    }

    private void Start()
    {
        spawnerObject = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * jeepSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, this.transform.position.y + 1.25f, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            jeepSpeed = PlayerControllerScript.Instance.jeepSpeed;
        }
        else if (collision.gameObject.tag == "Water" || collision.gameObject.tag == "NonDrivable")
        {
            jeepSpeed = PlayerControllerScript.Instance.decreasedSpeed;
        }
        else
            PlayerControllerScript.Instance.DetectType("Jeep", collision.gameObject);
    }
}