﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHelicopterScript : MonoBehaviour
{
    [SerializeField] private float helicopterSpeed;
    private GameObject spawnerObject;

    private bool helicopterBoost = false;

    private bool pushHelicopter = false;

    private static PlayerHelicopterScript instance;
    public static PlayerHelicopterScript Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<PlayerHelicopterScript>();
            }
            return instance;
        }
    }

    private void OnEnable()
    {
        StartCoroutine(wait());
    }

    IEnumerator wait()
    {
        yield return new WaitForSecondsRealtime(1f);
        pushHelicopter = true;
    }

    private void Start()
    {
        helicopterSpeed = PlayerControllerScript.Instance.helicopterSpeed;
        spawnerObject = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        if (GameManager.Instance.isGameStarted && helicopterBoost == false && pushHelicopter)
        {
            this.transform.Translate(Vector3.forward * Time.deltaTime * helicopterSpeed, Space.World);
            Vector3 newPos = new Vector3(this.transform.position.x, spawnerObject.transform.position.y, this.transform.position.z);
            spawnerObject.transform.position = newPos;
        }
    }

    public void BoostHelicopter(int boostValue)
    {
        helicopterBoost = true;
        StartCoroutine(BoostHelicopterNow(boostValue));
    }

    IEnumerator BoostHelicopterNow(int boostValue)
    {
        yield return new WaitForSecondsRealtime(1f);
        float startTime = Time.time;
        while (Time.time < startTime + 2f)
        {
            this.transform.Translate(Vector3.up * Time.deltaTime * boostValue, Space.World);
            Vector3 newPos = this.transform.position;
            spawnerObject.transform.position = newPos;
            yield return null;
        }
        helicopterBoost = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        //PlayerControllerScript.Instance.DetectType("Helicopter", collision.gameObject);
    }
}
