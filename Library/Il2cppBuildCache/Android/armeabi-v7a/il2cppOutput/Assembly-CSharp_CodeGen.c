﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void IAPDemo::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern void IAPDemo_OnInitialized_mF4B746A03FD737884C8142D7CBDD0021D31CDCA5 (void);
// 0x00000002 UnityEngine.Purchasing.PurchaseProcessingResult IAPDemo::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern void IAPDemo_ProcessPurchase_m70453229C85ABFCB4613956783461A64D2007EE8 (void);
// 0x00000003 System.Void IAPDemo::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern void IAPDemo_OnPurchaseFailed_mAAE3F514875596D53DD6A16120A5BBB0B0065A98 (void);
// 0x00000004 System.Void IAPDemo::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern void IAPDemo_OnInitializeFailed_m541F64EF4EEDFC57AA3767D4D525DC3C424AA642 (void);
// 0x00000005 System.Void IAPDemo::Awake()
extern void IAPDemo_Awake_m005133078B2E616BF5075EC2ADD13063B7CD7B5C (void);
// 0x00000006 System.Void IAPDemo::OnTransactionsRestored(System.Boolean)
extern void IAPDemo_OnTransactionsRestored_m9550142897B9C2FA2D4AFEB4CD9584398CF6C71B (void);
// 0x00000007 System.Void IAPDemo::OnDeferred(UnityEngine.Purchasing.Product)
extern void IAPDemo_OnDeferred_mA20AF9D1EF45E4A44FD75B3D38D7586797AF97FC (void);
// 0x00000008 System.Void IAPDemo::InitUI(System.Collections.Generic.IEnumerable`1<UnityEngine.Purchasing.Product>)
extern void IAPDemo_InitUI_mE24DC740110BB8636B4A775353A2AFBBC2BA58BA (void);
// 0x00000009 System.Void IAPDemo::PurchaseButtonClick(System.String)
extern void IAPDemo_PurchaseButtonClick_m874B72DC4ABC47339984EA54E6AFBD754E95204A (void);
// 0x0000000A System.Void IAPDemo::RestoreButtonClick()
extern void IAPDemo_RestoreButtonClick_m14DF5807196B157120C2D7DBE75E5688E588D5D6 (void);
// 0x0000000B System.Void IAPDemo::ClearProductUIs()
extern void IAPDemo_ClearProductUIs_m554CF87F773E47071C5F3982D7880162FDC14A2B (void);
// 0x0000000C System.Void IAPDemo::AddProductUIs(UnityEngine.Purchasing.Product[])
extern void IAPDemo_AddProductUIs_mC7252C99C2160AF51682D429EDD14CE657316160 (void);
// 0x0000000D System.Void IAPDemo::UpdateProductUI(UnityEngine.Purchasing.Product)
extern void IAPDemo_UpdateProductUI_m36C323106D395A613F1ACF62600932CE17BA9351 (void);
// 0x0000000E System.Void IAPDemo::UpdateProductPendingUI(UnityEngine.Purchasing.Product,System.Int32)
extern void IAPDemo_UpdateProductPendingUI_m5F428981126AB91982C3502B586901D41A834F78 (void);
// 0x0000000F System.Boolean IAPDemo::NeedRestoreButton()
extern void IAPDemo_NeedRestoreButton_m91A5AADF696AD26A93E82EFCE47AF22DC7D8B8B1 (void);
// 0x00000010 System.Void IAPDemo::LogProductDefinitions()
extern void IAPDemo_LogProductDefinitions_mBA524D8D904C3ECD52B6180920834FF1028386C6 (void);
// 0x00000011 System.Void IAPDemo::.ctor()
extern void IAPDemo__ctor_m3CDCBE3618DDBEEE75CA94F38A81B5E148BDEB68 (void);
// 0x00000012 System.Void IAPDemoProductUI::SetProduct(UnityEngine.Purchasing.Product,System.Action`1<System.String>)
extern void IAPDemoProductUI_SetProduct_mB5173BE25A6C23AA08D644D7EA9F45A441AC3B06 (void);
// 0x00000013 System.Void IAPDemoProductUI::SetPendingTime(System.Int32)
extern void IAPDemoProductUI_SetPendingTime_mFC56B6E828A8230D0D6E519778E86010BBB33205 (void);
// 0x00000014 System.Void IAPDemoProductUI::PurchaseButtonClick()
extern void IAPDemoProductUI_PurchaseButtonClick_m24D7B1B2C2CD35DD44CC194701E575F260C29237 (void);
// 0x00000015 System.Void IAPDemoProductUI::ReceiptButtonClick()
extern void IAPDemoProductUI_ReceiptButtonClick_m5A451F87378B856BD60F5F6C539881CF21CAC06D (void);
// 0x00000016 System.Void IAPDemoProductUI::.ctor()
extern void IAPDemoProductUI__ctor_mB354C957BDA3FE36DE9FDCD1806798EE31DF4AD4 (void);
// 0x00000017 BrownAiBikeScript BrownAiBikeScript::get_Instance()
extern void BrownAiBikeScript_get_Instance_m465D862301EB04F4BE7D6727BD296516F14D806F (void);
// 0x00000018 System.Void BrownAiBikeScript::Start()
extern void BrownAiBikeScript_Start_m07065169D8E07AF72BCABA2E99A6ED1058E71AC8 (void);
// 0x00000019 System.Void BrownAiBikeScript::Update()
extern void BrownAiBikeScript_Update_m1D612427365CB6C2AFA33285B2D4A07C450E6F66 (void);
// 0x0000001A System.Void BrownAiBikeScript::OnCollisionEnter(UnityEngine.Collision)
extern void BrownAiBikeScript_OnCollisionEnter_mC4C54423ADC14AD72FF8854027BD184D271D711E (void);
// 0x0000001B System.Void BrownAiBikeScript::.ctor()
extern void BrownAiBikeScript__ctor_m0FF389698CF34FD5CC986A5C55F2142DF49A4B02 (void);
// 0x0000001C System.Void BrownAiBoatScript::Start()
extern void BrownAiBoatScript_Start_mEA716F2F595A10E7F5036D445EAAAA3B68E87D9E (void);
// 0x0000001D System.Void BrownAiBoatScript::Update()
extern void BrownAiBoatScript_Update_m8A8BDCB7E37FE34E934403AF85B25E608834471B (void);
// 0x0000001E System.Void BrownAiBoatScript::OnCollisionEnter(UnityEngine.Collision)
extern void BrownAiBoatScript_OnCollisionEnter_mF758051EF4E55843BE33F9A770F0D42998C09808 (void);
// 0x0000001F System.Void BrownAiBoatScript::.ctor()
extern void BrownAiBoatScript__ctor_mCF6852D83F5FE727C88D3EE6C96233EA4E286D7F (void);
// 0x00000020 BrownAiCharacterScript BrownAiCharacterScript::get_Instance()
extern void BrownAiCharacterScript_get_Instance_m1F0C20DF2C35A5FE5830A37426CDE5EB59FD7ABD (void);
// 0x00000021 System.Void BrownAiCharacterScript::Start()
extern void BrownAiCharacterScript_Start_mCDD26B6269B0C12BAA4CB00513EA004760AC100A (void);
// 0x00000022 System.Void BrownAiCharacterScript::Update()
extern void BrownAiCharacterScript_Update_mB609954FE819F11360C810C30E65E68E57FE79B3 (void);
// 0x00000023 System.Collections.IEnumerator BrownAiCharacterScript::OnTriggerEnter(UnityEngine.Collider)
extern void BrownAiCharacterScript_OnTriggerEnter_m493718247AF0CF260CEB8A4C9DD1ED72F8BEBEFD (void);
// 0x00000024 System.Void BrownAiCharacterScript::OnCollisionEnter(UnityEngine.Collision)
extern void BrownAiCharacterScript_OnCollisionEnter_mCAEC6986C441673E8BB84598DE5C173506E089F4 (void);
// 0x00000025 System.Void BrownAiCharacterScript::Climb()
extern void BrownAiCharacterScript_Climb_mF32FD5E54388B9ECFC9717EED50EC25A254B6411 (void);
// 0x00000026 System.Collections.IEnumerator BrownAiCharacterScript::IsClimbing()
extern void BrownAiCharacterScript_IsClimbing_mE2168EA8FC2E7E70A1C69455C9FA9685BA147C76 (void);
// 0x00000027 System.Void BrownAiCharacterScript::.ctor()
extern void BrownAiCharacterScript__ctor_m942C1612AA2E9439EFE791AE29B0185277EF81E5 (void);
// 0x00000028 System.Void BrownAiCharacterScript/<OnTriggerEnter>d__11::.ctor(System.Int32)
extern void U3COnTriggerEnterU3Ed__11__ctor_m58CE187FD101838CFCA77561782A057DBE065674 (void);
// 0x00000029 System.Void BrownAiCharacterScript/<OnTriggerEnter>d__11::System.IDisposable.Dispose()
extern void U3COnTriggerEnterU3Ed__11_System_IDisposable_Dispose_m1B1ED971F3313F0B1835963EC4B2C73FA501A814 (void);
// 0x0000002A System.Boolean BrownAiCharacterScript/<OnTriggerEnter>d__11::MoveNext()
extern void U3COnTriggerEnterU3Ed__11_MoveNext_m19D97132558F55C5FD729C1B0589F1C403E6C95B (void);
// 0x0000002B System.Object BrownAiCharacterScript/<OnTriggerEnter>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnTriggerEnterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA19AE93EDBD946686F0548C8A3C8CF793C75AB6 (void);
// 0x0000002C System.Void BrownAiCharacterScript/<OnTriggerEnter>d__11::System.Collections.IEnumerator.Reset()
extern void U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_Reset_mDF0157FD691D1AA9DA10ED0CD2D171E2DAC63F6A (void);
// 0x0000002D System.Object BrownAiCharacterScript/<OnTriggerEnter>d__11::System.Collections.IEnumerator.get_Current()
extern void U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_get_Current_m362197B045B5D2FE9811917DBDAE5ABF6166D8BE (void);
// 0x0000002E System.Void BrownAiCharacterScript/<IsClimbing>d__14::.ctor(System.Int32)
extern void U3CIsClimbingU3Ed__14__ctor_mF524A37C748D43169BC4BEE0CD4FA5DDFA04969A (void);
// 0x0000002F System.Void BrownAiCharacterScript/<IsClimbing>d__14::System.IDisposable.Dispose()
extern void U3CIsClimbingU3Ed__14_System_IDisposable_Dispose_m4DC1A0A554B0D891F7D55A5706870108D70CB307 (void);
// 0x00000030 System.Boolean BrownAiCharacterScript/<IsClimbing>d__14::MoveNext()
extern void U3CIsClimbingU3Ed__14_MoveNext_mEF60A47A96781E2E74394D638FE12328053CAEF7 (void);
// 0x00000031 System.Object BrownAiCharacterScript/<IsClimbing>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIsClimbingU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6AC86978016C16339DB835397D58344D4CE84CEA (void);
// 0x00000032 System.Void BrownAiCharacterScript/<IsClimbing>d__14::System.Collections.IEnumerator.Reset()
extern void U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_Reset_m3EDEF13B8E0F81E027857D03E2AE87CC2E650733 (void);
// 0x00000033 System.Object BrownAiCharacterScript/<IsClimbing>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_get_Current_mEA8103E3D058EAD5EA2236B4F85784722145ACC7 (void);
// 0x00000034 BrownAiControllerScript BrownAiControllerScript::get_Instance()
extern void BrownAiControllerScript_get_Instance_m46541BE21636C5BD6E3151F56D24C33A030B06A0 (void);
// 0x00000035 System.Void BrownAiControllerScript::Awake()
extern void BrownAiControllerScript_Awake_m3D543C48F570DEA660AD063E9F0B2F4236C9D1E7 (void);
// 0x00000036 System.Void BrownAiControllerScript::Start()
extern void BrownAiControllerScript_Start_m759039CF18BC84CF3002EC14B5F87F8C2843FCBE (void);
// 0x00000037 System.Void BrownAiControllerScript::Update()
extern void BrownAiControllerScript_Update_m59A7ED9F249B23A9C20C8186BB842060D1116758 (void);
// 0x00000038 System.Void BrownAiControllerScript::GetCurrentObject()
extern void BrownAiControllerScript_GetCurrentObject_m6A87B70C034AA7E373E45A945C5AF8FA12D50C34 (void);
// 0x00000039 System.Void BrownAiControllerScript::GetRotationalObjects()
extern void BrownAiControllerScript_GetRotationalObjects_mC4988CE8149DD7D1452AA46804BFDA7AAD6BF92D (void);
// 0x0000003A System.Void BrownAiControllerScript::ChangeShape(System.Int32)
extern void BrownAiControllerScript_ChangeShape_m37BAD59EBF171000FA51CA38BDD069F96D9468B3 (void);
// 0x0000003B System.Collections.IEnumerator BrownAiControllerScript::MoveObject(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void BrownAiControllerScript_MoveObject_mE73BD22EC65468F1DB77EC6F56E369F3372B960C (void);
// 0x0000003C System.Int32 BrownAiControllerScript::ReturnIndex()
extern void BrownAiControllerScript_ReturnIndex_m2CF125966787084BBE4236948D567E020B3C4ACF (void);
// 0x0000003D System.Void BrownAiControllerScript::DetectType(System.String,UnityEngine.GameObject)
extern void BrownAiControllerScript_DetectType_m644D069E04A05C6782D903AF2F89C94E1C64FACA (void);
// 0x0000003E System.Collections.IEnumerator BrownAiControllerScript::Transform(System.Int32)
extern void BrownAiControllerScript_Transform_m1B6BB668C02AF1A1B1D119A58FEEB654E580C844 (void);
// 0x0000003F System.Collections.IEnumerator BrownAiControllerScript::TransformToRandom()
extern void BrownAiControllerScript_TransformToRandom_m235F4E738DF91B1007DBC556DE0015637C55A285 (void);
// 0x00000040 System.Void BrownAiControllerScript::.ctor()
extern void BrownAiControllerScript__ctor_m0AAA2FDE0535BD60BF34041EB7F17CC0F78DEDB1 (void);
// 0x00000041 System.Void BrownAiControllerScript::.cctor()
extern void BrownAiControllerScript__cctor_m7130E0A8851ABAAC3B7A856A052209E240C6E33C (void);
// 0x00000042 System.Void BrownAiControllerScript/<MoveObject>d__35::.ctor(System.Int32)
extern void U3CMoveObjectU3Ed__35__ctor_m09BED1D81F93D8216B7F0FAEE530C1CBF188A422 (void);
// 0x00000043 System.Void BrownAiControllerScript/<MoveObject>d__35::System.IDisposable.Dispose()
extern void U3CMoveObjectU3Ed__35_System_IDisposable_Dispose_mD48351D86E92E28331F2E598A87C699CDA727F75 (void);
// 0x00000044 System.Boolean BrownAiControllerScript/<MoveObject>d__35::MoveNext()
extern void U3CMoveObjectU3Ed__35_MoveNext_m4C620981279C8B015E0347F6D9092D33DED21095 (void);
// 0x00000045 System.Object BrownAiControllerScript/<MoveObject>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveObjectU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD24BAE8A82B777887972D591C6FCF0BF84E28EE (void);
// 0x00000046 System.Void BrownAiControllerScript/<MoveObject>d__35::System.Collections.IEnumerator.Reset()
extern void U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_Reset_m3AFFAEDB3F53A4EEE4966EA6E27C59B135C4AA95 (void);
// 0x00000047 System.Object BrownAiControllerScript/<MoveObject>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_get_Current_m98A9BA1D1B9DEA461AA45606CD526DF70322DD08 (void);
// 0x00000048 System.Void BrownAiControllerScript/<Transform>d__38::.ctor(System.Int32)
extern void U3CTransformU3Ed__38__ctor_m997EE86AA60658A13C21ED38BEC132A57A81EB01 (void);
// 0x00000049 System.Void BrownAiControllerScript/<Transform>d__38::System.IDisposable.Dispose()
extern void U3CTransformU3Ed__38_System_IDisposable_Dispose_mB8A89A2B908D9F9289714BC710A4AD681841105A (void);
// 0x0000004A System.Boolean BrownAiControllerScript/<Transform>d__38::MoveNext()
extern void U3CTransformU3Ed__38_MoveNext_m2D69E9C998DDB57DA83795B8C7C270BC2C40EB59 (void);
// 0x0000004B System.Object BrownAiControllerScript/<Transform>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTransformU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC790BF887E6356F5D81DBAF95C2DDBE7636E02E6 (void);
// 0x0000004C System.Void BrownAiControllerScript/<Transform>d__38::System.Collections.IEnumerator.Reset()
extern void U3CTransformU3Ed__38_System_Collections_IEnumerator_Reset_mB23DA13FBE8C5E65C1F051D050FB12C855C291F2 (void);
// 0x0000004D System.Object BrownAiControllerScript/<Transform>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CTransformU3Ed__38_System_Collections_IEnumerator_get_Current_mEBD9114DE61869220216C93567201F39A0ACECB0 (void);
// 0x0000004E System.Void BrownAiControllerScript/<TransformToRandom>d__39::.ctor(System.Int32)
extern void U3CTransformToRandomU3Ed__39__ctor_m551C7C45B492150BA04CEF5B3E2CCE43AF524DAF (void);
// 0x0000004F System.Void BrownAiControllerScript/<TransformToRandom>d__39::System.IDisposable.Dispose()
extern void U3CTransformToRandomU3Ed__39_System_IDisposable_Dispose_mB8D6EE430E1F60115A97625706C08E4175D9C266 (void);
// 0x00000050 System.Boolean BrownAiControllerScript/<TransformToRandom>d__39::MoveNext()
extern void U3CTransformToRandomU3Ed__39_MoveNext_m296CB708FEB96C582ADB4995DB0B386EDE54F080 (void);
// 0x00000051 System.Object BrownAiControllerScript/<TransformToRandom>d__39::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTransformToRandomU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE802F8C275160DBFE5A0E861778DB93AA30CA2A2 (void);
// 0x00000052 System.Void BrownAiControllerScript/<TransformToRandom>d__39::System.Collections.IEnumerator.Reset()
extern void U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_Reset_mF8829DE38EDE66FA018654CF651988DF3775B13D (void);
// 0x00000053 System.Object BrownAiControllerScript/<TransformToRandom>d__39::System.Collections.IEnumerator.get_Current()
extern void U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_get_Current_mBE8313C35CA232335D73F1F6529F45FC8E830ED2 (void);
// 0x00000054 System.Void BrownAiGliderScript::Start()
extern void BrownAiGliderScript_Start_m9E5627E39FE69319916EBEBA22ED2CBDBC51288A (void);
// 0x00000055 System.Void BrownAiGliderScript::Update()
extern void BrownAiGliderScript_Update_mE5F6F4E203F6004263EBADF3C42DCB28E3AB3CCE (void);
// 0x00000056 System.Void BrownAiGliderScript::OnTriggerEnter(UnityEngine.Collider)
extern void BrownAiGliderScript_OnTriggerEnter_m11F456F2DB8E89F43230EADD9333434268BED3E4 (void);
// 0x00000057 System.Void BrownAiGliderScript::OnTriggerExit(UnityEngine.Collider)
extern void BrownAiGliderScript_OnTriggerExit_mB1252AA024D000B386F53ED42FCA027D42699A03 (void);
// 0x00000058 System.Void BrownAiGliderScript::.ctor()
extern void BrownAiGliderScript__ctor_mD3E4949B1E044CE8BD7AA24D5E19284111582058 (void);
// 0x00000059 BrownAiHelicopterScript BrownAiHelicopterScript::get_Instance()
extern void BrownAiHelicopterScript_get_Instance_m753ABCE852B9042C09EE59469B04AEF4721621EF (void);
// 0x0000005A System.Void BrownAiHelicopterScript::OnEnable()
extern void BrownAiHelicopterScript_OnEnable_m60C6FD3D0FCF3E2C10A7DF854ED901BDE7EBAE54 (void);
// 0x0000005B System.Collections.IEnumerator BrownAiHelicopterScript::wait()
extern void BrownAiHelicopterScript_wait_m2F59CFA6957E47B9D0B98D2DB0CBC02F566206E1 (void);
// 0x0000005C System.Void BrownAiHelicopterScript::Start()
extern void BrownAiHelicopterScript_Start_mBA76D82F6E94B035F892E7A07EC033302C3FB65D (void);
// 0x0000005D System.Void BrownAiHelicopterScript::Update()
extern void BrownAiHelicopterScript_Update_mA6144E54255200ACA800461DB6755CB699F0F654 (void);
// 0x0000005E System.Void BrownAiHelicopterScript::BoostHelicopter(System.Int32)
extern void BrownAiHelicopterScript_BoostHelicopter_m62813B1028820F5FAAB00EFB571D736490B84DB3 (void);
// 0x0000005F System.Collections.IEnumerator BrownAiHelicopterScript::BoostHelicopterNow(System.Int32)
extern void BrownAiHelicopterScript_BoostHelicopterNow_mA24205F67B91CA8E96E37EE86EF4FEED72487AF8 (void);
// 0x00000060 System.Void BrownAiHelicopterScript::OnCollisionEnter(UnityEngine.Collision)
extern void BrownAiHelicopterScript_OnCollisionEnter_mAE4ECE86AC43C139BEFE4CFA472C20AC882C5411 (void);
// 0x00000061 System.Void BrownAiHelicopterScript::.ctor()
extern void BrownAiHelicopterScript__ctor_mB5AE4AAE800753E77F7A05EE89845E927A05BDCA (void);
// 0x00000062 System.Void BrownAiHelicopterScript/<wait>d__8::.ctor(System.Int32)
extern void U3CwaitU3Ed__8__ctor_m9CC191CFC8A05A0F2F712CB78E3D3C712D1B3BA8 (void);
// 0x00000063 System.Void BrownAiHelicopterScript/<wait>d__8::System.IDisposable.Dispose()
extern void U3CwaitU3Ed__8_System_IDisposable_Dispose_mB7ED5E1352F730692DA3719E632FE932074ECEEA (void);
// 0x00000064 System.Boolean BrownAiHelicopterScript/<wait>d__8::MoveNext()
extern void U3CwaitU3Ed__8_MoveNext_m2364624663E1BE42FED765DDA67B47BFF8A445D1 (void);
// 0x00000065 System.Object BrownAiHelicopterScript/<wait>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CwaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD9CC63EC2CA3F08870E2F09F5419F073045356EB (void);
// 0x00000066 System.Void BrownAiHelicopterScript/<wait>d__8::System.Collections.IEnumerator.Reset()
extern void U3CwaitU3Ed__8_System_Collections_IEnumerator_Reset_m8EDAC25117B4E697F2F433B11665FD810FB4FDB5 (void);
// 0x00000067 System.Object BrownAiHelicopterScript/<wait>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CwaitU3Ed__8_System_Collections_IEnumerator_get_Current_m2759321C2416D426BC60F3045E046A528C23E938 (void);
// 0x00000068 System.Void BrownAiHelicopterScript/<BoostHelicopterNow>d__12::.ctor(System.Int32)
extern void U3CBoostHelicopterNowU3Ed__12__ctor_m59B5CA3733B514DC5D43AB2BC40265EE4B6324CD (void);
// 0x00000069 System.Void BrownAiHelicopterScript/<BoostHelicopterNow>d__12::System.IDisposable.Dispose()
extern void U3CBoostHelicopterNowU3Ed__12_System_IDisposable_Dispose_mEFD999D4EF533ABFE2E27B1A50696CD34CF077B3 (void);
// 0x0000006A System.Boolean BrownAiHelicopterScript/<BoostHelicopterNow>d__12::MoveNext()
extern void U3CBoostHelicopterNowU3Ed__12_MoveNext_mABE08554EA1FDBB49B3100B7C2C571EA1755595E (void);
// 0x0000006B System.Object BrownAiHelicopterScript/<BoostHelicopterNow>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBoostHelicopterNowU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31180F90051ED150F895B5D6062E8C7EB35A6099 (void);
// 0x0000006C System.Void BrownAiHelicopterScript/<BoostHelicopterNow>d__12::System.Collections.IEnumerator.Reset()
extern void U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_Reset_m7F1A5716FF8139EDB930D8D75E2DA42121B3CA40 (void);
// 0x0000006D System.Object BrownAiHelicopterScript/<BoostHelicopterNow>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_get_Current_m4B7EE880FE971084FA8869BB14C0B17EE6A9665A (void);
// 0x0000006E BrownAiJeepScript BrownAiJeepScript::get_Instance()
extern void BrownAiJeepScript_get_Instance_mD082B7B35C051BADFB068B1BC6893417CDCF5335 (void);
// 0x0000006F System.Void BrownAiJeepScript::Start()
extern void BrownAiJeepScript_Start_mDD9235B66859E5727732766FBB9FBE5DC8AFE16C (void);
// 0x00000070 System.Void BrownAiJeepScript::Update()
extern void BrownAiJeepScript_Update_m3B8F71E1F81B5FDBC77E85A3E83BEFCA2F34C1FC (void);
// 0x00000071 System.Void BrownAiJeepScript::OnCollisionEnter(UnityEngine.Collision)
extern void BrownAiJeepScript_OnCollisionEnter_m959AE786C9705571472971DCDB3B175F8BB8647E (void);
// 0x00000072 System.Void BrownAiJeepScript::.ctor()
extern void BrownAiJeepScript__ctor_m4E70C9FC72FC2DE9558EB4DACCBC4A6266E22F1B (void);
// 0x00000073 BrownAiTankScript BrownAiTankScript::get_Instance()
extern void BrownAiTankScript_get_Instance_mE35F6404DB8E55B354F3D6D8CF1863D5AFB588FB (void);
// 0x00000074 System.Void BrownAiTankScript::Start()
extern void BrownAiTankScript_Start_m463EE5E493A0E08B9A889FE2F3B014FFE3391242 (void);
// 0x00000075 System.Void BrownAiTankScript::Update()
extern void BrownAiTankScript_Update_m9F06D1AAA509638C892EAE9307CDB390C38DDC4B (void);
// 0x00000076 System.Void BrownAiTankScript::OnCollisionEnter(UnityEngine.Collision)
extern void BrownAiTankScript_OnCollisionEnter_m5D1D79E1E58A3D8BEC40A44CB137566D0BA2EC20 (void);
// 0x00000077 System.Void BrownAiTankScript::.ctor()
extern void BrownAiTankScript__ctor_mC0C0BBEE68208354FB67ECE7886AB8692F794C25 (void);
// 0x00000078 GreenAiBikeScript GreenAiBikeScript::get_Instance()
extern void GreenAiBikeScript_get_Instance_m2220F81BDC77145D307EBBC512287C069FE29D70 (void);
// 0x00000079 System.Void GreenAiBikeScript::Start()
extern void GreenAiBikeScript_Start_m1C2D06B5ED16840E2D98F5FD97BAC3F097314888 (void);
// 0x0000007A System.Void GreenAiBikeScript::Update()
extern void GreenAiBikeScript_Update_m950625E1A3B2A25054BC0D8F02BE2F2944FBC80A (void);
// 0x0000007B System.Void GreenAiBikeScript::OnCollisionEnter(UnityEngine.Collision)
extern void GreenAiBikeScript_OnCollisionEnter_m2C5BEFBF5EF58CA2A0134CC848E59EE4B950A081 (void);
// 0x0000007C System.Void GreenAiBikeScript::.ctor()
extern void GreenAiBikeScript__ctor_mF6DDC8106E24176E5DCD537A5AFE58C1310FD06D (void);
// 0x0000007D System.Void GreenAiBoatScript::Start()
extern void GreenAiBoatScript_Start_m69008A0260D0430124222AC9A1D52FE940315FFD (void);
// 0x0000007E System.Void GreenAiBoatScript::Update()
extern void GreenAiBoatScript_Update_m9700E8364334E0E620A2A77268F582DB377A5CD4 (void);
// 0x0000007F System.Void GreenAiBoatScript::OnCollisionEnter(UnityEngine.Collision)
extern void GreenAiBoatScript_OnCollisionEnter_m876E9F2B07E70043F599FB53A67B741E8551927F (void);
// 0x00000080 System.Void GreenAiBoatScript::.ctor()
extern void GreenAiBoatScript__ctor_m58A3921C5182D33003EE68A162525941D570BAE8 (void);
// 0x00000081 GreenAiCharacterScript GreenAiCharacterScript::get_Instance()
extern void GreenAiCharacterScript_get_Instance_m7D45946E23E69E1A1299FEC7A3E172B13C1AA8D1 (void);
// 0x00000082 System.Void GreenAiCharacterScript::Start()
extern void GreenAiCharacterScript_Start_m87E6BF98B3C4BAF7FC34B08B0B68E95520BD7871 (void);
// 0x00000083 System.Void GreenAiCharacterScript::Update()
extern void GreenAiCharacterScript_Update_mE9637B716F54E0F9640963C343DC3B9E254C9F51 (void);
// 0x00000084 System.Collections.IEnumerator GreenAiCharacterScript::OnTriggerEnter(UnityEngine.Collider)
extern void GreenAiCharacterScript_OnTriggerEnter_mE41DA5EA2AE137B60804896A6C5B1CAE142271E6 (void);
// 0x00000085 System.Void GreenAiCharacterScript::OnCollisionEnter(UnityEngine.Collision)
extern void GreenAiCharacterScript_OnCollisionEnter_mEE9A6CB31997ED48D0CC78D3F6BFAF2E66999355 (void);
// 0x00000086 System.Void GreenAiCharacterScript::Climb()
extern void GreenAiCharacterScript_Climb_m7FAC4633F80D843F982A49EF69767DA52AFFFA8B (void);
// 0x00000087 System.Collections.IEnumerator GreenAiCharacterScript::IsClimbing()
extern void GreenAiCharacterScript_IsClimbing_m6104549DB6A64AC33BA7A9B683A4CE300389D87F (void);
// 0x00000088 System.Void GreenAiCharacterScript::.ctor()
extern void GreenAiCharacterScript__ctor_m3321D7145D9F76F82CA6F93646AAA0EFDC85AFC7 (void);
// 0x00000089 System.Void GreenAiCharacterScript/<OnTriggerEnter>d__11::.ctor(System.Int32)
extern void U3COnTriggerEnterU3Ed__11__ctor_m9A69DC7769006C0580046888DE33D5B3497E3158 (void);
// 0x0000008A System.Void GreenAiCharacterScript/<OnTriggerEnter>d__11::System.IDisposable.Dispose()
extern void U3COnTriggerEnterU3Ed__11_System_IDisposable_Dispose_m1FD5231A948EFABE2395EEDB9C08D376536BB94E (void);
// 0x0000008B System.Boolean GreenAiCharacterScript/<OnTriggerEnter>d__11::MoveNext()
extern void U3COnTriggerEnterU3Ed__11_MoveNext_m0510809FD1414952084F6A53826EEEAE6ADC5630 (void);
// 0x0000008C System.Object GreenAiCharacterScript/<OnTriggerEnter>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnTriggerEnterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m468266D865ED51B5686F8402CA721DCFD254D240 (void);
// 0x0000008D System.Void GreenAiCharacterScript/<OnTriggerEnter>d__11::System.Collections.IEnumerator.Reset()
extern void U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_Reset_mA18241CE3ADA61237D0B8ABE12D3EEF13C49C69C (void);
// 0x0000008E System.Object GreenAiCharacterScript/<OnTriggerEnter>d__11::System.Collections.IEnumerator.get_Current()
extern void U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_get_Current_m97498923C25B468E44FBDA18864B3E10ADEB7FB2 (void);
// 0x0000008F System.Void GreenAiCharacterScript/<IsClimbing>d__14::.ctor(System.Int32)
extern void U3CIsClimbingU3Ed__14__ctor_m101C21898DF05DB161D452C96753EEF525BD7066 (void);
// 0x00000090 System.Void GreenAiCharacterScript/<IsClimbing>d__14::System.IDisposable.Dispose()
extern void U3CIsClimbingU3Ed__14_System_IDisposable_Dispose_m6DBD346A64EE02ED6EDA1E3B242DDF77878C0065 (void);
// 0x00000091 System.Boolean GreenAiCharacterScript/<IsClimbing>d__14::MoveNext()
extern void U3CIsClimbingU3Ed__14_MoveNext_m975BBF993F321C75126744438BCF526082486567 (void);
// 0x00000092 System.Object GreenAiCharacterScript/<IsClimbing>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIsClimbingU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1470B7C3C6547F5A6DECCCD4E3811F7866E27ECC (void);
// 0x00000093 System.Void GreenAiCharacterScript/<IsClimbing>d__14::System.Collections.IEnumerator.Reset()
extern void U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_Reset_mCCC1079F77ABAA01075E34E30346DB874C61403E (void);
// 0x00000094 System.Object GreenAiCharacterScript/<IsClimbing>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_get_Current_m6BC85BF85A7A3BA1C81AD1A4D9EA1C7D25D7DECE (void);
// 0x00000095 GreenAiControllerScript GreenAiControllerScript::get_Instance()
extern void GreenAiControllerScript_get_Instance_mEA3646F25A1CC07B064249F51F75C216CA250737 (void);
// 0x00000096 System.Void GreenAiControllerScript::Awake()
extern void GreenAiControllerScript_Awake_m4728685D9995D393B22D480FD1778F7CF9630A95 (void);
// 0x00000097 System.Void GreenAiControllerScript::Start()
extern void GreenAiControllerScript_Start_m6921D8EE15D60002B529F91C8A0F3AEBD2585239 (void);
// 0x00000098 System.Void GreenAiControllerScript::Update()
extern void GreenAiControllerScript_Update_m5B18956006120CAA3C811688F52890AED7D8F2B5 (void);
// 0x00000099 System.Void GreenAiControllerScript::GetCurrentObject()
extern void GreenAiControllerScript_GetCurrentObject_m2A8E40C5594CA753E1434DEF4766A5F729C0091D (void);
// 0x0000009A System.Void GreenAiControllerScript::GetRotationalObjects()
extern void GreenAiControllerScript_GetRotationalObjects_m5345403C3CC1250A036B64FD30EFB9DDF3A3BE7E (void);
// 0x0000009B System.Void GreenAiControllerScript::ChangeShape(System.Int32)
extern void GreenAiControllerScript_ChangeShape_mD7E778512B0EE955AABA85BE09B5A13912F58C5E (void);
// 0x0000009C System.Collections.IEnumerator GreenAiControllerScript::MoveObject(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void GreenAiControllerScript_MoveObject_m428A17DBAFDD480889D2DC0910582B073B9BF276 (void);
// 0x0000009D System.Int32 GreenAiControllerScript::ReturnIndex()
extern void GreenAiControllerScript_ReturnIndex_m27B6E458707314A870CA3B2B5E0A0B651C6B72DF (void);
// 0x0000009E System.Void GreenAiControllerScript::DetectType(System.String,UnityEngine.GameObject)
extern void GreenAiControllerScript_DetectType_m07238BA4CD8DCD5BA9E7A497507FF4AEB1D618E2 (void);
// 0x0000009F System.Collections.IEnumerator GreenAiControllerScript::Transform(System.Int32)
extern void GreenAiControllerScript_Transform_m0C755F0EE9BB07E4D98ACD902D5D7D18B73EA0AC (void);
// 0x000000A0 System.Collections.IEnumerator GreenAiControllerScript::TransformToRandom()
extern void GreenAiControllerScript_TransformToRandom_mC3F7D234B52D8EB4EE29EBCE9DEB9F15F1A3F5DC (void);
// 0x000000A1 System.Void GreenAiControllerScript::.ctor()
extern void GreenAiControllerScript__ctor_mFD34D056C2E1D68D2EC7679CA2D269BEC3A976B7 (void);
// 0x000000A2 System.Void GreenAiControllerScript::.cctor()
extern void GreenAiControllerScript__cctor_m23C9C4CB42C3D51BAC48B941FE09CECBD8F3243A (void);
// 0x000000A3 System.Void GreenAiControllerScript/<MoveObject>d__35::.ctor(System.Int32)
extern void U3CMoveObjectU3Ed__35__ctor_m179E5FE2DF01B88D84D0D14765488F47DA4AB678 (void);
// 0x000000A4 System.Void GreenAiControllerScript/<MoveObject>d__35::System.IDisposable.Dispose()
extern void U3CMoveObjectU3Ed__35_System_IDisposable_Dispose_m9D27A724FD86ED921543813A4E7260E9A7F7B5D0 (void);
// 0x000000A5 System.Boolean GreenAiControllerScript/<MoveObject>d__35::MoveNext()
extern void U3CMoveObjectU3Ed__35_MoveNext_mAABE0B851B964CE90F5DB9C727F156C00F77DED9 (void);
// 0x000000A6 System.Object GreenAiControllerScript/<MoveObject>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveObjectU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD70A6C3669063054A3585BC89F8525F8A5223E7D (void);
// 0x000000A7 System.Void GreenAiControllerScript/<MoveObject>d__35::System.Collections.IEnumerator.Reset()
extern void U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_Reset_m817F8AD405448B9EDBC2F07BF7F5F83745689AD2 (void);
// 0x000000A8 System.Object GreenAiControllerScript/<MoveObject>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_get_Current_m199D40D6301C2AAB59A577024977E61946E95F8D (void);
// 0x000000A9 System.Void GreenAiControllerScript/<Transform>d__38::.ctor(System.Int32)
extern void U3CTransformU3Ed__38__ctor_mDA513F4551EA387CBE55ADCA55E2BA755F388B9D (void);
// 0x000000AA System.Void GreenAiControllerScript/<Transform>d__38::System.IDisposable.Dispose()
extern void U3CTransformU3Ed__38_System_IDisposable_Dispose_m48F34B6CA28107A6D9F588D85396B9C016B27D72 (void);
// 0x000000AB System.Boolean GreenAiControllerScript/<Transform>d__38::MoveNext()
extern void U3CTransformU3Ed__38_MoveNext_m7E01E0DE036A36762D874B2CD539ACDB2A7F3B00 (void);
// 0x000000AC System.Object GreenAiControllerScript/<Transform>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTransformU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCAC7D326D7467468072FA99AC95AB8A772F0D6EE (void);
// 0x000000AD System.Void GreenAiControllerScript/<Transform>d__38::System.Collections.IEnumerator.Reset()
extern void U3CTransformU3Ed__38_System_Collections_IEnumerator_Reset_mBE648FB20A3F7600472F2EEC5B8A27DA34001E66 (void);
// 0x000000AE System.Object GreenAiControllerScript/<Transform>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CTransformU3Ed__38_System_Collections_IEnumerator_get_Current_m2D984DD67D92B18296EDFFE2E4629A37B611271B (void);
// 0x000000AF System.Void GreenAiControllerScript/<TransformToRandom>d__39::.ctor(System.Int32)
extern void U3CTransformToRandomU3Ed__39__ctor_mA913800A026FDE676DCBEE781F12E7DB50359C3D (void);
// 0x000000B0 System.Void GreenAiControllerScript/<TransformToRandom>d__39::System.IDisposable.Dispose()
extern void U3CTransformToRandomU3Ed__39_System_IDisposable_Dispose_m2BC9278A0B00FAAC4B9BDAF82C99D175095C258F (void);
// 0x000000B1 System.Boolean GreenAiControllerScript/<TransformToRandom>d__39::MoveNext()
extern void U3CTransformToRandomU3Ed__39_MoveNext_mC257FD8DA4A75DB07400706F81372C8B083E0C3C (void);
// 0x000000B2 System.Object GreenAiControllerScript/<TransformToRandom>d__39::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTransformToRandomU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m918D1A177676B7AADDB603964ECF77380C9450FB (void);
// 0x000000B3 System.Void GreenAiControllerScript/<TransformToRandom>d__39::System.Collections.IEnumerator.Reset()
extern void U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_Reset_m7FB7AABD45545A92964F18E329B23FF75F0EF2F7 (void);
// 0x000000B4 System.Object GreenAiControllerScript/<TransformToRandom>d__39::System.Collections.IEnumerator.get_Current()
extern void U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_get_Current_mB99F58C3386E898F1FA7C1FD7E8BC1C92D53D7CE (void);
// 0x000000B5 System.Void GreenAiGliderScript::Start()
extern void GreenAiGliderScript_Start_m9B11DE8162055559022124478920806F143B7E3A (void);
// 0x000000B6 System.Void GreenAiGliderScript::Update()
extern void GreenAiGliderScript_Update_m202C2D77D2CED82339FDF6468DEA859A2B2EFCBD (void);
// 0x000000B7 System.Void GreenAiGliderScript::OnTriggerEnter(UnityEngine.Collider)
extern void GreenAiGliderScript_OnTriggerEnter_m4B2B4D6CEFD2DA074BFCD2B6B8F3CD35D2F50FF6 (void);
// 0x000000B8 System.Void GreenAiGliderScript::OnTriggerExit(UnityEngine.Collider)
extern void GreenAiGliderScript_OnTriggerExit_m0ECB692885700CD388934DFB4DF426222CE26DF9 (void);
// 0x000000B9 System.Void GreenAiGliderScript::.ctor()
extern void GreenAiGliderScript__ctor_m2DA8A230A01A44FD80F922C88308891DDB99117D (void);
// 0x000000BA GreenAiHelicopterScript GreenAiHelicopterScript::get_Instance()
extern void GreenAiHelicopterScript_get_Instance_m70714411C064B5CF772F5B19DE1D61FA52769CF9 (void);
// 0x000000BB System.Void GreenAiHelicopterScript::OnEnable()
extern void GreenAiHelicopterScript_OnEnable_mA89723C2B7193694787E5EACAB002FCEE44DF8DB (void);
// 0x000000BC System.Collections.IEnumerator GreenAiHelicopterScript::wait()
extern void GreenAiHelicopterScript_wait_m0AF37D5CFAE7BB8285C0EEFB023914D5828D75A3 (void);
// 0x000000BD System.Void GreenAiHelicopterScript::Start()
extern void GreenAiHelicopterScript_Start_m19507899B0773756B3033675FFCF019A23BB09EE (void);
// 0x000000BE System.Void GreenAiHelicopterScript::Update()
extern void GreenAiHelicopterScript_Update_m4DE2B35F4912D43E16482962B3B384F98A80A52A (void);
// 0x000000BF System.Void GreenAiHelicopterScript::BoostHelicopter(System.Int32)
extern void GreenAiHelicopterScript_BoostHelicopter_m7190157C45FE8FFDAF910C043E47C743A8F450A5 (void);
// 0x000000C0 System.Collections.IEnumerator GreenAiHelicopterScript::BoostHelicopterNow(System.Int32)
extern void GreenAiHelicopterScript_BoostHelicopterNow_m2C697483CBC2445C941B05ED38FA730989961DDB (void);
// 0x000000C1 System.Void GreenAiHelicopterScript::OnCollisionEnter(UnityEngine.Collision)
extern void GreenAiHelicopterScript_OnCollisionEnter_m2E2D40F6E8134A2BEA4908B49FD71D0EC0B21B27 (void);
// 0x000000C2 System.Void GreenAiHelicopterScript::.ctor()
extern void GreenAiHelicopterScript__ctor_m3F4F446FF2CA37F26367B5D8CF351249A388F9D8 (void);
// 0x000000C3 System.Void GreenAiHelicopterScript/<wait>d__8::.ctor(System.Int32)
extern void U3CwaitU3Ed__8__ctor_mDCC058A30FD6F11314464F59B1D944452DEDE31A (void);
// 0x000000C4 System.Void GreenAiHelicopterScript/<wait>d__8::System.IDisposable.Dispose()
extern void U3CwaitU3Ed__8_System_IDisposable_Dispose_m32B292493C2C2097837B382D6F3BBD3FE5936571 (void);
// 0x000000C5 System.Boolean GreenAiHelicopterScript/<wait>d__8::MoveNext()
extern void U3CwaitU3Ed__8_MoveNext_m50A36B8C5032A4F78F832B45919DE01F5639677A (void);
// 0x000000C6 System.Object GreenAiHelicopterScript/<wait>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CwaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD11542748D2157BF3ECB8D0449FB8EBBF824C2C6 (void);
// 0x000000C7 System.Void GreenAiHelicopterScript/<wait>d__8::System.Collections.IEnumerator.Reset()
extern void U3CwaitU3Ed__8_System_Collections_IEnumerator_Reset_mD4370B65472947DCCF75489E73F2FCDA3C060708 (void);
// 0x000000C8 System.Object GreenAiHelicopterScript/<wait>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CwaitU3Ed__8_System_Collections_IEnumerator_get_Current_mCDA0D7011D71B6E161157D399A915B705D0F4356 (void);
// 0x000000C9 System.Void GreenAiHelicopterScript/<BoostHelicopterNow>d__12::.ctor(System.Int32)
extern void U3CBoostHelicopterNowU3Ed__12__ctor_mBBE717D794FC2AA7DD4F448CA792743B1729B381 (void);
// 0x000000CA System.Void GreenAiHelicopterScript/<BoostHelicopterNow>d__12::System.IDisposable.Dispose()
extern void U3CBoostHelicopterNowU3Ed__12_System_IDisposable_Dispose_m86EE5D0048CCD362408A8A627CA7C249E7B22704 (void);
// 0x000000CB System.Boolean GreenAiHelicopterScript/<BoostHelicopterNow>d__12::MoveNext()
extern void U3CBoostHelicopterNowU3Ed__12_MoveNext_mC577FB3003CB6AA6B9CBEF0BF4FC23489FD6CBF2 (void);
// 0x000000CC System.Object GreenAiHelicopterScript/<BoostHelicopterNow>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBoostHelicopterNowU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m757362FC7C672367D082B76F985400626F3704D5 (void);
// 0x000000CD System.Void GreenAiHelicopterScript/<BoostHelicopterNow>d__12::System.Collections.IEnumerator.Reset()
extern void U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_Reset_m80DE021A4A053E4CA424F5CE4BBD788881F70BD5 (void);
// 0x000000CE System.Object GreenAiHelicopterScript/<BoostHelicopterNow>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_get_Current_mACFAF342ACBF07EC3B30B9176B0D4B5374E94A2E (void);
// 0x000000CF GreenAiJeepScript GreenAiJeepScript::get_Instance()
extern void GreenAiJeepScript_get_Instance_m6335CB0256C9EF6F4A3822B35454EA5237F6BDA1 (void);
// 0x000000D0 System.Void GreenAiJeepScript::Start()
extern void GreenAiJeepScript_Start_m814472BE906B453B4F3AF65CF113C00730FB2197 (void);
// 0x000000D1 System.Void GreenAiJeepScript::Update()
extern void GreenAiJeepScript_Update_m4EF21C45A2FB195A8682287248A60BD3D6485760 (void);
// 0x000000D2 System.Void GreenAiJeepScript::OnCollisionEnter(UnityEngine.Collision)
extern void GreenAiJeepScript_OnCollisionEnter_m3382E5A97CD7CEDA7261B57C9FE8F11BF1EEAB3A (void);
// 0x000000D3 System.Void GreenAiJeepScript::.ctor()
extern void GreenAiJeepScript__ctor_m3B064C69EAF3B99844387B4010828515EB152263 (void);
// 0x000000D4 GreenAiTankScript GreenAiTankScript::get_Instance()
extern void GreenAiTankScript_get_Instance_m7B9C328C5C4AD4EABBCA8F957D8C4DA8E09EAB82 (void);
// 0x000000D5 System.Void GreenAiTankScript::Start()
extern void GreenAiTankScript_Start_m11B28CA7637646A3033C192B41EAA9E948122D14 (void);
// 0x000000D6 System.Void GreenAiTankScript::Update()
extern void GreenAiTankScript_Update_mA1BAA5453D2F3A9D6B2BE4BF07E1A72F1E088989 (void);
// 0x000000D7 System.Void GreenAiTankScript::OnCollisionEnter(UnityEngine.Collision)
extern void GreenAiTankScript_OnCollisionEnter_m1A7997C10849AF4EFD1D31D3E57A0BE960DABA36 (void);
// 0x000000D8 System.Void GreenAiTankScript::.ctor()
extern void GreenAiTankScript__ctor_mFE0469B4CFD3A1302CF9231B27DF2567A85175B2 (void);
// 0x000000D9 AdsScript AdsScript::get_Instance()
extern void AdsScript_get_Instance_m4FB4102F5A4BC7E3EE64CBCFC886063457DCACA2 (void);
// 0x000000DA System.Void AdsScript::Start()
extern void AdsScript_Start_mC84EBE63B5A0B689E3F68E85E3394C679EC8AFF0 (void);
// 0x000000DB System.Void AdsScript::ShowBanner()
extern void AdsScript_ShowBanner_m0761AD3515781D5E4F3BE15816265BAD74AC006A (void);
// 0x000000DC System.Void AdsScript::ShowInterstitial()
extern void AdsScript_ShowInterstitial_m63112541FB3ED033CDD9F9E61FDC9923E0CD7DC4 (void);
// 0x000000DD System.Void AdsScript::ShowRewarded(System.String)
extern void AdsScript_ShowRewarded_mB5BE1E972A04023FE55C862CA7AFD9A4B8D205A6 (void);
// 0x000000DE System.Void AdsScript::FreeCurrencyFunction()
extern void AdsScript_FreeCurrencyFunction_mFDC5140D5495D039D8597741F7167FB51C9DC7AC (void);
// 0x000000DF System.Void AdsScript::NextScene()
extern void AdsScript_NextScene_mC67E5AF3B0A613B83B6DE848F26B1F7863584519 (void);
// 0x000000E0 System.Void AdsScript::.ctor()
extern void AdsScript__ctor_m8474CB9DAFB51EC3CD40E0AE475BF645A0E9CE34 (void);
// 0x000000E1 System.Void CameraFollowScript::Awake()
extern void CameraFollowScript_Awake_mAF5739B6CD862895C63CE9C39711463B9FF2D3E6 (void);
// 0x000000E2 System.Void CameraFollowScript::Update()
extern void CameraFollowScript_Update_mF192A26F7F4DF05A87DF95EDBDBFAFF059A440D1 (void);
// 0x000000E3 System.Void CameraFollowScript::.ctor()
extern void CameraFollowScript__ctor_mBD431AD8E83D69A7D39DA198B4BAEB4B74509E6E (void);
// 0x000000E4 System.Void ClimbingScript::OnTriggerEnter(UnityEngine.Collider)
extern void ClimbingScript_OnTriggerEnter_m6E04BBE1C3179B6C0DAC79DD6650CAC68CAFA6F8 (void);
// 0x000000E5 System.Void ClimbingScript::.ctor()
extern void ClimbingScript__ctor_m20C9500ABB32AFE223D713EF892AA9DD396C9C02 (void);
// 0x000000E6 System.Int32 CoinAnimationScript::get_Coins()
extern void CoinAnimationScript_get_Coins_m5D9C9D7652222B61ED25424E7932EDECAF235F9F (void);
// 0x000000E7 System.Void CoinAnimationScript::set_Coins(System.Int32)
extern void CoinAnimationScript_set_Coins_m410C3428A0FB8421C96BEEE4B2E339A2ECCE019F (void);
// 0x000000E8 System.Void CoinAnimationScript::Awake()
extern void CoinAnimationScript_Awake_m7919538DA9664FCEB56C0582A80B8B3649DFBE93 (void);
// 0x000000E9 System.Void CoinAnimationScript::PrepareCoins()
extern void CoinAnimationScript_PrepareCoins_m5752CA3010EFEDE2EB829FAC5E11572C2465C326 (void);
// 0x000000EA System.Void CoinAnimationScript::Animate(UnityEngine.Vector3,System.Int32)
extern void CoinAnimationScript_Animate_mBF079E67A8A691059AFAA70035CA4D72C5026B29 (void);
// 0x000000EB System.Void CoinAnimationScript::AddCoins()
extern void CoinAnimationScript_AddCoins_m7608052138776E46EEF6E0C747335C8BDDEF0B2E (void);
// 0x000000EC System.Void CoinAnimationScript::Animatee(UnityEngine.Vector3,System.Int32)
extern void CoinAnimationScript_Animatee_m6AD35D90C057DC2896113DFE843E49A9BDC67205 (void);
// 0x000000ED System.Void CoinAnimationScript::AddCoinss()
extern void CoinAnimationScript_AddCoinss_m76D0ADFE473F02A284ABDF4F692A35BB41606E69 (void);
// 0x000000EE System.Void CoinAnimationScript::.ctor()
extern void CoinAnimationScript__ctor_m0DE121A43212AFC42617665FB7601BB2EF586F06 (void);
// 0x000000EF System.Void CoinAnimationScript/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_mF220195862ED81A39CF3C19315F7C2A8752506A0 (void);
// 0x000000F0 System.Void CoinAnimationScript/<>c__DisplayClass17_0::<Animate>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CAnimateU3Eb__0_mCF707375C2A1E6B10EC0E885D9403D3E160798C9 (void);
// 0x000000F1 System.Void CoinAnimationScript/<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_m28B1E092BF5DB9349ECA4D3B7CE6A82AFDD22424 (void);
// 0x000000F2 System.Void CoinAnimationScript/<>c__DisplayClass19_0::<Animatee>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CAnimateeU3Eb__0_m157562F0A58060E353D735C0190E8C9879E086E5 (void);
// 0x000000F3 System.Void CrateScript::Awake()
extern void CrateScript_Awake_m83765479471475B349FBD3B2CA11780B2D62F16F (void);
// 0x000000F4 System.Void CrateScript::.ctor()
extern void CrateScript__ctor_mFC2FB8CA3607ECC9573BE49B53B2200BCD561EE3 (void);
// 0x000000F5 System.Void FinishLineScript::OnCollisionEnter(UnityEngine.Collision)
extern void FinishLineScript_OnCollisionEnter_mCF1015F532A4C3CDBEDEE8AE138D70BF13FE425E (void);
// 0x000000F6 System.Void FinishLineScript::OnTriggerEnter(UnityEngine.Collider)
extern void FinishLineScript_OnTriggerEnter_m3099C24D64C518B5536B56A567D2CA2922B9AF85 (void);
// 0x000000F7 System.Void FinishLineScript::.ctor()
extern void FinishLineScript__ctor_m24BBEB8E64FECD60502106A2DC03BE5B3BC95248 (void);
// 0x000000F8 GameManager GameManager::get_Instance()
extern void GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232 (void);
// 0x000000F9 System.Void GameManager::Awake()
extern void GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30 (void);
// 0x000000FA System.Void GameManager::Start()
extern void GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E (void);
// 0x000000FB System.Void GameManager::StartGame()
extern void GameManager_StartGame_m6022C5CDD590728691B22E9B87185BFE3D6A8EC1 (void);
// 0x000000FC System.Void GameManager::InitializeShapeTypes()
extern void GameManager_InitializeShapeTypes_m2D21708D347EFCAA20EED03A1584D6F3D1617EBE (void);
// 0x000000FD System.Void GameManager::InitializeLevel()
extern void GameManager_InitializeLevel_mFFF07BEA50269AA6085AB7627C09A34E5B3D00E5 (void);
// 0x000000FE System.Void GameManager::InstantiateCharacters()
extern void GameManager_InstantiateCharacters_mFEDA4146335BAB91165D7E4F32EDFBE243514F8F (void);
// 0x000000FF System.Void GameManager::ToggleVibration(System.Boolean)
extern void GameManager_ToggleVibration_m391A0DEF2BA4D01EFBBE688A892AF3EFBDCD7E7E (void);
// 0x00000100 System.Void GameManager::LevelCompleted()
extern void GameManager_LevelCompleted_m5EAEF7C26C85CC857CE878FAD5814F0ADE0D47BE (void);
// 0x00000101 System.Void GameManager::LevelFailed()
extern void GameManager_LevelFailed_m7496C08897FDFDF0BD8B224C91FB1583FCFEBB18 (void);
// 0x00000102 System.Collections.IEnumerator GameManager::IsLevelFailed()
extern void GameManager_IsLevelFailed_m70DE3501FE1BEEC60023C7A9F649FD8640D97E67 (void);
// 0x00000103 System.Void GameManager::RestartLevel()
extern void GameManager_RestartLevel_mF2D674BF811DF9B8C3893C1E061BED7E2ACB6D76 (void);
// 0x00000104 System.Collections.IEnumerator GameManager::RestartLevell()
extern void GameManager_RestartLevell_m5AB99A05E7E5BDEBB8CA7F99D6CA514F1C3A5FD6 (void);
// 0x00000105 System.Void GameManager::DelayInAnimation1()
extern void GameManager_DelayInAnimation1_mD1E86C35E0F2415F93A2FE732B5262F0072CA6D4 (void);
// 0x00000106 System.Collections.IEnumerator GameManager::isGameCompleted()
extern void GameManager_isGameCompleted_m5C2694B3FDB66DED81583ECC138D4BF54463CFFD (void);
// 0x00000107 System.Void GameManager::LoadNextLevel()
extern void GameManager_LoadNextLevel_m2DEFC4D28BC1C25B56CBCA894590882BBAC2F7E3 (void);
// 0x00000108 System.Collections.IEnumerator GameManager::StartNextLevel()
extern void GameManager_StartNextLevel_mD9B579F6AFA8F8346C0A922BDBC727F750C52E10 (void);
// 0x00000109 System.Void GameManager::DelayInAnimation()
extern void GameManager_DelayInAnimation_m49310C2247930BB1BDA080E6CC44AFE4F56B6FD9 (void);
// 0x0000010A System.Void GameManager::WatchFor2X()
extern void GameManager_WatchFor2X_mE4756F64167C7D648B1083F384AE32B739539E03 (void);
// 0x0000010B System.Void GameManager::WatchFor2X2()
extern void GameManager_WatchFor2X2_m914BBF74A2AF546A27D56C2EC50D3E1DF3B58D97 (void);
// 0x0000010C System.Void GameManager::UpgradeShape(System.Int32)
extern void GameManager_UpgradeShape_mF068D25B28015654DF48D7FC07F9DF15224E9971 (void);
// 0x0000010D System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x0000010E System.Void GameManager/<IsLevelFailed>d__21::.ctor(System.Int32)
extern void U3CIsLevelFailedU3Ed__21__ctor_mD10B2FD438BC1BEE23004BD942AB31491C0A124E (void);
// 0x0000010F System.Void GameManager/<IsLevelFailed>d__21::System.IDisposable.Dispose()
extern void U3CIsLevelFailedU3Ed__21_System_IDisposable_Dispose_m076581E89A05A366FBAF808DE98241BDA23E05EB (void);
// 0x00000110 System.Boolean GameManager/<IsLevelFailed>d__21::MoveNext()
extern void U3CIsLevelFailedU3Ed__21_MoveNext_m67CBBE1D176FC2C5DD1E85C46DE7D5D98725C763 (void);
// 0x00000111 System.Object GameManager/<IsLevelFailed>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIsLevelFailedU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC589789B869BCDF5F0F5F602E0404335DBE4644E (void);
// 0x00000112 System.Void GameManager/<IsLevelFailed>d__21::System.Collections.IEnumerator.Reset()
extern void U3CIsLevelFailedU3Ed__21_System_Collections_IEnumerator_Reset_m8A3ADC2BABE6D0B8A7B65EEE2CEBEF36F600C629 (void);
// 0x00000113 System.Object GameManager/<IsLevelFailed>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CIsLevelFailedU3Ed__21_System_Collections_IEnumerator_get_Current_m5A479CAD77A871F46E639F24FA9DC80CED301DE6 (void);
// 0x00000114 System.Void GameManager/<RestartLevell>d__23::.ctor(System.Int32)
extern void U3CRestartLevellU3Ed__23__ctor_mD698104A3344AAD8AB7F375C1FCDFE48A90FEF8A (void);
// 0x00000115 System.Void GameManager/<RestartLevell>d__23::System.IDisposable.Dispose()
extern void U3CRestartLevellU3Ed__23_System_IDisposable_Dispose_m917325C36BA67A294D14EC471D182B556BCE62D7 (void);
// 0x00000116 System.Boolean GameManager/<RestartLevell>d__23::MoveNext()
extern void U3CRestartLevellU3Ed__23_MoveNext_m4A5E1B55D23C57FDDF66C8E117FD7AF3C098D283 (void);
// 0x00000117 System.Object GameManager/<RestartLevell>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRestartLevellU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE78F622C02552C6AFC3AC0A35FA99A639B581A06 (void);
// 0x00000118 System.Void GameManager/<RestartLevell>d__23::System.Collections.IEnumerator.Reset()
extern void U3CRestartLevellU3Ed__23_System_Collections_IEnumerator_Reset_m631238FEBA0FDA032E30FF7EEA31F50A1E220ADA (void);
// 0x00000119 System.Object GameManager/<RestartLevell>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CRestartLevellU3Ed__23_System_Collections_IEnumerator_get_Current_m509B652FC4478E31C51E1EADF6D16C8D8D1F3E68 (void);
// 0x0000011A System.Void GameManager/<isGameCompleted>d__25::.ctor(System.Int32)
extern void U3CisGameCompletedU3Ed__25__ctor_m2957E84768DAED7DE588D515A6A1CE2B39C026C7 (void);
// 0x0000011B System.Void GameManager/<isGameCompleted>d__25::System.IDisposable.Dispose()
extern void U3CisGameCompletedU3Ed__25_System_IDisposable_Dispose_m3ADF2A8F3FA8F0BE8595C6CA0625BC752B5B852E (void);
// 0x0000011C System.Boolean GameManager/<isGameCompleted>d__25::MoveNext()
extern void U3CisGameCompletedU3Ed__25_MoveNext_m83EEF57C7729AA525C1B7A9FEC106A54A5100071 (void);
// 0x0000011D System.Object GameManager/<isGameCompleted>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CisGameCompletedU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m42B4BF6B0DC26D917B0E47D89DCF7AD201804E77 (void);
// 0x0000011E System.Void GameManager/<isGameCompleted>d__25::System.Collections.IEnumerator.Reset()
extern void U3CisGameCompletedU3Ed__25_System_Collections_IEnumerator_Reset_m35439684D43C4B3E3217C106A4C1939FF2C02A97 (void);
// 0x0000011F System.Object GameManager/<isGameCompleted>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CisGameCompletedU3Ed__25_System_Collections_IEnumerator_get_Current_mA00BAAD4560CDC6CF6CC18BC9435689084F4D065 (void);
// 0x00000120 System.Void GameManager/<StartNextLevel>d__27::.ctor(System.Int32)
extern void U3CStartNextLevelU3Ed__27__ctor_m84E1B3F4F8E539CC4A3741E7A8B6958EB8D0AEBC (void);
// 0x00000121 System.Void GameManager/<StartNextLevel>d__27::System.IDisposable.Dispose()
extern void U3CStartNextLevelU3Ed__27_System_IDisposable_Dispose_m475BB8B13202250A345D0FADAD797D36B9E403EF (void);
// 0x00000122 System.Boolean GameManager/<StartNextLevel>d__27::MoveNext()
extern void U3CStartNextLevelU3Ed__27_MoveNext_mD5AB4DE7B97D6B33185363AA4E97B06CC5AD5B21 (void);
// 0x00000123 System.Object GameManager/<StartNextLevel>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartNextLevelU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEFE495A03A02293456740B5F11C350835D1396FF (void);
// 0x00000124 System.Void GameManager/<StartNextLevel>d__27::System.Collections.IEnumerator.Reset()
extern void U3CStartNextLevelU3Ed__27_System_Collections_IEnumerator_Reset_m720323159B46EA9B529D6511653DB54F79272B18 (void);
// 0x00000125 System.Object GameManager/<StartNextLevel>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CStartNextLevelU3Ed__27_System_Collections_IEnumerator_get_Current_m5B09F71419B61AF21B156A7DF3D43E1995EBE375 (void);
// 0x00000126 System.Void GliderScript::OnTriggerEnter(UnityEngine.Collider)
extern void GliderScript_OnTriggerEnter_m749EBE7B9A1A696FD06C21AFD9BA7354155E8F78 (void);
// 0x00000127 System.Void GliderScript::.ctor()
extern void GliderScript__ctor_m54A840F90C5A28D26B895FFC315D18F1E897CA61 (void);
// 0x00000128 GoogleAdsScript GoogleAdsScript::get_Instance()
extern void GoogleAdsScript_get_Instance_m38225F756D6B5AEF51C172562F4D5769C7CB2714 (void);
// 0x00000129 System.Void GoogleAdsScript::Start()
extern void GoogleAdsScript_Start_mFC852DE5ABD512F608F433DD8194B9073EB68306 (void);
// 0x0000012A System.Void GoogleAdsScript::RequestBanner()
extern void GoogleAdsScript_RequestBanner_m05A3A6A96CFA1AA6E2A169B022CA6F848E40D73A (void);
// 0x0000012B System.Void GoogleAdsScript::RequestBannerForScript(System.Int32)
extern void GoogleAdsScript_RequestBannerForScript_m8F366A0098E5442AE8EDDBDDD8CA8E3E4CC8AFA4 (void);
// 0x0000012C GoogleMobileAds.Api.AdRequest GoogleAdsScript::GenerateBannerRequest()
extern void GoogleAdsScript_GenerateBannerRequest_m4FD6CC8181CD5E1E355B1B0DE0A1F065722454EB (void);
// 0x0000012D System.Void GoogleAdsScript::ShowBanner()
extern void GoogleAdsScript_ShowBanner_m568D43860F0C771CCCEC3E9AA89F125B753A3A4D (void);
// 0x0000012E System.Void GoogleAdsScript::HideBanner()
extern void GoogleAdsScript_HideBanner_m5475ED3CA8E66176A10D5FBE3FFFCB9EFC4D5D85 (void);
// 0x0000012F System.Void GoogleAdsScript::DestroyBanner()
extern void GoogleAdsScript_DestroyBanner_mA335A3F6F4F7306E736522A1952ADE313E18E3BF (void);
// 0x00000130 System.Void GoogleAdsScript::RequestInterstitialAd()
extern void GoogleAdsScript_RequestInterstitialAd_m38B6EAF1358E475E2FE3AB6210C85959772B6612 (void);
// 0x00000131 System.Void GoogleAdsScript::RequestRewardedAd()
extern void GoogleAdsScript_RequestRewardedAd_mA2464C990359DF43266ED683108DCB750DF07C7D (void);
// 0x00000132 System.Void GoogleAdsScript::GenerateInterstitalRequest()
extern void GoogleAdsScript_GenerateInterstitalRequest_m49B12C39489E5542FF65509E0B6083CC8A7E9167 (void);
// 0x00000133 System.Boolean GoogleAdsScript::RequestInterstitialForScript()
extern void GoogleAdsScript_RequestInterstitialForScript_m4DD273F4D597D1530E340F9D706D4CD0B10A07B0 (void);
// 0x00000134 System.Void GoogleAdsScript::GenerateRewardedRequest()
extern void GoogleAdsScript_GenerateRewardedRequest_m77FAA7B0558C72A6D2576B901B85FAD05814166E (void);
// 0x00000135 System.Boolean GoogleAdsScript::RequestRewardedForScript()
extern void GoogleAdsScript_RequestRewardedForScript_m27B2345502109F1C6952BAA6844A2FD68F3B6485 (void);
// 0x00000136 System.Void GoogleAdsScript::HandleOnAdLoaded(System.Object,System.EventArgs)
extern void GoogleAdsScript_HandleOnAdLoaded_mF021FC6DF1B92C80952981DDE32BCDA2C0A84E87 (void);
// 0x00000137 System.Void GoogleAdsScript::HandleOnAdFailedToLoad(System.Object,GoogleMobileAds.Api.AdFailedToLoadEventArgs)
extern void GoogleAdsScript_HandleOnAdFailedToLoad_m7B62B61CF4C814C4623001C2B6870FAE837B88C1 (void);
// 0x00000138 System.Void GoogleAdsScript::HandleOnAdOpened(System.Object,System.EventArgs)
extern void GoogleAdsScript_HandleOnAdOpened_m61DE882E4DF5FDF69953D08E191EB6EDA3AF658E (void);
// 0x00000139 System.Void GoogleAdsScript::HandleOnAdClosed(System.Object,System.EventArgs)
extern void GoogleAdsScript_HandleOnAdClosed_mAF7C2B2F70C98417C9E5B77FFEA482727A109DA8 (void);
// 0x0000013A System.Void GoogleAdsScript::HandleOnAdLeavingApplication(System.Object,System.EventArgs)
extern void GoogleAdsScript_HandleOnAdLeavingApplication_mC438D1886BBAE26BCD72D408335FC8FA72769D97 (void);
// 0x0000013B System.Void GoogleAdsScript::HandleOnInterstitialLoaded(System.Object,System.EventArgs)
extern void GoogleAdsScript_HandleOnInterstitialLoaded_mD5199FB8FD87FED59C29E7E8FA892B6DE3105C64 (void);
// 0x0000013C System.Void GoogleAdsScript::HandleOnInterstitialFailedToLoad(System.Object,GoogleMobileAds.Api.AdFailedToLoadEventArgs)
extern void GoogleAdsScript_HandleOnInterstitialFailedToLoad_m5F8FAA2C38EE99E056E6EF9F0843A08537AEF838 (void);
// 0x0000013D System.Void GoogleAdsScript::HandleOnInterstitialOpened(System.Object,System.EventArgs)
extern void GoogleAdsScript_HandleOnInterstitialOpened_m6779CA7EC01DDC3408BDC7A1F08D70658998E70A (void);
// 0x0000013E System.Void GoogleAdsScript::HandleOnInterstitialClosed(System.Object,System.EventArgs)
extern void GoogleAdsScript_HandleOnInterstitialClosed_mBB0550CD0BF03A755488F9526143A467C7473FC6 (void);
// 0x0000013F System.Void GoogleAdsScript::HandleOnInterstitialLeavingApplication(System.Object,System.EventArgs)
extern void GoogleAdsScript_HandleOnInterstitialLeavingApplication_mFB1294456AB2C79187FA04D25C916709B917D07D (void);
// 0x00000140 System.Void GoogleAdsScript::HandleRewardedAdLoaded(System.Object,System.EventArgs)
extern void GoogleAdsScript_HandleRewardedAdLoaded_mCD5876255FECF0FE0DEBC9932679D5E513102FB6 (void);
// 0x00000141 System.Void GoogleAdsScript::HandleRewardedAdFailedToLoad(System.Object,GoogleMobileAds.Api.AdErrorEventArgs)
extern void GoogleAdsScript_HandleRewardedAdFailedToLoad_m21B78136B6D57E96BC4517DB018ABF1EE00855C6 (void);
// 0x00000142 System.Void GoogleAdsScript::HandleRewardedAdOpening(System.Object,System.EventArgs)
extern void GoogleAdsScript_HandleRewardedAdOpening_m1C3D5EC140BB3705B5C5BD2AA13401F440C0D2EA (void);
// 0x00000143 System.Void GoogleAdsScript::HandleRewardedAdFailedToShow(System.Object,GoogleMobileAds.Api.AdErrorEventArgs)
extern void GoogleAdsScript_HandleRewardedAdFailedToShow_m90A23CCFBBD52283DC30676292CE26F210C3F7CC (void);
// 0x00000144 System.Void GoogleAdsScript::HandleRewardedAdClosed(System.Object,System.EventArgs)
extern void GoogleAdsScript_HandleRewardedAdClosed_m0E089886060F56B705FEC3CCA823EE7456DE2D83 (void);
// 0x00000145 System.Void GoogleAdsScript::HandleUserEarnedReward(System.Object,GoogleMobileAds.Api.Reward)
extern void GoogleAdsScript_HandleUserEarnedReward_mD7B47DE1AE53065157875CEF34EF308A9F34B961 (void);
// 0x00000146 System.Void GoogleAdsScript::.ctor()
extern void GoogleAdsScript__ctor_mBBDA372736895DEBC58C4C70DD13270AF7340726 (void);
// 0x00000147 System.Void GoogleAdsScript/<>c::.cctor()
extern void U3CU3Ec__cctor_mE534DE5FE344001FFDEAE48CD46F97BEC69DA61A (void);
// 0x00000148 System.Void GoogleAdsScript/<>c::.ctor()
extern void U3CU3Ec__ctor_m582C8D421E0FED66ECA4716038D9B6F7642F7B65 (void);
// 0x00000149 System.Void GoogleAdsScript/<>c::<Start>b__9_0(GoogleMobileAds.Api.InitializationStatus)
extern void U3CU3Ec_U3CStartU3Eb__9_0_m59F252F459F08B008AC2BB7C808C6E4A7131AE36 (void);
// 0x0000014A System.Void HelicopterBoostScript::OnTriggerEnter(UnityEngine.Collider)
extern void HelicopterBoostScript_OnTriggerEnter_mC579BBD05ED6BC9DBAC6AC761FEAD68FC95DA26D (void);
// 0x0000014B System.Void HelicopterBoostScript::.ctor()
extern void HelicopterBoostScript__ctor_mE08F4190B9CBD0926150A056149E96674AB5B66C (void);
// 0x0000014C System.Void IAPScript::OnPurchaseComplete(UnityEngine.Purchasing.Product)
extern void IAPScript_OnPurchaseComplete_m1BEE2C1C767AC6FE849ABF2FF08A33017D96507B (void);
// 0x0000014D System.Void IAPScript::OnpurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern void IAPScript_OnpurchaseFailed_m4D5C5BDC47BE4A75268DE05C706449CCB37AA2B9 (void);
// 0x0000014E System.Void IAPScript::.ctor()
extern void IAPScript__ctor_m84CE2C939D0F82F5BEED7D8F630697A40D4BF3C9 (void);
// 0x0000014F System.Void IAPScriptold::Start()
extern void IAPScriptold_Start_mF868E03F2894490F531ED884E06228D55D880631 (void);
// 0x00000150 System.Void IAPScriptold::InitializePurchasing()
extern void IAPScriptold_InitializePurchasing_m93B12994255E72ACC3D4ADD322938D69F1C8FE4D (void);
// 0x00000151 System.Boolean IAPScriptold::IsInitialized()
extern void IAPScriptold_IsInitialized_mA0F0B7FB493885642E6EF4CDB43A01B678109630 (void);
// 0x00000152 System.Void IAPScriptold::BuyConsumable()
extern void IAPScriptold_BuyConsumable_mBEAEE13F23E6DC0A31F1950FF3EB0044BD577200 (void);
// 0x00000153 System.Void IAPScriptold::BuyNonConsumable()
extern void IAPScriptold_BuyNonConsumable_m6FD3DD4475950DF96B9ECBC9C25AFAB1100690A9 (void);
// 0x00000154 System.Void IAPScriptold::BuySubscription()
extern void IAPScriptold_BuySubscription_mB8D27214D977075E1FC0A5CDED2D2CAF9A3F7762 (void);
// 0x00000155 System.Void IAPScriptold::BuyProductID(System.String)
extern void IAPScriptold_BuyProductID_mA9FB82539BAACB7B911344933DEA2F03EF5EC095 (void);
// 0x00000156 System.Void IAPScriptold::RestorePurchases()
extern void IAPScriptold_RestorePurchases_m53059CC2F98C7A87AB2FF766345E8C4006ED1425 (void);
// 0x00000157 System.Void IAPScriptold::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern void IAPScriptold_OnInitialized_m756C5DD7545EEA9D9337CA5599730AD3AD9382A0 (void);
// 0x00000158 System.Void IAPScriptold::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern void IAPScriptold_OnInitializeFailed_m6FB6FD22E0D121AAC70DC353369910B6638EBA1F (void);
// 0x00000159 UnityEngine.Purchasing.PurchaseProcessingResult IAPScriptold::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern void IAPScriptold_ProcessPurchase_m66131992633A40D05A69DE7F28DFCA50D86E2195 (void);
// 0x0000015A System.Void IAPScriptold::OnPurchaseComplete(UnityEngine.Purchasing.Product)
extern void IAPScriptold_OnPurchaseComplete_mEB06379B2D48B0BA574E49134F791E32523C2D34 (void);
// 0x0000015B System.Void IAPScriptold::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern void IAPScriptold_OnPurchaseFailed_m423E4E67D947C7CD744C539F05CC7FAEF4285A20 (void);
// 0x0000015C System.Void IAPScriptold::.ctor()
extern void IAPScriptold__ctor_m97245D995471F7CE026C0B00C715403675912797 (void);
// 0x0000015D System.Void IAPScriptold::.cctor()
extern void IAPScriptold__cctor_m5818E0E8176A61EA2C30BDB87BC91E2A462EFE2E (void);
// 0x0000015E System.Void IAPScriptold/<>c::.cctor()
extern void U3CU3Ec__cctor_m7BF686D4B4324D9E7613F4D2E2F8E508C2C72B56 (void);
// 0x0000015F System.Void IAPScriptold/<>c::.ctor()
extern void U3CU3Ec__ctor_m77B858C4A94863D279C1102185D94C8E3FB762C8 (void);
// 0x00000160 System.Void IAPScriptold/<>c::<RestorePurchases>b__15_0(System.Boolean)
extern void U3CU3Ec_U3CRestorePurchasesU3Eb__15_0_m23C59D29921F08DC2215FBBEF3FF1E62156FBD1B (void);
// 0x00000161 System.Void ObjectStopperScript::Update()
extern void ObjectStopperScript_Update_m951A4EA57A3AD465E356B1414753C65E579ADA6D (void);
// 0x00000162 System.Void ObjectStopperScript::.ctor()
extern void ObjectStopperScript__ctor_m60B54B51B815461E496E2CCDF409C8ADCC666E29 (void);
// 0x00000163 System.Void ProgressBarScript::Awake()
extern void ProgressBarScript_Awake_m97295865A342E8E18E37C6737933C30D34F9728D (void);
// 0x00000164 System.Void ProgressBarScript::FixedUpdate()
extern void ProgressBarScript_FixedUpdate_mD1ADB68BE4AB6845059FE0E3833C9EBC2B635F3D (void);
// 0x00000165 System.Void ProgressBarScript::CalculateDistance()
extern void ProgressBarScript_CalculateDistance_m3A36CA5BA68989B1C139DA14322E3C81092259FA (void);
// 0x00000166 System.Single ProgressBarScript::ReturnDistance(System.String)
extern void ProgressBarScript_ReturnDistance_m6CB221C387D2E22B6106871C4C3653DBBD237ADD (void);
// 0x00000167 System.Void ProgressBarScript::.ctor()
extern void ProgressBarScript__ctor_m88FA30239255C973E8265D3DC01B95AAB223778D (void);
// 0x00000168 SoundScript SoundScript::get_Instance()
extern void SoundScript_get_Instance_m588A31703AF32878E698169D9D192205BB014C0C (void);
// 0x00000169 System.Void SoundScript::Awake()
extern void SoundScript_Awake_m24E6BC9EF3471D211479695810539A871DE914C0 (void);
// 0x0000016A System.Void SoundScript::PlaySound(System.Int32)
extern void SoundScript_PlaySound_m5CB008D0C12148E01A10B47C05226AEB731D3109 (void);
// 0x0000016B System.Void SoundScript::StopSound()
extern void SoundScript_StopSound_m249154A9771FCD4F97022632E4C6504C130E4EE1 (void);
// 0x0000016C System.Void SoundScript::PlaySound()
extern void SoundScript_PlaySound_m0230F62058D0CC829DB59E4B9351E32F9B3028A1 (void);
// 0x0000016D System.Void SoundScript::.ctor()
extern void SoundScript__ctor_m18EEFD45E8CB8DCEFAC2D8EDFD7FABFFD76492B0 (void);
// 0x0000016E System.Void SoundScript::.cctor()
extern void SoundScript__cctor_mE4B974D3325440FB35D0EDEE653EE762941B7D55 (void);
// 0x0000016F System.Void StopperScript::Update()
extern void StopperScript_Update_m039C7712DB09047FAA7E961CBDD09A33367A4931 (void);
// 0x00000170 System.Void StopperScript::.ctor()
extern void StopperScript__ctor_m47D23717864F42ABB229F0CF88FD82307CEEF6E0 (void);
// 0x00000171 System.Void TextScript::Start()
extern void TextScript_Start_m6B843464C4DDC4D0D3D94CF6F075786A006E112C (void);
// 0x00000172 System.Void TextScript::Update()
extern void TextScript_Update_mE04D85ECA4D06599BADEAA78B9ACDA6B28542FC6 (void);
// 0x00000173 System.Void TextScript::.ctor()
extern void TextScript__ctor_m2B311CB37F42D159140EEEBFE5313ECAAA926584 (void);
// 0x00000174 UIManager UIManager::get_Instance()
extern void UIManager_get_Instance_mF5BAFE20E0B76E9CDCA3DBBF53D5AC853CC5BA7A (void);
// 0x00000175 System.Int32 UIManager::get_totalGameCoins()
extern void UIManager_get_totalGameCoins_m422F9B1E2C19370BB7829FC6DD312A5454CADDA8 (void);
// 0x00000176 System.Void UIManager::set_totalGameCoins(System.Int32)
extern void UIManager_set_totalGameCoins_mE7652FAD4B69356DE6285DADEB5987EA6285D390 (void);
// 0x00000177 System.Int32 UIManager::get_totalGameFailedCoins()
extern void UIManager_get_totalGameFailedCoins_m3C54911483089212BCB097698541C324AC6D2712 (void);
// 0x00000178 System.Void UIManager::set_totalGameFailedCoins(System.Int32)
extern void UIManager_set_totalGameFailedCoins_mB81037D2904B9DE2D1229E40B4296ED0E8840B21 (void);
// 0x00000179 System.Void UIManager::UpdateUI()
extern void UIManager_UpdateUI_m9076046EDAF6AD406801B952F8BE9CC84D9CE60F (void);
// 0x0000017A System.Void UIManager::Awake()
extern void UIManager_Awake_mCED93604270B1E209B4E0D32F6A26DDC5AB06E30 (void);
// 0x0000017B System.Void UIManager::OnSliderValueChanged()
extern void UIManager_OnSliderValueChanged_m6C5F22B93530A25A22A878E9350730DFD55EE544 (void);
// 0x0000017C System.Void UIManager::Start()
extern void UIManager_Start_mAA4B371DC406146E84A9D8803B9C861939B2E04E (void);
// 0x0000017D System.Void UIManager::isGameStarted()
extern void UIManager_isGameStarted_mC09A9D63831304BFB0966ED69CF2F2DF5AFA5FCF (void);
// 0x0000017E System.Void UIManager::isGameCompleted()
extern void UIManager_isGameCompleted_m6F7F0CA31571B25BED14841BF832229689D28FF6 (void);
// 0x0000017F System.Void UIManager::LevelFailed()
extern void UIManager_LevelFailed_m3417032B40DD1ECCBB38819D4B1C251B93D0F783 (void);
// 0x00000180 System.Void UIManager::TogglePanel(UnityEngine.GameObject)
extern void UIManager_TogglePanel_m2EAA407393950CA7311F96A049131194D1A9A5D1 (void);
// 0x00000181 System.Void UIManager::VibrationFunction(System.Boolean)
extern void UIManager_VibrationFunction_m44562D63358D41A14071BDA32EA83CE958A19395 (void);
// 0x00000182 System.Void UIManager::isGameOver()
extern void UIManager_isGameOver_m4AF6162FFE9091767AEB06E4EAE418DC9A129379 (void);
// 0x00000183 System.Void UIManager::UpgradeFunction(System.Int32)
extern void UIManager_UpgradeFunction_m411524678FDBCC34E24D716975EFC9D4B40C797E (void);
// 0x00000184 System.Void UIManager::ShowCoins(System.Int32)
extern void UIManager_ShowCoins_m57EBFCBA6BA1B8F62B3C5A3CC0DDE442CE7E5D16 (void);
// 0x00000185 System.Void UIManager::ShowGameFailedCoins(System.Int32)
extern void UIManager_ShowGameFailedCoins_m4C612C9E57A8844091E8A7008FD046C7420B6584 (void);
// 0x00000186 System.Collections.IEnumerator UIManager::PlayToast()
extern void UIManager_PlayToast_m033AE9F7CFA3D901AF314A312D49B4E6085D3195 (void);
// 0x00000187 System.Void UIManager::DisplayButtonRenders()
extern void UIManager_DisplayButtonRenders_m50D0BE31C70162C34ADE6CB1F2876DA2C2DEED8D (void);
// 0x00000188 System.Void UIManager::DisplayUpgradeRenders()
extern void UIManager_DisplayUpgradeRenders_m557F16E524D14DBA429D4D7346189865A8BB4649 (void);
// 0x00000189 System.Void UIManager::SetState(System.Int32)
extern void UIManager_SetState_m49207865690C9DA622C33A2B6378DBFDEC85F550 (void);
// 0x0000018A System.Void UIManager::OpenLink(System.String)
extern void UIManager_OpenLink_mB5D81EAACE6DC29FCB12D1DA88F1AACEE397C798 (void);
// 0x0000018B System.Void UIManager::.ctor()
extern void UIManager__ctor_mDADE1D724D40AF63AE78D51FC1CF1FE4784B4D4B (void);
// 0x0000018C System.Void UIManager::<Awake>b__31_0()
extern void UIManager_U3CAwakeU3Eb__31_0_m3122A16C76C0271B4114543368B4AE16D96F87C5 (void);
// 0x0000018D System.Void UIManager::<Awake>b__31_1()
extern void UIManager_U3CAwakeU3Eb__31_1_mE40D9985291979A55391AD6F32851A8B125816EE (void);
// 0x0000018E System.Void UIManager::<Awake>b__31_2()
extern void UIManager_U3CAwakeU3Eb__31_2_m9D115D348778764D78362E208E83AE06A029371A (void);
// 0x0000018F System.Void UIManager/<PlayToast>d__44::.ctor(System.Int32)
extern void U3CPlayToastU3Ed__44__ctor_mEAE19C120DB097EDD95DBE6FDE063A848F41EBA6 (void);
// 0x00000190 System.Void UIManager/<PlayToast>d__44::System.IDisposable.Dispose()
extern void U3CPlayToastU3Ed__44_System_IDisposable_Dispose_mF18FC39DAF7831611A1FF47716A054C35DE81982 (void);
// 0x00000191 System.Boolean UIManager/<PlayToast>d__44::MoveNext()
extern void U3CPlayToastU3Ed__44_MoveNext_mC0FFC23DFB6D654D9DE52BDBE2CB3D2CB252879F (void);
// 0x00000192 System.Object UIManager/<PlayToast>d__44::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayToastU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE9752D8C7E3C66B17F947A16473FD29B271BA717 (void);
// 0x00000193 System.Void UIManager/<PlayToast>d__44::System.Collections.IEnumerator.Reset()
extern void U3CPlayToastU3Ed__44_System_Collections_IEnumerator_Reset_m2349BD58837265E99891C68919BDBA1C7B12C32C (void);
// 0x00000194 System.Object UIManager/<PlayToast>d__44::System.Collections.IEnumerator.get_Current()
extern void U3CPlayToastU3Ed__44_System_Collections_IEnumerator_get_Current_m147D97967E8C7C1764873D48F08478FE3C16E7E6 (void);
// 0x00000195 UnityAdsScript UnityAdsScript::get_Instance()
extern void UnityAdsScript_get_Instance_m9AA6639260CB393D3C0076E7FA8BAE2344FED37C (void);
// 0x00000196 System.Void UnityAdsScript::Start()
extern void UnityAdsScript_Start_mBD07AFBBFCBC065B7CF4E42BFFF448DF6C3B9E2E (void);
// 0x00000197 System.Void UnityAdsScript::RequestBanner()
extern void UnityAdsScript_RequestBanner_m6D3D46CA34A99016AC31E28BCF344CF82A509FB4 (void);
// 0x00000198 System.Void UnityAdsScript::ShowBanner()
extern void UnityAdsScript_ShowBanner_m1091C4281C642F79947002795B7DC90A2C49A2A2 (void);
// 0x00000199 System.Void UnityAdsScript::HideBanner()
extern void UnityAdsScript_HideBanner_m3191F73A8A170D41BF0C224F493469A9E05AF07F (void);
// 0x0000019A System.Void UnityAdsScript::RequestInterstitialAd()
extern void UnityAdsScript_RequestInterstitialAd_m77F6E6E6222F8898633C5CF5DD2395BEED94690D (void);
// 0x0000019B System.Void UnityAdsScript::RequestRewardedAd()
extern void UnityAdsScript_RequestRewardedAd_m90A831C43CB70369C2187EECF4F9FB7C07C494C9 (void);
// 0x0000019C System.Boolean UnityAdsScript::RequestBannerForScript(System.Int32)
extern void UnityAdsScript_RequestBannerForScript_m9AE49D08942D8C483AA4BE34272F2659B48EAEF0 (void);
// 0x0000019D System.Boolean UnityAdsScript::RequestInterstitialForScript()
extern void UnityAdsScript_RequestInterstitialForScript_m10984B7874A0D7E9070C0197FD373498F7046CEC (void);
// 0x0000019E System.Boolean UnityAdsScript::RequestRewardedForScript()
extern void UnityAdsScript_RequestRewardedForScript_m4FCE6F58125E22FF36C4E82E15E1FE3676627ABE (void);
// 0x0000019F System.Void UnityAdsScript::OnUnityAdsDidFinish(System.String,UnityEngine.Advertisements.ShowResult)
extern void UnityAdsScript_OnUnityAdsDidFinish_mFED4A64C2A7C989A344AA595526268C7D9BFF9EB (void);
// 0x000001A0 System.Void UnityAdsScript::OnAdsViewed(System.String)
extern void UnityAdsScript_OnAdsViewed_m9476226B60FD483AEED95265B3210FA24CF65678 (void);
// 0x000001A1 System.Void UnityAdsScript::OnUnityAdsReady(System.String)
extern void UnityAdsScript_OnUnityAdsReady_mA8B1B0229EE8486C309FEBD7C95D0837114E2B32 (void);
// 0x000001A2 System.Void UnityAdsScript::OnUnityAdsDidError(System.String)
extern void UnityAdsScript_OnUnityAdsDidError_m34D5DA14CEEC6D5B0018A214AC944A260FCC6F7E (void);
// 0x000001A3 System.Void UnityAdsScript::OnUnityAdsDidStart(System.String)
extern void UnityAdsScript_OnUnityAdsDidStart_m96CC78729B5AF76820A6D6C7861AF723A90D6AE7 (void);
// 0x000001A4 System.Void UnityAdsScript::OnDestroy()
extern void UnityAdsScript_OnDestroy_m0DEDD77F8ADB54E376C393B3D37BF81BDFD9A477 (void);
// 0x000001A5 System.Void UnityAdsScript::.ctor()
extern void UnityAdsScript__ctor_mE6F969CB1601280A6B9B026D6D160B52FC7D162B (void);
// 0x000001A6 PlayerBikeScript PlayerBikeScript::get_Instance()
extern void PlayerBikeScript_get_Instance_m9A1B750B4414EDFF792AB83ED7588A84D1DA75CB (void);
// 0x000001A7 System.Void PlayerBikeScript::Start()
extern void PlayerBikeScript_Start_m56BE1349FF7B5B91249F0E502DBFA3022535CE38 (void);
// 0x000001A8 System.Void PlayerBikeScript::Update()
extern void PlayerBikeScript_Update_m89A238F5A903B1F83753A69006BB656D06205AFE (void);
// 0x000001A9 System.Void PlayerBikeScript::OnCollisionEnter(UnityEngine.Collision)
extern void PlayerBikeScript_OnCollisionEnter_m9F235D50820CF56278D11B7F73E75AC21CFE71C4 (void);
// 0x000001AA System.Void PlayerBikeScript::.ctor()
extern void PlayerBikeScript__ctor_m57B01C5C1BB557B113515F494A2323527B313DB7 (void);
// 0x000001AB System.Void PlayerBoatScript::Start()
extern void PlayerBoatScript_Start_m0FC864569AF5C17C6064906B966956085196DDD5 (void);
// 0x000001AC System.Void PlayerBoatScript::Update()
extern void PlayerBoatScript_Update_mF5F66AB6F769783B18B4A5F0E38D2057F2135848 (void);
// 0x000001AD System.Void PlayerBoatScript::OnCollisionEnter(UnityEngine.Collision)
extern void PlayerBoatScript_OnCollisionEnter_m53F1DCE53AE92F7EBE6DB9CC9ED24D6DC60E038B (void);
// 0x000001AE System.Void PlayerBoatScript::.ctor()
extern void PlayerBoatScript__ctor_mAE89E528EBBB6E3530C8E5919927A69E5DD91511 (void);
// 0x000001AF PlayerCharacterScript PlayerCharacterScript::get_Instance()
extern void PlayerCharacterScript_get_Instance_m7336BCF939FABE3905490ADA3E1CDAA1577F5F9C (void);
// 0x000001B0 System.Void PlayerCharacterScript::Start()
extern void PlayerCharacterScript_Start_m8AD7CE1EF5D42313B3671E2C8B203E7059D85DF9 (void);
// 0x000001B1 System.Void PlayerCharacterScript::Update()
extern void PlayerCharacterScript_Update_m5B476B49CD9BB048672EEB322CAD3A3949190D91 (void);
// 0x000001B2 System.Collections.IEnumerator PlayerCharacterScript::OnTriggerEnter(UnityEngine.Collider)
extern void PlayerCharacterScript_OnTriggerEnter_mDBE8BD73187B0A928C13B9E0275563B9B2569958 (void);
// 0x000001B3 System.Void PlayerCharacterScript::Climb()
extern void PlayerCharacterScript_Climb_m6B59955A91F300029BC2B2E1AB47B20EBBDC917C (void);
// 0x000001B4 System.Collections.IEnumerator PlayerCharacterScript::IsClimbing()
extern void PlayerCharacterScript_IsClimbing_m19886905495A4440976ACF0F2D25BA17011DDAD0 (void);
// 0x000001B5 System.Void PlayerCharacterScript::.ctor()
extern void PlayerCharacterScript__ctor_m0C2133B7A565EAB02D20617D11ADEC621736DB78 (void);
// 0x000001B6 System.Void PlayerCharacterScript/<OnTriggerEnter>d__11::.ctor(System.Int32)
extern void U3COnTriggerEnterU3Ed__11__ctor_m157690149B9EFC064D41233255CFF1D06F6E39EA (void);
// 0x000001B7 System.Void PlayerCharacterScript/<OnTriggerEnter>d__11::System.IDisposable.Dispose()
extern void U3COnTriggerEnterU3Ed__11_System_IDisposable_Dispose_m3608C29B0A3E3E32B1042BB1B65824B7BCBEC6C1 (void);
// 0x000001B8 System.Boolean PlayerCharacterScript/<OnTriggerEnter>d__11::MoveNext()
extern void U3COnTriggerEnterU3Ed__11_MoveNext_mC9BF00C5C113090006A7F0EA8BEC3798E726BDBC (void);
// 0x000001B9 System.Object PlayerCharacterScript/<OnTriggerEnter>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnTriggerEnterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2CB4290008F905A4825A8D09AC73523EED0D715B (void);
// 0x000001BA System.Void PlayerCharacterScript/<OnTriggerEnter>d__11::System.Collections.IEnumerator.Reset()
extern void U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_Reset_m980E27465640D7E4636D68BEDFDD1222A71C3454 (void);
// 0x000001BB System.Object PlayerCharacterScript/<OnTriggerEnter>d__11::System.Collections.IEnumerator.get_Current()
extern void U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_get_Current_m298E21F99DFA30907415C3D27D3AF0AD04F4B3FD (void);
// 0x000001BC System.Void PlayerCharacterScript/<IsClimbing>d__13::.ctor(System.Int32)
extern void U3CIsClimbingU3Ed__13__ctor_mE046B29894718B2CEE95FC9F23E6C957C3FFA998 (void);
// 0x000001BD System.Void PlayerCharacterScript/<IsClimbing>d__13::System.IDisposable.Dispose()
extern void U3CIsClimbingU3Ed__13_System_IDisposable_Dispose_m367474145BDE9A57815C89B1370EE3DC4F317AD0 (void);
// 0x000001BE System.Boolean PlayerCharacterScript/<IsClimbing>d__13::MoveNext()
extern void U3CIsClimbingU3Ed__13_MoveNext_m9F068AF0C5ADC004F4354AFB426D9AB7E0658BE9 (void);
// 0x000001BF System.Object PlayerCharacterScript/<IsClimbing>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIsClimbingU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D8834D7DDF29F107743C566E8036B4323BB1688 (void);
// 0x000001C0 System.Void PlayerCharacterScript/<IsClimbing>d__13::System.Collections.IEnumerator.Reset()
extern void U3CIsClimbingU3Ed__13_System_Collections_IEnumerator_Reset_m271A0D38A671C486E12E68AD956D59A536B65DE7 (void);
// 0x000001C1 System.Object PlayerCharacterScript/<IsClimbing>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CIsClimbingU3Ed__13_System_Collections_IEnumerator_get_Current_mA4A718648508AFFC8455A5D13E62BB8091F46067 (void);
// 0x000001C2 PlayerControllerScript PlayerControllerScript::get_Instance()
extern void PlayerControllerScript_get_Instance_mDEFC9F8B64434B73A1D6DD76CC984857014ABC56 (void);
// 0x000001C3 System.Void PlayerControllerScript::Awake()
extern void PlayerControllerScript_Awake_m7F252A4E6890B1AEF73DDEEA6006895644A0A7DB (void);
// 0x000001C4 System.Void PlayerControllerScript::Start()
extern void PlayerControllerScript_Start_m41BF79F36DEBC8A7298A9B3C5939A2C11C4FF685 (void);
// 0x000001C5 System.Void PlayerControllerScript::Update()
extern void PlayerControllerScript_Update_mD049DEC0380F99907FA2CFD01DAF5897833E9685 (void);
// 0x000001C6 System.Void PlayerControllerScript::GetCurrentObject()
extern void PlayerControllerScript_GetCurrentObject_m72769A10B3CB41EE9B84B3B17A3294C833F500F6 (void);
// 0x000001C7 System.Void PlayerControllerScript::GetRotationalObjects()
extern void PlayerControllerScript_GetRotationalObjects_mAA0BEFAB51642F2320F6592DFB33004654506ADD (void);
// 0x000001C8 System.Void PlayerControllerScript::ChangeShape(System.Int32)
extern void PlayerControllerScript_ChangeShape_m0ECF7F460920C2CA5F98BED4696097BB6F087240 (void);
// 0x000001C9 System.Collections.IEnumerator PlayerControllerScript::MoveObject(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void PlayerControllerScript_MoveObject_mFDBF554082AA89295044CEA450F59F2184EF2FF4 (void);
// 0x000001CA System.Void PlayerControllerScript::DetectType(System.String,UnityEngine.GameObject)
extern void PlayerControllerScript_DetectType_m4AD40DD38785577DA9A3165720EB102BF4425BD9 (void);
// 0x000001CB System.Void PlayerControllerScript::OnSliderValueChanged()
extern void PlayerControllerScript_OnSliderValueChanged_m4541F8D97135067D4FB288CACA9763F4FBB17881 (void);
// 0x000001CC System.Int32 PlayerControllerScript::ReturnIndex()
extern void PlayerControllerScript_ReturnIndex_m90D508FD77D6F4C99C46ACF0EB21E12AE6003233 (void);
// 0x000001CD System.Void PlayerControllerScript::.ctor()
extern void PlayerControllerScript__ctor_m6B9E7554069250D21CFBEBC24C4D47313E4AB27F (void);
// 0x000001CE System.Void PlayerControllerScript/<MoveObject>d__34::.ctor(System.Int32)
extern void U3CMoveObjectU3Ed__34__ctor_m7679949E4579F0514F360B36E50FF224BD59B3DA (void);
// 0x000001CF System.Void PlayerControllerScript/<MoveObject>d__34::System.IDisposable.Dispose()
extern void U3CMoveObjectU3Ed__34_System_IDisposable_Dispose_mE95ED8C6FDC5F84ED1D757A53DA602E240778666 (void);
// 0x000001D0 System.Boolean PlayerControllerScript/<MoveObject>d__34::MoveNext()
extern void U3CMoveObjectU3Ed__34_MoveNext_m4D512AE9C629A535FAFDF770AA89968637F15954 (void);
// 0x000001D1 System.Object PlayerControllerScript/<MoveObject>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveObjectU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A7854A57AE804B9BB83BDE78DB37D0766FB65CD (void);
// 0x000001D2 System.Void PlayerControllerScript/<MoveObject>d__34::System.Collections.IEnumerator.Reset()
extern void U3CMoveObjectU3Ed__34_System_Collections_IEnumerator_Reset_m1752A1686309401EC13831FBF562A3FFAEBB69B1 (void);
// 0x000001D3 System.Object PlayerControllerScript/<MoveObject>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CMoveObjectU3Ed__34_System_Collections_IEnumerator_get_Current_m7B5FC4B34965C4A5011095B7BAE82B2399652B2E (void);
// 0x000001D4 System.Void PlayerGliderScript::Start()
extern void PlayerGliderScript_Start_m739DA40127549CDA0184CA19CFCCF4559540C506 (void);
// 0x000001D5 System.Void PlayerGliderScript::Update()
extern void PlayerGliderScript_Update_m4EF16675970B19C7B13166E79874B5440FF07B94 (void);
// 0x000001D6 System.Void PlayerGliderScript::OnTriggerEnter(UnityEngine.Collider)
extern void PlayerGliderScript_OnTriggerEnter_mA807B366442823366C8BD014FF1E0C1B8394D6D4 (void);
// 0x000001D7 System.Void PlayerGliderScript::OnTriggerExit(UnityEngine.Collider)
extern void PlayerGliderScript_OnTriggerExit_mF423D167D76A42E04DC5F14A93DF3B148A6809A3 (void);
// 0x000001D8 System.Void PlayerGliderScript::.ctor()
extern void PlayerGliderScript__ctor_mA4928F67FB36344901115E5B13BDC62A1B895AC1 (void);
// 0x000001D9 PlayerHelicopterScript PlayerHelicopterScript::get_Instance()
extern void PlayerHelicopterScript_get_Instance_m35A8E5D665160B040181ABA773F797810B406E51 (void);
// 0x000001DA System.Void PlayerHelicopterScript::OnEnable()
extern void PlayerHelicopterScript_OnEnable_m17535DD0D895CAB6955EAAF5742B42258E0142FC (void);
// 0x000001DB System.Collections.IEnumerator PlayerHelicopterScript::wait()
extern void PlayerHelicopterScript_wait_m2A2F63CFEB547FFAF4C6D64FB02D898B7244894E (void);
// 0x000001DC System.Void PlayerHelicopterScript::Start()
extern void PlayerHelicopterScript_Start_m99B8EA9AAE41033650BD75AFA225243A950C6A94 (void);
// 0x000001DD System.Void PlayerHelicopterScript::Update()
extern void PlayerHelicopterScript_Update_m8AC5A6122BF3ECDF83F3189D226F216E7C45B37F (void);
// 0x000001DE System.Void PlayerHelicopterScript::BoostHelicopter(System.Int32)
extern void PlayerHelicopterScript_BoostHelicopter_m2449A206A938A831C3F0513A9708A59FF50E1CCA (void);
// 0x000001DF System.Collections.IEnumerator PlayerHelicopterScript::BoostHelicopterNow(System.Int32)
extern void PlayerHelicopterScript_BoostHelicopterNow_m394AF4AD515DAF71CD02DD1BD10B8904F6B9F68A (void);
// 0x000001E0 System.Void PlayerHelicopterScript::OnCollisionEnter(UnityEngine.Collision)
extern void PlayerHelicopterScript_OnCollisionEnter_mC91D6B47DF96BFA8AA888D85ACF896354039A57F (void);
// 0x000001E1 System.Void PlayerHelicopterScript::.ctor()
extern void PlayerHelicopterScript__ctor_mBB83718A7EF4D16DCA7CDA046DFBCD766455AFC9 (void);
// 0x000001E2 System.Void PlayerHelicopterScript/<wait>d__8::.ctor(System.Int32)
extern void U3CwaitU3Ed__8__ctor_m6D1BEC088C88A0A2E14F1560F02C07733ABBE2F9 (void);
// 0x000001E3 System.Void PlayerHelicopterScript/<wait>d__8::System.IDisposable.Dispose()
extern void U3CwaitU3Ed__8_System_IDisposable_Dispose_m78B42166C4DD382EFDFA89374A11B19BFE49A446 (void);
// 0x000001E4 System.Boolean PlayerHelicopterScript/<wait>d__8::MoveNext()
extern void U3CwaitU3Ed__8_MoveNext_m8ABDF16FD351C7C5886DE215266527E9D88EC835 (void);
// 0x000001E5 System.Object PlayerHelicopterScript/<wait>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CwaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC4E14E44E3C1B5EA4BD55F528B43CB7B250DC8A7 (void);
// 0x000001E6 System.Void PlayerHelicopterScript/<wait>d__8::System.Collections.IEnumerator.Reset()
extern void U3CwaitU3Ed__8_System_Collections_IEnumerator_Reset_mFC68D47EBA84A0CB1C50D57E3A6ECF7C7C2EFB0F (void);
// 0x000001E7 System.Object PlayerHelicopterScript/<wait>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CwaitU3Ed__8_System_Collections_IEnumerator_get_Current_mF4AA3F4158AEC974F5500B66AB2C5D0E7F560C0F (void);
// 0x000001E8 System.Void PlayerHelicopterScript/<BoostHelicopterNow>d__12::.ctor(System.Int32)
extern void U3CBoostHelicopterNowU3Ed__12__ctor_mA348438E255D83BF6D31C4A904B1C9D0BFDF18BB (void);
// 0x000001E9 System.Void PlayerHelicopterScript/<BoostHelicopterNow>d__12::System.IDisposable.Dispose()
extern void U3CBoostHelicopterNowU3Ed__12_System_IDisposable_Dispose_m4C43CFFF5FF0DEC50CA5F4AF019157A2404DFA69 (void);
// 0x000001EA System.Boolean PlayerHelicopterScript/<BoostHelicopterNow>d__12::MoveNext()
extern void U3CBoostHelicopterNowU3Ed__12_MoveNext_m1CFC611201F3B9A1AC1525DFE6C1F8FFAC24E6A7 (void);
// 0x000001EB System.Object PlayerHelicopterScript/<BoostHelicopterNow>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBoostHelicopterNowU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAA789A47FD4E85D770398F31F458D991E57D5822 (void);
// 0x000001EC System.Void PlayerHelicopterScript/<BoostHelicopterNow>d__12::System.Collections.IEnumerator.Reset()
extern void U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_Reset_mEAD31E2B8E6B9460D8BC89E00A58DC568DFEDCE7 (void);
// 0x000001ED System.Object PlayerHelicopterScript/<BoostHelicopterNow>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_get_Current_m1E39FFAB0BA8AD5590084840D6C6B1147B388A3A (void);
// 0x000001EE PlayerJeepScript PlayerJeepScript::get_Instance()
extern void PlayerJeepScript_get_Instance_m23EF556DCC502B65E305B3C3179EB2776990483D (void);
// 0x000001EF System.Void PlayerJeepScript::Start()
extern void PlayerJeepScript_Start_m600105456C1DDAB5EA1BCC54041815D61703DA4A (void);
// 0x000001F0 System.Void PlayerJeepScript::Update()
extern void PlayerJeepScript_Update_mD06608FB3A7908C5FC4E3045ADEF2D0E6840A6DD (void);
// 0x000001F1 System.Void PlayerJeepScript::OnCollisionEnter(UnityEngine.Collision)
extern void PlayerJeepScript_OnCollisionEnter_m387EB4E142B338B9A710C0415DAEE10E447CA791 (void);
// 0x000001F2 System.Void PlayerJeepScript::.ctor()
extern void PlayerJeepScript__ctor_m78A06ED6D1EB28BDB245A42B9A7A883BF4897234 (void);
// 0x000001F3 PlayerTankScript PlayerTankScript::get_Instance()
extern void PlayerTankScript_get_Instance_m9A208859F15E640A23D44994B828670CB94E5537 (void);
// 0x000001F4 System.Void PlayerTankScript::Start()
extern void PlayerTankScript_Start_m8FCBA12EF49FEF37CB144A256A62F85764F7D07C (void);
// 0x000001F5 System.Void PlayerTankScript::Update()
extern void PlayerTankScript_Update_m8D35C701E683E337AB4CC11481770DDE5DA59CED (void);
// 0x000001F6 System.Void PlayerTankScript::OnCollisionEnter(UnityEngine.Collision)
extern void PlayerTankScript_OnCollisionEnter_m0482EC0AB7F289D15C3417FD97C0030FAB7E89D3 (void);
// 0x000001F7 System.Void PlayerTankScript::.ctor()
extern void PlayerTankScript__ctor_mBE5F102B8F5F1E433DDD44480BEC7CE91F8962C8 (void);
// 0x000001F8 WhiteAiBikeScript WhiteAiBikeScript::get_Instance()
extern void WhiteAiBikeScript_get_Instance_m4815F582C72887FE5966D94FCB3061029F720773 (void);
// 0x000001F9 System.Void WhiteAiBikeScript::Start()
extern void WhiteAiBikeScript_Start_m183B881B8F630A732F1C37759BE5B1805053B378 (void);
// 0x000001FA System.Void WhiteAiBikeScript::Update()
extern void WhiteAiBikeScript_Update_m26BC6DE23DA712DE3B96041884BCA74814DAB54F (void);
// 0x000001FB System.Void WhiteAiBikeScript::OnCollisionEnter(UnityEngine.Collision)
extern void WhiteAiBikeScript_OnCollisionEnter_m714FB633CA5BA0809A447EAA8328181F6E205C7F (void);
// 0x000001FC System.Void WhiteAiBikeScript::.ctor()
extern void WhiteAiBikeScript__ctor_mE99B24832D737249C8BF52C4E635FE53367CF91E (void);
// 0x000001FD System.Void WhiteAiBoatScript::Start()
extern void WhiteAiBoatScript_Start_mE3A19D3744710B44CD10939819F8CB9328C9AC2E (void);
// 0x000001FE System.Void WhiteAiBoatScript::Update()
extern void WhiteAiBoatScript_Update_m58EF1C02217F03A04DB86035C27C76CD8585B9C7 (void);
// 0x000001FF System.Void WhiteAiBoatScript::OnCollisionEnter(UnityEngine.Collision)
extern void WhiteAiBoatScript_OnCollisionEnter_m5AC4913CB12DAF8A12082E429972318FA2B1CBF5 (void);
// 0x00000200 System.Void WhiteAiBoatScript::.ctor()
extern void WhiteAiBoatScript__ctor_m215CF73212A095F63AF8161E1845B55C014B28A5 (void);
// 0x00000201 WhiteAiCharacterScript WhiteAiCharacterScript::get_Instance()
extern void WhiteAiCharacterScript_get_Instance_mD521B6671A709334246F23E17AB16FAC35B5DA97 (void);
// 0x00000202 System.Void WhiteAiCharacterScript::Start()
extern void WhiteAiCharacterScript_Start_mE2E325B3CFD8899EA1C0BF4551242A07ACB32D26 (void);
// 0x00000203 System.Void WhiteAiCharacterScript::Update()
extern void WhiteAiCharacterScript_Update_m60283B756DBCFA2BBDD3FFFA51FFA97DE0018986 (void);
// 0x00000204 System.Collections.IEnumerator WhiteAiCharacterScript::OnTriggerEnter(UnityEngine.Collider)
extern void WhiteAiCharacterScript_OnTriggerEnter_mF98389C957252DAA658D0C6782844B50DD3BB48A (void);
// 0x00000205 System.Void WhiteAiCharacterScript::OnCollisionEnter(UnityEngine.Collision)
extern void WhiteAiCharacterScript_OnCollisionEnter_m55BE18719820F352208D6DAC460DA262588C37CC (void);
// 0x00000206 System.Void WhiteAiCharacterScript::Climb()
extern void WhiteAiCharacterScript_Climb_m1EC35FCC064D0C9DD1E20E0DB13B93D4D8D482AE (void);
// 0x00000207 System.Collections.IEnumerator WhiteAiCharacterScript::IsClimbing()
extern void WhiteAiCharacterScript_IsClimbing_m96BAB0B2C35DDFC80B654B636317E9C44D8D2C82 (void);
// 0x00000208 System.Void WhiteAiCharacterScript::.ctor()
extern void WhiteAiCharacterScript__ctor_m2B72204CBEF905EA44746243E2096C64DB5DB007 (void);
// 0x00000209 System.Void WhiteAiCharacterScript/<OnTriggerEnter>d__11::.ctor(System.Int32)
extern void U3COnTriggerEnterU3Ed__11__ctor_mEBE528454705838D7F327CA5E247BA9D56E91EF7 (void);
// 0x0000020A System.Void WhiteAiCharacterScript/<OnTriggerEnter>d__11::System.IDisposable.Dispose()
extern void U3COnTriggerEnterU3Ed__11_System_IDisposable_Dispose_m516C2E127C3B3045FEEF4FF7AAA607FE145EDEF9 (void);
// 0x0000020B System.Boolean WhiteAiCharacterScript/<OnTriggerEnter>d__11::MoveNext()
extern void U3COnTriggerEnterU3Ed__11_MoveNext_m097B0C2FA97C9BF3D5EF7CC213542D3629547B2F (void);
// 0x0000020C System.Object WhiteAiCharacterScript/<OnTriggerEnter>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnTriggerEnterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3A149DD87BBDCDA6FB6654FCB0075EF9BA2BF827 (void);
// 0x0000020D System.Void WhiteAiCharacterScript/<OnTriggerEnter>d__11::System.Collections.IEnumerator.Reset()
extern void U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_Reset_m9A6AF1050DBC028A87125D7D75181368217E7842 (void);
// 0x0000020E System.Object WhiteAiCharacterScript/<OnTriggerEnter>d__11::System.Collections.IEnumerator.get_Current()
extern void U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_get_Current_m89517AA72A5C2553EA773B7BA5633B6E782E8B5D (void);
// 0x0000020F System.Void WhiteAiCharacterScript/<IsClimbing>d__14::.ctor(System.Int32)
extern void U3CIsClimbingU3Ed__14__ctor_mD50ABC879D4CF442FE82D0600C944384F089EE09 (void);
// 0x00000210 System.Void WhiteAiCharacterScript/<IsClimbing>d__14::System.IDisposable.Dispose()
extern void U3CIsClimbingU3Ed__14_System_IDisposable_Dispose_m24EADF4B39D6E9C8BB81DD85DF9C5DF33E513E0B (void);
// 0x00000211 System.Boolean WhiteAiCharacterScript/<IsClimbing>d__14::MoveNext()
extern void U3CIsClimbingU3Ed__14_MoveNext_m7607086996A838339B965D67F537A5240238C075 (void);
// 0x00000212 System.Object WhiteAiCharacterScript/<IsClimbing>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIsClimbingU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD193DA26A09635216183F2B99C9518351610909D (void);
// 0x00000213 System.Void WhiteAiCharacterScript/<IsClimbing>d__14::System.Collections.IEnumerator.Reset()
extern void U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_Reset_m3001C0A5FCB20D56031F2BEAA18BBDED1E26C86C (void);
// 0x00000214 System.Object WhiteAiCharacterScript/<IsClimbing>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_get_Current_m1F4082A4FC0430E5D727271AC49902284A2326E9 (void);
// 0x00000215 WhiteAiControllerScript WhiteAiControllerScript::get_Instance()
extern void WhiteAiControllerScript_get_Instance_m19396D2B80A00D0EBB7915311A13CCD3D1625A31 (void);
// 0x00000216 System.Void WhiteAiControllerScript::Awake()
extern void WhiteAiControllerScript_Awake_mD627F7FB68D528D27C1B1075CBC7E8678DB9A42C (void);
// 0x00000217 System.Void WhiteAiControllerScript::Start()
extern void WhiteAiControllerScript_Start_m5512717D34902431CEE6AA11219D56650F03BC26 (void);
// 0x00000218 System.Void WhiteAiControllerScript::Update()
extern void WhiteAiControllerScript_Update_m36BFF700C9784AC1BE6FFBF86576500C301BDD35 (void);
// 0x00000219 System.Void WhiteAiControllerScript::GetCurrentObject()
extern void WhiteAiControllerScript_GetCurrentObject_mA4D909C0BE6141E4602138FA305EAD254DDD9C7E (void);
// 0x0000021A System.Void WhiteAiControllerScript::GetRotationalObjects()
extern void WhiteAiControllerScript_GetRotationalObjects_m2A051C2E6DA7CEA35956C832196156E20D496AC3 (void);
// 0x0000021B System.Void WhiteAiControllerScript::ChangeShape(System.Int32)
extern void WhiteAiControllerScript_ChangeShape_mE41BF60D27CFA75C0621A9219BB2CE2D410ECB3A (void);
// 0x0000021C System.Collections.IEnumerator WhiteAiControllerScript::MoveObject(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void WhiteAiControllerScript_MoveObject_mD0A4644E2AC3C736E82D453DD534CD123D126A18 (void);
// 0x0000021D System.Int32 WhiteAiControllerScript::ReturnIndex()
extern void WhiteAiControllerScript_ReturnIndex_m04D691A6423A34CDE69E3995BF0A296ACE812C34 (void);
// 0x0000021E System.Void WhiteAiControllerScript::DetectType(System.String,UnityEngine.GameObject)
extern void WhiteAiControllerScript_DetectType_mB32AC06DFF0B7C4DBC0D5EC59DF48B909ECFD4C0 (void);
// 0x0000021F System.Collections.IEnumerator WhiteAiControllerScript::Transform(System.Int32)
extern void WhiteAiControllerScript_Transform_m6D5DF53567C35622727179CED636CC9F5432E595 (void);
// 0x00000220 System.Collections.IEnumerator WhiteAiControllerScript::TransformToRandom()
extern void WhiteAiControllerScript_TransformToRandom_mEE31E2CE3B2E990473C6C3FF5DA49ECABB3C8368 (void);
// 0x00000221 System.Void WhiteAiControllerScript::.ctor()
extern void WhiteAiControllerScript__ctor_m6AEBC70DE3A517743D550A343DCCE59A2102656E (void);
// 0x00000222 System.Void WhiteAiControllerScript::.cctor()
extern void WhiteAiControllerScript__cctor_mD263C819FEC052837C68C77F77420A107C406A8F (void);
// 0x00000223 System.Void WhiteAiControllerScript/<MoveObject>d__35::.ctor(System.Int32)
extern void U3CMoveObjectU3Ed__35__ctor_m46FAD7C53D13345B8A77D4EC983C9B26FB309376 (void);
// 0x00000224 System.Void WhiteAiControllerScript/<MoveObject>d__35::System.IDisposable.Dispose()
extern void U3CMoveObjectU3Ed__35_System_IDisposable_Dispose_m17574B27BD597AD8546BC11CD6A58C8EB9040CBE (void);
// 0x00000225 System.Boolean WhiteAiControllerScript/<MoveObject>d__35::MoveNext()
extern void U3CMoveObjectU3Ed__35_MoveNext_mDB9EDEDAB3DFBC6A96726C537E1FDD84B0F3E409 (void);
// 0x00000226 System.Object WhiteAiControllerScript/<MoveObject>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveObjectU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE77F08164CB742A73CC623896C303BE46F18789 (void);
// 0x00000227 System.Void WhiteAiControllerScript/<MoveObject>d__35::System.Collections.IEnumerator.Reset()
extern void U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_Reset_m74279CE4296C2A6C033D3378BFB1E1B48CFF9FF0 (void);
// 0x00000228 System.Object WhiteAiControllerScript/<MoveObject>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_get_Current_m159DA0631BCEEC3665E0265F971D1F9B27D6472C (void);
// 0x00000229 System.Void WhiteAiControllerScript/<Transform>d__38::.ctor(System.Int32)
extern void U3CTransformU3Ed__38__ctor_m9BD5E5BF43D39641AC0247391E4797A7319B1AD4 (void);
// 0x0000022A System.Void WhiteAiControllerScript/<Transform>d__38::System.IDisposable.Dispose()
extern void U3CTransformU3Ed__38_System_IDisposable_Dispose_m0B2B7B2528B459935A3F95DFE3E91B482848C801 (void);
// 0x0000022B System.Boolean WhiteAiControllerScript/<Transform>d__38::MoveNext()
extern void U3CTransformU3Ed__38_MoveNext_mE4A4BEB23FB4D85C5F72B646D8A8658E8946DB46 (void);
// 0x0000022C System.Object WhiteAiControllerScript/<Transform>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTransformU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB25722084BED3E54E2949574C55216D8F602E057 (void);
// 0x0000022D System.Void WhiteAiControllerScript/<Transform>d__38::System.Collections.IEnumerator.Reset()
extern void U3CTransformU3Ed__38_System_Collections_IEnumerator_Reset_mABC4575D59FB478E3D6019D8FFD280E2B7777ACE (void);
// 0x0000022E System.Object WhiteAiControllerScript/<Transform>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CTransformU3Ed__38_System_Collections_IEnumerator_get_Current_m6148983E37AABFC28BA9819C013473ABF88C5F77 (void);
// 0x0000022F System.Void WhiteAiControllerScript/<TransformToRandom>d__39::.ctor(System.Int32)
extern void U3CTransformToRandomU3Ed__39__ctor_mE4E2E68B2E5D58BF723A15D1B39E372FC8B04C43 (void);
// 0x00000230 System.Void WhiteAiControllerScript/<TransformToRandom>d__39::System.IDisposable.Dispose()
extern void U3CTransformToRandomU3Ed__39_System_IDisposable_Dispose_m6194F8626FBFB3D09711A844BE285169751F003F (void);
// 0x00000231 System.Boolean WhiteAiControllerScript/<TransformToRandom>d__39::MoveNext()
extern void U3CTransformToRandomU3Ed__39_MoveNext_m4CA3AD3027881545BEBA56AF9804B6B748617E0C (void);
// 0x00000232 System.Object WhiteAiControllerScript/<TransformToRandom>d__39::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTransformToRandomU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C6B2A3331459A3A3E8A7640B058F0B0C99BAD48 (void);
// 0x00000233 System.Void WhiteAiControllerScript/<TransformToRandom>d__39::System.Collections.IEnumerator.Reset()
extern void U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_Reset_m80BF425047633FFBDC3BD6A67D0A4C77490717F0 (void);
// 0x00000234 System.Object WhiteAiControllerScript/<TransformToRandom>d__39::System.Collections.IEnumerator.get_Current()
extern void U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_get_Current_mED3B378B7A0BC6D929D9A73DED17AC2D7CE5C3DB (void);
// 0x00000235 System.Void WhiteAiGliderScript::Start()
extern void WhiteAiGliderScript_Start_mBE0564D31E40AC6468AB260680114B00563DE859 (void);
// 0x00000236 System.Void WhiteAiGliderScript::Update()
extern void WhiteAiGliderScript_Update_mD606E23C1026E997826BFF87A38C62C6EAED3141 (void);
// 0x00000237 System.Void WhiteAiGliderScript::OnTriggerEnter(UnityEngine.Collider)
extern void WhiteAiGliderScript_OnTriggerEnter_m2486881B7C830A1B70E2A6F9DEF5C59D44FF6583 (void);
// 0x00000238 System.Void WhiteAiGliderScript::OnTriggerExit(UnityEngine.Collider)
extern void WhiteAiGliderScript_OnTriggerExit_m5A3A08A14863988C496B804387854A4F5BF39FD8 (void);
// 0x00000239 System.Void WhiteAiGliderScript::.ctor()
extern void WhiteAiGliderScript__ctor_m5D76D4C56986750C05236F4FE0034D73158066AD (void);
// 0x0000023A WhiteAiHelicopterScript WhiteAiHelicopterScript::get_Instance()
extern void WhiteAiHelicopterScript_get_Instance_mB60AA3F270D7D79783B5CAF73D549FEC83B1CE92 (void);
// 0x0000023B System.Void WhiteAiHelicopterScript::OnEnable()
extern void WhiteAiHelicopterScript_OnEnable_m96803C775C3D71B80FBD2AD9E3A33C7A9FACB319 (void);
// 0x0000023C System.Collections.IEnumerator WhiteAiHelicopterScript::wait()
extern void WhiteAiHelicopterScript_wait_mED718A5D65ACB507B40E9AB7C3D5A527C94608F0 (void);
// 0x0000023D System.Void WhiteAiHelicopterScript::Start()
extern void WhiteAiHelicopterScript_Start_m10A1BC75049098E7DF2C85438BCB4E826CC69B1E (void);
// 0x0000023E System.Void WhiteAiHelicopterScript::Update()
extern void WhiteAiHelicopterScript_Update_m8C258C803E68B1F83A5B6AC5BFF193EAE1755E77 (void);
// 0x0000023F System.Void WhiteAiHelicopterScript::BoostHelicopter(System.Int32)
extern void WhiteAiHelicopterScript_BoostHelicopter_mD90914A8EE4FB8D744D4BE0C341817F0CB34BE96 (void);
// 0x00000240 System.Collections.IEnumerator WhiteAiHelicopterScript::BoostHelicopterNow(System.Int32)
extern void WhiteAiHelicopterScript_BoostHelicopterNow_mADA21CCA99D5687E6BC26F7BD3193889086B4AD8 (void);
// 0x00000241 System.Void WhiteAiHelicopterScript::OnCollisionEnter(UnityEngine.Collision)
extern void WhiteAiHelicopterScript_OnCollisionEnter_mCDC50EA9AB2D9E082A5DCCE60502B6336C5D6B51 (void);
// 0x00000242 System.Void WhiteAiHelicopterScript::.ctor()
extern void WhiteAiHelicopterScript__ctor_m39416132BD37B4BD36F4379749DB91530A957833 (void);
// 0x00000243 System.Void WhiteAiHelicopterScript/<wait>d__8::.ctor(System.Int32)
extern void U3CwaitU3Ed__8__ctor_mCDB3D87DB2A64800169702361168C4CDC6B3D37C (void);
// 0x00000244 System.Void WhiteAiHelicopterScript/<wait>d__8::System.IDisposable.Dispose()
extern void U3CwaitU3Ed__8_System_IDisposable_Dispose_mABACCB7EB2890077CA87A31B4FD0CF548274E171 (void);
// 0x00000245 System.Boolean WhiteAiHelicopterScript/<wait>d__8::MoveNext()
extern void U3CwaitU3Ed__8_MoveNext_mC789EBAE70A80924E73ABAF89ECBC1950CB441F3 (void);
// 0x00000246 System.Object WhiteAiHelicopterScript/<wait>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CwaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8272CCE65FF2619EBBA0D2C0A7BD4212C3125869 (void);
// 0x00000247 System.Void WhiteAiHelicopterScript/<wait>d__8::System.Collections.IEnumerator.Reset()
extern void U3CwaitU3Ed__8_System_Collections_IEnumerator_Reset_mB2BE0C91E4E9A386047094591B5B86CDA8DB7810 (void);
// 0x00000248 System.Object WhiteAiHelicopterScript/<wait>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CwaitU3Ed__8_System_Collections_IEnumerator_get_Current_mABCAEFC067279C876F020818FBBAEF9CBF999BD6 (void);
// 0x00000249 System.Void WhiteAiHelicopterScript/<BoostHelicopterNow>d__12::.ctor(System.Int32)
extern void U3CBoostHelicopterNowU3Ed__12__ctor_mEBCFF3C87A4DB90A3566DB58CC56588429F746EE (void);
// 0x0000024A System.Void WhiteAiHelicopterScript/<BoostHelicopterNow>d__12::System.IDisposable.Dispose()
extern void U3CBoostHelicopterNowU3Ed__12_System_IDisposable_Dispose_mA987C60121DC0B5C6BEA47187B70D7D69BB47BB8 (void);
// 0x0000024B System.Boolean WhiteAiHelicopterScript/<BoostHelicopterNow>d__12::MoveNext()
extern void U3CBoostHelicopterNowU3Ed__12_MoveNext_m28B7BA9016A84721EA4466318A997BCC7AAD556D (void);
// 0x0000024C System.Object WhiteAiHelicopterScript/<BoostHelicopterNow>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBoostHelicopterNowU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2931D5D08309E702D72BC52D93601CC4D7308A8F (void);
// 0x0000024D System.Void WhiteAiHelicopterScript/<BoostHelicopterNow>d__12::System.Collections.IEnumerator.Reset()
extern void U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_Reset_mFF46627F7E9356628562AF2AA430E389043A735B (void);
// 0x0000024E System.Object WhiteAiHelicopterScript/<BoostHelicopterNow>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_get_Current_mDD5107EF134E88023F3DD165A5D3CB19F0FFA802 (void);
// 0x0000024F WhiteAiJeepScript WhiteAiJeepScript::get_Instance()
extern void WhiteAiJeepScript_get_Instance_m3B9B8CB7CEFCC8F6425CDD802679BF683A845795 (void);
// 0x00000250 System.Void WhiteAiJeepScript::Start()
extern void WhiteAiJeepScript_Start_m7C1B6A9B007CBBF6D9C4407855CD8365635E3047 (void);
// 0x00000251 System.Void WhiteAiJeepScript::Update()
extern void WhiteAiJeepScript_Update_m0922C52BE833BA934DA30B63267F74E2CA04E251 (void);
// 0x00000252 System.Void WhiteAiJeepScript::OnCollisionEnter(UnityEngine.Collision)
extern void WhiteAiJeepScript_OnCollisionEnter_m149BE4550A49A2797AE013DE3BD4AE5E8623385C (void);
// 0x00000253 System.Void WhiteAiJeepScript::.ctor()
extern void WhiteAiJeepScript__ctor_mB341D8DD586E3E2BBA300EC9E7B628883F024E59 (void);
// 0x00000254 WhiteAiTankScript WhiteAiTankScript::get_Instance()
extern void WhiteAiTankScript_get_Instance_mCB0A922527238D3E55B4C4439AD6F10DBA3FE64A (void);
// 0x00000255 System.Void WhiteAiTankScript::Start()
extern void WhiteAiTankScript_Start_m0E47323B9142583528D7B3513A6454B07CDF7DCC (void);
// 0x00000256 System.Void WhiteAiTankScript::Update()
extern void WhiteAiTankScript_Update_m9317F9D78147E69E287097F5650A3748F5AED3DB (void);
// 0x00000257 System.Void WhiteAiTankScript::OnCollisionEnter(UnityEngine.Collision)
extern void WhiteAiTankScript_OnCollisionEnter_m18B103DEA17F4CFC79E2913F6D032510EC092DEF (void);
// 0x00000258 System.Void WhiteAiTankScript::.ctor()
extern void WhiteAiTankScript__ctor_m55999400B1E0CA5BF4E713DF9266CFA2B052EBAB (void);
// 0x00000259 System.Void UnityEngine.Purchasing.DemoInventory::Fulfill(System.String)
extern void DemoInventory_Fulfill_m9589871E7FDFA83941CF675170D6130A22F566D4 (void);
// 0x0000025A System.Void UnityEngine.Purchasing.DemoInventory::.ctor()
extern void DemoInventory__ctor_m223CB4347D66DE6D6C28204FDEAAD5C84F7CE4E0 (void);
// 0x0000025B System.Void LowPolyWater.LowPolyWater::Awake()
extern void LowPolyWater_Awake_mBF6D32C683E8BD39148DB6E7AB59FA73EAB6B1C9 (void);
// 0x0000025C System.Void LowPolyWater.LowPolyWater::Start()
extern void LowPolyWater_Start_m89AFA69FF7508A8D612A6C5E5DACB95F2B94AC5B (void);
// 0x0000025D UnityEngine.MeshFilter LowPolyWater.LowPolyWater::CreateMeshLowPoly(UnityEngine.MeshFilter)
extern void LowPolyWater_CreateMeshLowPoly_mE6F5928759402E4367E29A84DB1B4C8FFAAE63D0 (void);
// 0x0000025E System.Void LowPolyWater.LowPolyWater::Update()
extern void LowPolyWater_Update_m5E2FF7C7587E28FA08EA29B9BDD359F190156CA9 (void);
// 0x0000025F System.Void LowPolyWater.LowPolyWater::GenerateWaves()
extern void LowPolyWater_GenerateWaves_m128D40285442A523C43AAB1BA22EF0E21544CEEE (void);
// 0x00000260 System.Void LowPolyWater.LowPolyWater::.ctor()
extern void LowPolyWater__ctor_m616A0E96E64BD42A2ECBAB3FE69EE017057502DE (void);
// 0x00000261 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOColor_mA67D7B77174B479F8AE20F739A74BA7A2BBC9F0F (void);
// 0x00000262 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOFade(UnityEngine.SpriteRenderer,System.Single,System.Single)
extern void DOTweenModuleSprite_DOFade_mEED04CEE3EE9D32B87478A3CFDB9CFB710307769 (void);
// 0x00000263 DG.Tweening.Sequence DG.Tweening.DOTweenModuleSprite::DOGradientColor(UnityEngine.SpriteRenderer,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleSprite_DOGradientColor_m47670ABD64AEDC972E8210CAF4C8A778B0B7676D (void);
// 0x00000264 DG.Tweening.Tweener DG.Tweening.DOTweenModuleSprite::DOBlendableColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOBlendableColor_m51BBDB4A69B0560481736E6734EA2BF52DB1537D (void);
// 0x00000265 System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mD77E24E24423ADEC43F9983504BD6796D2671E99 (void);
// 0x00000266 UnityEngine.Color DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m12BAC3674340BDF408F41A0E9BEEF6BB87C3F504 (void);
// 0x00000267 System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_m89E34AE3BBC636BD2B38C1AB7300F1F20F25E616 (void);
// 0x00000268 System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m40F18C57ED175BA68D1D65FFA97879AEAF2E3325 (void);
// 0x00000269 UnityEngine.Color DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__0_m8B9E647B54E5DA35EE71D65866372C8C90B7F452 (void);
// 0x0000026A System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__1_m935CE2E2B7CB25D7151DA0BF3B4420C8099E0A45 (void);
// 0x0000026B System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mB0AA92A12DE97E8954819AC2BAD916065D4BECDB (void);
// 0x0000026C UnityEngine.Color DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__0_m371A7E7E53DB5FC70A6534A7956B27A3A8B7EA03 (void);
// 0x0000026D System.Void DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__1_m36515014C810F349D12C86DC636349E452DA6974 (void);
// 0x0000026E DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mF927A1EDCA91814D8A466A343E3223EEF4ED7478 (void);
// 0x0000026F DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m7067683474EE6A600294C2CCFD941403FA789A6E (void);
// 0x00000270 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Graphic,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m41638D05EB7678C523D0D39E320FAC80B8E48658 (void);
// 0x00000271 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mF4B41443760536F3C399612AD0283A52937D95CC (void);
// 0x00000272 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754 (void);
// 0x00000273 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFillAmount(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFillAmount_mE4ED52B826A6F8035320E980F7C701B4047C4DBA (void);
// 0x00000274 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOGradientColor(UnityEngine.UI.Image,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUI_DOGradientColor_mB4B8D1DEDA92464670EB8A6D985973477D51A4D5 (void);
// 0x00000275 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOFlexibleSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOFlexibleSize_mF0916C4E7A906202EFBAEA89CC2B55A9C3CF48B3 (void);
// 0x00000276 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOMinSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOMinSize_m54A6919625499AFD6F801C6EC9672965DBDC96E1 (void);
// 0x00000277 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPreferredSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPreferredSize_mE26AFB2C606F095483EDED449FB8A5A80C08A8DE (void);
// 0x00000278 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Outline,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m15BC0FCF035E3DEEC3CFC30465EDE0FE6FD6D2EA (void);
// 0x00000279 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Outline,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m86DB80308D92FC4DFD3E981F9E958FF2A207EA74 (void);
// 0x0000027A DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOScale(UnityEngine.UI.Outline,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOScale_mFA27F29E3DFB6B1CFCAC3CE73F06DF3737A3EB06 (void);
// 0x0000027B DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos_m63999D434CE7E8F6BE66BBA92532DE9D98DC9F79 (void);
// 0x0000027C DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosX_m9B57E02E9902C7A3F1A8DD1021AF630C10478380 (void);
// 0x0000027D DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosY_m14CCA63E545D7C28AD1A76488D0084F55EE94CA8 (void);
// 0x0000027E DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3D(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3D_m6DEB7C4AE4CD2118BF83D268945340F3373DC7BB (void);
// 0x0000027F DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DX_mA40303AC769280BDE74EE58BE5AD80BEA427CE08 (void);
// 0x00000280 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DY_mE6BE56AE421B959F22CBA6318BABC3CD367A7277 (void);
// 0x00000281 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DZ(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DZ_m702F7D4FE9DC8E6116FB5E0DD478D2BB88ED1242 (void);
// 0x00000282 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMax(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMax_mDD97F6A8B4468622E3808507CB6B89A8A109BDCE (void);
// 0x00000283 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMin(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMin_m60C4775B1EEDBCF2CD3E96A8171C8A83FD7F400D (void);
// 0x00000284 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivot(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOPivot_mF623939EDCC152F664FECC8B71E321B0AA13F0A9 (void);
// 0x00000285 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotX(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotX_mC21C031A61BDB7C3A8087719F23CBC6D1D762D6B (void);
// 0x00000286 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotY(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotY_m9922C28ADECA433362D3C6C816318CA00705996A (void);
// 0x00000287 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOSizeDelta(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOSizeDelta_mC4FD07525E1CA5BBF577030D571CE0CCEFD4E6B7 (void);
// 0x00000288 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOPunchAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPunchAnchorPos_m058E3DA140B9C497727F05AEAA296425E585B9EA (void);
// 0x00000289 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,System.Single,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_m89D1C74E940B9434B2234FB49C031AD8FD1170AB (void);
// 0x0000028A DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,UnityEngine.Vector2,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_m7818867062313A535AB2CD6F86267E2AB9357AC6 (void);
// 0x0000028B DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOJumpAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOJumpAnchorPos_m7046EE1C82AF0159CFAAA460E4C3D22797625480 (void);
// 0x0000028C DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DONormalizedPos(UnityEngine.UI.ScrollRect,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DONormalizedPos_m300F112FE2F67D4DC718083340C7FD27BC96F248 (void);
// 0x0000028D DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOHorizontalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOHorizontalNormalizedPos_mCB4BCD85B49C458BE871F890A86FEFE0D7F41B42 (void);
// 0x0000028E DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOVerticalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOVerticalNormalizedPos_mC0269B4644816A0D5C09717CA92E4229CB31CDF1 (void);
// 0x0000028F DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOValue(UnityEngine.UI.Slider,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOValue_mFDF45D60F52C82AACD8B50859A51B8F315A44F08 (void);
// 0x00000290 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mFD36CA1EA683A0D2312EE87BCF3B0A924504E797 (void);
// 0x00000291 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1 (void);
// 0x00000292 DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions> DG.Tweening.DOTweenModuleUI::DOText(UnityEngine.UI.Text,System.String,System.Single,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern void DOTweenModuleUI_DOText_mC0932E45473278B2FFB1698954CC0EB0422C626F (void);
// 0x00000293 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m4C5E9A68BFBEE876E1FF8D8E9A4BA77856F144C1 (void);
// 0x00000294 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m18196516A24780A550314E9327868D6D6F3EAA4E (void);
// 0x00000295 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_mC30D1F805FE976FB25EC361AB0ABA94413361B6F (void);
// 0x00000296 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/Utils::SwitchToRectTransform(UnityEngine.RectTransform,UnityEngine.RectTransform)
extern void Utils_SwitchToRectTransform_m260A15449F1C7A8F4356730DF4A59A386C45D200 (void);
// 0x00000297 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m14E6397A137D3CB8A7638F9254B4694553B4DC7C (void);
// 0x00000298 System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m6F0CD242FAEF332D726AD4CE2EB2691CCB3748B1 (void);
// 0x00000299 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mDBDE14547A6DF70C0ADB82403282C26FBB4F6A27 (void);
// 0x0000029A System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mCB63EFBBE399EEB9892054E0FAB2AB42E2BBB886 (void);
// 0x0000029B UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_mE8D0790E2303D84F8D513854D9F004E5E3127A3C (void);
// 0x0000029C System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m07CB6A64D93955BC07E8AE290B01F9F39195A5A9 (void);
// 0x0000029D System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m61C588C0764BA908B02EBAB1C19076EBB2898A9E (void);
// 0x0000029E UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_m85CC4106263DD670C5BF648B8A64C6BA84297B65 (void);
// 0x0000029F System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_m140BDA0B825925A5051A2565C055CCBBF005A680 (void);
// 0x000002A0 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m09228DABE56F6866E09591FE27C255A39A71E48D (void);
// 0x000002A1 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_mA7BF7CEB8AA94694104E8D048F5045D122A5D980 (void);
// 0x000002A2 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_mBC7108789E76320E9507A9087273C7D89227F0F3 (void);
// 0x000002A3 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m5CD451B862F3CC71D8D25BC3A69725FCDD6144C4 (void);
// 0x000002A4 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m507CD55516001BB4A76D7E58CB159B5394D6413C (void);
// 0x000002A5 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_mE65F1E4D380EC305AF0AE93E0315B0B23CA36310 (void);
// 0x000002A6 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m879B3843E0C43157D8044AD52E2F865EE50FD041 (void);
// 0x000002A7 System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0::<DOFillAmount>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_mAFE3629BC44AE61C84399809F742CEF0235004E1 (void);
// 0x000002A8 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0::<DOFillAmount>b__1(System.Single)
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_m9EB6C21BCE8D1A08FF84815C05A6664637C510CE (void);
// 0x000002A9 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m47B2E10FFFD9FCACC88586DEF6A4E675DC9EC406 (void);
// 0x000002AA UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0::<DOFlexibleSize>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_m7A7EA5F66922413743B8C1AB4234645A32098EA8 (void);
// 0x000002AB System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0::<DOFlexibleSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_m0E76B7C6F893FD2F7F24E302CB55882765DD1153 (void);
// 0x000002AC System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m0953E12771619B6F14FC14CA853AF5CA110A0553 (void);
// 0x000002AD UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0::<DOMinSize>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m9BD9F63B884498F9AA02C307EB1CA15FA29BD094 (void);
// 0x000002AE System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0::<DOMinSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_m546E530F9DE46CA1359450860E62DCC3BD3D72B3 (void);
// 0x000002AF System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m3EB28823EF4D152A06983986C8120490212A8DF7 (void);
// 0x000002B0 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0::<DOPreferredSize>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mB87EAB728455B68D07773FA10DCA169804482CBD (void);
// 0x000002B1 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0::<DOPreferredSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m24617F67F48227A961C0543BF7925F8D2F2A90B6 (void);
// 0x000002B2 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m11212BA4C7EB97E0EE079E78FA1C95C02D517C75 (void);
// 0x000002B3 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_m08737F267CCB60C0CA0244070DA3C50845FC8B1F (void);
// 0x000002B4 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_mAFFD9D2D2F3FD2BE4E2423286098B9BCC1C0C193 (void);
// 0x000002B5 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m9B3BB08FF1DC2F3101E0D580891268445B7763CA (void);
// 0x000002B6 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_mA57545026A9AE0CB3A20B6FFCDCF6F2F1CDA6AB0 (void);
// 0x000002B7 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_mCC70305FC1C1B93A838838519D023AC608D3E23E (void);
// 0x000002B8 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m05F6F905C11A344C5681CE1CD406DE85940356E5 (void);
// 0x000002B9 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0::<DOScale>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_mEE927D8596DDB335403416BF63FF4E7E27D49D51 (void);
// 0x000002BA System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0::<DOScale>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_m65C60213296203590F596B6A3481E4B8621F96D5 (void);
// 0x000002BB System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_mC6D360265628BCFBFDD42CCAF1136983FDD186BE (void);
// 0x000002BC UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0::<DOAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_m693A41C7FCF87BDE6211DE0ADED05AA5C476D71F (void);
// 0x000002BD System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0::<DOAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_m0F8F9CA39BC8894705C1EE9983E59D24A7BF218B (void);
// 0x000002BE System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m3BB3CA37E5912C211B954D7EE850C77F0B04BFF6 (void);
// 0x000002BF UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0::<DOAnchorPosX>b__0()
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m02BA203D0A6C9D64BC8149C197099DA29370C872 (void);
// 0x000002C0 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0::<DOAnchorPosX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m47B6E3E511B7E234B49FDBD4D6BC32E5EC92B1E9 (void);
// 0x000002C1 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m011D15F0644147EABAFA4ED415CD3F5E8782CCE8 (void);
// 0x000002C2 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0::<DOAnchorPosY>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_mCBF0F2691FBC7AED64284B0FB4CCD75792AAB51C (void);
// 0x000002C3 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0::<DOAnchorPosY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m0E6D417FD9062B34D8C0E8B58C4C688B39CD9603 (void);
// 0x000002C4 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_mF4FDD3E32D1C9FDD48079A148410AAB0CF4855B2 (void);
// 0x000002C5 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0::<DOAnchorPos3D>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_mFBE3FE1AC3F56C148108234A9A1285F87EEF50E8 (void);
// 0x000002C6 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0::<DOAnchorPos3D>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_mFEF34D901D1E18E74D7E93297BDD75ABF146514C (void);
// 0x000002C7 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_mEEC98B74C2A5EDE984A5A59F4289ADFADAB3F804 (void);
// 0x000002C8 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0::<DOAnchorPos3DX>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_mC8D86325D798D952B390643A8BAE1D995368D36A (void);
// 0x000002C9 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0::<DOAnchorPos3DX>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_mAB78FA2C6EEC74D6E259B2ABF94ECBD3CB62DB82 (void);
// 0x000002CA System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_mE272C380064D4945F3C7FDC2357A8B765BC52841 (void);
// 0x000002CB UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0::<DOAnchorPos3DY>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_m8C53152ED72D09B8F2BE2DE14963561650D0C42B (void);
// 0x000002CC System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0::<DOAnchorPos3DY>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_m9E1FC98A6DF461320AC4B6F294F1A69AFD058E3E (void);
// 0x000002CD System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_mF4A0F29D8F3315E7DD0BC8103DA0023E16C341CB (void);
// 0x000002CE UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_mD27E4DF91E21C64D13997EA12A7659B1E31AA77B (void);
// 0x000002CF System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_mD6F74CB34BAEA91A512586538770AB27FFB68A1D (void);
// 0x000002D0 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_mCE70C1CB9A605F65BE4D31D224111EE4C6FB7DE6 (void);
// 0x000002D1 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0::<DOAnchorMax>b__0()
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_mAB5724DF7472A7CED2070B75A1B0E12378D5E6DD (void);
// 0x000002D2 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0::<DOAnchorMax>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_m7E357679D17273D809A39CDF435B5E64E46A64A0 (void);
// 0x000002D3 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_m47D7A3B4DA7AE2FE2F83164406834CF9150E5308 (void);
// 0x000002D4 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0::<DOAnchorMin>b__0()
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_m4804C6D34689DD21311DA86CC22008D3B0774391 (void);
// 0x000002D5 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0::<DOAnchorMin>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_m73EF1691143A0E7E0254A73F52FDC73BB5AAA3E6 (void);
// 0x000002D6 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m0D4210D174301CE26FA7A519B4168E7D87F6D92A (void);
// 0x000002D7 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0::<DOPivot>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_mB3D6F3AADE069EED625038A53491AFEA1DAFD9DF (void);
// 0x000002D8 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0::<DOPivot>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_mDCA8630A9398A411ACB0950973AB99C0723CC074 (void);
// 0x000002D9 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_mA47954A6646E43342880718EE9B5E66A0D18FDF2 (void);
// 0x000002DA UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0::<DOPivotX>b__0()
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_mFC16CCA332D908526AC3DC2BD8B4456969000CF5 (void);
// 0x000002DB System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0::<DOPivotX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_m7E0DAFAB836AC4F133B995B50578D8911BA3113B (void);
// 0x000002DC System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m0524772292383368F9C9F6BDFF0A114D60A35F8F (void);
// 0x000002DD UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0::<DOPivotY>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_m959088FDF2C1CD6EF40EE35BA531719E60C5327D (void);
// 0x000002DE System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0::<DOPivotY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_mCECF34E3A27F03AA2DB5719CA1241927DC99BC5B (void);
// 0x000002DF System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_m1ABD5A4A91526CA156C55C23A54822DC898A34AF (void);
// 0x000002E0 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0::<DOSizeDelta>b__0()
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_mF87E563BB15F601A91B0DA1012A1604A52FCBDA5 (void);
// 0x000002E1 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0::<DOSizeDelta>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_m26B3B99EDB1D47D81931CC6CABFED8406BF4E90E (void);
// 0x000002E2 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_mC4836A2FD37334971703D95C49ED35BCAD56AFB8 (void);
// 0x000002E3 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0::<DOPunchAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_m357B263CA209B3E6EBB7F95AC352AA8B28A5CFC5 (void);
// 0x000002E4 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0::<DOPunchAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_mE7127F5C6AB73DBA7D40F67FD61BA4076E2B8D23 (void);
// 0x000002E5 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_m07ADC405B65035A17DC97097DED9B4C1D21EFB17 (void);
// 0x000002E6 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_m1AF2B166591EEA50BEE7FD3C2E9B7EFFC0D39F8E (void);
// 0x000002E7 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_mF7180DEAAB00C14DD6F3DB8B815D0C0B2DB64725 (void);
// 0x000002E8 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_m4969157094B09547B26225881BD87025C928B506 (void);
// 0x000002E9 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_mB4E05CBCDBD4732D154E1A64678339D3FC5FBCE9 (void);
// 0x000002EA System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_mBDA6378719836E14C155B4648BB47429C125590C (void);
// 0x000002EB System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_m00E6EAFA57923B235554FA8A236AD64992F363FE (void);
// 0x000002EC UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_mAE0CBA3D52D03EB2F2C37D85C5832B412F816591 (void);
// 0x000002ED System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_mB685B77262E778382FAF923C04FD328DC494426C (void);
// 0x000002EE System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__2()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_mB23D92AF70DCD33F838372FF59B1F39DD3775824 (void);
// 0x000002EF UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__3()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_mB86E86358B9C1671706EA489B6FC30C3874224E7 (void);
// 0x000002F0 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__4(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_m1E0D9286E08822DF086E86BA60F0EFF6B62A32C7 (void);
// 0x000002F1 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::<DOJumpAnchorPos>b__5()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_m6E6A274B0B852663D3ED8CDD2B4646B9D618E535 (void);
// 0x000002F2 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_mCC70550D8AD7B4F88C913940A644E81F11F5898A (void);
// 0x000002F3 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0::<DONormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_m92C7B1817960CE4D1D7333872D37C8AF131F68FE (void);
// 0x000002F4 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0::<DONormalizedPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_mFA97248E3617865FEF6C3F36E633266FF044050F (void);
// 0x000002F5 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_m82279F879253DF896CCD126EC0B7E16B295D165A (void);
// 0x000002F6 System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_mB33367FCE1348BA9F855A9D1366FA19F5DDA4AEB (void);
// 0x000002F7 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_mEBA7CDAA06332B1E56991D8E2299E24B9348D627 (void);
// 0x000002F8 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_mD93F08EDF6689C6BA223067191EE8C9E0FBE509C (void);
// 0x000002F9 System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_m83DD02084574CCFB9F26C19BDE6AF6DE2C6387FE (void);
// 0x000002FA System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_mAE94A6E7E27147AA530D056E286A00C8EAE56331 (void);
// 0x000002FB System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_mB387BC26BCA1B460D5B3B1CE767228FA244B7703 (void);
// 0x000002FC System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0::<DOValue>b__0()
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_mBB603DBBDA542D7F00142102A667BCB9682EC145 (void);
// 0x000002FD System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0::<DOValue>b__1(System.Single)
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_mA3DF76574D5E76992AF228A3C4AFFFC19D6BAE15 (void);
// 0x000002FE System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_mA88FD675FEF2971D3F8145822B844ECFFF59BF17 (void);
// 0x000002FF UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_m5DC2ED4C56B0A1D8CFCB8A4E2D2078EA79E8B66F (void);
// 0x00000300 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_m9BA1BADA179249B9F0AD1142BE826798FA114E13 (void);
// 0x00000301 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_m5533AE6A72AB74A97282032790D0DE81F0EAF811 (void);
// 0x00000302 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__0_m8E531F7061E585AC849F3B6C9CF458C5C0C5A410 (void);
// 0x00000303 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__1_m018514D25780FBD3A4D3A83A9209FCA89FCB163E (void);
// 0x00000304 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_mE2912C6BF4318E28870DE7AA92E4B05C80D92EE0 (void);
// 0x00000305 System.String DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::<DOText>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__0_m576159CD95104376F24DEFFAC786AC5D91B2036A (void);
// 0x00000306 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::<DOText>b__1(System.String)
extern void U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__1_m564BDB58BF1EE683A9DE9DB1691CDA8216CE34CA (void);
// 0x00000307 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_m59CE3FBA46AF3F7C3597AD84DEC898DB9B84EE39 (void);
// 0x00000308 UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__0_m7D79A486598AC337B32C2930F8CE462DDF58D961 (void);
// 0x00000309 System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__1_m552CD0C3E6A65CF57519CFAD7B9636DD697E1D47 (void);
// 0x0000030A System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_mB3E91AEAFAE65DD5FC36DD530E74CE1F4FA24AEF (void);
// 0x0000030B UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_mB976655AEC70D25B43AAAF577CC953C7EB03D2EE (void);
// 0x0000030C System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_mF14436C4F4EEB5B7664C3444B969B694BC6E3E5E (void);
// 0x0000030D System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m1DB7B55A6B3E4B184E35968EACBF9E214E0CED7D (void);
// 0x0000030E UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_mE93AC95325F69D83EE746421FA5992DC25009961 (void);
// 0x0000030F System.Void DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_m45DD6E7ED579816AC18DF6B88901D9F11DB1B36F (void);
// 0x00000310 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_m7E9B0D004D4B3D602EBB852CA1755FE863C6B839 (void);
// 0x00000311 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.String,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_mA4E191E86F2EF81966259A40B2CDADE375437920 (void);
// 0x00000312 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForCompletion(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForCompletion_m4BCEDFA5ED1ACF4BC1B3531C0C306A7AC553C1F2 (void);
// 0x00000313 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForRewind(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForRewind_mD9F37D5AAD9B665EE07F083D026E65194A8E3E6E (void);
// 0x00000314 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForKill(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForKill_m02BD7F3B018BB6B4ED4FCAF47B8DA435EC60BD6B (void);
// 0x00000315 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForElapsedLoops(DG.Tweening.Tween,System.Int32,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForElapsedLoops_m46D46788899029A20F73BE252F7427D853131404 (void);
// 0x00000316 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForPosition(DG.Tweening.Tween,System.Single,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForPosition_m1771625E294B38C5D13DCA40846AEA881C1B23E6 (void);
// 0x00000317 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForStart(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForStart_mB4AAFE83B50AEF3A167F31C14B2F58794527F905 (void);
// 0x00000318 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOOffset(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOOffset_m09CA228EBED193314D3D9A41BBDC97393E1FC4D2 (void);
// 0x00000319 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOTiling(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOTiling_m6A3CD06EC3030A93EC3E93DE678F34048D5C1141 (void);
// 0x0000031A System.Void DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m0EACBC706CDB7B8BCE4C21D3AD250FE25CD825CF (void);
// 0x0000031B UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0::<DOOffset>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m6D616025F8610086B30B83BC8ED13213DB2AC7CC (void);
// 0x0000031C System.Void DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0::<DOOffset>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m4171C9EB0226E0E06E574EFD507A8D352EC5B557 (void);
// 0x0000031D System.Void DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m6CC810F5A3B1779C2569B21779361FD5F94C2C9C (void);
// 0x0000031E UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0::<DOTiling>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_m747CC91BE7EDF7D2644045B72BF689A2552B855F (void);
// 0x0000031F System.Void DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0::<DOTiling>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_m0D5FC9115C0FFE113CA2277BA5A17562550B19F6 (void);
// 0x00000320 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForCompletion::get_keepWaiting()
extern void WaitForCompletion_get_keepWaiting_mCA7642C5A8C37F8C0A2CAE990CE1CB6AEE8FD2D9 (void);
// 0x00000321 System.Void DG.Tweening.DOTweenCYInstruction/WaitForCompletion::.ctor(DG.Tweening.Tween)
extern void WaitForCompletion__ctor_m818111A77A3380EE626346FE03A1A604BB896A1A (void);
// 0x00000322 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForRewind::get_keepWaiting()
extern void WaitForRewind_get_keepWaiting_mB480319CB28155CA977F94C7FA03EE5353AA1285 (void);
// 0x00000323 System.Void DG.Tweening.DOTweenCYInstruction/WaitForRewind::.ctor(DG.Tweening.Tween)
extern void WaitForRewind__ctor_m66B575E497C363CB5137629B4D6A00D13B7CD5AE (void);
// 0x00000324 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForKill::get_keepWaiting()
extern void WaitForKill_get_keepWaiting_m7979151F1AD842D2E8004FE37A2C51B47AB36647 (void);
// 0x00000325 System.Void DG.Tweening.DOTweenCYInstruction/WaitForKill::.ctor(DG.Tweening.Tween)
extern void WaitForKill__ctor_m1C9624CE32A1C83CEA14BB5EADD587B6AD79D829 (void);
// 0x00000326 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::get_keepWaiting()
extern void WaitForElapsedLoops_get_keepWaiting_m6F7A59CCCC45BBA5125C6FC7AB667CD24359E8F4 (void);
// 0x00000327 System.Void DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::.ctor(DG.Tweening.Tween,System.Int32)
extern void WaitForElapsedLoops__ctor_m8E720B450DD1350EE81EC3CCB5B6280BE5C51D8B (void);
// 0x00000328 System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForPosition::get_keepWaiting()
extern void WaitForPosition_get_keepWaiting_m34DFE8356EAFEE916828BFAF4A17A822B47AD687 (void);
// 0x00000329 System.Void DG.Tweening.DOTweenCYInstruction/WaitForPosition::.ctor(DG.Tweening.Tween,System.Single)
extern void WaitForPosition__ctor_m94DD0A05EF293B8AA83F343A12015C107AF7FDB8 (void);
// 0x0000032A System.Boolean DG.Tweening.DOTweenCYInstruction/WaitForStart::get_keepWaiting()
extern void WaitForStart_get_keepWaiting_m59700AA1AB726A22C76BFAA0C52FCA460F6E337D (void);
// 0x0000032B System.Void DG.Tweening.DOTweenCYInstruction/WaitForStart::.ctor(DG.Tweening.Tween)
extern void WaitForStart__ctor_mD7AB17A603CF22568EEF0D9861C49F6CFD632284 (void);
// 0x0000032C System.Void DG.Tweening.DOTweenModuleUtils::Init()
extern void DOTweenModuleUtils_Init_mFF2188F42FA7128FE1A9BBC2CB9E6351919C9D15 (void);
// 0x0000032D System.Void DG.Tweening.DOTweenModuleUtils::Preserver()
extern void DOTweenModuleUtils_Preserver_mBE8D1FE2AE913FBFFCCF43B5A615D941B3706F43 (void);
// 0x0000032E System.Void DG.Tweening.DOTweenModuleUtils/Physics::SetOrientationOnPath(DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform)
extern void Physics_SetOrientationOnPath_mCC376173A621DA244564EDF8A6347AB2A0F47816 (void);
// 0x0000032F System.Boolean DG.Tweening.DOTweenModuleUtils/Physics::HasRigidbody2D(UnityEngine.Component)
extern void Physics_HasRigidbody2D_m86FAA0450979B8AFE6A9EF5E27837387C57765C1 (void);
// 0x00000330 System.Boolean DG.Tweening.DOTweenModuleUtils/Physics::HasRigidbody(UnityEngine.Component)
extern void Physics_HasRigidbody_m3082D72A658CCDDF6BB1F3D4A3EFBD6397D6E862 (void);
// 0x00000331 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModuleUtils/Physics::CreateDOTweenPathTween(UnityEngine.MonoBehaviour,System.Boolean,System.Boolean,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void Physics_CreateDOTweenPathTween_m94D41B65E37500E5ACCF6B6F3B5D53FB031C4245 (void);
static Il2CppMethodPointer s_methodPointers[817] = 
{
	IAPDemo_OnInitialized_mF4B746A03FD737884C8142D7CBDD0021D31CDCA5,
	IAPDemo_ProcessPurchase_m70453229C85ABFCB4613956783461A64D2007EE8,
	IAPDemo_OnPurchaseFailed_mAAE3F514875596D53DD6A16120A5BBB0B0065A98,
	IAPDemo_OnInitializeFailed_m541F64EF4EEDFC57AA3767D4D525DC3C424AA642,
	IAPDemo_Awake_m005133078B2E616BF5075EC2ADD13063B7CD7B5C,
	IAPDemo_OnTransactionsRestored_m9550142897B9C2FA2D4AFEB4CD9584398CF6C71B,
	IAPDemo_OnDeferred_mA20AF9D1EF45E4A44FD75B3D38D7586797AF97FC,
	IAPDemo_InitUI_mE24DC740110BB8636B4A775353A2AFBBC2BA58BA,
	IAPDemo_PurchaseButtonClick_m874B72DC4ABC47339984EA54E6AFBD754E95204A,
	IAPDemo_RestoreButtonClick_m14DF5807196B157120C2D7DBE75E5688E588D5D6,
	IAPDemo_ClearProductUIs_m554CF87F773E47071C5F3982D7880162FDC14A2B,
	IAPDemo_AddProductUIs_mC7252C99C2160AF51682D429EDD14CE657316160,
	IAPDemo_UpdateProductUI_m36C323106D395A613F1ACF62600932CE17BA9351,
	IAPDemo_UpdateProductPendingUI_m5F428981126AB91982C3502B586901D41A834F78,
	IAPDemo_NeedRestoreButton_m91A5AADF696AD26A93E82EFCE47AF22DC7D8B8B1,
	IAPDemo_LogProductDefinitions_mBA524D8D904C3ECD52B6180920834FF1028386C6,
	IAPDemo__ctor_m3CDCBE3618DDBEEE75CA94F38A81B5E148BDEB68,
	IAPDemoProductUI_SetProduct_mB5173BE25A6C23AA08D644D7EA9F45A441AC3B06,
	IAPDemoProductUI_SetPendingTime_mFC56B6E828A8230D0D6E519778E86010BBB33205,
	IAPDemoProductUI_PurchaseButtonClick_m24D7B1B2C2CD35DD44CC194701E575F260C29237,
	IAPDemoProductUI_ReceiptButtonClick_m5A451F87378B856BD60F5F6C539881CF21CAC06D,
	IAPDemoProductUI__ctor_mB354C957BDA3FE36DE9FDCD1806798EE31DF4AD4,
	BrownAiBikeScript_get_Instance_m465D862301EB04F4BE7D6727BD296516F14D806F,
	BrownAiBikeScript_Start_m07065169D8E07AF72BCABA2E99A6ED1058E71AC8,
	BrownAiBikeScript_Update_m1D612427365CB6C2AFA33285B2D4A07C450E6F66,
	BrownAiBikeScript_OnCollisionEnter_mC4C54423ADC14AD72FF8854027BD184D271D711E,
	BrownAiBikeScript__ctor_m0FF389698CF34FD5CC986A5C55F2142DF49A4B02,
	BrownAiBoatScript_Start_mEA716F2F595A10E7F5036D445EAAAA3B68E87D9E,
	BrownAiBoatScript_Update_m8A8BDCB7E37FE34E934403AF85B25E608834471B,
	BrownAiBoatScript_OnCollisionEnter_mF758051EF4E55843BE33F9A770F0D42998C09808,
	BrownAiBoatScript__ctor_mCF6852D83F5FE727C88D3EE6C96233EA4E286D7F,
	BrownAiCharacterScript_get_Instance_m1F0C20DF2C35A5FE5830A37426CDE5EB59FD7ABD,
	BrownAiCharacterScript_Start_mCDD26B6269B0C12BAA4CB00513EA004760AC100A,
	BrownAiCharacterScript_Update_mB609954FE819F11360C810C30E65E68E57FE79B3,
	BrownAiCharacterScript_OnTriggerEnter_m493718247AF0CF260CEB8A4C9DD1ED72F8BEBEFD,
	BrownAiCharacterScript_OnCollisionEnter_mCAEC6986C441673E8BB84598DE5C173506E089F4,
	BrownAiCharacterScript_Climb_mF32FD5E54388B9ECFC9717EED50EC25A254B6411,
	BrownAiCharacterScript_IsClimbing_mE2168EA8FC2E7E70A1C69455C9FA9685BA147C76,
	BrownAiCharacterScript__ctor_m942C1612AA2E9439EFE791AE29B0185277EF81E5,
	U3COnTriggerEnterU3Ed__11__ctor_m58CE187FD101838CFCA77561782A057DBE065674,
	U3COnTriggerEnterU3Ed__11_System_IDisposable_Dispose_m1B1ED971F3313F0B1835963EC4B2C73FA501A814,
	U3COnTriggerEnterU3Ed__11_MoveNext_m19D97132558F55C5FD729C1B0589F1C403E6C95B,
	U3COnTriggerEnterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA19AE93EDBD946686F0548C8A3C8CF793C75AB6,
	U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_Reset_mDF0157FD691D1AA9DA10ED0CD2D171E2DAC63F6A,
	U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_get_Current_m362197B045B5D2FE9811917DBDAE5ABF6166D8BE,
	U3CIsClimbingU3Ed__14__ctor_mF524A37C748D43169BC4BEE0CD4FA5DDFA04969A,
	U3CIsClimbingU3Ed__14_System_IDisposable_Dispose_m4DC1A0A554B0D891F7D55A5706870108D70CB307,
	U3CIsClimbingU3Ed__14_MoveNext_mEF60A47A96781E2E74394D638FE12328053CAEF7,
	U3CIsClimbingU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6AC86978016C16339DB835397D58344D4CE84CEA,
	U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_Reset_m3EDEF13B8E0F81E027857D03E2AE87CC2E650733,
	U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_get_Current_mEA8103E3D058EAD5EA2236B4F85784722145ACC7,
	BrownAiControllerScript_get_Instance_m46541BE21636C5BD6E3151F56D24C33A030B06A0,
	BrownAiControllerScript_Awake_m3D543C48F570DEA660AD063E9F0B2F4236C9D1E7,
	BrownAiControllerScript_Start_m759039CF18BC84CF3002EC14B5F87F8C2843FCBE,
	BrownAiControllerScript_Update_m59A7ED9F249B23A9C20C8186BB842060D1116758,
	BrownAiControllerScript_GetCurrentObject_m6A87B70C034AA7E373E45A945C5AF8FA12D50C34,
	BrownAiControllerScript_GetRotationalObjects_mC4988CE8149DD7D1452AA46804BFDA7AAD6BF92D,
	BrownAiControllerScript_ChangeShape_m37BAD59EBF171000FA51CA38BDD069F96D9468B3,
	BrownAiControllerScript_MoveObject_mE73BD22EC65468F1DB77EC6F56E369F3372B960C,
	BrownAiControllerScript_ReturnIndex_m2CF125966787084BBE4236948D567E020B3C4ACF,
	BrownAiControllerScript_DetectType_m644D069E04A05C6782D903AF2F89C94E1C64FACA,
	BrownAiControllerScript_Transform_m1B6BB668C02AF1A1B1D119A58FEEB654E580C844,
	BrownAiControllerScript_TransformToRandom_m235F4E738DF91B1007DBC556DE0015637C55A285,
	BrownAiControllerScript__ctor_m0AAA2FDE0535BD60BF34041EB7F17CC0F78DEDB1,
	BrownAiControllerScript__cctor_m7130E0A8851ABAAC3B7A856A052209E240C6E33C,
	U3CMoveObjectU3Ed__35__ctor_m09BED1D81F93D8216B7F0FAEE530C1CBF188A422,
	U3CMoveObjectU3Ed__35_System_IDisposable_Dispose_mD48351D86E92E28331F2E598A87C699CDA727F75,
	U3CMoveObjectU3Ed__35_MoveNext_m4C620981279C8B015E0347F6D9092D33DED21095,
	U3CMoveObjectU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD24BAE8A82B777887972D591C6FCF0BF84E28EE,
	U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_Reset_m3AFFAEDB3F53A4EEE4966EA6E27C59B135C4AA95,
	U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_get_Current_m98A9BA1D1B9DEA461AA45606CD526DF70322DD08,
	U3CTransformU3Ed__38__ctor_m997EE86AA60658A13C21ED38BEC132A57A81EB01,
	U3CTransformU3Ed__38_System_IDisposable_Dispose_mB8A89A2B908D9F9289714BC710A4AD681841105A,
	U3CTransformU3Ed__38_MoveNext_m2D69E9C998DDB57DA83795B8C7C270BC2C40EB59,
	U3CTransformU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC790BF887E6356F5D81DBAF95C2DDBE7636E02E6,
	U3CTransformU3Ed__38_System_Collections_IEnumerator_Reset_mB23DA13FBE8C5E65C1F051D050FB12C855C291F2,
	U3CTransformU3Ed__38_System_Collections_IEnumerator_get_Current_mEBD9114DE61869220216C93567201F39A0ACECB0,
	U3CTransformToRandomU3Ed__39__ctor_m551C7C45B492150BA04CEF5B3E2CCE43AF524DAF,
	U3CTransformToRandomU3Ed__39_System_IDisposable_Dispose_mB8D6EE430E1F60115A97625706C08E4175D9C266,
	U3CTransformToRandomU3Ed__39_MoveNext_m296CB708FEB96C582ADB4995DB0B386EDE54F080,
	U3CTransformToRandomU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE802F8C275160DBFE5A0E861778DB93AA30CA2A2,
	U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_Reset_mF8829DE38EDE66FA018654CF651988DF3775B13D,
	U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_get_Current_mBE8313C35CA232335D73F1F6529F45FC8E830ED2,
	BrownAiGliderScript_Start_m9E5627E39FE69319916EBEBA22ED2CBDBC51288A,
	BrownAiGliderScript_Update_mE5F6F4E203F6004263EBADF3C42DCB28E3AB3CCE,
	BrownAiGliderScript_OnTriggerEnter_m11F456F2DB8E89F43230EADD9333434268BED3E4,
	BrownAiGliderScript_OnTriggerExit_mB1252AA024D000B386F53ED42FCA027D42699A03,
	BrownAiGliderScript__ctor_mD3E4949B1E044CE8BD7AA24D5E19284111582058,
	BrownAiHelicopterScript_get_Instance_m753ABCE852B9042C09EE59469B04AEF4721621EF,
	BrownAiHelicopterScript_OnEnable_m60C6FD3D0FCF3E2C10A7DF854ED901BDE7EBAE54,
	BrownAiHelicopterScript_wait_m2F59CFA6957E47B9D0B98D2DB0CBC02F566206E1,
	BrownAiHelicopterScript_Start_mBA76D82F6E94B035F892E7A07EC033302C3FB65D,
	BrownAiHelicopterScript_Update_mA6144E54255200ACA800461DB6755CB699F0F654,
	BrownAiHelicopterScript_BoostHelicopter_m62813B1028820F5FAAB00EFB571D736490B84DB3,
	BrownAiHelicopterScript_BoostHelicopterNow_mA24205F67B91CA8E96E37EE86EF4FEED72487AF8,
	BrownAiHelicopterScript_OnCollisionEnter_mAE4ECE86AC43C139BEFE4CFA472C20AC882C5411,
	BrownAiHelicopterScript__ctor_mB5AE4AAE800753E77F7A05EE89845E927A05BDCA,
	U3CwaitU3Ed__8__ctor_m9CC191CFC8A05A0F2F712CB78E3D3C712D1B3BA8,
	U3CwaitU3Ed__8_System_IDisposable_Dispose_mB7ED5E1352F730692DA3719E632FE932074ECEEA,
	U3CwaitU3Ed__8_MoveNext_m2364624663E1BE42FED765DDA67B47BFF8A445D1,
	U3CwaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD9CC63EC2CA3F08870E2F09F5419F073045356EB,
	U3CwaitU3Ed__8_System_Collections_IEnumerator_Reset_m8EDAC25117B4E697F2F433B11665FD810FB4FDB5,
	U3CwaitU3Ed__8_System_Collections_IEnumerator_get_Current_m2759321C2416D426BC60F3045E046A528C23E938,
	U3CBoostHelicopterNowU3Ed__12__ctor_m59B5CA3733B514DC5D43AB2BC40265EE4B6324CD,
	U3CBoostHelicopterNowU3Ed__12_System_IDisposable_Dispose_mEFD999D4EF533ABFE2E27B1A50696CD34CF077B3,
	U3CBoostHelicopterNowU3Ed__12_MoveNext_mABE08554EA1FDBB49B3100B7C2C571EA1755595E,
	U3CBoostHelicopterNowU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31180F90051ED150F895B5D6062E8C7EB35A6099,
	U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_Reset_m7F1A5716FF8139EDB930D8D75E2DA42121B3CA40,
	U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_get_Current_m4B7EE880FE971084FA8869BB14C0B17EE6A9665A,
	BrownAiJeepScript_get_Instance_mD082B7B35C051BADFB068B1BC6893417CDCF5335,
	BrownAiJeepScript_Start_mDD9235B66859E5727732766FBB9FBE5DC8AFE16C,
	BrownAiJeepScript_Update_m3B8F71E1F81B5FDBC77E85A3E83BEFCA2F34C1FC,
	BrownAiJeepScript_OnCollisionEnter_m959AE786C9705571472971DCDB3B175F8BB8647E,
	BrownAiJeepScript__ctor_m4E70C9FC72FC2DE9558EB4DACCBC4A6266E22F1B,
	BrownAiTankScript_get_Instance_mE35F6404DB8E55B354F3D6D8CF1863D5AFB588FB,
	BrownAiTankScript_Start_m463EE5E493A0E08B9A889FE2F3B014FFE3391242,
	BrownAiTankScript_Update_m9F06D1AAA509638C892EAE9307CDB390C38DDC4B,
	BrownAiTankScript_OnCollisionEnter_m5D1D79E1E58A3D8BEC40A44CB137566D0BA2EC20,
	BrownAiTankScript__ctor_mC0C0BBEE68208354FB67ECE7886AB8692F794C25,
	GreenAiBikeScript_get_Instance_m2220F81BDC77145D307EBBC512287C069FE29D70,
	GreenAiBikeScript_Start_m1C2D06B5ED16840E2D98F5FD97BAC3F097314888,
	GreenAiBikeScript_Update_m950625E1A3B2A25054BC0D8F02BE2F2944FBC80A,
	GreenAiBikeScript_OnCollisionEnter_m2C5BEFBF5EF58CA2A0134CC848E59EE4B950A081,
	GreenAiBikeScript__ctor_mF6DDC8106E24176E5DCD537A5AFE58C1310FD06D,
	GreenAiBoatScript_Start_m69008A0260D0430124222AC9A1D52FE940315FFD,
	GreenAiBoatScript_Update_m9700E8364334E0E620A2A77268F582DB377A5CD4,
	GreenAiBoatScript_OnCollisionEnter_m876E9F2B07E70043F599FB53A67B741E8551927F,
	GreenAiBoatScript__ctor_m58A3921C5182D33003EE68A162525941D570BAE8,
	GreenAiCharacterScript_get_Instance_m7D45946E23E69E1A1299FEC7A3E172B13C1AA8D1,
	GreenAiCharacterScript_Start_m87E6BF98B3C4BAF7FC34B08B0B68E95520BD7871,
	GreenAiCharacterScript_Update_mE9637B716F54E0F9640963C343DC3B9E254C9F51,
	GreenAiCharacterScript_OnTriggerEnter_mE41DA5EA2AE137B60804896A6C5B1CAE142271E6,
	GreenAiCharacterScript_OnCollisionEnter_mEE9A6CB31997ED48D0CC78D3F6BFAF2E66999355,
	GreenAiCharacterScript_Climb_m7FAC4633F80D843F982A49EF69767DA52AFFFA8B,
	GreenAiCharacterScript_IsClimbing_m6104549DB6A64AC33BA7A9B683A4CE300389D87F,
	GreenAiCharacterScript__ctor_m3321D7145D9F76F82CA6F93646AAA0EFDC85AFC7,
	U3COnTriggerEnterU3Ed__11__ctor_m9A69DC7769006C0580046888DE33D5B3497E3158,
	U3COnTriggerEnterU3Ed__11_System_IDisposable_Dispose_m1FD5231A948EFABE2395EEDB9C08D376536BB94E,
	U3COnTriggerEnterU3Ed__11_MoveNext_m0510809FD1414952084F6A53826EEEAE6ADC5630,
	U3COnTriggerEnterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m468266D865ED51B5686F8402CA721DCFD254D240,
	U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_Reset_mA18241CE3ADA61237D0B8ABE12D3EEF13C49C69C,
	U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_get_Current_m97498923C25B468E44FBDA18864B3E10ADEB7FB2,
	U3CIsClimbingU3Ed__14__ctor_m101C21898DF05DB161D452C96753EEF525BD7066,
	U3CIsClimbingU3Ed__14_System_IDisposable_Dispose_m6DBD346A64EE02ED6EDA1E3B242DDF77878C0065,
	U3CIsClimbingU3Ed__14_MoveNext_m975BBF993F321C75126744438BCF526082486567,
	U3CIsClimbingU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1470B7C3C6547F5A6DECCCD4E3811F7866E27ECC,
	U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_Reset_mCCC1079F77ABAA01075E34E30346DB874C61403E,
	U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_get_Current_m6BC85BF85A7A3BA1C81AD1A4D9EA1C7D25D7DECE,
	GreenAiControllerScript_get_Instance_mEA3646F25A1CC07B064249F51F75C216CA250737,
	GreenAiControllerScript_Awake_m4728685D9995D393B22D480FD1778F7CF9630A95,
	GreenAiControllerScript_Start_m6921D8EE15D60002B529F91C8A0F3AEBD2585239,
	GreenAiControllerScript_Update_m5B18956006120CAA3C811688F52890AED7D8F2B5,
	GreenAiControllerScript_GetCurrentObject_m2A8E40C5594CA753E1434DEF4766A5F729C0091D,
	GreenAiControllerScript_GetRotationalObjects_m5345403C3CC1250A036B64FD30EFB9DDF3A3BE7E,
	GreenAiControllerScript_ChangeShape_mD7E778512B0EE955AABA85BE09B5A13912F58C5E,
	GreenAiControllerScript_MoveObject_m428A17DBAFDD480889D2DC0910582B073B9BF276,
	GreenAiControllerScript_ReturnIndex_m27B6E458707314A870CA3B2B5E0A0B651C6B72DF,
	GreenAiControllerScript_DetectType_m07238BA4CD8DCD5BA9E7A497507FF4AEB1D618E2,
	GreenAiControllerScript_Transform_m0C755F0EE9BB07E4D98ACD902D5D7D18B73EA0AC,
	GreenAiControllerScript_TransformToRandom_mC3F7D234B52D8EB4EE29EBCE9DEB9F15F1A3F5DC,
	GreenAiControllerScript__ctor_mFD34D056C2E1D68D2EC7679CA2D269BEC3A976B7,
	GreenAiControllerScript__cctor_m23C9C4CB42C3D51BAC48B941FE09CECBD8F3243A,
	U3CMoveObjectU3Ed__35__ctor_m179E5FE2DF01B88D84D0D14765488F47DA4AB678,
	U3CMoveObjectU3Ed__35_System_IDisposable_Dispose_m9D27A724FD86ED921543813A4E7260E9A7F7B5D0,
	U3CMoveObjectU3Ed__35_MoveNext_mAABE0B851B964CE90F5DB9C727F156C00F77DED9,
	U3CMoveObjectU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD70A6C3669063054A3585BC89F8525F8A5223E7D,
	U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_Reset_m817F8AD405448B9EDBC2F07BF7F5F83745689AD2,
	U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_get_Current_m199D40D6301C2AAB59A577024977E61946E95F8D,
	U3CTransformU3Ed__38__ctor_mDA513F4551EA387CBE55ADCA55E2BA755F388B9D,
	U3CTransformU3Ed__38_System_IDisposable_Dispose_m48F34B6CA28107A6D9F588D85396B9C016B27D72,
	U3CTransformU3Ed__38_MoveNext_m7E01E0DE036A36762D874B2CD539ACDB2A7F3B00,
	U3CTransformU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCAC7D326D7467468072FA99AC95AB8A772F0D6EE,
	U3CTransformU3Ed__38_System_Collections_IEnumerator_Reset_mBE648FB20A3F7600472F2EEC5B8A27DA34001E66,
	U3CTransformU3Ed__38_System_Collections_IEnumerator_get_Current_m2D984DD67D92B18296EDFFE2E4629A37B611271B,
	U3CTransformToRandomU3Ed__39__ctor_mA913800A026FDE676DCBEE781F12E7DB50359C3D,
	U3CTransformToRandomU3Ed__39_System_IDisposable_Dispose_m2BC9278A0B00FAAC4B9BDAF82C99D175095C258F,
	U3CTransformToRandomU3Ed__39_MoveNext_mC257FD8DA4A75DB07400706F81372C8B083E0C3C,
	U3CTransformToRandomU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m918D1A177676B7AADDB603964ECF77380C9450FB,
	U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_Reset_m7FB7AABD45545A92964F18E329B23FF75F0EF2F7,
	U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_get_Current_mB99F58C3386E898F1FA7C1FD7E8BC1C92D53D7CE,
	GreenAiGliderScript_Start_m9B11DE8162055559022124478920806F143B7E3A,
	GreenAiGliderScript_Update_m202C2D77D2CED82339FDF6468DEA859A2B2EFCBD,
	GreenAiGliderScript_OnTriggerEnter_m4B2B4D6CEFD2DA074BFCD2B6B8F3CD35D2F50FF6,
	GreenAiGliderScript_OnTriggerExit_m0ECB692885700CD388934DFB4DF426222CE26DF9,
	GreenAiGliderScript__ctor_m2DA8A230A01A44FD80F922C88308891DDB99117D,
	GreenAiHelicopterScript_get_Instance_m70714411C064B5CF772F5B19DE1D61FA52769CF9,
	GreenAiHelicopterScript_OnEnable_mA89723C2B7193694787E5EACAB002FCEE44DF8DB,
	GreenAiHelicopterScript_wait_m0AF37D5CFAE7BB8285C0EEFB023914D5828D75A3,
	GreenAiHelicopterScript_Start_m19507899B0773756B3033675FFCF019A23BB09EE,
	GreenAiHelicopterScript_Update_m4DE2B35F4912D43E16482962B3B384F98A80A52A,
	GreenAiHelicopterScript_BoostHelicopter_m7190157C45FE8FFDAF910C043E47C743A8F450A5,
	GreenAiHelicopterScript_BoostHelicopterNow_m2C697483CBC2445C941B05ED38FA730989961DDB,
	GreenAiHelicopterScript_OnCollisionEnter_m2E2D40F6E8134A2BEA4908B49FD71D0EC0B21B27,
	GreenAiHelicopterScript__ctor_m3F4F446FF2CA37F26367B5D8CF351249A388F9D8,
	U3CwaitU3Ed__8__ctor_mDCC058A30FD6F11314464F59B1D944452DEDE31A,
	U3CwaitU3Ed__8_System_IDisposable_Dispose_m32B292493C2C2097837B382D6F3BBD3FE5936571,
	U3CwaitU3Ed__8_MoveNext_m50A36B8C5032A4F78F832B45919DE01F5639677A,
	U3CwaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD11542748D2157BF3ECB8D0449FB8EBBF824C2C6,
	U3CwaitU3Ed__8_System_Collections_IEnumerator_Reset_mD4370B65472947DCCF75489E73F2FCDA3C060708,
	U3CwaitU3Ed__8_System_Collections_IEnumerator_get_Current_mCDA0D7011D71B6E161157D399A915B705D0F4356,
	U3CBoostHelicopterNowU3Ed__12__ctor_mBBE717D794FC2AA7DD4F448CA792743B1729B381,
	U3CBoostHelicopterNowU3Ed__12_System_IDisposable_Dispose_m86EE5D0048CCD362408A8A627CA7C249E7B22704,
	U3CBoostHelicopterNowU3Ed__12_MoveNext_mC577FB3003CB6AA6B9CBEF0BF4FC23489FD6CBF2,
	U3CBoostHelicopterNowU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m757362FC7C672367D082B76F985400626F3704D5,
	U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_Reset_m80DE021A4A053E4CA424F5CE4BBD788881F70BD5,
	U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_get_Current_mACFAF342ACBF07EC3B30B9176B0D4B5374E94A2E,
	GreenAiJeepScript_get_Instance_m6335CB0256C9EF6F4A3822B35454EA5237F6BDA1,
	GreenAiJeepScript_Start_m814472BE906B453B4F3AF65CF113C00730FB2197,
	GreenAiJeepScript_Update_m4EF21C45A2FB195A8682287248A60BD3D6485760,
	GreenAiJeepScript_OnCollisionEnter_m3382E5A97CD7CEDA7261B57C9FE8F11BF1EEAB3A,
	GreenAiJeepScript__ctor_m3B064C69EAF3B99844387B4010828515EB152263,
	GreenAiTankScript_get_Instance_m7B9C328C5C4AD4EABBCA8F957D8C4DA8E09EAB82,
	GreenAiTankScript_Start_m11B28CA7637646A3033C192B41EAA9E948122D14,
	GreenAiTankScript_Update_mA1BAA5453D2F3A9D6B2BE4BF07E1A72F1E088989,
	GreenAiTankScript_OnCollisionEnter_m1A7997C10849AF4EFD1D31D3E57A0BE960DABA36,
	GreenAiTankScript__ctor_mFE0469B4CFD3A1302CF9231B27DF2567A85175B2,
	AdsScript_get_Instance_m4FB4102F5A4BC7E3EE64CBCFC886063457DCACA2,
	AdsScript_Start_mC84EBE63B5A0B689E3F68E85E3394C679EC8AFF0,
	AdsScript_ShowBanner_m0761AD3515781D5E4F3BE15816265BAD74AC006A,
	AdsScript_ShowInterstitial_m63112541FB3ED033CDD9F9E61FDC9923E0CD7DC4,
	AdsScript_ShowRewarded_mB5BE1E972A04023FE55C862CA7AFD9A4B8D205A6,
	AdsScript_FreeCurrencyFunction_mFDC5140D5495D039D8597741F7167FB51C9DC7AC,
	AdsScript_NextScene_mC67E5AF3B0A613B83B6DE848F26B1F7863584519,
	AdsScript__ctor_m8474CB9DAFB51EC3CD40E0AE475BF645A0E9CE34,
	CameraFollowScript_Awake_mAF5739B6CD862895C63CE9C39711463B9FF2D3E6,
	CameraFollowScript_Update_mF192A26F7F4DF05A87DF95EDBDBFAFF059A440D1,
	CameraFollowScript__ctor_mBD431AD8E83D69A7D39DA198B4BAEB4B74509E6E,
	ClimbingScript_OnTriggerEnter_m6E04BBE1C3179B6C0DAC79DD6650CAC68CAFA6F8,
	ClimbingScript__ctor_m20C9500ABB32AFE223D713EF892AA9DD396C9C02,
	CoinAnimationScript_get_Coins_m5D9C9D7652222B61ED25424E7932EDECAF235F9F,
	CoinAnimationScript_set_Coins_m410C3428A0FB8421C96BEEE4B2E339A2ECCE019F,
	CoinAnimationScript_Awake_m7919538DA9664FCEB56C0582A80B8B3649DFBE93,
	CoinAnimationScript_PrepareCoins_m5752CA3010EFEDE2EB829FAC5E11572C2465C326,
	CoinAnimationScript_Animate_mBF079E67A8A691059AFAA70035CA4D72C5026B29,
	CoinAnimationScript_AddCoins_m7608052138776E46EEF6E0C747335C8BDDEF0B2E,
	CoinAnimationScript_Animatee_m6AD35D90C057DC2896113DFE843E49A9BDC67205,
	CoinAnimationScript_AddCoinss_m76D0ADFE473F02A284ABDF4F692A35BB41606E69,
	CoinAnimationScript__ctor_m0DE121A43212AFC42617665FB7601BB2EF586F06,
	U3CU3Ec__DisplayClass17_0__ctor_mF220195862ED81A39CF3C19315F7C2A8752506A0,
	U3CU3Ec__DisplayClass17_0_U3CAnimateU3Eb__0_mCF707375C2A1E6B10EC0E885D9403D3E160798C9,
	U3CU3Ec__DisplayClass19_0__ctor_m28B1E092BF5DB9349ECA4D3B7CE6A82AFDD22424,
	U3CU3Ec__DisplayClass19_0_U3CAnimateeU3Eb__0_m157562F0A58060E353D735C0190E8C9879E086E5,
	CrateScript_Awake_m83765479471475B349FBD3B2CA11780B2D62F16F,
	CrateScript__ctor_mFC2FB8CA3607ECC9573BE49B53B2200BCD561EE3,
	FinishLineScript_OnCollisionEnter_mCF1015F532A4C3CDBEDEE8AE138D70BF13FE425E,
	FinishLineScript_OnTriggerEnter_m3099C24D64C518B5536B56A567D2CA2922B9AF85,
	FinishLineScript__ctor_m24BBEB8E64FECD60502106A2DC03BE5B3BC95248,
	GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232,
	GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30,
	GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E,
	GameManager_StartGame_m6022C5CDD590728691B22E9B87185BFE3D6A8EC1,
	GameManager_InitializeShapeTypes_m2D21708D347EFCAA20EED03A1584D6F3D1617EBE,
	GameManager_InitializeLevel_mFFF07BEA50269AA6085AB7627C09A34E5B3D00E5,
	GameManager_InstantiateCharacters_mFEDA4146335BAB91165D7E4F32EDFBE243514F8F,
	GameManager_ToggleVibration_m391A0DEF2BA4D01EFBBE688A892AF3EFBDCD7E7E,
	GameManager_LevelCompleted_m5EAEF7C26C85CC857CE878FAD5814F0ADE0D47BE,
	GameManager_LevelFailed_m7496C08897FDFDF0BD8B224C91FB1583FCFEBB18,
	GameManager_IsLevelFailed_m70DE3501FE1BEEC60023C7A9F649FD8640D97E67,
	GameManager_RestartLevel_mF2D674BF811DF9B8C3893C1E061BED7E2ACB6D76,
	GameManager_RestartLevell_m5AB99A05E7E5BDEBB8CA7F99D6CA514F1C3A5FD6,
	GameManager_DelayInAnimation1_mD1E86C35E0F2415F93A2FE732B5262F0072CA6D4,
	GameManager_isGameCompleted_m5C2694B3FDB66DED81583ECC138D4BF54463CFFD,
	GameManager_LoadNextLevel_m2DEFC4D28BC1C25B56CBCA894590882BBAC2F7E3,
	GameManager_StartNextLevel_mD9B579F6AFA8F8346C0A922BDBC727F750C52E10,
	GameManager_DelayInAnimation_m49310C2247930BB1BDA080E6CC44AFE4F56B6FD9,
	GameManager_WatchFor2X_mE4756F64167C7D648B1083F384AE32B739539E03,
	GameManager_WatchFor2X2_m914BBF74A2AF546A27D56C2EC50D3E1DF3B58D97,
	GameManager_UpgradeShape_mF068D25B28015654DF48D7FC07F9DF15224E9971,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	U3CIsLevelFailedU3Ed__21__ctor_mD10B2FD438BC1BEE23004BD942AB31491C0A124E,
	U3CIsLevelFailedU3Ed__21_System_IDisposable_Dispose_m076581E89A05A366FBAF808DE98241BDA23E05EB,
	U3CIsLevelFailedU3Ed__21_MoveNext_m67CBBE1D176FC2C5DD1E85C46DE7D5D98725C763,
	U3CIsLevelFailedU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC589789B869BCDF5F0F5F602E0404335DBE4644E,
	U3CIsLevelFailedU3Ed__21_System_Collections_IEnumerator_Reset_m8A3ADC2BABE6D0B8A7B65EEE2CEBEF36F600C629,
	U3CIsLevelFailedU3Ed__21_System_Collections_IEnumerator_get_Current_m5A479CAD77A871F46E639F24FA9DC80CED301DE6,
	U3CRestartLevellU3Ed__23__ctor_mD698104A3344AAD8AB7F375C1FCDFE48A90FEF8A,
	U3CRestartLevellU3Ed__23_System_IDisposable_Dispose_m917325C36BA67A294D14EC471D182B556BCE62D7,
	U3CRestartLevellU3Ed__23_MoveNext_m4A5E1B55D23C57FDDF66C8E117FD7AF3C098D283,
	U3CRestartLevellU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE78F622C02552C6AFC3AC0A35FA99A639B581A06,
	U3CRestartLevellU3Ed__23_System_Collections_IEnumerator_Reset_m631238FEBA0FDA032E30FF7EEA31F50A1E220ADA,
	U3CRestartLevellU3Ed__23_System_Collections_IEnumerator_get_Current_m509B652FC4478E31C51E1EADF6D16C8D8D1F3E68,
	U3CisGameCompletedU3Ed__25__ctor_m2957E84768DAED7DE588D515A6A1CE2B39C026C7,
	U3CisGameCompletedU3Ed__25_System_IDisposable_Dispose_m3ADF2A8F3FA8F0BE8595C6CA0625BC752B5B852E,
	U3CisGameCompletedU3Ed__25_MoveNext_m83EEF57C7729AA525C1B7A9FEC106A54A5100071,
	U3CisGameCompletedU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m42B4BF6B0DC26D917B0E47D89DCF7AD201804E77,
	U3CisGameCompletedU3Ed__25_System_Collections_IEnumerator_Reset_m35439684D43C4B3E3217C106A4C1939FF2C02A97,
	U3CisGameCompletedU3Ed__25_System_Collections_IEnumerator_get_Current_mA00BAAD4560CDC6CF6CC18BC9435689084F4D065,
	U3CStartNextLevelU3Ed__27__ctor_m84E1B3F4F8E539CC4A3741E7A8B6958EB8D0AEBC,
	U3CStartNextLevelU3Ed__27_System_IDisposable_Dispose_m475BB8B13202250A345D0FADAD797D36B9E403EF,
	U3CStartNextLevelU3Ed__27_MoveNext_mD5AB4DE7B97D6B33185363AA4E97B06CC5AD5B21,
	U3CStartNextLevelU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEFE495A03A02293456740B5F11C350835D1396FF,
	U3CStartNextLevelU3Ed__27_System_Collections_IEnumerator_Reset_m720323159B46EA9B529D6511653DB54F79272B18,
	U3CStartNextLevelU3Ed__27_System_Collections_IEnumerator_get_Current_m5B09F71419B61AF21B156A7DF3D43E1995EBE375,
	GliderScript_OnTriggerEnter_m749EBE7B9A1A696FD06C21AFD9BA7354155E8F78,
	GliderScript__ctor_m54A840F90C5A28D26B895FFC315D18F1E897CA61,
	GoogleAdsScript_get_Instance_m38225F756D6B5AEF51C172562F4D5769C7CB2714,
	GoogleAdsScript_Start_mFC852DE5ABD512F608F433DD8194B9073EB68306,
	GoogleAdsScript_RequestBanner_m05A3A6A96CFA1AA6E2A169B022CA6F848E40D73A,
	GoogleAdsScript_RequestBannerForScript_m8F366A0098E5442AE8EDDBDDD8CA8E3E4CC8AFA4,
	GoogleAdsScript_GenerateBannerRequest_m4FD6CC8181CD5E1E355B1B0DE0A1F065722454EB,
	GoogleAdsScript_ShowBanner_m568D43860F0C771CCCEC3E9AA89F125B753A3A4D,
	GoogleAdsScript_HideBanner_m5475ED3CA8E66176A10D5FBE3FFFCB9EFC4D5D85,
	GoogleAdsScript_DestroyBanner_mA335A3F6F4F7306E736522A1952ADE313E18E3BF,
	GoogleAdsScript_RequestInterstitialAd_m38B6EAF1358E475E2FE3AB6210C85959772B6612,
	GoogleAdsScript_RequestRewardedAd_mA2464C990359DF43266ED683108DCB750DF07C7D,
	GoogleAdsScript_GenerateInterstitalRequest_m49B12C39489E5542FF65509E0B6083CC8A7E9167,
	GoogleAdsScript_RequestInterstitialForScript_m4DD273F4D597D1530E340F9D706D4CD0B10A07B0,
	GoogleAdsScript_GenerateRewardedRequest_m77FAA7B0558C72A6D2576B901B85FAD05814166E,
	GoogleAdsScript_RequestRewardedForScript_m27B2345502109F1C6952BAA6844A2FD68F3B6485,
	GoogleAdsScript_HandleOnAdLoaded_mF021FC6DF1B92C80952981DDE32BCDA2C0A84E87,
	GoogleAdsScript_HandleOnAdFailedToLoad_m7B62B61CF4C814C4623001C2B6870FAE837B88C1,
	GoogleAdsScript_HandleOnAdOpened_m61DE882E4DF5FDF69953D08E191EB6EDA3AF658E,
	GoogleAdsScript_HandleOnAdClosed_mAF7C2B2F70C98417C9E5B77FFEA482727A109DA8,
	GoogleAdsScript_HandleOnAdLeavingApplication_mC438D1886BBAE26BCD72D408335FC8FA72769D97,
	GoogleAdsScript_HandleOnInterstitialLoaded_mD5199FB8FD87FED59C29E7E8FA892B6DE3105C64,
	GoogleAdsScript_HandleOnInterstitialFailedToLoad_m5F8FAA2C38EE99E056E6EF9F0843A08537AEF838,
	GoogleAdsScript_HandleOnInterstitialOpened_m6779CA7EC01DDC3408BDC7A1F08D70658998E70A,
	GoogleAdsScript_HandleOnInterstitialClosed_mBB0550CD0BF03A755488F9526143A467C7473FC6,
	GoogleAdsScript_HandleOnInterstitialLeavingApplication_mFB1294456AB2C79187FA04D25C916709B917D07D,
	GoogleAdsScript_HandleRewardedAdLoaded_mCD5876255FECF0FE0DEBC9932679D5E513102FB6,
	GoogleAdsScript_HandleRewardedAdFailedToLoad_m21B78136B6D57E96BC4517DB018ABF1EE00855C6,
	GoogleAdsScript_HandleRewardedAdOpening_m1C3D5EC140BB3705B5C5BD2AA13401F440C0D2EA,
	GoogleAdsScript_HandleRewardedAdFailedToShow_m90A23CCFBBD52283DC30676292CE26F210C3F7CC,
	GoogleAdsScript_HandleRewardedAdClosed_m0E089886060F56B705FEC3CCA823EE7456DE2D83,
	GoogleAdsScript_HandleUserEarnedReward_mD7B47DE1AE53065157875CEF34EF308A9F34B961,
	GoogleAdsScript__ctor_mBBDA372736895DEBC58C4C70DD13270AF7340726,
	U3CU3Ec__cctor_mE534DE5FE344001FFDEAE48CD46F97BEC69DA61A,
	U3CU3Ec__ctor_m582C8D421E0FED66ECA4716038D9B6F7642F7B65,
	U3CU3Ec_U3CStartU3Eb__9_0_m59F252F459F08B008AC2BB7C808C6E4A7131AE36,
	HelicopterBoostScript_OnTriggerEnter_mC579BBD05ED6BC9DBAC6AC761FEAD68FC95DA26D,
	HelicopterBoostScript__ctor_mE08F4190B9CBD0926150A056149E96674AB5B66C,
	IAPScript_OnPurchaseComplete_m1BEE2C1C767AC6FE849ABF2FF08A33017D96507B,
	IAPScript_OnpurchaseFailed_m4D5C5BDC47BE4A75268DE05C706449CCB37AA2B9,
	IAPScript__ctor_m84CE2C939D0F82F5BEED7D8F630697A40D4BF3C9,
	IAPScriptold_Start_mF868E03F2894490F531ED884E06228D55D880631,
	IAPScriptold_InitializePurchasing_m93B12994255E72ACC3D4ADD322938D69F1C8FE4D,
	IAPScriptold_IsInitialized_mA0F0B7FB493885642E6EF4CDB43A01B678109630,
	IAPScriptold_BuyConsumable_mBEAEE13F23E6DC0A31F1950FF3EB0044BD577200,
	IAPScriptold_BuyNonConsumable_m6FD3DD4475950DF96B9ECBC9C25AFAB1100690A9,
	IAPScriptold_BuySubscription_mB8D27214D977075E1FC0A5CDED2D2CAF9A3F7762,
	IAPScriptold_BuyProductID_mA9FB82539BAACB7B911344933DEA2F03EF5EC095,
	IAPScriptold_RestorePurchases_m53059CC2F98C7A87AB2FF766345E8C4006ED1425,
	IAPScriptold_OnInitialized_m756C5DD7545EEA9D9337CA5599730AD3AD9382A0,
	IAPScriptold_OnInitializeFailed_m6FB6FD22E0D121AAC70DC353369910B6638EBA1F,
	IAPScriptold_ProcessPurchase_m66131992633A40D05A69DE7F28DFCA50D86E2195,
	IAPScriptold_OnPurchaseComplete_mEB06379B2D48B0BA574E49134F791E32523C2D34,
	IAPScriptold_OnPurchaseFailed_m423E4E67D947C7CD744C539F05CC7FAEF4285A20,
	IAPScriptold__ctor_m97245D995471F7CE026C0B00C715403675912797,
	IAPScriptold__cctor_m5818E0E8176A61EA2C30BDB87BC91E2A462EFE2E,
	U3CU3Ec__cctor_m7BF686D4B4324D9E7613F4D2E2F8E508C2C72B56,
	U3CU3Ec__ctor_m77B858C4A94863D279C1102185D94C8E3FB762C8,
	U3CU3Ec_U3CRestorePurchasesU3Eb__15_0_m23C59D29921F08DC2215FBBEF3FF1E62156FBD1B,
	ObjectStopperScript_Update_m951A4EA57A3AD465E356B1414753C65E579ADA6D,
	ObjectStopperScript__ctor_m60B54B51B815461E496E2CCDF409C8ADCC666E29,
	ProgressBarScript_Awake_m97295865A342E8E18E37C6737933C30D34F9728D,
	ProgressBarScript_FixedUpdate_mD1ADB68BE4AB6845059FE0E3833C9EBC2B635F3D,
	ProgressBarScript_CalculateDistance_m3A36CA5BA68989B1C139DA14322E3C81092259FA,
	ProgressBarScript_ReturnDistance_m6CB221C387D2E22B6106871C4C3653DBBD237ADD,
	ProgressBarScript__ctor_m88FA30239255C973E8265D3DC01B95AAB223778D,
	SoundScript_get_Instance_m588A31703AF32878E698169D9D192205BB014C0C,
	SoundScript_Awake_m24E6BC9EF3471D211479695810539A871DE914C0,
	SoundScript_PlaySound_m5CB008D0C12148E01A10B47C05226AEB731D3109,
	SoundScript_StopSound_m249154A9771FCD4F97022632E4C6504C130E4EE1,
	SoundScript_PlaySound_m0230F62058D0CC829DB59E4B9351E32F9B3028A1,
	SoundScript__ctor_m18EEFD45E8CB8DCEFAC2D8EDFD7FABFFD76492B0,
	SoundScript__cctor_mE4B974D3325440FB35D0EDEE653EE762941B7D55,
	StopperScript_Update_m039C7712DB09047FAA7E961CBDD09A33367A4931,
	StopperScript__ctor_m47D23717864F42ABB229F0CF88FD82307CEEF6E0,
	TextScript_Start_m6B843464C4DDC4D0D3D94CF6F075786A006E112C,
	TextScript_Update_mE04D85ECA4D06599BADEAA78B9ACDA6B28542FC6,
	TextScript__ctor_m2B311CB37F42D159140EEEBFE5313ECAAA926584,
	UIManager_get_Instance_mF5BAFE20E0B76E9CDCA3DBBF53D5AC853CC5BA7A,
	UIManager_get_totalGameCoins_m422F9B1E2C19370BB7829FC6DD312A5454CADDA8,
	UIManager_set_totalGameCoins_mE7652FAD4B69356DE6285DADEB5987EA6285D390,
	UIManager_get_totalGameFailedCoins_m3C54911483089212BCB097698541C324AC6D2712,
	UIManager_set_totalGameFailedCoins_mB81037D2904B9DE2D1229E40B4296ED0E8840B21,
	UIManager_UpdateUI_m9076046EDAF6AD406801B952F8BE9CC84D9CE60F,
	UIManager_Awake_mCED93604270B1E209B4E0D32F6A26DDC5AB06E30,
	UIManager_OnSliderValueChanged_m6C5F22B93530A25A22A878E9350730DFD55EE544,
	UIManager_Start_mAA4B371DC406146E84A9D8803B9C861939B2E04E,
	UIManager_isGameStarted_mC09A9D63831304BFB0966ED69CF2F2DF5AFA5FCF,
	UIManager_isGameCompleted_m6F7F0CA31571B25BED14841BF832229689D28FF6,
	UIManager_LevelFailed_m3417032B40DD1ECCBB38819D4B1C251B93D0F783,
	UIManager_TogglePanel_m2EAA407393950CA7311F96A049131194D1A9A5D1,
	UIManager_VibrationFunction_m44562D63358D41A14071BDA32EA83CE958A19395,
	UIManager_isGameOver_m4AF6162FFE9091767AEB06E4EAE418DC9A129379,
	UIManager_UpgradeFunction_m411524678FDBCC34E24D716975EFC9D4B40C797E,
	UIManager_ShowCoins_m57EBFCBA6BA1B8F62B3C5A3CC0DDE442CE7E5D16,
	UIManager_ShowGameFailedCoins_m4C612C9E57A8844091E8A7008FD046C7420B6584,
	UIManager_PlayToast_m033AE9F7CFA3D901AF314A312D49B4E6085D3195,
	UIManager_DisplayButtonRenders_m50D0BE31C70162C34ADE6CB1F2876DA2C2DEED8D,
	UIManager_DisplayUpgradeRenders_m557F16E524D14DBA429D4D7346189865A8BB4649,
	UIManager_SetState_m49207865690C9DA622C33A2B6378DBFDEC85F550,
	UIManager_OpenLink_mB5D81EAACE6DC29FCB12D1DA88F1AACEE397C798,
	UIManager__ctor_mDADE1D724D40AF63AE78D51FC1CF1FE4784B4D4B,
	UIManager_U3CAwakeU3Eb__31_0_m3122A16C76C0271B4114543368B4AE16D96F87C5,
	UIManager_U3CAwakeU3Eb__31_1_mE40D9985291979A55391AD6F32851A8B125816EE,
	UIManager_U3CAwakeU3Eb__31_2_m9D115D348778764D78362E208E83AE06A029371A,
	U3CPlayToastU3Ed__44__ctor_mEAE19C120DB097EDD95DBE6FDE063A848F41EBA6,
	U3CPlayToastU3Ed__44_System_IDisposable_Dispose_mF18FC39DAF7831611A1FF47716A054C35DE81982,
	U3CPlayToastU3Ed__44_MoveNext_mC0FFC23DFB6D654D9DE52BDBE2CB3D2CB252879F,
	U3CPlayToastU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE9752D8C7E3C66B17F947A16473FD29B271BA717,
	U3CPlayToastU3Ed__44_System_Collections_IEnumerator_Reset_m2349BD58837265E99891C68919BDBA1C7B12C32C,
	U3CPlayToastU3Ed__44_System_Collections_IEnumerator_get_Current_m147D97967E8C7C1764873D48F08478FE3C16E7E6,
	UnityAdsScript_get_Instance_m9AA6639260CB393D3C0076E7FA8BAE2344FED37C,
	UnityAdsScript_Start_mBD07AFBBFCBC065B7CF4E42BFFF448DF6C3B9E2E,
	UnityAdsScript_RequestBanner_m6D3D46CA34A99016AC31E28BCF344CF82A509FB4,
	UnityAdsScript_ShowBanner_m1091C4281C642F79947002795B7DC90A2C49A2A2,
	UnityAdsScript_HideBanner_m3191F73A8A170D41BF0C224F493469A9E05AF07F,
	UnityAdsScript_RequestInterstitialAd_m77F6E6E6222F8898633C5CF5DD2395BEED94690D,
	UnityAdsScript_RequestRewardedAd_m90A831C43CB70369C2187EECF4F9FB7C07C494C9,
	UnityAdsScript_RequestBannerForScript_m9AE49D08942D8C483AA4BE34272F2659B48EAEF0,
	UnityAdsScript_RequestInterstitialForScript_m10984B7874A0D7E9070C0197FD373498F7046CEC,
	UnityAdsScript_RequestRewardedForScript_m4FCE6F58125E22FF36C4E82E15E1FE3676627ABE,
	UnityAdsScript_OnUnityAdsDidFinish_mFED4A64C2A7C989A344AA595526268C7D9BFF9EB,
	UnityAdsScript_OnAdsViewed_m9476226B60FD483AEED95265B3210FA24CF65678,
	UnityAdsScript_OnUnityAdsReady_mA8B1B0229EE8486C309FEBD7C95D0837114E2B32,
	UnityAdsScript_OnUnityAdsDidError_m34D5DA14CEEC6D5B0018A214AC944A260FCC6F7E,
	UnityAdsScript_OnUnityAdsDidStart_m96CC78729B5AF76820A6D6C7861AF723A90D6AE7,
	UnityAdsScript_OnDestroy_m0DEDD77F8ADB54E376C393B3D37BF81BDFD9A477,
	UnityAdsScript__ctor_mE6F969CB1601280A6B9B026D6D160B52FC7D162B,
	PlayerBikeScript_get_Instance_m9A1B750B4414EDFF792AB83ED7588A84D1DA75CB,
	PlayerBikeScript_Start_m56BE1349FF7B5B91249F0E502DBFA3022535CE38,
	PlayerBikeScript_Update_m89A238F5A903B1F83753A69006BB656D06205AFE,
	PlayerBikeScript_OnCollisionEnter_m9F235D50820CF56278D11B7F73E75AC21CFE71C4,
	PlayerBikeScript__ctor_m57B01C5C1BB557B113515F494A2323527B313DB7,
	PlayerBoatScript_Start_m0FC864569AF5C17C6064906B966956085196DDD5,
	PlayerBoatScript_Update_mF5F66AB6F769783B18B4A5F0E38D2057F2135848,
	PlayerBoatScript_OnCollisionEnter_m53F1DCE53AE92F7EBE6DB9CC9ED24D6DC60E038B,
	PlayerBoatScript__ctor_mAE89E528EBBB6E3530C8E5919927A69E5DD91511,
	PlayerCharacterScript_get_Instance_m7336BCF939FABE3905490ADA3E1CDAA1577F5F9C,
	PlayerCharacterScript_Start_m8AD7CE1EF5D42313B3671E2C8B203E7059D85DF9,
	PlayerCharacterScript_Update_m5B476B49CD9BB048672EEB322CAD3A3949190D91,
	PlayerCharacterScript_OnTriggerEnter_mDBE8BD73187B0A928C13B9E0275563B9B2569958,
	PlayerCharacterScript_Climb_m6B59955A91F300029BC2B2E1AB47B20EBBDC917C,
	PlayerCharacterScript_IsClimbing_m19886905495A4440976ACF0F2D25BA17011DDAD0,
	PlayerCharacterScript__ctor_m0C2133B7A565EAB02D20617D11ADEC621736DB78,
	U3COnTriggerEnterU3Ed__11__ctor_m157690149B9EFC064D41233255CFF1D06F6E39EA,
	U3COnTriggerEnterU3Ed__11_System_IDisposable_Dispose_m3608C29B0A3E3E32B1042BB1B65824B7BCBEC6C1,
	U3COnTriggerEnterU3Ed__11_MoveNext_mC9BF00C5C113090006A7F0EA8BEC3798E726BDBC,
	U3COnTriggerEnterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2CB4290008F905A4825A8D09AC73523EED0D715B,
	U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_Reset_m980E27465640D7E4636D68BEDFDD1222A71C3454,
	U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_get_Current_m298E21F99DFA30907415C3D27D3AF0AD04F4B3FD,
	U3CIsClimbingU3Ed__13__ctor_mE046B29894718B2CEE95FC9F23E6C957C3FFA998,
	U3CIsClimbingU3Ed__13_System_IDisposable_Dispose_m367474145BDE9A57815C89B1370EE3DC4F317AD0,
	U3CIsClimbingU3Ed__13_MoveNext_m9F068AF0C5ADC004F4354AFB426D9AB7E0658BE9,
	U3CIsClimbingU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D8834D7DDF29F107743C566E8036B4323BB1688,
	U3CIsClimbingU3Ed__13_System_Collections_IEnumerator_Reset_m271A0D38A671C486E12E68AD956D59A536B65DE7,
	U3CIsClimbingU3Ed__13_System_Collections_IEnumerator_get_Current_mA4A718648508AFFC8455A5D13E62BB8091F46067,
	PlayerControllerScript_get_Instance_mDEFC9F8B64434B73A1D6DD76CC984857014ABC56,
	PlayerControllerScript_Awake_m7F252A4E6890B1AEF73DDEEA6006895644A0A7DB,
	PlayerControllerScript_Start_m41BF79F36DEBC8A7298A9B3C5939A2C11C4FF685,
	PlayerControllerScript_Update_mD049DEC0380F99907FA2CFD01DAF5897833E9685,
	PlayerControllerScript_GetCurrentObject_m72769A10B3CB41EE9B84B3B17A3294C833F500F6,
	PlayerControllerScript_GetRotationalObjects_mAA0BEFAB51642F2320F6592DFB33004654506ADD,
	PlayerControllerScript_ChangeShape_m0ECF7F460920C2CA5F98BED4696097BB6F087240,
	PlayerControllerScript_MoveObject_mFDBF554082AA89295044CEA450F59F2184EF2FF4,
	PlayerControllerScript_DetectType_m4AD40DD38785577DA9A3165720EB102BF4425BD9,
	PlayerControllerScript_OnSliderValueChanged_m4541F8D97135067D4FB288CACA9763F4FBB17881,
	PlayerControllerScript_ReturnIndex_m90D508FD77D6F4C99C46ACF0EB21E12AE6003233,
	PlayerControllerScript__ctor_m6B9E7554069250D21CFBEBC24C4D47313E4AB27F,
	U3CMoveObjectU3Ed__34__ctor_m7679949E4579F0514F360B36E50FF224BD59B3DA,
	U3CMoveObjectU3Ed__34_System_IDisposable_Dispose_mE95ED8C6FDC5F84ED1D757A53DA602E240778666,
	U3CMoveObjectU3Ed__34_MoveNext_m4D512AE9C629A535FAFDF770AA89968637F15954,
	U3CMoveObjectU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A7854A57AE804B9BB83BDE78DB37D0766FB65CD,
	U3CMoveObjectU3Ed__34_System_Collections_IEnumerator_Reset_m1752A1686309401EC13831FBF562A3FFAEBB69B1,
	U3CMoveObjectU3Ed__34_System_Collections_IEnumerator_get_Current_m7B5FC4B34965C4A5011095B7BAE82B2399652B2E,
	PlayerGliderScript_Start_m739DA40127549CDA0184CA19CFCCF4559540C506,
	PlayerGliderScript_Update_m4EF16675970B19C7B13166E79874B5440FF07B94,
	PlayerGliderScript_OnTriggerEnter_mA807B366442823366C8BD014FF1E0C1B8394D6D4,
	PlayerGliderScript_OnTriggerExit_mF423D167D76A42E04DC5F14A93DF3B148A6809A3,
	PlayerGliderScript__ctor_mA4928F67FB36344901115E5B13BDC62A1B895AC1,
	PlayerHelicopterScript_get_Instance_m35A8E5D665160B040181ABA773F797810B406E51,
	PlayerHelicopterScript_OnEnable_m17535DD0D895CAB6955EAAF5742B42258E0142FC,
	PlayerHelicopterScript_wait_m2A2F63CFEB547FFAF4C6D64FB02D898B7244894E,
	PlayerHelicopterScript_Start_m99B8EA9AAE41033650BD75AFA225243A950C6A94,
	PlayerHelicopterScript_Update_m8AC5A6122BF3ECDF83F3189D226F216E7C45B37F,
	PlayerHelicopterScript_BoostHelicopter_m2449A206A938A831C3F0513A9708A59FF50E1CCA,
	PlayerHelicopterScript_BoostHelicopterNow_m394AF4AD515DAF71CD02DD1BD10B8904F6B9F68A,
	PlayerHelicopterScript_OnCollisionEnter_mC91D6B47DF96BFA8AA888D85ACF896354039A57F,
	PlayerHelicopterScript__ctor_mBB83718A7EF4D16DCA7CDA046DFBCD766455AFC9,
	U3CwaitU3Ed__8__ctor_m6D1BEC088C88A0A2E14F1560F02C07733ABBE2F9,
	U3CwaitU3Ed__8_System_IDisposable_Dispose_m78B42166C4DD382EFDFA89374A11B19BFE49A446,
	U3CwaitU3Ed__8_MoveNext_m8ABDF16FD351C7C5886DE215266527E9D88EC835,
	U3CwaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC4E14E44E3C1B5EA4BD55F528B43CB7B250DC8A7,
	U3CwaitU3Ed__8_System_Collections_IEnumerator_Reset_mFC68D47EBA84A0CB1C50D57E3A6ECF7C7C2EFB0F,
	U3CwaitU3Ed__8_System_Collections_IEnumerator_get_Current_mF4AA3F4158AEC974F5500B66AB2C5D0E7F560C0F,
	U3CBoostHelicopterNowU3Ed__12__ctor_mA348438E255D83BF6D31C4A904B1C9D0BFDF18BB,
	U3CBoostHelicopterNowU3Ed__12_System_IDisposable_Dispose_m4C43CFFF5FF0DEC50CA5F4AF019157A2404DFA69,
	U3CBoostHelicopterNowU3Ed__12_MoveNext_m1CFC611201F3B9A1AC1525DFE6C1F8FFAC24E6A7,
	U3CBoostHelicopterNowU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAA789A47FD4E85D770398F31F458D991E57D5822,
	U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_Reset_mEAD31E2B8E6B9460D8BC89E00A58DC568DFEDCE7,
	U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_get_Current_m1E39FFAB0BA8AD5590084840D6C6B1147B388A3A,
	PlayerJeepScript_get_Instance_m23EF556DCC502B65E305B3C3179EB2776990483D,
	PlayerJeepScript_Start_m600105456C1DDAB5EA1BCC54041815D61703DA4A,
	PlayerJeepScript_Update_mD06608FB3A7908C5FC4E3045ADEF2D0E6840A6DD,
	PlayerJeepScript_OnCollisionEnter_m387EB4E142B338B9A710C0415DAEE10E447CA791,
	PlayerJeepScript__ctor_m78A06ED6D1EB28BDB245A42B9A7A883BF4897234,
	PlayerTankScript_get_Instance_m9A208859F15E640A23D44994B828670CB94E5537,
	PlayerTankScript_Start_m8FCBA12EF49FEF37CB144A256A62F85764F7D07C,
	PlayerTankScript_Update_m8D35C701E683E337AB4CC11481770DDE5DA59CED,
	PlayerTankScript_OnCollisionEnter_m0482EC0AB7F289D15C3417FD97C0030FAB7E89D3,
	PlayerTankScript__ctor_mBE5F102B8F5F1E433DDD44480BEC7CE91F8962C8,
	WhiteAiBikeScript_get_Instance_m4815F582C72887FE5966D94FCB3061029F720773,
	WhiteAiBikeScript_Start_m183B881B8F630A732F1C37759BE5B1805053B378,
	WhiteAiBikeScript_Update_m26BC6DE23DA712DE3B96041884BCA74814DAB54F,
	WhiteAiBikeScript_OnCollisionEnter_m714FB633CA5BA0809A447EAA8328181F6E205C7F,
	WhiteAiBikeScript__ctor_mE99B24832D737249C8BF52C4E635FE53367CF91E,
	WhiteAiBoatScript_Start_mE3A19D3744710B44CD10939819F8CB9328C9AC2E,
	WhiteAiBoatScript_Update_m58EF1C02217F03A04DB86035C27C76CD8585B9C7,
	WhiteAiBoatScript_OnCollisionEnter_m5AC4913CB12DAF8A12082E429972318FA2B1CBF5,
	WhiteAiBoatScript__ctor_m215CF73212A095F63AF8161E1845B55C014B28A5,
	WhiteAiCharacterScript_get_Instance_mD521B6671A709334246F23E17AB16FAC35B5DA97,
	WhiteAiCharacterScript_Start_mE2E325B3CFD8899EA1C0BF4551242A07ACB32D26,
	WhiteAiCharacterScript_Update_m60283B756DBCFA2BBDD3FFFA51FFA97DE0018986,
	WhiteAiCharacterScript_OnTriggerEnter_mF98389C957252DAA658D0C6782844B50DD3BB48A,
	WhiteAiCharacterScript_OnCollisionEnter_m55BE18719820F352208D6DAC460DA262588C37CC,
	WhiteAiCharacterScript_Climb_m1EC35FCC064D0C9DD1E20E0DB13B93D4D8D482AE,
	WhiteAiCharacterScript_IsClimbing_m96BAB0B2C35DDFC80B654B636317E9C44D8D2C82,
	WhiteAiCharacterScript__ctor_m2B72204CBEF905EA44746243E2096C64DB5DB007,
	U3COnTriggerEnterU3Ed__11__ctor_mEBE528454705838D7F327CA5E247BA9D56E91EF7,
	U3COnTriggerEnterU3Ed__11_System_IDisposable_Dispose_m516C2E127C3B3045FEEF4FF7AAA607FE145EDEF9,
	U3COnTriggerEnterU3Ed__11_MoveNext_m097B0C2FA97C9BF3D5EF7CC213542D3629547B2F,
	U3COnTriggerEnterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3A149DD87BBDCDA6FB6654FCB0075EF9BA2BF827,
	U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_Reset_m9A6AF1050DBC028A87125D7D75181368217E7842,
	U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_get_Current_m89517AA72A5C2553EA773B7BA5633B6E782E8B5D,
	U3CIsClimbingU3Ed__14__ctor_mD50ABC879D4CF442FE82D0600C944384F089EE09,
	U3CIsClimbingU3Ed__14_System_IDisposable_Dispose_m24EADF4B39D6E9C8BB81DD85DF9C5DF33E513E0B,
	U3CIsClimbingU3Ed__14_MoveNext_m7607086996A838339B965D67F537A5240238C075,
	U3CIsClimbingU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD193DA26A09635216183F2B99C9518351610909D,
	U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_Reset_m3001C0A5FCB20D56031F2BEAA18BBDED1E26C86C,
	U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_get_Current_m1F4082A4FC0430E5D727271AC49902284A2326E9,
	WhiteAiControllerScript_get_Instance_m19396D2B80A00D0EBB7915311A13CCD3D1625A31,
	WhiteAiControllerScript_Awake_mD627F7FB68D528D27C1B1075CBC7E8678DB9A42C,
	WhiteAiControllerScript_Start_m5512717D34902431CEE6AA11219D56650F03BC26,
	WhiteAiControllerScript_Update_m36BFF700C9784AC1BE6FFBF86576500C301BDD35,
	WhiteAiControllerScript_GetCurrentObject_mA4D909C0BE6141E4602138FA305EAD254DDD9C7E,
	WhiteAiControllerScript_GetRotationalObjects_m2A051C2E6DA7CEA35956C832196156E20D496AC3,
	WhiteAiControllerScript_ChangeShape_mE41BF60D27CFA75C0621A9219BB2CE2D410ECB3A,
	WhiteAiControllerScript_MoveObject_mD0A4644E2AC3C736E82D453DD534CD123D126A18,
	WhiteAiControllerScript_ReturnIndex_m04D691A6423A34CDE69E3995BF0A296ACE812C34,
	WhiteAiControllerScript_DetectType_mB32AC06DFF0B7C4DBC0D5EC59DF48B909ECFD4C0,
	WhiteAiControllerScript_Transform_m6D5DF53567C35622727179CED636CC9F5432E595,
	WhiteAiControllerScript_TransformToRandom_mEE31E2CE3B2E990473C6C3FF5DA49ECABB3C8368,
	WhiteAiControllerScript__ctor_m6AEBC70DE3A517743D550A343DCCE59A2102656E,
	WhiteAiControllerScript__cctor_mD263C819FEC052837C68C77F77420A107C406A8F,
	U3CMoveObjectU3Ed__35__ctor_m46FAD7C53D13345B8A77D4EC983C9B26FB309376,
	U3CMoveObjectU3Ed__35_System_IDisposable_Dispose_m17574B27BD597AD8546BC11CD6A58C8EB9040CBE,
	U3CMoveObjectU3Ed__35_MoveNext_mDB9EDEDAB3DFBC6A96726C537E1FDD84B0F3E409,
	U3CMoveObjectU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE77F08164CB742A73CC623896C303BE46F18789,
	U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_Reset_m74279CE4296C2A6C033D3378BFB1E1B48CFF9FF0,
	U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_get_Current_m159DA0631BCEEC3665E0265F971D1F9B27D6472C,
	U3CTransformU3Ed__38__ctor_m9BD5E5BF43D39641AC0247391E4797A7319B1AD4,
	U3CTransformU3Ed__38_System_IDisposable_Dispose_m0B2B7B2528B459935A3F95DFE3E91B482848C801,
	U3CTransformU3Ed__38_MoveNext_mE4A4BEB23FB4D85C5F72B646D8A8658E8946DB46,
	U3CTransformU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB25722084BED3E54E2949574C55216D8F602E057,
	U3CTransformU3Ed__38_System_Collections_IEnumerator_Reset_mABC4575D59FB478E3D6019D8FFD280E2B7777ACE,
	U3CTransformU3Ed__38_System_Collections_IEnumerator_get_Current_m6148983E37AABFC28BA9819C013473ABF88C5F77,
	U3CTransformToRandomU3Ed__39__ctor_mE4E2E68B2E5D58BF723A15D1B39E372FC8B04C43,
	U3CTransformToRandomU3Ed__39_System_IDisposable_Dispose_m6194F8626FBFB3D09711A844BE285169751F003F,
	U3CTransformToRandomU3Ed__39_MoveNext_m4CA3AD3027881545BEBA56AF9804B6B748617E0C,
	U3CTransformToRandomU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C6B2A3331459A3A3E8A7640B058F0B0C99BAD48,
	U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_Reset_m80BF425047633FFBDC3BD6A67D0A4C77490717F0,
	U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_get_Current_mED3B378B7A0BC6D929D9A73DED17AC2D7CE5C3DB,
	WhiteAiGliderScript_Start_mBE0564D31E40AC6468AB260680114B00563DE859,
	WhiteAiGliderScript_Update_mD606E23C1026E997826BFF87A38C62C6EAED3141,
	WhiteAiGliderScript_OnTriggerEnter_m2486881B7C830A1B70E2A6F9DEF5C59D44FF6583,
	WhiteAiGliderScript_OnTriggerExit_m5A3A08A14863988C496B804387854A4F5BF39FD8,
	WhiteAiGliderScript__ctor_m5D76D4C56986750C05236F4FE0034D73158066AD,
	WhiteAiHelicopterScript_get_Instance_mB60AA3F270D7D79783B5CAF73D549FEC83B1CE92,
	WhiteAiHelicopterScript_OnEnable_m96803C775C3D71B80FBD2AD9E3A33C7A9FACB319,
	WhiteAiHelicopterScript_wait_mED718A5D65ACB507B40E9AB7C3D5A527C94608F0,
	WhiteAiHelicopterScript_Start_m10A1BC75049098E7DF2C85438BCB4E826CC69B1E,
	WhiteAiHelicopterScript_Update_m8C258C803E68B1F83A5B6AC5BFF193EAE1755E77,
	WhiteAiHelicopterScript_BoostHelicopter_mD90914A8EE4FB8D744D4BE0C341817F0CB34BE96,
	WhiteAiHelicopterScript_BoostHelicopterNow_mADA21CCA99D5687E6BC26F7BD3193889086B4AD8,
	WhiteAiHelicopterScript_OnCollisionEnter_mCDC50EA9AB2D9E082A5DCCE60502B6336C5D6B51,
	WhiteAiHelicopterScript__ctor_m39416132BD37B4BD36F4379749DB91530A957833,
	U3CwaitU3Ed__8__ctor_mCDB3D87DB2A64800169702361168C4CDC6B3D37C,
	U3CwaitU3Ed__8_System_IDisposable_Dispose_mABACCB7EB2890077CA87A31B4FD0CF548274E171,
	U3CwaitU3Ed__8_MoveNext_mC789EBAE70A80924E73ABAF89ECBC1950CB441F3,
	U3CwaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8272CCE65FF2619EBBA0D2C0A7BD4212C3125869,
	U3CwaitU3Ed__8_System_Collections_IEnumerator_Reset_mB2BE0C91E4E9A386047094591B5B86CDA8DB7810,
	U3CwaitU3Ed__8_System_Collections_IEnumerator_get_Current_mABCAEFC067279C876F020818FBBAEF9CBF999BD6,
	U3CBoostHelicopterNowU3Ed__12__ctor_mEBCFF3C87A4DB90A3566DB58CC56588429F746EE,
	U3CBoostHelicopterNowU3Ed__12_System_IDisposable_Dispose_mA987C60121DC0B5C6BEA47187B70D7D69BB47BB8,
	U3CBoostHelicopterNowU3Ed__12_MoveNext_m28B7BA9016A84721EA4466318A997BCC7AAD556D,
	U3CBoostHelicopterNowU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2931D5D08309E702D72BC52D93601CC4D7308A8F,
	U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_Reset_mFF46627F7E9356628562AF2AA430E389043A735B,
	U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_get_Current_mDD5107EF134E88023F3DD165A5D3CB19F0FFA802,
	WhiteAiJeepScript_get_Instance_m3B9B8CB7CEFCC8F6425CDD802679BF683A845795,
	WhiteAiJeepScript_Start_m7C1B6A9B007CBBF6D9C4407855CD8365635E3047,
	WhiteAiJeepScript_Update_m0922C52BE833BA934DA30B63267F74E2CA04E251,
	WhiteAiJeepScript_OnCollisionEnter_m149BE4550A49A2797AE013DE3BD4AE5E8623385C,
	WhiteAiJeepScript__ctor_mB341D8DD586E3E2BBA300EC9E7B628883F024E59,
	WhiteAiTankScript_get_Instance_mCB0A922527238D3E55B4C4439AD6F10DBA3FE64A,
	WhiteAiTankScript_Start_m0E47323B9142583528D7B3513A6454B07CDF7DCC,
	WhiteAiTankScript_Update_m9317F9D78147E69E287097F5650A3748F5AED3DB,
	WhiteAiTankScript_OnCollisionEnter_m18B103DEA17F4CFC79E2913F6D032510EC092DEF,
	WhiteAiTankScript__ctor_m55999400B1E0CA5BF4E713DF9266CFA2B052EBAB,
	DemoInventory_Fulfill_m9589871E7FDFA83941CF675170D6130A22F566D4,
	DemoInventory__ctor_m223CB4347D66DE6D6C28204FDEAAD5C84F7CE4E0,
	LowPolyWater_Awake_mBF6D32C683E8BD39148DB6E7AB59FA73EAB6B1C9,
	LowPolyWater_Start_m89AFA69FF7508A8D612A6C5E5DACB95F2B94AC5B,
	LowPolyWater_CreateMeshLowPoly_mE6F5928759402E4367E29A84DB1B4C8FFAAE63D0,
	LowPolyWater_Update_m5E2FF7C7587E28FA08EA29B9BDD359F190156CA9,
	LowPolyWater_GenerateWaves_m128D40285442A523C43AAB1BA22EF0E21544CEEE,
	LowPolyWater__ctor_m616A0E96E64BD42A2ECBAB3FE69EE017057502DE,
	DOTweenModuleSprite_DOColor_mA67D7B77174B479F8AE20F739A74BA7A2BBC9F0F,
	DOTweenModuleSprite_DOFade_mEED04CEE3EE9D32B87478A3CFDB9CFB710307769,
	DOTweenModuleSprite_DOGradientColor_m47670ABD64AEDC972E8210CAF4C8A778B0B7676D,
	DOTweenModuleSprite_DOBlendableColor_m51BBDB4A69B0560481736E6734EA2BF52DB1537D,
	U3CU3Ec__DisplayClass0_0__ctor_mD77E24E24423ADEC43F9983504BD6796D2671E99,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m12BAC3674340BDF408F41A0E9BEEF6BB87C3F504,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_m89E34AE3BBC636BD2B38C1AB7300F1F20F25E616,
	U3CU3Ec__DisplayClass1_0__ctor_m40F18C57ED175BA68D1D65FFA97879AEAF2E3325,
	U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__0_m8B9E647B54E5DA35EE71D65866372C8C90B7F452,
	U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__1_m935CE2E2B7CB25D7151DA0BF3B4420C8099E0A45,
	U3CU3Ec__DisplayClass3_0__ctor_mB0AA92A12DE97E8954819AC2BAD916065D4BECDB,
	U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__0_m371A7E7E53DB5FC70A6534A7956B27A3A8B7EA03,
	U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__1_m36515014C810F349D12C86DC636349E452DA6974,
	DOTweenModuleUI_DOFade_mF927A1EDCA91814D8A466A343E3223EEF4ED7478,
	DOTweenModuleUI_DOColor_m7067683474EE6A600294C2CCFD941403FA789A6E,
	DOTweenModuleUI_DOFade_m41638D05EB7678C523D0D39E320FAC80B8E48658,
	DOTweenModuleUI_DOColor_mF4B41443760536F3C399612AD0283A52937D95CC,
	DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754,
	DOTweenModuleUI_DOFillAmount_mE4ED52B826A6F8035320E980F7C701B4047C4DBA,
	DOTweenModuleUI_DOGradientColor_mB4B8D1DEDA92464670EB8A6D985973477D51A4D5,
	DOTweenModuleUI_DOFlexibleSize_mF0916C4E7A906202EFBAEA89CC2B55A9C3CF48B3,
	DOTweenModuleUI_DOMinSize_m54A6919625499AFD6F801C6EC9672965DBDC96E1,
	DOTweenModuleUI_DOPreferredSize_mE26AFB2C606F095483EDED449FB8A5A80C08A8DE,
	DOTweenModuleUI_DOColor_m15BC0FCF035E3DEEC3CFC30465EDE0FE6FD6D2EA,
	DOTweenModuleUI_DOFade_m86DB80308D92FC4DFD3E981F9E958FF2A207EA74,
	DOTweenModuleUI_DOScale_mFA27F29E3DFB6B1CFCAC3CE73F06DF3737A3EB06,
	DOTweenModuleUI_DOAnchorPos_m63999D434CE7E8F6BE66BBA92532DE9D98DC9F79,
	DOTweenModuleUI_DOAnchorPosX_m9B57E02E9902C7A3F1A8DD1021AF630C10478380,
	DOTweenModuleUI_DOAnchorPosY_m14CCA63E545D7C28AD1A76488D0084F55EE94CA8,
	DOTweenModuleUI_DOAnchorPos3D_m6DEB7C4AE4CD2118BF83D268945340F3373DC7BB,
	DOTweenModuleUI_DOAnchorPos3DX_mA40303AC769280BDE74EE58BE5AD80BEA427CE08,
	DOTweenModuleUI_DOAnchorPos3DY_mE6BE56AE421B959F22CBA6318BABC3CD367A7277,
	DOTweenModuleUI_DOAnchorPos3DZ_m702F7D4FE9DC8E6116FB5E0DD478D2BB88ED1242,
	DOTweenModuleUI_DOAnchorMax_mDD97F6A8B4468622E3808507CB6B89A8A109BDCE,
	DOTweenModuleUI_DOAnchorMin_m60C4775B1EEDBCF2CD3E96A8171C8A83FD7F400D,
	DOTweenModuleUI_DOPivot_mF623939EDCC152F664FECC8B71E321B0AA13F0A9,
	DOTweenModuleUI_DOPivotX_mC21C031A61BDB7C3A8087719F23CBC6D1D762D6B,
	DOTweenModuleUI_DOPivotY_m9922C28ADECA433362D3C6C816318CA00705996A,
	DOTweenModuleUI_DOSizeDelta_mC4FD07525E1CA5BBF577030D571CE0CCEFD4E6B7,
	DOTweenModuleUI_DOPunchAnchorPos_m058E3DA140B9C497727F05AEAA296425E585B9EA,
	DOTweenModuleUI_DOShakeAnchorPos_m89D1C74E940B9434B2234FB49C031AD8FD1170AB,
	DOTweenModuleUI_DOShakeAnchorPos_m7818867062313A535AB2CD6F86267E2AB9357AC6,
	DOTweenModuleUI_DOJumpAnchorPos_m7046EE1C82AF0159CFAAA460E4C3D22797625480,
	DOTweenModuleUI_DONormalizedPos_m300F112FE2F67D4DC718083340C7FD27BC96F248,
	DOTweenModuleUI_DOHorizontalNormalizedPos_mCB4BCD85B49C458BE871F890A86FEFE0D7F41B42,
	DOTweenModuleUI_DOVerticalNormalizedPos_mC0269B4644816A0D5C09717CA92E4229CB31CDF1,
	DOTweenModuleUI_DOValue_mFDF45D60F52C82AACD8B50859A51B8F315A44F08,
	DOTweenModuleUI_DOColor_mFD36CA1EA683A0D2312EE87BCF3B0A924504E797,
	DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1,
	DOTweenModuleUI_DOText_mC0932E45473278B2FFB1698954CC0EB0422C626F,
	DOTweenModuleUI_DOBlendableColor_m4C5E9A68BFBEE876E1FF8D8E9A4BA77856F144C1,
	DOTweenModuleUI_DOBlendableColor_m18196516A24780A550314E9327868D6D6F3EAA4E,
	DOTweenModuleUI_DOBlendableColor_mC30D1F805FE976FB25EC361AB0ABA94413361B6F,
	Utils_SwitchToRectTransform_m260A15449F1C7A8F4356730DF4A59A386C45D200,
	U3CU3Ec__DisplayClass0_0__ctor_m14E6397A137D3CB8A7638F9254B4694553B4DC7C,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m6F0CD242FAEF332D726AD4CE2EB2691CCB3748B1,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_mDBDE14547A6DF70C0ADB82403282C26FBB4F6A27,
	U3CU3Ec__DisplayClass1_0__ctor_mCB63EFBBE399EEB9892054E0FAB2AB42E2BBB886,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_mE8D0790E2303D84F8D513854D9F004E5E3127A3C,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m07CB6A64D93955BC07E8AE290B01F9F39195A5A9,
	U3CU3Ec__DisplayClass2_0__ctor_m61C588C0764BA908B02EBAB1C19076EBB2898A9E,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_m85CC4106263DD670C5BF648B8A64C6BA84297B65,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_m140BDA0B825925A5051A2565C055CCBBF005A680,
	U3CU3Ec__DisplayClass3_0__ctor_m09228DABE56F6866E09591FE27C255A39A71E48D,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_mA7BF7CEB8AA94694104E8D048F5045D122A5D980,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_mBC7108789E76320E9507A9087273C7D89227F0F3,
	U3CU3Ec__DisplayClass4_0__ctor_m5CD451B862F3CC71D8D25BC3A69725FCDD6144C4,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m507CD55516001BB4A76D7E58CB159B5394D6413C,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_mE65F1E4D380EC305AF0AE93E0315B0B23CA36310,
	U3CU3Ec__DisplayClass5_0__ctor_m879B3843E0C43157D8044AD52E2F865EE50FD041,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_mAFE3629BC44AE61C84399809F742CEF0235004E1,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_m9EB6C21BCE8D1A08FF84815C05A6664637C510CE,
	U3CU3Ec__DisplayClass7_0__ctor_m47B2E10FFFD9FCACC88586DEF6A4E675DC9EC406,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_m7A7EA5F66922413743B8C1AB4234645A32098EA8,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_m0E76B7C6F893FD2F7F24E302CB55882765DD1153,
	U3CU3Ec__DisplayClass8_0__ctor_m0953E12771619B6F14FC14CA853AF5CA110A0553,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m9BD9F63B884498F9AA02C307EB1CA15FA29BD094,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_m546E530F9DE46CA1359450860E62DCC3BD3D72B3,
	U3CU3Ec__DisplayClass9_0__ctor_m3EB28823EF4D152A06983986C8120490212A8DF7,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mB87EAB728455B68D07773FA10DCA169804482CBD,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m24617F67F48227A961C0543BF7925F8D2F2A90B6,
	U3CU3Ec__DisplayClass10_0__ctor_m11212BA4C7EB97E0EE079E78FA1C95C02D517C75,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_m08737F267CCB60C0CA0244070DA3C50845FC8B1F,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_mAFFD9D2D2F3FD2BE4E2423286098B9BCC1C0C193,
	U3CU3Ec__DisplayClass11_0__ctor_m9B3BB08FF1DC2F3101E0D580891268445B7763CA,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_mA57545026A9AE0CB3A20B6FFCDCF6F2F1CDA6AB0,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_mCC70305FC1C1B93A838838519D023AC608D3E23E,
	U3CU3Ec__DisplayClass12_0__ctor_m05F6F905C11A344C5681CE1CD406DE85940356E5,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_mEE927D8596DDB335403416BF63FF4E7E27D49D51,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_m65C60213296203590F596B6A3481E4B8621F96D5,
	U3CU3Ec__DisplayClass13_0__ctor_mC6D360265628BCFBFDD42CCAF1136983FDD186BE,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_m693A41C7FCF87BDE6211DE0ADED05AA5C476D71F,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_m0F8F9CA39BC8894705C1EE9983E59D24A7BF218B,
	U3CU3Ec__DisplayClass14_0__ctor_m3BB3CA37E5912C211B954D7EE850C77F0B04BFF6,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m02BA203D0A6C9D64BC8149C197099DA29370C872,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m47B6E3E511B7E234B49FDBD4D6BC32E5EC92B1E9,
	U3CU3Ec__DisplayClass15_0__ctor_m011D15F0644147EABAFA4ED415CD3F5E8782CCE8,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_mCBF0F2691FBC7AED64284B0FB4CCD75792AAB51C,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m0E6D417FD9062B34D8C0E8B58C4C688B39CD9603,
	U3CU3Ec__DisplayClass16_0__ctor_mF4FDD3E32D1C9FDD48079A148410AAB0CF4855B2,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_mFBE3FE1AC3F56C148108234A9A1285F87EEF50E8,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_mFEF34D901D1E18E74D7E93297BDD75ABF146514C,
	U3CU3Ec__DisplayClass17_0__ctor_mEEC98B74C2A5EDE984A5A59F4289ADFADAB3F804,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_mC8D86325D798D952B390643A8BAE1D995368D36A,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_mAB78FA2C6EEC74D6E259B2ABF94ECBD3CB62DB82,
	U3CU3Ec__DisplayClass18_0__ctor_mE272C380064D4945F3C7FDC2357A8B765BC52841,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_m8C53152ED72D09B8F2BE2DE14963561650D0C42B,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_m9E1FC98A6DF461320AC4B6F294F1A69AFD058E3E,
	U3CU3Ec__DisplayClass19_0__ctor_mF4A0F29D8F3315E7DD0BC8103DA0023E16C341CB,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_mD27E4DF91E21C64D13997EA12A7659B1E31AA77B,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_mD6F74CB34BAEA91A512586538770AB27FFB68A1D,
	U3CU3Ec__DisplayClass20_0__ctor_mCE70C1CB9A605F65BE4D31D224111EE4C6FB7DE6,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_mAB5724DF7472A7CED2070B75A1B0E12378D5E6DD,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_m7E357679D17273D809A39CDF435B5E64E46A64A0,
	U3CU3Ec__DisplayClass21_0__ctor_m47D7A3B4DA7AE2FE2F83164406834CF9150E5308,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_m4804C6D34689DD21311DA86CC22008D3B0774391,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_m73EF1691143A0E7E0254A73F52FDC73BB5AAA3E6,
	U3CU3Ec__DisplayClass22_0__ctor_m0D4210D174301CE26FA7A519B4168E7D87F6D92A,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_mB3D6F3AADE069EED625038A53491AFEA1DAFD9DF,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_mDCA8630A9398A411ACB0950973AB99C0723CC074,
	U3CU3Ec__DisplayClass23_0__ctor_mA47954A6646E43342880718EE9B5E66A0D18FDF2,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_mFC16CCA332D908526AC3DC2BD8B4456969000CF5,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_m7E0DAFAB836AC4F133B995B50578D8911BA3113B,
	U3CU3Ec__DisplayClass24_0__ctor_m0524772292383368F9C9F6BDFF0A114D60A35F8F,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_m959088FDF2C1CD6EF40EE35BA531719E60C5327D,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_mCECF34E3A27F03AA2DB5719CA1241927DC99BC5B,
	U3CU3Ec__DisplayClass25_0__ctor_m1ABD5A4A91526CA156C55C23A54822DC898A34AF,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_mF87E563BB15F601A91B0DA1012A1604A52FCBDA5,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_m26B3B99EDB1D47D81931CC6CABFED8406BF4E90E,
	U3CU3Ec__DisplayClass26_0__ctor_mC4836A2FD37334971703D95C49ED35BCAD56AFB8,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_m357B263CA209B3E6EBB7F95AC352AA8B28A5CFC5,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_mE7127F5C6AB73DBA7D40F67FD61BA4076E2B8D23,
	U3CU3Ec__DisplayClass27_0__ctor_m07ADC405B65035A17DC97097DED9B4C1D21EFB17,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_m1AF2B166591EEA50BEE7FD3C2E9B7EFFC0D39F8E,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_mF7180DEAAB00C14DD6F3DB8B815D0C0B2DB64725,
	U3CU3Ec__DisplayClass28_0__ctor_m4969157094B09547B26225881BD87025C928B506,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_mB4E05CBCDBD4732D154E1A64678339D3FC5FBCE9,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_mBDA6378719836E14C155B4648BB47429C125590C,
	U3CU3Ec__DisplayClass29_0__ctor_m00E6EAFA57923B235554FA8A236AD64992F363FE,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_mAE0CBA3D52D03EB2F2C37D85C5832B412F816591,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_mB685B77262E778382FAF923C04FD328DC494426C,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_mB23D92AF70DCD33F838372FF59B1F39DD3775824,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_mB86E86358B9C1671706EA489B6FC30C3874224E7,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_m1E0D9286E08822DF086E86BA60F0EFF6B62A32C7,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_m6E6A274B0B852663D3ED8CDD2B4646B9D618E535,
	U3CU3Ec__DisplayClass30_0__ctor_mCC70550D8AD7B4F88C913940A644E81F11F5898A,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_m92C7B1817960CE4D1D7333872D37C8AF131F68FE,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_mFA97248E3617865FEF6C3F36E633266FF044050F,
	U3CU3Ec__DisplayClass31_0__ctor_m82279F879253DF896CCD126EC0B7E16B295D165A,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_mB33367FCE1348BA9F855A9D1366FA19F5DDA4AEB,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_mEBA7CDAA06332B1E56991D8E2299E24B9348D627,
	U3CU3Ec__DisplayClass32_0__ctor_mD93F08EDF6689C6BA223067191EE8C9E0FBE509C,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_m83DD02084574CCFB9F26C19BDE6AF6DE2C6387FE,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_mAE94A6E7E27147AA530D056E286A00C8EAE56331,
	U3CU3Ec__DisplayClass33_0__ctor_mB387BC26BCA1B460D5B3B1CE767228FA244B7703,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_mBB603DBBDA542D7F00142102A667BCB9682EC145,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_mA3DF76574D5E76992AF228A3C4AFFFC19D6BAE15,
	U3CU3Ec__DisplayClass34_0__ctor_mA88FD675FEF2971D3F8145822B844ECFFF59BF17,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_m5DC2ED4C56B0A1D8CFCB8A4E2D2078EA79E8B66F,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_m9BA1BADA179249B9F0AD1142BE826798FA114E13,
	U3CU3Ec__DisplayClass35_0__ctor_m5533AE6A72AB74A97282032790D0DE81F0EAF811,
	U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__0_m8E531F7061E585AC849F3B6C9CF458C5C0C5A410,
	U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__1_m018514D25780FBD3A4D3A83A9209FCA89FCB163E,
	U3CU3Ec__DisplayClass36_0__ctor_mE2912C6BF4318E28870DE7AA92E4B05C80D92EE0,
	U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__0_m576159CD95104376F24DEFFAC786AC5D91B2036A,
	U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__1_m564BDB58BF1EE683A9DE9DB1691CDA8216CE34CA,
	U3CU3Ec__DisplayClass37_0__ctor_m59CE3FBA46AF3F7C3597AD84DEC898DB9B84EE39,
	U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__0_m7D79A486598AC337B32C2930F8CE462DDF58D961,
	U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__1_m552CD0C3E6A65CF57519CFAD7B9636DD697E1D47,
	U3CU3Ec__DisplayClass38_0__ctor_mB3E91AEAFAE65DD5FC36DD530E74CE1F4FA24AEF,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_mB976655AEC70D25B43AAAF577CC953C7EB03D2EE,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_mF14436C4F4EEB5B7664C3444B969B694BC6E3E5E,
	U3CU3Ec__DisplayClass39_0__ctor_m1DB7B55A6B3E4B184E35968EACBF9E214E0CED7D,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_mE93AC95325F69D83EE746421FA5992DC25009961,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_m45DD6E7ED579816AC18DF6B88901D9F11DB1B36F,
	DOTweenModuleUnityVersion_DOGradientColor_m7E9B0D004D4B3D602EBB852CA1755FE863C6B839,
	DOTweenModuleUnityVersion_DOGradientColor_mA4E191E86F2EF81966259A40B2CDADE375437920,
	DOTweenModuleUnityVersion_WaitForCompletion_m4BCEDFA5ED1ACF4BC1B3531C0C306A7AC553C1F2,
	DOTweenModuleUnityVersion_WaitForRewind_mD9F37D5AAD9B665EE07F083D026E65194A8E3E6E,
	DOTweenModuleUnityVersion_WaitForKill_m02BD7F3B018BB6B4ED4FCAF47B8DA435EC60BD6B,
	DOTweenModuleUnityVersion_WaitForElapsedLoops_m46D46788899029A20F73BE252F7427D853131404,
	DOTweenModuleUnityVersion_WaitForPosition_m1771625E294B38C5D13DCA40846AEA881C1B23E6,
	DOTweenModuleUnityVersion_WaitForStart_mB4AAFE83B50AEF3A167F31C14B2F58794527F905,
	DOTweenModuleUnityVersion_DOOffset_m09CA228EBED193314D3D9A41BBDC97393E1FC4D2,
	DOTweenModuleUnityVersion_DOTiling_m6A3CD06EC3030A93EC3E93DE678F34048D5C1141,
	U3CU3Ec__DisplayClass8_0__ctor_m0EACBC706CDB7B8BCE4C21D3AD250FE25CD825CF,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m6D616025F8610086B30B83BC8ED13213DB2AC7CC,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m4171C9EB0226E0E06E574EFD507A8D352EC5B557,
	U3CU3Ec__DisplayClass9_0__ctor_m6CC810F5A3B1779C2569B21779361FD5F94C2C9C,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_m747CC91BE7EDF7D2644045B72BF689A2552B855F,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_m0D5FC9115C0FFE113CA2277BA5A17562550B19F6,
	WaitForCompletion_get_keepWaiting_mCA7642C5A8C37F8C0A2CAE990CE1CB6AEE8FD2D9,
	WaitForCompletion__ctor_m818111A77A3380EE626346FE03A1A604BB896A1A,
	WaitForRewind_get_keepWaiting_mB480319CB28155CA977F94C7FA03EE5353AA1285,
	WaitForRewind__ctor_m66B575E497C363CB5137629B4D6A00D13B7CD5AE,
	WaitForKill_get_keepWaiting_m7979151F1AD842D2E8004FE37A2C51B47AB36647,
	WaitForKill__ctor_m1C9624CE32A1C83CEA14BB5EADD587B6AD79D829,
	WaitForElapsedLoops_get_keepWaiting_m6F7A59CCCC45BBA5125C6FC7AB667CD24359E8F4,
	WaitForElapsedLoops__ctor_m8E720B450DD1350EE81EC3CCB5B6280BE5C51D8B,
	WaitForPosition_get_keepWaiting_m34DFE8356EAFEE916828BFAF4A17A822B47AD687,
	WaitForPosition__ctor_m94DD0A05EF293B8AA83F343A12015C107AF7FDB8,
	WaitForStart_get_keepWaiting_m59700AA1AB726A22C76BFAA0C52FCA460F6E337D,
	WaitForStart__ctor_mD7AB17A603CF22568EEF0D9861C49F6CFD632284,
	DOTweenModuleUtils_Init_mFF2188F42FA7128FE1A9BBC2CB9E6351919C9D15,
	DOTweenModuleUtils_Preserver_mBE8D1FE2AE913FBFFCCF43B5A615D941B3706F43,
	Physics_SetOrientationOnPath_mCC376173A621DA244564EDF8A6347AB2A0F47816,
	Physics_HasRigidbody2D_m86FAA0450979B8AFE6A9EF5E27837387C57765C1,
	Physics_HasRigidbody_m3082D72A658CCDDF6BB1F3D4A3EFBD6397D6E862,
	Physics_CreateDOTweenPathTween_m94D41B65E37500E5ACCF6B6F3B5D53FB031C4245,
};
static const int32_t s_InvokerIndices[817] = 
{
	932,
	1075,
	929,
	1368,
	1592,
	1397,
	1379,
	1379,
	1379,
	1592,
	1592,
	1379,
	1379,
	929,
	1580,
	1592,
	1592,
	932,
	1368,
	1592,
	1592,
	1592,
	2737,
	1592,
	1592,
	1379,
	1592,
	1592,
	1592,
	1379,
	1592,
	2737,
	1592,
	1592,
	1131,
	1379,
	1592,
	1559,
	1592,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	2737,
	1592,
	1592,
	1592,
	1592,
	1592,
	1368,
	315,
	1547,
	932,
	1127,
	1559,
	1592,
	2752,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1592,
	1592,
	1379,
	1379,
	1592,
	2737,
	1592,
	1559,
	1592,
	1592,
	1368,
	1127,
	1379,
	1592,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	2737,
	1592,
	1592,
	1379,
	1592,
	2737,
	1592,
	1592,
	1379,
	1592,
	2737,
	1592,
	1592,
	1379,
	1592,
	1592,
	1592,
	1379,
	1592,
	2737,
	1592,
	1592,
	1131,
	1379,
	1592,
	1559,
	1592,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	2737,
	1592,
	1592,
	1592,
	1592,
	1592,
	1368,
	315,
	1547,
	932,
	1127,
	1559,
	1592,
	2752,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1592,
	1592,
	1379,
	1379,
	1592,
	2737,
	1592,
	1559,
	1592,
	1592,
	1368,
	1127,
	1379,
	1592,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	2737,
	1592,
	1592,
	1379,
	1592,
	2737,
	1592,
	1592,
	1379,
	1592,
	2737,
	1592,
	1592,
	1592,
	1379,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1379,
	1592,
	1547,
	1368,
	1592,
	1592,
	959,
	1592,
	959,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1379,
	1379,
	1592,
	2737,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1397,
	1592,
	1592,
	1559,
	1592,
	1559,
	1592,
	1559,
	1592,
	1559,
	1592,
	1592,
	1592,
	1368,
	1592,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1379,
	1592,
	2737,
	1592,
	1592,
	1368,
	1559,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1580,
	1592,
	1580,
	932,
	932,
	932,
	932,
	932,
	932,
	932,
	932,
	932,
	932,
	932,
	932,
	932,
	932,
	932,
	932,
	1592,
	2752,
	1592,
	1379,
	1379,
	1592,
	1379,
	929,
	1592,
	1592,
	1592,
	1580,
	1592,
	1592,
	1592,
	1379,
	1592,
	932,
	1368,
	1075,
	1379,
	929,
	1592,
	2752,
	2752,
	1592,
	1397,
	1592,
	1592,
	1592,
	1592,
	1592,
	1288,
	1592,
	2737,
	1592,
	1368,
	1592,
	1592,
	1592,
	2752,
	1592,
	1592,
	1592,
	1592,
	1592,
	2737,
	1547,
	1368,
	1547,
	1368,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1379,
	1397,
	1592,
	1368,
	1368,
	1368,
	1559,
	1592,
	1592,
	1368,
	1379,
	1592,
	1592,
	1592,
	1592,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	2737,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1230,
	1580,
	1580,
	929,
	1379,
	1379,
	1379,
	1379,
	1592,
	1592,
	2737,
	1592,
	1592,
	1379,
	1592,
	1592,
	1592,
	1379,
	1592,
	2737,
	1592,
	1592,
	1131,
	1592,
	1559,
	1592,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	2737,
	1592,
	1592,
	1592,
	1592,
	1592,
	1368,
	315,
	932,
	1592,
	1547,
	1592,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1592,
	1592,
	1379,
	1379,
	1592,
	2737,
	1592,
	1559,
	1592,
	1592,
	1368,
	1127,
	1379,
	1592,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	2737,
	1592,
	1592,
	1379,
	1592,
	2737,
	1592,
	1592,
	1379,
	1592,
	2737,
	1592,
	1592,
	1379,
	1592,
	1592,
	1592,
	1379,
	1592,
	2737,
	1592,
	1592,
	1131,
	1379,
	1592,
	1559,
	1592,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	2737,
	1592,
	1592,
	1592,
	1592,
	1592,
	1368,
	315,
	1547,
	932,
	1127,
	1559,
	1592,
	2752,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1592,
	1592,
	1379,
	1379,
	1592,
	2737,
	1592,
	1559,
	1592,
	1592,
	1368,
	1127,
	1379,
	1592,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	1368,
	1592,
	1580,
	1559,
	1592,
	1559,
	2737,
	1592,
	1592,
	1379,
	1592,
	2737,
	1592,
	1592,
	1379,
	1592,
	1379,
	1592,
	1592,
	1592,
	1131,
	1592,
	1592,
	1592,
	2148,
	2171,
	2164,
	2148,
	1592,
	1528,
	1345,
	1592,
	1528,
	1345,
	1592,
	1528,
	1345,
	2171,
	2148,
	2171,
	2148,
	2171,
	2171,
	2164,
	1978,
	1978,
	1978,
	2148,
	2171,
	2173,
	1978,
	1973,
	1973,
	1982,
	1973,
	1973,
	1973,
	1978,
	1978,
	2173,
	2171,
	2171,
	1978,
	1705,
	1658,
	1659,
	1705,
	1978,
	1973,
	1973,
	1973,
	2148,
	2171,
	1698,
	2148,
	2148,
	2148,
	2480,
	1592,
	1582,
	1399,
	1592,
	1528,
	1345,
	1592,
	1528,
	1345,
	1592,
	1528,
	1345,
	1592,
	1528,
	1345,
	1592,
	1582,
	1399,
	1592,
	1588,
	1406,
	1592,
	1588,
	1406,
	1592,
	1588,
	1406,
	1592,
	1528,
	1345,
	1592,
	1528,
	1345,
	1592,
	1588,
	1406,
	1592,
	1588,
	1406,
	1592,
	1588,
	1406,
	1592,
	1588,
	1406,
	1592,
	1590,
	1408,
	1592,
	1590,
	1408,
	1592,
	1590,
	1408,
	1592,
	1590,
	1408,
	1592,
	1588,
	1406,
	1592,
	1588,
	1406,
	1592,
	1588,
	1406,
	1592,
	1588,
	1406,
	1592,
	1588,
	1406,
	1592,
	1588,
	1406,
	1592,
	1590,
	1408,
	1592,
	1590,
	1408,
	1592,
	1590,
	1408,
	1592,
	1588,
	1406,
	1592,
	1588,
	1406,
	1592,
	1592,
	1588,
	1406,
	1592,
	1582,
	1399,
	1592,
	1582,
	1399,
	1592,
	1582,
	1399,
	1592,
	1528,
	1345,
	1592,
	1528,
	1345,
	1592,
	1559,
	1379,
	1592,
	1528,
	1345,
	1592,
	1528,
	1345,
	1592,
	1528,
	1345,
	2164,
	1953,
	2389,
	2389,
	2389,
	2155,
	2170,
	2389,
	1976,
	1976,
	1592,
	1588,
	1406,
	1592,
	1588,
	1406,
	1580,
	1379,
	1580,
	1379,
	1580,
	1379,
	1580,
	929,
	1580,
	936,
	1580,
	1379,
	2752,
	2752,
	2070,
	2666,
	2666,
	1701,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	817,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
