﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.Advertisements.Advertisement::.cctor()
extern void Advertisement__cctor_m55B37EAA6778C0DFDD8C5B1774FFA8E037DB291C (void);
// 0x00000002 System.Boolean UnityEngine.Advertisements.Advertisement::get_isInitialized()
extern void Advertisement_get_isInitialized_mE1F3FD392D5AC064468EBAFF0DAE86C7E9AAE4D3 (void);
// 0x00000003 System.Boolean UnityEngine.Advertisements.Advertisement::get_isSupported()
extern void Advertisement_get_isSupported_mCE49A7421985B605454CCF5CC7DB18D552C2FEBC (void);
// 0x00000004 System.Boolean UnityEngine.Advertisements.Advertisement::get_debugMode()
extern void Advertisement_get_debugMode_m3082EF5FBB7DD803EF16D5A8C46316C28D73C5B5 (void);
// 0x00000005 System.Void UnityEngine.Advertisements.Advertisement::set_debugMode(System.Boolean)
extern void Advertisement_set_debugMode_mFB9DE772DF5B7347DFE341A652D7F72FE6D55CC5 (void);
// 0x00000006 System.String UnityEngine.Advertisements.Advertisement::get_version()
extern void Advertisement_get_version_mD95EC5466A3238701D664FCC578AF4BD9F99E713 (void);
// 0x00000007 System.Boolean UnityEngine.Advertisements.Advertisement::get_isShowing()
extern void Advertisement_get_isShowing_m617BBBEC42A0995A6E4A7E719A43E61876BD561E (void);
// 0x00000008 System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String)
extern void Advertisement_Initialize_m92660FA629A7638E5AB650231550C9DC00A4607A (void);
// 0x00000009 System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean)
extern void Advertisement_Initialize_m5664C993F54DE75774CC7804CB0FCAE359F11FB4 (void);
// 0x0000000A System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean,System.Boolean)
extern void Advertisement_Initialize_m68FDC0B87FEF592AC4F6A3479D767928705B705E (void);
// 0x0000000B System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void Advertisement_Initialize_mF6B7ADD1D170CC4E1CA8E48B8D68D86DC049A609 (void);
// 0x0000000C System.Boolean UnityEngine.Advertisements.Advertisement::IsReady()
extern void Advertisement_IsReady_m4A073797AB507C7545D0E0D5AC2CFBA2C736235B (void);
// 0x0000000D System.Boolean UnityEngine.Advertisements.Advertisement::IsReady(System.String)
extern void Advertisement_IsReady_mFAA92CBCFC74928F5B3699F51236B46FCA687892 (void);
// 0x0000000E System.Void UnityEngine.Advertisements.Advertisement::Load(System.String)
extern void Advertisement_Load_mB6B3DB5E25F516967DC6F2D7537D0E0B17FB0607 (void);
// 0x0000000F System.Void UnityEngine.Advertisements.Advertisement::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void Advertisement_Load_mBB6ADE31C37A5689064E8734164988821A285917 (void);
// 0x00000010 System.Void UnityEngine.Advertisements.Advertisement::Show()
extern void Advertisement_Show_m42F084D05E5A058BCBC370A2D73E507AADE9DD04 (void);
// 0x00000011 System.Void UnityEngine.Advertisements.Advertisement::Show(UnityEngine.Advertisements.ShowOptions)
extern void Advertisement_Show_m4CB6F75CBC3C9D05C721D8009249940F2EE38A16 (void);
// 0x00000012 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String)
extern void Advertisement_Show_m5FC47893F04B467D5579AF4ABEF792CE746B01CA (void);
// 0x00000013 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.ShowOptions)
extern void Advertisement_Show_m0B8ADCA10ECD3484C03DD6D1BB19BBB0EC4E491B (void);
// 0x00000014 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void Advertisement_Show_m34902C3B7988454CC49D7A8998DE989A026EE011 (void);
// 0x00000015 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.ShowOptions,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void Advertisement_Show_m7D59675BDE03FE109C86AC54E8633E0080F1D2AF (void);
// 0x00000016 System.Void UnityEngine.Advertisements.Advertisement::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void Advertisement_SetMetaData_mCB58637A39EBA7E0C85B1F313313B46A97F075FC (void);
// 0x00000017 System.Void UnityEngine.Advertisements.Advertisement::AddListener(UnityEngine.Advertisements.IUnityAdsListener)
extern void Advertisement_AddListener_m68BE3528110C03EB81A2B16BF014FAA1B2495A8B (void);
// 0x00000018 System.Void UnityEngine.Advertisements.Advertisement::RemoveListener(UnityEngine.Advertisements.IUnityAdsListener)
extern void Advertisement_RemoveListener_mB68A567A15484638624F76761FF8A9B1077F2322 (void);
// 0x00000019 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Advertisement::GetPlacementState()
extern void Advertisement_GetPlacementState_m51E01B35332445AA86BC63FE8D94229AFE18323A (void);
// 0x0000001A UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Advertisement::GetPlacementState(System.String)
extern void Advertisement_GetPlacementState_m4245EDD08E0353F04FFF4CAAB281EB4928F22EFB (void);
// 0x0000001B UnityEngine.Advertisements.Platform.IPlatform UnityEngine.Advertisements.Advertisement::CreatePlatform()
extern void Advertisement_CreatePlatform_mE3E09BC5DC0960DB198EBB5A0181268CEFB53B26 (void);
// 0x0000001C System.Boolean UnityEngine.Advertisements.Advertisement::IsSupported()
extern void Advertisement_IsSupported_m5738C47975B90FBBB3CA78220874852DCB13F1EF (void);
// 0x0000001D System.Void UnityEngine.Advertisements.Advertisement/Banner::Load()
extern void Banner_Load_m543A220672814D7329E3306070E00A4684D68BCC (void);
// 0x0000001E System.Void UnityEngine.Advertisements.Advertisement/Banner::Load(UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_Load_m4576FDC64A45C1E5CA9CF541113A24C3692EFAA8 (void);
// 0x0000001F System.Void UnityEngine.Advertisements.Advertisement/Banner::Load(System.String)
extern void Banner_Load_mB061EE7F25E1F59E4955FA9698B7FAE42E85133B (void);
// 0x00000020 System.Void UnityEngine.Advertisements.Advertisement/Banner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_Load_mEE4C2419EFC6B8A01AF76F8EE3C6EF41C1D7525D (void);
// 0x00000021 System.Void UnityEngine.Advertisements.Advertisement/Banner::Show()
extern void Banner_Show_mBF33E7A4D61302CCDAD07294976557DB8C18CC80 (void);
// 0x00000022 System.Void UnityEngine.Advertisements.Advertisement/Banner::Show(UnityEngine.Advertisements.BannerOptions)
extern void Banner_Show_m319A41E5B6978242F6A4BD3AF5E23E36F6748901 (void);
// 0x00000023 System.Void UnityEngine.Advertisements.Advertisement/Banner::Show(System.String)
extern void Banner_Show_m2AF3F1A0D3DDE735A8C65E3326E68A43F3197D0B (void);
// 0x00000024 System.Void UnityEngine.Advertisements.Advertisement/Banner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_Show_m59FDCFA112321CF7B9EBF0970D4385527404470E (void);
// 0x00000025 System.Void UnityEngine.Advertisements.Advertisement/Banner::Hide(System.Boolean)
extern void Banner_Hide_m480E589E3D6627B36D18B6C7828DD9D92D81D7DE (void);
// 0x00000026 System.Void UnityEngine.Advertisements.Advertisement/Banner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void Banner_SetPosition_m21160D8F6A0B968BE1C0DE1EF58F52C586861B50 (void);
// 0x00000027 System.Boolean UnityEngine.Advertisements.Advertisement/Banner::get_isLoaded()
extern void Banner_get_isLoaded_m7492B9D63B3223AC970B6F553E5B29CCCDF030A2 (void);
// 0x00000028 System.Void UnityEngine.Advertisements.Advertisement/<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_mC9305C166F1CCBFA2902722C61B33D35D2B688E9 (void);
// 0x00000029 System.Void UnityEngine.Advertisements.Advertisement/<>c__DisplayClass32_0::<CreatePlatform>b__0()
extern void U3CU3Ec__DisplayClass32_0_U3CCreatePlatformU3Eb__0_mD647534AF94720DD11E4D07E9474BAA34EA3E262 (void);
// 0x0000002A UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.Banner::get_UnityLifecycleManager()
extern void Banner_get_UnityLifecycleManager_m4298440183255280539AFEEC0D67BC665AF3D9D3 (void);
// 0x0000002B System.Boolean UnityEngine.Advertisements.Banner::get_IsLoaded()
extern void Banner_get_IsLoaded_m9D681407B9841D075AB9B83CF09845BB7370FB75 (void);
// 0x0000002C System.Boolean UnityEngine.Advertisements.Banner::get_ShowAfterLoad()
extern void Banner_get_ShowAfterLoad_mE2C539504647365336CD4887931416DE66DAECFF (void);
// 0x0000002D System.Void UnityEngine.Advertisements.Banner::set_ShowAfterLoad(System.Boolean)
extern void Banner_set_ShowAfterLoad_m0AFE75383CABFAAF830DA0F596ECBBD1F03E30A5 (void);
// 0x0000002E System.Void UnityEngine.Advertisements.Banner::.ctor(UnityEngine.Advertisements.INativeBanner,UnityEngine.Advertisements.Utilities.IUnityLifecycleManager)
extern void Banner__ctor_m52317E5AA6F85656EC736E9E9380E6298A729005 (void);
// 0x0000002F System.Void UnityEngine.Advertisements.Banner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_Load_m0FBA8D60FFFF13458F0958AE09A9B298FB0EE1AF (void);
// 0x00000030 System.Void UnityEngine.Advertisements.Banner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_Show_m26A6B33CE09D54771D70BB78262C70B929909D1A (void);
// 0x00000031 System.Void UnityEngine.Advertisements.Banner::Hide(System.Boolean)
extern void Banner_Hide_m58289608D32D0E5DCEAE2F603B3550E6B06B5D70 (void);
// 0x00000032 System.Void UnityEngine.Advertisements.Banner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void Banner_SetPosition_m19E19023F13AABBE4A514A82AF96449602FE49AB (void);
// 0x00000033 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidShow(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_UnityAdsBannerDidShow_mCA87BD2F6AC8F685CCE4FCB754FF0640FF9AF141 (void);
// 0x00000034 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidHide(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_UnityAdsBannerDidHide_m576934D95174441C51B1F5D1C5933DE11E958C1A (void);
// 0x00000035 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerClick(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_UnityAdsBannerClick_mE6CCF79E0B78723BA010A58726AB6F775AAE7CD4 (void);
// 0x00000036 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidLoad(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_UnityAdsBannerDidLoad_m2FD24BA7CC8119182CFCA22848C08A30E304A410 (void);
// 0x00000037 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidError(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_UnityAdsBannerDidError_mAAE9664EEB6C74C43304233425916A57622ECAF1 (void);
// 0x00000038 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m666F7A0193AE0BC9A6046F75C784C6BDC8BEB6C5 (void);
// 0x00000039 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass15_0::<UnityAdsBannerDidShow>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CUnityAdsBannerDidShowU3Eb__0_m0CA194E73E32C8CE25F408A3D03EE986453EF4DC (void);
// 0x0000003A System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_mDEA1A989E11F4A8E4118A33DBECE3589EF50F7E3 (void);
// 0x0000003B System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass16_0::<UnityAdsBannerDidHide>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CUnityAdsBannerDidHideU3Eb__0_mB5ADF99F40033E8F0F37D3CD91D541B998866913 (void);
// 0x0000003C System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_mE5F311BA199E31690D9DA03E04DD83B36AD4CA83 (void);
// 0x0000003D System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass17_0::<UnityAdsBannerClick>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CUnityAdsBannerClickU3Eb__0_m1F371AC329130AAAD060F6882718B6CF32004CDB (void);
// 0x0000003E System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m7568DDEE129B203E325828F83B21F7B6AD52BCDE (void);
// 0x0000003F System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass18_0::<UnityAdsBannerDidLoad>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CUnityAdsBannerDidLoadU3Eb__0_m2CC8BBF2D27963593B5100A32C6C2696240F0D78 (void);
// 0x00000040 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_mDC1B997DAF7BF7B93399EABA0D6DD8680B2A0D53 (void);
// 0x00000041 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass19_0::<UnityAdsBannerDidError>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CUnityAdsBannerDidErrorU3Eb__0_m9A0B524EF9038AC91C943ABFB918704AEC567676 (void);
// 0x00000042 UnityEngine.Advertisements.BannerLoadOptions/LoadCallback UnityEngine.Advertisements.BannerLoadOptions::get_loadCallback()
extern void BannerLoadOptions_get_loadCallback_mE8ABEE9AD952F980D2461F833003903CEAFEDEE7 (void);
// 0x00000043 System.Void UnityEngine.Advertisements.BannerLoadOptions::set_loadCallback(UnityEngine.Advertisements.BannerLoadOptions/LoadCallback)
extern void BannerLoadOptions_set_loadCallback_m70938AC155B34D5EB87615C2C8E0DE7D7F75029C (void);
// 0x00000044 UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback UnityEngine.Advertisements.BannerLoadOptions::get_errorCallback()
extern void BannerLoadOptions_get_errorCallback_mFB13E6445519326E8DA9EADF963504B9EC8829D4 (void);
// 0x00000045 System.Void UnityEngine.Advertisements.BannerLoadOptions::set_errorCallback(UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback)
extern void BannerLoadOptions_set_errorCallback_mC365EFE41F1A7974355B3E71AFCA92BB4D68B29F (void);
// 0x00000046 System.Void UnityEngine.Advertisements.BannerLoadOptions::.ctor()
extern void BannerLoadOptions__ctor_m11C46F5B31C61DCCC3F11EAA79A8F100BB2927EA (void);
// 0x00000047 System.Void UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::.ctor(System.Object,System.IntPtr)
extern void LoadCallback__ctor_m5C6B156898E4A0BF7F63331E1BA003102D010E7D (void);
// 0x00000048 System.Void UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::Invoke()
extern void LoadCallback_Invoke_mB7E5112ADB372535197BE17D07DBB0549972F2B1 (void);
// 0x00000049 System.IAsyncResult UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void LoadCallback_BeginInvoke_mBE67F6EE102F66C742C480C3EE04CEACC613CBB8 (void);
// 0x0000004A System.Void UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::EndInvoke(System.IAsyncResult)
extern void LoadCallback_EndInvoke_mBBE460F9CDC5EE9AFDD5BC1F0EDA5F6E4BD9B27D (void);
// 0x0000004B System.Void UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::.ctor(System.Object,System.IntPtr)
extern void ErrorCallback__ctor_m64E564F751D8613649A24FD6248CC0F15881B290 (void);
// 0x0000004C System.Void UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::Invoke(System.String)
extern void ErrorCallback_Invoke_mE37BDF37B126110B0389122400D95050C7CCF2DC (void);
// 0x0000004D System.IAsyncResult UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void ErrorCallback_BeginInvoke_mFFCCDA11A8B57C19F874B2DAF32788B853EFF89A (void);
// 0x0000004E System.Void UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::EndInvoke(System.IAsyncResult)
extern void ErrorCallback_EndInvoke_m2CCB7F607ABBB52FECE82E577D3F2654535E9EE6 (void);
// 0x0000004F UnityEngine.Advertisements.BannerOptions/BannerCallback UnityEngine.Advertisements.BannerOptions::get_showCallback()
extern void BannerOptions_get_showCallback_m3B442270F8690D7E4EF1220C9679501E5924AA00 (void);
// 0x00000050 System.Void UnityEngine.Advertisements.BannerOptions::set_showCallback(UnityEngine.Advertisements.BannerOptions/BannerCallback)
extern void BannerOptions_set_showCallback_mD534AFCE62B60095EBC35BFAF2AAB18E1A00EF18 (void);
// 0x00000051 UnityEngine.Advertisements.BannerOptions/BannerCallback UnityEngine.Advertisements.BannerOptions::get_hideCallback()
extern void BannerOptions_get_hideCallback_m323343D8356C62E3AB02268F6DF2F3B4E3E8F83D (void);
// 0x00000052 System.Void UnityEngine.Advertisements.BannerOptions::set_hideCallback(UnityEngine.Advertisements.BannerOptions/BannerCallback)
extern void BannerOptions_set_hideCallback_mB317257D7CF7C6644EEE71D1FDA2A9AE60FCFFFC (void);
// 0x00000053 UnityEngine.Advertisements.BannerOptions/BannerCallback UnityEngine.Advertisements.BannerOptions::get_clickCallback()
extern void BannerOptions_get_clickCallback_mC0A074CB4F0E32BA76CEF9D06C4E4B442810B7AB (void);
// 0x00000054 System.Void UnityEngine.Advertisements.BannerOptions::set_clickCallback(UnityEngine.Advertisements.BannerOptions/BannerCallback)
extern void BannerOptions_set_clickCallback_m1820691B6A579CC5E45AFE8495492AF5B1678B10 (void);
// 0x00000055 System.Void UnityEngine.Advertisements.BannerOptions::.ctor()
extern void BannerOptions__ctor_m475BE772F884A0ED8AA86394A9DE5E90EC4B36CC (void);
// 0x00000056 System.Void UnityEngine.Advertisements.BannerOptions/BannerCallback::.ctor(System.Object,System.IntPtr)
extern void BannerCallback__ctor_m16A5562A17AF3F938A667EA5B012C3CB8FAF27DF (void);
// 0x00000057 System.Void UnityEngine.Advertisements.BannerOptions/BannerCallback::Invoke()
extern void BannerCallback_Invoke_m5B9FE80ACEB47D7682C4086592C98EA517C1F047 (void);
// 0x00000058 System.IAsyncResult UnityEngine.Advertisements.BannerOptions/BannerCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void BannerCallback_BeginInvoke_m358C6F3BFE6DD0930F5F8E9EEDDBE82E41F57AA4 (void);
// 0x00000059 System.Void UnityEngine.Advertisements.BannerOptions/BannerCallback::EndInvoke(System.IAsyncResult)
extern void BannerCallback_EndInvoke_m5CDBE59D8BF7BBA4FB90021DBA165946C646D8CB (void);
// 0x0000005A System.Boolean UnityEngine.Advertisements.Configuration::get_enabled()
extern void Configuration_get_enabled_mB1B054D1A03505A4869B25EE3729E2A63C7CE7F4 (void);
// 0x0000005B System.String UnityEngine.Advertisements.Configuration::get_defaultPlacement()
extern void Configuration_get_defaultPlacement_m4B8BB68526D93DFDF92BBEC3750EBAEC351A60E0 (void);
// 0x0000005C System.Collections.Generic.Dictionary`2<System.String,System.Boolean> UnityEngine.Advertisements.Configuration::get_placements()
extern void Configuration_get_placements_mF146B5494CA8F1922582EA35A59FD52FDBB457A6 (void);
// 0x0000005D System.Void UnityEngine.Advertisements.Configuration::.ctor(System.String)
extern void Configuration__ctor_m252F492C6D21FEDFAC92F1EDEA03FE1FBFA9C44C (void);
// 0x0000005E UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.IBanner::get_UnityLifecycleManager()
// 0x0000005F System.Boolean UnityEngine.Advertisements.IBanner::get_IsLoaded()
// 0x00000060 System.Boolean UnityEngine.Advertisements.IBanner::get_ShowAfterLoad()
// 0x00000061 System.Void UnityEngine.Advertisements.IBanner::set_ShowAfterLoad(System.Boolean)
// 0x00000062 System.Void UnityEngine.Advertisements.IBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x00000063 System.Void UnityEngine.Advertisements.IBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000064 System.Void UnityEngine.Advertisements.IBanner::Hide(System.Boolean)
// 0x00000065 System.Void UnityEngine.Advertisements.IBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
// 0x00000066 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerDidShow(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000067 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerDidHide(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000068 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerClick(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000069 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerDidLoad(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x0000006A System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerDidError(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x0000006B System.Boolean UnityEngine.Advertisements.INativeBanner::get_IsLoaded()
// 0x0000006C System.Void UnityEngine.Advertisements.INativeBanner::SetupBanner(UnityEngine.Advertisements.IBanner)
// 0x0000006D System.Void UnityEngine.Advertisements.INativeBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x0000006E System.Void UnityEngine.Advertisements.INativeBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x0000006F System.Void UnityEngine.Advertisements.INativeBanner::Hide(System.Boolean)
// 0x00000070 System.Void UnityEngine.Advertisements.INativeBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
// 0x00000071 System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsReady(System.String)
// 0x00000072 System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsDidError(System.String)
// 0x00000073 System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsDidStart(System.String)
// 0x00000074 System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsDidFinish(System.String,UnityEngine.Advertisements.ShowResult)
// 0x00000075 System.Void UnityEngine.Advertisements.IUnityAdsInitializationListener::OnInitializationComplete()
// 0x00000076 System.Void UnityEngine.Advertisements.IUnityAdsInitializationListener::OnInitializationFailed(UnityEngine.Advertisements.UnityAdsInitializationError,System.String)
// 0x00000077 System.Void UnityEngine.Advertisements.IUnityAdsLoadListener::OnUnityAdsAdLoaded(System.String)
// 0x00000078 System.Void UnityEngine.Advertisements.IUnityAdsLoadListener::OnUnityAdsFailedToLoad(System.String,UnityEngine.Advertisements.UnityAdsLoadError,System.String)
// 0x00000079 System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowFailure(System.String,UnityEngine.Advertisements.UnityAdsShowError,System.String)
// 0x0000007A System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowStart(System.String)
// 0x0000007B System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowClick(System.String)
// 0x0000007C System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowComplete(System.String,UnityEngine.Advertisements.UnityAdsShowCompletionState)
// 0x0000007D System.String UnityEngine.Advertisements.MetaData::get_category()
extern void MetaData_get_category_mD83A7B88A5187E0CE29A26379205B193A8910FAC (void);
// 0x0000007E System.Void UnityEngine.Advertisements.MetaData::set_category(System.String)
extern void MetaData_set_category_mC44964210205E2CA4F22879C9951D3192252536B (void);
// 0x0000007F System.Void UnityEngine.Advertisements.MetaData::.ctor(System.String)
extern void MetaData__ctor_mADC212484C7A336E5C005E8CDC761BA44F3473DF (void);
// 0x00000080 System.Void UnityEngine.Advertisements.MetaData::Set(System.String,System.Object)
extern void MetaData_Set_m1E117B9B540A164E3E0E16D0F86E10A7D5F61D20 (void);
// 0x00000081 System.Object UnityEngine.Advertisements.MetaData::Get(System.String)
extern void MetaData_Get_mD79A32153785D05BEF1FA97F20B138A88E18344B (void);
// 0x00000082 System.Collections.Generic.IDictionary`2<System.String,System.Object> UnityEngine.Advertisements.MetaData::Values()
extern void MetaData_Values_mAA34EFAC1D98BEFEE81F756B24FAB1DC4C5CADEC (void);
// 0x00000083 System.String UnityEngine.Advertisements.MetaData::ToJSON()
extern void MetaData_ToJSON_m2C9C500EFA0377F8C17660D7D415BCA7E23AE4B6 (void);
// 0x00000084 System.Void UnityEngine.Advertisements.AndroidInitializationListener::.ctor(UnityEngine.Advertisements.Platform.IPlatform,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void AndroidInitializationListener__ctor_m390D6AB97AA27C1A174B3018E27A3ECCC1277136 (void);
// 0x00000085 System.Void UnityEngine.Advertisements.AndroidInitializationListener::onInitializationComplete()
extern void AndroidInitializationListener_onInitializationComplete_mA7F23C70D5C18F0F401C824FB264D236B2CAE990 (void);
// 0x00000086 System.Void UnityEngine.Advertisements.AndroidInitializationListener::onInitializationFailed(UnityEngine.AndroidJavaObject,System.String)
extern void AndroidInitializationListener_onInitializationFailed_mA895BB86AC1F820FDAFF3237507B48A7DB7DBC35 (void);
// 0x00000087 System.Void UnityEngine.Advertisements.AndroidLoadListener::.ctor(UnityEngine.Advertisements.Platform.IPlatform,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void AndroidLoadListener__ctor_mFEDFE065C4A48039F24D44266D614C59D9009557 (void);
// 0x00000088 System.Void UnityEngine.Advertisements.AndroidLoadListener::onUnityAdsAdLoaded(System.String)
extern void AndroidLoadListener_onUnityAdsAdLoaded_m076BE77109B73FBE785616436DF7B1B616B957D1 (void);
// 0x00000089 System.Void UnityEngine.Advertisements.AndroidLoadListener::onUnityAdsFailedToLoad(System.String,UnityEngine.AndroidJavaObject,System.String)
extern void AndroidLoadListener_onUnityAdsFailedToLoad_mED87CEFEEFDE6E74AEE5BECCEAEEED45B93DA908 (void);
// 0x0000008A System.Void UnityEngine.Advertisements.AndroidShowListener::.ctor(UnityEngine.Advertisements.Platform.IPlatform,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void AndroidShowListener__ctor_mA58C7BF0BA7F63A671DABABF4165302C0E712E29 (void);
// 0x0000008B System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowFailure(System.String,UnityEngine.AndroidJavaObject,System.String)
extern void AndroidShowListener_onUnityAdsShowFailure_mF860BEBD12F621D9404647310D58C5AB3BA40ACC (void);
// 0x0000008C System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowStart(System.String)
extern void AndroidShowListener_onUnityAdsShowStart_m145CE17471FAA3F08D8EC19ECC2883585FF8D2A5 (void);
// 0x0000008D System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowClick(System.String)
extern void AndroidShowListener_onUnityAdsShowClick_mB195E707292A21577945EA98C73B1BF3DB60B4B7 (void);
// 0x0000008E System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowComplete(System.String,UnityEngine.AndroidJavaObject)
extern void AndroidShowListener_onUnityAdsShowComplete_mEE317B77C88A6F3517D2F8EA9386FA78E701ED20 (void);
// 0x0000008F System.Void UnityEngine.Advertisements.INativePlatform::SetupPlatform(UnityEngine.Advertisements.Platform.IPlatform)
// 0x00000090 System.Void UnityEngine.Advertisements.INativePlatform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
// 0x00000091 System.Void UnityEngine.Advertisements.INativePlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
// 0x00000092 System.Void UnityEngine.Advertisements.INativePlatform::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
// 0x00000093 System.Void UnityEngine.Advertisements.INativePlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
// 0x00000094 System.Boolean UnityEngine.Advertisements.INativePlatform::GetDebugMode()
// 0x00000095 System.Void UnityEngine.Advertisements.INativePlatform::SetDebugMode(System.Boolean)
// 0x00000096 System.String UnityEngine.Advertisements.INativePlatform::GetVersion()
// 0x00000097 System.Boolean UnityEngine.Advertisements.INativePlatform::IsInitialized()
// 0x00000098 System.Boolean UnityEngine.Advertisements.INativePlatform::IsReady(System.String)
// 0x00000099 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.INativePlatform::GetPlacementState(System.String)
// 0x0000009A System.String UnityEngine.Advertisements.INativePlatform::GetDefaultPlacement()
// 0x0000009B System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.ShowOptions::get_resultCallback()
extern void ShowOptions_get_resultCallback_mF55D6D3446642D4F59EDCEDF697411A4D9684CE2 (void);
// 0x0000009C System.Void UnityEngine.Advertisements.ShowOptions::set_resultCallback(System.Action`1<UnityEngine.Advertisements.ShowResult>)
extern void ShowOptions_set_resultCallback_mBCD882830D968BC1D65AA0CCD8E57D577D158CEF (void);
// 0x0000009D System.String UnityEngine.Advertisements.ShowOptions::get_gamerSid()
extern void ShowOptions_get_gamerSid_mBF1D8EEA975EE847C7707416D153551D1F501284 (void);
// 0x0000009E System.Void UnityEngine.Advertisements.ShowOptions::set_gamerSid(System.String)
extern void ShowOptions_set_gamerSid_mCC249C4308EC9F4137359C2006ABC9D8FEDDBC1C (void);
// 0x0000009F System.Void UnityEngine.Advertisements.ShowOptions::.ctor()
extern void ShowOptions__ctor_m2E5353F6B1CFBF5F3C861F110851816148FF8629 (void);
// 0x000000A0 System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::add_OnApplicationQuitEventHandler(UnityEngine.Events.UnityAction)
extern void ApplicationQuit_add_OnApplicationQuitEventHandler_m5BA122587548132F8998C86230E390E0A70CA554 (void);
// 0x000000A1 System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::remove_OnApplicationQuitEventHandler(UnityEngine.Events.UnityAction)
extern void ApplicationQuit_remove_OnApplicationQuitEventHandler_m3D8D434F1B90CFC3DB470A50266B062F9AB6EB08 (void);
// 0x000000A2 System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::OnApplicationQuit()
extern void ApplicationQuit_OnApplicationQuit_mC36553870FB7111FA72118A04F4B6E08F63D4102 (void);
// 0x000000A3 System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::.ctor()
extern void ApplicationQuit__ctor_m6EE8BD1A05F13596893DB4DE1087F8735727CC6A (void);
// 0x000000A4 System.Void UnityEngine.Advertisements.Utilities.CoroutineExecutor::Update()
extern void CoroutineExecutor_Update_m57A3B6869444E3857B714F8082FD864D97C47283 (void);
// 0x000000A5 System.Void UnityEngine.Advertisements.Utilities.CoroutineExecutor::.ctor()
extern void CoroutineExecutor__ctor_m9017C8A0007D2FDE70B49F0276FABD41B3053881 (void);
// 0x000000A6 UnityEngine.Advertisements.ShowResult UnityEngine.Advertisements.Utilities.EnumUtilities::GetShowResultsFromCompletionState(UnityEngine.Advertisements.UnityAdsShowCompletionState)
extern void EnumUtilities_GetShowResultsFromCompletionState_mABD0D3CC79B8B33939F3A02C707F995A8A7D1D1C (void);
// 0x000000A7 T UnityEngine.Advertisements.Utilities.EnumUtilities::GetEnumFromAndroidJavaObject(UnityEngine.AndroidJavaObject,T)
// 0x000000A8 UnityEngine.Coroutine UnityEngine.Advertisements.Utilities.IUnityLifecycleManager::StartCoroutine(System.Collections.IEnumerator)
// 0x000000A9 System.Void UnityEngine.Advertisements.Utilities.IUnityLifecycleManager::Post(System.Action)
// 0x000000AA System.Void UnityEngine.Advertisements.Utilities.IUnityLifecycleManager::SetOnApplicationQuitCallback(UnityEngine.Events.UnityAction)
// 0x000000AB System.Object UnityEngine.Advertisements.Utilities.Json::Deserialize(System.String)
extern void Json_Deserialize_mBC5908FA40F6BD660F593A824C6CA7BED95E953D (void);
// 0x000000AC System.String UnityEngine.Advertisements.Utilities.Json::Serialize(System.Object)
extern void Json_Serialize_mB750CC5CEA90A869583AA1215F9464A7F89DC986 (void);
// 0x000000AD System.Boolean UnityEngine.Advertisements.Utilities.Json/Parser::IsWordBreak(System.Char)
extern void Parser_IsWordBreak_m92D76D951FFDF12111E4B5A7BDF6CAD892303C7F (void);
// 0x000000AE System.Void UnityEngine.Advertisements.Utilities.Json/Parser::.ctor(System.String)
extern void Parser__ctor_mA9E14C5AD9DFFDEA3CA07047C22162C5A861C1DB (void);
// 0x000000AF System.Object UnityEngine.Advertisements.Utilities.Json/Parser::Parse(System.String)
extern void Parser_Parse_mF946767D334740FDB518608297077778B5FDAB72 (void);
// 0x000000B0 System.Void UnityEngine.Advertisements.Utilities.Json/Parser::Dispose()
extern void Parser_Dispose_mE368EE5AA939C1A14D9ABD914F56ABD0D5E7646D (void);
// 0x000000B1 System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Advertisements.Utilities.Json/Parser::ParseObject()
extern void Parser_ParseObject_m610E118B2F01BC43D3955AA03661423CDFF3F81B (void);
// 0x000000B2 System.Collections.Generic.List`1<System.Object> UnityEngine.Advertisements.Utilities.Json/Parser::ParseArray()
extern void Parser_ParseArray_m9313844911AB0BAABF715C0160884D582FD75E5C (void);
// 0x000000B3 System.Object UnityEngine.Advertisements.Utilities.Json/Parser::ParseValue()
extern void Parser_ParseValue_m8AB8FB4B4261E8BD880D06D73C7E057FAE06961D (void);
// 0x000000B4 System.Object UnityEngine.Advertisements.Utilities.Json/Parser::ParseByToken(UnityEngine.Advertisements.Utilities.Json/Parser/TOKEN)
extern void Parser_ParseByToken_mAE13997C473CA8948DEE24D82AF8CE35E3E6AE21 (void);
// 0x000000B5 System.String UnityEngine.Advertisements.Utilities.Json/Parser::ParseString()
extern void Parser_ParseString_mFBF054B310FDCCC2189EFB0FA3050F103072C3A2 (void);
// 0x000000B6 System.Object UnityEngine.Advertisements.Utilities.Json/Parser::ParseNumber()
extern void Parser_ParseNumber_mEF76830818879BCD52D4F5C10616CA400E83BFBE (void);
// 0x000000B7 System.Void UnityEngine.Advertisements.Utilities.Json/Parser::EatWhitespace()
extern void Parser_EatWhitespace_m26FD89BF05A8EAC8251AA2EDDE1823FE89A3C420 (void);
// 0x000000B8 System.Char UnityEngine.Advertisements.Utilities.Json/Parser::get_PeekChar()
extern void Parser_get_PeekChar_mC8BD88BD89E4BB7C4A802F5EE5699F34366AB104 (void);
// 0x000000B9 System.Char UnityEngine.Advertisements.Utilities.Json/Parser::get_NextChar()
extern void Parser_get_NextChar_mAC16B4C5762A51EDFEB4D408B44E716F2676793C (void);
// 0x000000BA System.String UnityEngine.Advertisements.Utilities.Json/Parser::get_NextWord()
extern void Parser_get_NextWord_m2B11A27C774F3CA1B79C99C1CD2A578BB5DFA021 (void);
// 0x000000BB UnityEngine.Advertisements.Utilities.Json/Parser/TOKEN UnityEngine.Advertisements.Utilities.Json/Parser::get_NextToken()
extern void Parser_get_NextToken_m103C32FD7FEF6E4042BAB5C677542B484686F12C (void);
// 0x000000BC System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::.ctor()
extern void Serializer__ctor_mFA3827F3C7C52833050A2ADE9624D88530E7CA0F (void);
// 0x000000BD System.String UnityEngine.Advertisements.Utilities.Json/Serializer::Serialize(System.Object)
extern void Serializer_Serialize_m95376D17EC08D37E9651166ECD71CFAD0739D145 (void);
// 0x000000BE System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_mD7A69B1BB2EFCD26B508F2294000D61CA9522040 (void);
// 0x000000BF System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_mD265453BE69C5CCEE6EB1BFE93BABB917C023E1C (void);
// 0x000000C0 System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_m3B0D8184A2567D820C04D068E2407D9C779481A8 (void);
// 0x000000C1 System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_mBE823537F71AB3CD68F0A37B83C16F2CE035E671 (void);
// 0x000000C2 System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_mF44D246AA44FAA80EFD3831A5280DCD0C94977D5 (void);
// 0x000000C3 System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::.ctor()
extern void UnityLifecycleManager__ctor_mC028F74E5A0BF6351FB7C12779A4387FF8D7B13F (void);
// 0x000000C4 System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::Initialize()
extern void UnityLifecycleManager_Initialize_mF044C0FEA75BA482CDCDA11811FB3AE86BCBE78C (void);
// 0x000000C5 UnityEngine.Coroutine UnityEngine.Advertisements.Utilities.UnityLifecycleManager::StartCoroutine(System.Collections.IEnumerator)
extern void UnityLifecycleManager_StartCoroutine_m76BD162C15E4D98DE492242D032CF6C7F8DAE95F (void);
// 0x000000C6 System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::Post(System.Action)
extern void UnityLifecycleManager_Post_mE7F8331C2A4486D2587FB87B40CC3C60364B44E1 (void);
// 0x000000C7 System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::Dispose()
extern void UnityLifecycleManager_Dispose_mE81B52BB490DB4898DA231F1E37C03A2733E99D2 (void);
// 0x000000C8 System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::SetOnApplicationQuitCallback(UnityEngine.Events.UnityAction)
extern void UnityLifecycleManager_SetOnApplicationQuitCallback_mA8166BF6F0FE51C1E631A973FA86795491864E15 (void);
// 0x000000C9 System.Void UnityEngine.Advertisements.Purchasing.IPurchase::Initialize(UnityEngine.Advertisements.Purchasing.IPurchasingEventSender)
// 0x000000CA System.Void UnityEngine.Advertisements.Purchasing.IPurchase::SendEvent(System.String)
// 0x000000CB System.Void UnityEngine.Advertisements.Purchasing.IPurchase::onPurchasingCommand(System.String)
// 0x000000CC System.Void UnityEngine.Advertisements.Purchasing.IPurchase::onGetPurchasingVersion()
// 0x000000CD System.Void UnityEngine.Advertisements.Purchasing.IPurchase::onGetProductCatalog()
// 0x000000CE System.Void UnityEngine.Advertisements.Purchasing.IPurchase::onInitializePurchasing()
// 0x000000CF System.Void UnityEngine.Advertisements.Purchasing.IPurchasingEventSender::SendPurchasingEvent(System.String)
// 0x000000D0 System.Void UnityEngine.Advertisements.Purchasing.Purchase::onPurchasingCommand(System.String)
extern void Purchase_onPurchasingCommand_m4BD315135425D7BACC579F03E21D0F5286665F43 (void);
// 0x000000D1 System.Void UnityEngine.Advertisements.Purchasing.Purchase::onGetPurchasingVersion()
extern void Purchase_onGetPurchasingVersion_m0D90490EAAEA5702A978ECF58C93BD129C8877CC (void);
// 0x000000D2 System.Void UnityEngine.Advertisements.Purchasing.Purchase::onGetProductCatalog()
extern void Purchase_onGetProductCatalog_m1D748479ABAAFCBC1D2687A8E7AD2C728E1FC3AB (void);
// 0x000000D3 System.Void UnityEngine.Advertisements.Purchasing.Purchase::onInitializePurchasing()
extern void Purchase_onInitializePurchasing_m8C9275459E4230B25940336D30EB0142A6DD6578 (void);
// 0x000000D4 System.Void UnityEngine.Advertisements.Purchasing.Purchase::SendEvent(System.String)
extern void Purchase_SendEvent_m6F38AFFCC3E92DA3F4E095903F077BDC6E25F878 (void);
// 0x000000D5 System.Void UnityEngine.Advertisements.Purchasing.Purchase::Initialize(UnityEngine.Advertisements.Purchasing.IPurchasingEventSender)
extern void Purchase_Initialize_mFD80DE82B064CCBEB6F8C91C4C297C7069F7ECE5 (void);
// 0x000000D6 System.Void UnityEngine.Advertisements.Purchasing.Purchase::.ctor()
extern void Purchase__ctor_m0DDAA09E7127F7E1DF77E62A6622055D9821080B (void);
// 0x000000D7 System.Boolean UnityEngine.Advertisements.Purchasing.Purchasing::Initialize(UnityEngine.Advertisements.Purchasing.IPurchasingEventSender)
extern void Purchasing_Initialize_mB01307AD43F5C2C0CE24427A261FA1D7ADA9FE62 (void);
// 0x000000D8 System.Boolean UnityEngine.Advertisements.Purchasing.Purchasing::InitiatePurchasingCommand(System.String)
extern void Purchasing_InitiatePurchasingCommand_m68215657AA2A1DE4C28C39967975B5D55865B40B (void);
// 0x000000D9 System.String UnityEngine.Advertisements.Purchasing.Purchasing::GetPurchasingCatalog()
extern void Purchasing_GetPurchasingCatalog_m098F82AD49D95D3B81EDED0B1C96A95781C32DD9 (void);
// 0x000000DA System.String UnityEngine.Advertisements.Purchasing.Purchasing::GetPromoVersion()
extern void Purchasing_GetPromoVersion_m7D0958D7F10D3431213477611D03C7240BCB6C90 (void);
// 0x000000DB System.Boolean UnityEngine.Advertisements.Purchasing.Purchasing::SendEvent(System.String)
extern void Purchasing_SendEvent_mE7DA63FB35A5D3FAD21FE973D6EB717612B817FD (void);
// 0x000000DC System.Void UnityEngine.Advertisements.Purchasing.Purchasing::.cctor()
extern void Purchasing__cctor_m1362565F616AF8A89583563AC935BED776F2709A (void);
// 0x000000DD System.Void UnityEngine.Advertisements.Platform.IPlatform::add_OnStart(System.EventHandler`1<UnityEngine.Advertisements.Events.StartEventArgs>)
// 0x000000DE System.Void UnityEngine.Advertisements.Platform.IPlatform::remove_OnStart(System.EventHandler`1<UnityEngine.Advertisements.Events.StartEventArgs>)
// 0x000000DF System.Void UnityEngine.Advertisements.Platform.IPlatform::add_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.Events.FinishEventArgs>)
// 0x000000E0 System.Void UnityEngine.Advertisements.Platform.IPlatform::remove_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.Events.FinishEventArgs>)
// 0x000000E1 UnityEngine.Advertisements.IBanner UnityEngine.Advertisements.Platform.IPlatform::get_Banner()
// 0x000000E2 UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.Platform.IPlatform::get_UnityLifecycleManager()
// 0x000000E3 UnityEngine.Advertisements.INativePlatform UnityEngine.Advertisements.Platform.IPlatform::get_NativePlatform()
// 0x000000E4 System.Boolean UnityEngine.Advertisements.Platform.IPlatform::get_IsInitialized()
// 0x000000E5 System.Boolean UnityEngine.Advertisements.Platform.IPlatform::get_IsShowing()
// 0x000000E6 System.String UnityEngine.Advertisements.Platform.IPlatform::get_Version()
// 0x000000E7 System.Boolean UnityEngine.Advertisements.Platform.IPlatform::get_DebugMode()
// 0x000000E8 System.Void UnityEngine.Advertisements.Platform.IPlatform::set_DebugMode(System.Boolean)
// 0x000000E9 System.Collections.Generic.HashSet`1<UnityEngine.Advertisements.IUnityAdsListener> UnityEngine.Advertisements.Platform.IPlatform::get_Listeners()
// 0x000000EA System.Void UnityEngine.Advertisements.Platform.IPlatform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
// 0x000000EB System.Void UnityEngine.Advertisements.Platform.IPlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
// 0x000000EC System.Void UnityEngine.Advertisements.Platform.IPlatform::Show(System.String,UnityEngine.Advertisements.ShowOptions,UnityEngine.Advertisements.IUnityAdsShowListener)
// 0x000000ED System.Void UnityEngine.Advertisements.Platform.IPlatform::Show()
// 0x000000EE System.Void UnityEngine.Advertisements.Platform.IPlatform::AddListener(UnityEngine.Advertisements.IUnityAdsListener)
// 0x000000EF System.Void UnityEngine.Advertisements.Platform.IPlatform::RemoveListener(UnityEngine.Advertisements.IUnityAdsListener)
// 0x000000F0 System.Boolean UnityEngine.Advertisements.Platform.IPlatform::IsReady(System.String)
// 0x000000F1 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Platform.IPlatform::GetPlacementState(System.String)
// 0x000000F2 System.Void UnityEngine.Advertisements.Platform.IPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
// 0x000000F3 System.Void UnityEngine.Advertisements.Platform.IPlatform::UnityAdsReady(System.String)
// 0x000000F4 System.Void UnityEngine.Advertisements.Platform.IPlatform::UnityAdsDidError(System.String)
// 0x000000F5 System.Void UnityEngine.Advertisements.Platform.IPlatform::UnityAdsDidStart(System.String)
// 0x000000F6 System.Void UnityEngine.Advertisements.Platform.IPlatform::UnityAdsDidFinish(System.String,UnityEngine.Advertisements.ShowResult)
// 0x000000F7 System.Void UnityEngine.Advertisements.Platform.Platform::add_OnStart(System.EventHandler`1<UnityEngine.Advertisements.Events.StartEventArgs>)
extern void Platform_add_OnStart_m0E359896C499DA111FA6D7BBCB579370B6DC6818 (void);
// 0x000000F8 System.Void UnityEngine.Advertisements.Platform.Platform::remove_OnStart(System.EventHandler`1<UnityEngine.Advertisements.Events.StartEventArgs>)
extern void Platform_remove_OnStart_mA5827B814FBFD3DD6FE528F8A51A86B793A55091 (void);
// 0x000000F9 System.Void UnityEngine.Advertisements.Platform.Platform::add_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.Events.FinishEventArgs>)
extern void Platform_add_OnFinish_m077D1B927413A2D4E1AB207D1403FB8DE63BFEE1 (void);
// 0x000000FA System.Void UnityEngine.Advertisements.Platform.Platform::remove_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.Events.FinishEventArgs>)
extern void Platform_remove_OnFinish_m02EF7DAD5880B09ED29A21B73158C5F6557F5E3C (void);
// 0x000000FB UnityEngine.Advertisements.IBanner UnityEngine.Advertisements.Platform.Platform::get_Banner()
extern void Platform_get_Banner_m32E6640F032949FD021273065D8ACD8E787D8797 (void);
// 0x000000FC UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.Platform.Platform::get_UnityLifecycleManager()
extern void Platform_get_UnityLifecycleManager_m3277D76F465C3BD1480160A27C0910039D7596ED (void);
// 0x000000FD UnityEngine.Advertisements.INativePlatform UnityEngine.Advertisements.Platform.Platform::get_NativePlatform()
extern void Platform_get_NativePlatform_m59B0E3DA713B8EDB3DB0312CADAAE62E7589E24D (void);
// 0x000000FE System.Boolean UnityEngine.Advertisements.Platform.Platform::get_IsInitialized()
extern void Platform_get_IsInitialized_m916A6B8BDCD47C10EB81C38966BADD4C594F0D73 (void);
// 0x000000FF System.Boolean UnityEngine.Advertisements.Platform.Platform::get_IsShowing()
extern void Platform_get_IsShowing_m9CB1DE3B8D8A12530B2E07C062C65926928483FB (void);
// 0x00000100 System.Void UnityEngine.Advertisements.Platform.Platform::set_IsShowing(System.Boolean)
extern void Platform_set_IsShowing_m0EDC8FA0575E9DFADC2DEA59E9A1A140C129EABA (void);
// 0x00000101 System.String UnityEngine.Advertisements.Platform.Platform::get_Version()
extern void Platform_get_Version_mF51946AC7BA222C082E3F749723DF58BF71062F9 (void);
// 0x00000102 System.Boolean UnityEngine.Advertisements.Platform.Platform::get_DebugMode()
extern void Platform_get_DebugMode_mFB6A1AD18C0521D0DB1BD06B563CAE9DC5B0FC44 (void);
// 0x00000103 System.Void UnityEngine.Advertisements.Platform.Platform::set_DebugMode(System.Boolean)
extern void Platform_set_DebugMode_m4470F3109E718281ED0AA7872355126E89FDE602 (void);
// 0x00000104 System.Collections.Generic.HashSet`1<UnityEngine.Advertisements.IUnityAdsListener> UnityEngine.Advertisements.Platform.Platform::get_Listeners()
extern void Platform_get_Listeners_m766F8CE93F16E9F26050BCBE90B2FB091B5084F0 (void);
// 0x00000105 System.Void UnityEngine.Advertisements.Platform.Platform::.ctor(UnityEngine.Advertisements.INativePlatform,UnityEngine.Advertisements.IBanner,UnityEngine.Advertisements.Utilities.IUnityLifecycleManager)
extern void Platform__ctor_mB21B8BAFDDF845992AC21EA7DC4D8CAC28E560AC (void);
// 0x00000106 System.Void UnityEngine.Advertisements.Platform.Platform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void Platform_Initialize_m034B9400C2D88F5FC92F3948E166F92B786787A5 (void);
// 0x00000107 System.Void UnityEngine.Advertisements.Platform.Platform::Load(System.String)
extern void Platform_Load_m614909C27C4E2EDBB0FD1F6F40AE9CB93579D223 (void);
// 0x00000108 System.Void UnityEngine.Advertisements.Platform.Platform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void Platform_Load_m6407AEB8653C57CFA86911B46E81765AEA31ACCC (void);
// 0x00000109 System.Void UnityEngine.Advertisements.Platform.Platform::Show(System.String,UnityEngine.Advertisements.ShowOptions,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void Platform_Show_m5A18F9AFF5FB017CB067E5A6FFCA308BDE8A792B (void);
// 0x0000010A System.Void UnityEngine.Advertisements.Platform.Platform::Show()
extern void Platform_Show_m1A91CAEBDFB6D251D865578D9084D232C3F8E0D6 (void);
// 0x0000010B System.Void UnityEngine.Advertisements.Platform.Platform::AddListener(UnityEngine.Advertisements.IUnityAdsListener)
extern void Platform_AddListener_m9B5D2B74E7E5FF80EAF3C089B5A68863C505B3FE (void);
// 0x0000010C System.Void UnityEngine.Advertisements.Platform.Platform::RemoveListener(UnityEngine.Advertisements.IUnityAdsListener)
extern void Platform_RemoveListener_mBBBF242157AA72AF36AF14ABC9D5150F81772B0C (void);
// 0x0000010D System.Boolean UnityEngine.Advertisements.Platform.Platform::IsReady(System.String)
extern void Platform_IsReady_mB39150ABBFEFB422F75250E04144E258CC609F73 (void);
// 0x0000010E UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Platform.Platform::GetPlacementState(System.String)
extern void Platform_GetPlacementState_m5C7B7C8320F7AA7B2EC4DA0CDC0718C60E41B02B (void);
// 0x0000010F System.Void UnityEngine.Advertisements.Platform.Platform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void Platform_SetMetaData_m10EC61FECF881CC83AF4C6AA8A61F357CF2C7951 (void);
// 0x00000110 System.Void UnityEngine.Advertisements.Platform.Platform::UnityAdsReady(System.String)
extern void Platform_UnityAdsReady_m0072B251E469746C710D32B35DD6B7C1F9FD408B (void);
// 0x00000111 System.Void UnityEngine.Advertisements.Platform.Platform::UnityAdsDidError(System.String)
extern void Platform_UnityAdsDidError_m555871023730B8C771820952022945D5BBB4CBF5 (void);
// 0x00000112 System.Void UnityEngine.Advertisements.Platform.Platform::UnityAdsDidStart(System.String)
extern void Platform_UnityAdsDidStart_m01F88ABD0988E47705B26BAE0F77683EE57C75FB (void);
// 0x00000113 System.Void UnityEngine.Advertisements.Platform.Platform::UnityAdsDidFinish(System.String,UnityEngine.Advertisements.ShowResult)
extern void Platform_UnityAdsDidFinish_m8F69FE8F0EDAAF40FE606ED0771ADF9709A53CB6 (void);
// 0x00000114 System.Collections.Generic.HashSet`1<UnityEngine.Advertisements.IUnityAdsListener> UnityEngine.Advertisements.Platform.Platform::GetClonedHashSet(System.Collections.Generic.HashSet`1<UnityEngine.Advertisements.IUnityAdsListener>)
extern void Platform_GetClonedHashSet_m66222BCAC39FD6C20D5AB9007AE80B9EFAFA9D95 (void);
// 0x00000115 System.Void UnityEngine.Advertisements.Platform.Platform::<Initialize>b__30_0(System.Object,UnityEngine.Advertisements.Events.StartEventArgs)
extern void Platform_U3CInitializeU3Eb__30_0_mDE420A6B013AD539E0EA6B8FEA62B89B4F4C3DAE (void);
// 0x00000116 System.Void UnityEngine.Advertisements.Platform.Platform::<Initialize>b__30_1(System.Object,UnityEngine.Advertisements.Events.FinishEventArgs)
extern void Platform_U3CInitializeU3Eb__30_1_m36DEE7079FD9D677115115F4E033774AD1C27C3B (void);
// 0x00000117 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_m1187FAB01F2614A4389FA984DC10BD54A338E00B (void);
// 0x00000118 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass33_0::<Show>b__0(System.Object,UnityEngine.Advertisements.Events.FinishEventArgs)
extern void U3CU3Ec__DisplayClass33_0_U3CShowU3Eb__0_m66FA1D6F50F98B04BAFBB70A76C9DC144917B27C (void);
// 0x00000119 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_m992ECC5612F37563BEC9042E903C5D55AF66F842 (void);
// 0x0000011A System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass40_0::<UnityAdsReady>b__0()
extern void U3CU3Ec__DisplayClass40_0_U3CUnityAdsReadyU3Eb__0_m5D9FCE7625E28193E42D2D5E6164DCF6A96722EE (void);
// 0x0000011B System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass41_0::.ctor()
extern void U3CU3Ec__DisplayClass41_0__ctor_m64B6FBEA32AC10232A458AFCE600E5C1188A7955 (void);
// 0x0000011C System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass41_0::<UnityAdsDidError>b__0()
extern void U3CU3Ec__DisplayClass41_0_U3CUnityAdsDidErrorU3Eb__0_m601A08B3575F723406BFB9EAA5F7F86BB376F6FA (void);
// 0x0000011D System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass42_0::.ctor()
extern void U3CU3Ec__DisplayClass42_0__ctor_m692817492B2CAD5D2352F01F1D33AA838DF87A22 (void);
// 0x0000011E System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass42_0::<UnityAdsDidStart>b__0()
extern void U3CU3Ec__DisplayClass42_0_U3CUnityAdsDidStartU3Eb__0_mFA8724785A3DEE6D7F8613DD7C747A6288CB44F3 (void);
// 0x0000011F System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass43_0::.ctor()
extern void U3CU3Ec__DisplayClass43_0__ctor_m5D5F46F63A4BEC7B8470D68B55C6A3A21FE9E6F2 (void);
// 0x00000120 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass43_0::<UnityAdsDidFinish>b__0()
extern void U3CU3Ec__DisplayClass43_0_U3CUnityAdsDidFinishU3Eb__0_mB85579E15AE8BAB99E8F8AFB46EFF2CE6BA8B41B (void);
// 0x00000121 System.Boolean UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::get_IsLoaded()
extern void UnsupportedBanner_get_IsLoaded_mE65F2C64D685286044CDAF302E25ED51F917B462 (void);
// 0x00000122 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::SetupBanner(UnityEngine.Advertisements.IBanner)
extern void UnsupportedBanner_SetupBanner_m3B67680B83A772F8AFDB76E5273FE56FED6436BE (void);
// 0x00000123 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void UnsupportedBanner_Load_m660B71A6BA0CFA10857E0D2C55A06FB49361E64A (void);
// 0x00000124 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void UnsupportedBanner_Show_m206C9C930CB43C80DEFDCFBC0054DCE4C8CEC285 (void);
// 0x00000125 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::Hide(System.Boolean)
extern void UnsupportedBanner_Hide_mAAE6B3ED6A1908106E267844B1AC134DE727E5FF (void);
// 0x00000126 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void UnsupportedBanner_SetPosition_m3E869559906446B30EF088B97273331BC3209682 (void);
// 0x00000127 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::.ctor()
extern void UnsupportedBanner__ctor_m86ADC14A483A4805F76F94E750A3EF6CE28E52C7 (void);
// 0x00000128 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::SetupPlatform(UnityEngine.Advertisements.Platform.IPlatform)
extern void UnsupportedPlatform_SetupPlatform_m06B4CEE7F10AA180B5944AF466667D6C54A9A52A (void);
// 0x00000129 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void UnsupportedPlatform_Initialize_m8A2DA4302A24E0D168B672B3CF06102C96B3804F (void);
// 0x0000012A System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void UnsupportedPlatform_Load_m8FA1B6E44F1AB2E588630066EFF4B71311AD2311 (void);
// 0x0000012B System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void UnsupportedPlatform_Show_m94E02923D066E0E1805BFA2048F4DAE213226BC7 (void);
// 0x0000012C System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void UnsupportedPlatform_SetMetaData_mFC4DC0C68985337AC82BA740AD963E39A77A5D57 (void);
// 0x0000012D System.Boolean UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::GetDebugMode()
extern void UnsupportedPlatform_GetDebugMode_mF8CFF327B872E632A70758C9334B471256C27630 (void);
// 0x0000012E System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::SetDebugMode(System.Boolean)
extern void UnsupportedPlatform_SetDebugMode_mE199E6F904AF3FE4FECD68686BA57641EB55248F (void);
// 0x0000012F System.String UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::GetVersion()
extern void UnsupportedPlatform_GetVersion_mD6297F8A8B03FD67329F6B0B0A485530C7914DCC (void);
// 0x00000130 System.Boolean UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::IsInitialized()
extern void UnsupportedPlatform_IsInitialized_m15CD3A14F0CF65F2BC515D70C3DADC8D1B7FC807 (void);
// 0x00000131 System.Boolean UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::IsReady(System.String)
extern void UnsupportedPlatform_IsReady_mFC68DA517BD9850A7C2672FDDE9DE1E0CA543833 (void);
// 0x00000132 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::GetPlacementState(System.String)
extern void UnsupportedPlatform_GetPlacementState_m6D9540107DFD6F22BBD973338E9A8FCA4B10931E (void);
// 0x00000133 System.String UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::GetDefaultPlacement()
extern void UnsupportedPlatform_GetDefaultPlacement_m4C98E4CA23734293DA07A424ACFEDB238867F390 (void);
// 0x00000134 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::.ctor()
extern void UnsupportedPlatform__ctor_m1FE0C164063872EAF39905DA329C90CB977C2DEB (void);
// 0x00000135 System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::Awake()
extern void BannerPlaceholder_Awake_m1783DFD8FA2883CD29F01DCA393975C867E6D4F2 (void);
// 0x00000136 System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::OnGUI()
extern void BannerPlaceholder_OnGUI_m08A5A45A5B550B2A37D4786E1A099EC2E8D80FAC (void);
// 0x00000137 System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::ShowBanner(UnityEngine.Advertisements.BannerPosition,UnityEngine.Advertisements.BannerOptions)
extern void BannerPlaceholder_ShowBanner_mB8F269FC0B5DF7112CA0EDD3EF1DA477E3E1A267 (void);
// 0x00000138 System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::HideBanner()
extern void BannerPlaceholder_HideBanner_mAD7A3396C3435CCA87F10D71833920946364E6D4 (void);
// 0x00000139 UnityEngine.Texture2D UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::BackgroundTexture(System.Int32,System.Int32,UnityEngine.Color)
extern void BannerPlaceholder_BackgroundTexture_mC1DD7A1B2E83AAC9DC633BD6E2024BF29C32800A (void);
// 0x0000013A UnityEngine.Rect UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::GetBannerRect(UnityEngine.Advertisements.BannerPosition)
extern void BannerPlaceholder_GetBannerRect_mBBBDD5FFE24400B0C37C7EF3D2A0278ABD13A9ED (void);
// 0x0000013B System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::.ctor()
extern void BannerPlaceholder__ctor_m349970D7AF7E121F8232772574AADA517ACA754E (void);
// 0x0000013C System.Boolean UnityEngine.Advertisements.Platform.Android.AndroidBanner::get_IsLoaded()
extern void AndroidBanner_get_IsLoaded_m8CE03D99BDFD299D8EEDE5593F10013023EA21DB (void);
// 0x0000013D System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::.ctor()
extern void AndroidBanner__ctor_m6D0C4E14B95B0C9284F6F001FDB31BA3C5D5E01C (void);
// 0x0000013E System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::SetupBanner(UnityEngine.Advertisements.IBanner)
extern void AndroidBanner_SetupBanner_mA31F5D9E49562B13742CB300B9C5A49CFBFA189C (void);
// 0x0000013F System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void AndroidBanner_Load_mB2F18EB9FEE2B571AB059AABFB018D5B0AA3A5B6 (void);
// 0x00000140 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void AndroidBanner_Show_m9ADF7BF50973CC398A77EDB4CAE208348ED4B04F (void);
// 0x00000141 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::Hide(System.Boolean)
extern void AndroidBanner_Hide_mCA030BC63E5FF06F8D58AFD5647F16B22396F403 (void);
// 0x00000142 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void AndroidBanner_SetPosition_m327F21DD13057A81CBA490D36063E1129C3F2A32 (void);
// 0x00000143 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerShow(System.String)
extern void AndroidBanner_onUnityBannerShow_m852EAD7BB7F1F3E97D699450C51B392409432349 (void);
// 0x00000144 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerHide(System.String)
extern void AndroidBanner_onUnityBannerHide_m142E384D01222C2A36AF3E8BACA0CD2942A9663C (void);
// 0x00000145 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerLoaded(System.String,UnityEngine.AndroidJavaObject)
extern void AndroidBanner_onUnityBannerLoaded_m7C6C0669FC1FC14A674128E65181CCF57F1F3D9D (void);
// 0x00000146 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerUnloaded(System.String)
extern void AndroidBanner_onUnityBannerUnloaded_m9524179D96C969FBD587742509E88E59D51521B9 (void);
// 0x00000147 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerClick(System.String)
extern void AndroidBanner_onUnityBannerClick_m538D7B3B599C9A795112A2A43F5DE26F778082CB (void);
// 0x00000148 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerError(System.String)
extern void AndroidBanner_onUnityBannerError_m09BC37E8395F43264AF6933F372645854ED5A636 (void);
// 0x00000149 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<Hide>b__13_0()
extern void AndroidBanner_U3CHideU3Eb__13_0_m3957E5BAB32E9A56C5C478BD196E4C422735157C (void);
// 0x0000014A System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<Hide>b__13_1()
extern void AndroidBanner_U3CHideU3Eb__13_1_m6500FF48D73FC00ACAA6B8C55E90CFD101D459C3 (void);
// 0x0000014B System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<onUnityBannerLoaded>b__17_1()
extern void AndroidBanner_U3ConUnityBannerLoadedU3Eb__17_1_m3936D1ABFFDBB0980AE5BA125BD669307A038984 (void);
// 0x0000014C System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<onUnityBannerLoaded>b__17_0()
extern void AndroidBanner_U3ConUnityBannerLoadedU3Eb__17_0_m47738B03A2F733F83668FCEFE22F4B619669A714 (void);
// 0x0000014D System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<onUnityBannerClick>b__19_0()
extern void AndroidBanner_U3ConUnityBannerClickU3Eb__19_0_m533279DEEFB0428E3E7EB332EF30FF28C9B24F09 (void);
// 0x0000014E System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m61EECFBFF478610A3A83D1DD81DCA59996513941 (void);
// 0x0000014F System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass11_0::<Load>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CLoadU3Eb__0_mE48A2C7608F0B60DC2A9C8846745D5922B889272 (void);
// 0x00000150 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mCD7F9F1F6313D7F785F95EF8F7C90EB85E122DBE (void);
// 0x00000151 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass12_0::<Show>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CShowU3Eb__0_m834F6787AD0936F93D36DA9F6680109FCC125BBA (void);
// 0x00000152 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass12_0::<Show>b__1()
extern void U3CU3Ec__DisplayClass12_0_U3CShowU3Eb__1_m30846CB3B0359F0D3E60B50ED3F7FADA1F9C2395 (void);
// 0x00000153 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_mAC1BA867F70CA6C312DBBCC2726CA19DD6EB8990 (void);
// 0x00000154 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass20_0::<onUnityBannerError>b__0()
extern void U3CU3Ec__DisplayClass20_0_U3ConUnityBannerErrorU3Eb__0_mAB3AD08A70159A423E61CFFCD57D28FCBFB87524 (void);
// 0x00000155 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::.ctor()
extern void AndroidPlatform__ctor_m76DC84F7585EF615BC4A661976709260551793BC (void);
// 0x00000156 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::SetupPlatform(UnityEngine.Advertisements.Platform.IPlatform)
extern void AndroidPlatform_SetupPlatform_m4008B3DF1C3D3772AD6AEF161CEADD77AA200DAB (void);
// 0x00000157 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void AndroidPlatform_Initialize_m140A70BA00CCC852FA8DF65D4279E353C7357F24 (void);
// 0x00000158 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void AndroidPlatform_Load_m20195CECB14A60565930BB65528E61F45E096D76 (void);
// 0x00000159 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void AndroidPlatform_Show_m586367A6EBB44DBD0BC2D215444180AC7DCF0CF7 (void);
// 0x0000015A System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void AndroidPlatform_SetMetaData_m27242C2C132416417F889FC436BEAA4E73E80939 (void);
// 0x0000015B System.Boolean UnityEngine.Advertisements.Platform.Android.AndroidPlatform::GetDebugMode()
extern void AndroidPlatform_GetDebugMode_m7C28F41ECFD79733DE83A67463F152B4D4ED22AB (void);
// 0x0000015C System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::SetDebugMode(System.Boolean)
extern void AndroidPlatform_SetDebugMode_m63485B1452D2C62E45E63CB55D0A19A2ED93E32D (void);
// 0x0000015D System.String UnityEngine.Advertisements.Platform.Android.AndroidPlatform::GetVersion()
extern void AndroidPlatform_GetVersion_mBFFBB947E3763FA940F82DEC4621C19585BB1F96 (void);
// 0x0000015E System.Boolean UnityEngine.Advertisements.Platform.Android.AndroidPlatform::IsInitialized()
extern void AndroidPlatform_IsInitialized_m16CEC3F046F84AE9995EE0628C6CB3A7B95DD752 (void);
// 0x0000015F System.Boolean UnityEngine.Advertisements.Platform.Android.AndroidPlatform::IsReady(System.String)
extern void AndroidPlatform_IsReady_mCE702B3DC1BDA2BA826B9FBC91716443111D43C2 (void);
// 0x00000160 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::RemoveListener()
extern void AndroidPlatform_RemoveListener_m9AA88B7424892BDCF1EFF825F3783F10C161D1F2 (void);
// 0x00000161 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Platform.Android.AndroidPlatform::GetPlacementState(System.String)
extern void AndroidPlatform_GetPlacementState_m9DE9F3042377589C046C0E70BB64B96F1CB6D0E7 (void);
// 0x00000162 System.String UnityEngine.Advertisements.Platform.Android.AndroidPlatform::GetDefaultPlacement()
extern void AndroidPlatform_GetDefaultPlacement_m7ABD068074E732D95E117AE6CEA9719546853AFA (void);
// 0x00000163 UnityEngine.AndroidJavaObject UnityEngine.Advertisements.Platform.Android.AndroidPlatform::GetCurrentAndroidActivity()
extern void AndroidPlatform_GetCurrentAndroidActivity_m4A5C5DD81F44160F72681B68F9A5586120108E2C (void);
// 0x00000164 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::UnityEngine.Advertisements.Purchasing.IPurchasingEventSender.SendPurchasingEvent(System.String)
extern void AndroidPlatform_UnityEngine_Advertisements_Purchasing_IPurchasingEventSender_SendPurchasingEvent_mF9D578F7C80462C92B1083F4C38C10591D3AB87E (void);
// 0x00000165 UnityEngine.AndroidJavaObject UnityEngine.Advertisements.Platform.Android.BannerBundle::get_bannerView()
extern void BannerBundle_get_bannerView_m2A07888D66D072F8F43ABE06EA39197020DF4D5A (void);
// 0x00000166 System.String UnityEngine.Advertisements.Platform.Android.BannerBundle::get_bannerPlacementId()
extern void BannerBundle_get_bannerPlacementId_m20B1FE891D7C20179215F44ED6417456388ED960 (void);
// 0x00000167 System.Void UnityEngine.Advertisements.Platform.Android.BannerBundle::.ctor(System.String,UnityEngine.AndroidJavaObject)
extern void BannerBundle__ctor_mB8E86C4736F19E581E4B5104CEC42B2B550BFB31 (void);
// 0x00000168 System.String UnityEngine.Advertisements.Events.FinishEventArgs::get_placementId()
extern void FinishEventArgs_get_placementId_mA8B3D5A682EEE6B014E376E2DD8C98B9E6E9E233 (void);
// 0x00000169 UnityEngine.Advertisements.ShowResult UnityEngine.Advertisements.Events.FinishEventArgs::get_showResult()
extern void FinishEventArgs_get_showResult_m5700DEF03A63C7EAA2BAB5727F4A04CBF8C8C42D (void);
// 0x0000016A System.Void UnityEngine.Advertisements.Events.FinishEventArgs::.ctor(System.String,UnityEngine.Advertisements.ShowResult)
extern void FinishEventArgs__ctor_m8C6D99257E86B1479B9684C5AC883C0450D04A04 (void);
// 0x0000016B System.String UnityEngine.Advertisements.Events.StartEventArgs::get_placementId()
extern void StartEventArgs_get_placementId_m8CDF99616359E073EBEAA383CE17204C08AE6D79 (void);
// 0x0000016C System.Void UnityEngine.Advertisements.Events.StartEventArgs::.ctor(System.String)
extern void StartEventArgs__ctor_m98BE55E9D7876241E508BC0603098602E986DD6A (void);
static Il2CppMethodPointer s_methodPointers[364] = 
{
	Advertisement__cctor_m55B37EAA6778C0DFDD8C5B1774FFA8E037DB291C,
	Advertisement_get_isInitialized_mE1F3FD392D5AC064468EBAFF0DAE86C7E9AAE4D3,
	Advertisement_get_isSupported_mCE49A7421985B605454CCF5CC7DB18D552C2FEBC,
	Advertisement_get_debugMode_m3082EF5FBB7DD803EF16D5A8C46316C28D73C5B5,
	Advertisement_set_debugMode_mFB9DE772DF5B7347DFE341A652D7F72FE6D55CC5,
	Advertisement_get_version_mD95EC5466A3238701D664FCC578AF4BD9F99E713,
	Advertisement_get_isShowing_m617BBBEC42A0995A6E4A7E719A43E61876BD561E,
	Advertisement_Initialize_m92660FA629A7638E5AB650231550C9DC00A4607A,
	Advertisement_Initialize_m5664C993F54DE75774CC7804CB0FCAE359F11FB4,
	Advertisement_Initialize_m68FDC0B87FEF592AC4F6A3479D767928705B705E,
	Advertisement_Initialize_mF6B7ADD1D170CC4E1CA8E48B8D68D86DC049A609,
	Advertisement_IsReady_m4A073797AB507C7545D0E0D5AC2CFBA2C736235B,
	Advertisement_IsReady_mFAA92CBCFC74928F5B3699F51236B46FCA687892,
	Advertisement_Load_mB6B3DB5E25F516967DC6F2D7537D0E0B17FB0607,
	Advertisement_Load_mBB6ADE31C37A5689064E8734164988821A285917,
	Advertisement_Show_m42F084D05E5A058BCBC370A2D73E507AADE9DD04,
	Advertisement_Show_m4CB6F75CBC3C9D05C721D8009249940F2EE38A16,
	Advertisement_Show_m5FC47893F04B467D5579AF4ABEF792CE746B01CA,
	Advertisement_Show_m0B8ADCA10ECD3484C03DD6D1BB19BBB0EC4E491B,
	Advertisement_Show_m34902C3B7988454CC49D7A8998DE989A026EE011,
	Advertisement_Show_m7D59675BDE03FE109C86AC54E8633E0080F1D2AF,
	Advertisement_SetMetaData_mCB58637A39EBA7E0C85B1F313313B46A97F075FC,
	Advertisement_AddListener_m68BE3528110C03EB81A2B16BF014FAA1B2495A8B,
	Advertisement_RemoveListener_mB68A567A15484638624F76761FF8A9B1077F2322,
	Advertisement_GetPlacementState_m51E01B35332445AA86BC63FE8D94229AFE18323A,
	Advertisement_GetPlacementState_m4245EDD08E0353F04FFF4CAAB281EB4928F22EFB,
	Advertisement_CreatePlatform_mE3E09BC5DC0960DB198EBB5A0181268CEFB53B26,
	Advertisement_IsSupported_m5738C47975B90FBBB3CA78220874852DCB13F1EF,
	Banner_Load_m543A220672814D7329E3306070E00A4684D68BCC,
	Banner_Load_m4576FDC64A45C1E5CA9CF541113A24C3692EFAA8,
	Banner_Load_mB061EE7F25E1F59E4955FA9698B7FAE42E85133B,
	Banner_Load_mEE4C2419EFC6B8A01AF76F8EE3C6EF41C1D7525D,
	Banner_Show_mBF33E7A4D61302CCDAD07294976557DB8C18CC80,
	Banner_Show_m319A41E5B6978242F6A4BD3AF5E23E36F6748901,
	Banner_Show_m2AF3F1A0D3DDE735A8C65E3326E68A43F3197D0B,
	Banner_Show_m59FDCFA112321CF7B9EBF0970D4385527404470E,
	Banner_Hide_m480E589E3D6627B36D18B6C7828DD9D92D81D7DE,
	Banner_SetPosition_m21160D8F6A0B968BE1C0DE1EF58F52C586861B50,
	Banner_get_isLoaded_m7492B9D63B3223AC970B6F553E5B29CCCDF030A2,
	U3CU3Ec__DisplayClass32_0__ctor_mC9305C166F1CCBFA2902722C61B33D35D2B688E9,
	U3CU3Ec__DisplayClass32_0_U3CCreatePlatformU3Eb__0_mD647534AF94720DD11E4D07E9474BAA34EA3E262,
	Banner_get_UnityLifecycleManager_m4298440183255280539AFEEC0D67BC665AF3D9D3,
	Banner_get_IsLoaded_m9D681407B9841D075AB9B83CF09845BB7370FB75,
	Banner_get_ShowAfterLoad_mE2C539504647365336CD4887931416DE66DAECFF,
	Banner_set_ShowAfterLoad_m0AFE75383CABFAAF830DA0F596ECBBD1F03E30A5,
	Banner__ctor_m52317E5AA6F85656EC736E9E9380E6298A729005,
	Banner_Load_m0FBA8D60FFFF13458F0958AE09A9B298FB0EE1AF,
	Banner_Show_m26A6B33CE09D54771D70BB78262C70B929909D1A,
	Banner_Hide_m58289608D32D0E5DCEAE2F603B3550E6B06B5D70,
	Banner_SetPosition_m19E19023F13AABBE4A514A82AF96449602FE49AB,
	Banner_UnityAdsBannerDidShow_mCA87BD2F6AC8F685CCE4FCB754FF0640FF9AF141,
	Banner_UnityAdsBannerDidHide_m576934D95174441C51B1F5D1C5933DE11E958C1A,
	Banner_UnityAdsBannerClick_mE6CCF79E0B78723BA010A58726AB6F775AAE7CD4,
	Banner_UnityAdsBannerDidLoad_m2FD24BA7CC8119182CFCA22848C08A30E304A410,
	Banner_UnityAdsBannerDidError_mAAE9664EEB6C74C43304233425916A57622ECAF1,
	U3CU3Ec__DisplayClass15_0__ctor_m666F7A0193AE0BC9A6046F75C784C6BDC8BEB6C5,
	U3CU3Ec__DisplayClass15_0_U3CUnityAdsBannerDidShowU3Eb__0_m0CA194E73E32C8CE25F408A3D03EE986453EF4DC,
	U3CU3Ec__DisplayClass16_0__ctor_mDEA1A989E11F4A8E4118A33DBECE3589EF50F7E3,
	U3CU3Ec__DisplayClass16_0_U3CUnityAdsBannerDidHideU3Eb__0_mB5ADF99F40033E8F0F37D3CD91D541B998866913,
	U3CU3Ec__DisplayClass17_0__ctor_mE5F311BA199E31690D9DA03E04DD83B36AD4CA83,
	U3CU3Ec__DisplayClass17_0_U3CUnityAdsBannerClickU3Eb__0_m1F371AC329130AAAD060F6882718B6CF32004CDB,
	U3CU3Ec__DisplayClass18_0__ctor_m7568DDEE129B203E325828F83B21F7B6AD52BCDE,
	U3CU3Ec__DisplayClass18_0_U3CUnityAdsBannerDidLoadU3Eb__0_m2CC8BBF2D27963593B5100A32C6C2696240F0D78,
	U3CU3Ec__DisplayClass19_0__ctor_mDC1B997DAF7BF7B93399EABA0D6DD8680B2A0D53,
	U3CU3Ec__DisplayClass19_0_U3CUnityAdsBannerDidErrorU3Eb__0_m9A0B524EF9038AC91C943ABFB918704AEC567676,
	BannerLoadOptions_get_loadCallback_mE8ABEE9AD952F980D2461F833003903CEAFEDEE7,
	BannerLoadOptions_set_loadCallback_m70938AC155B34D5EB87615C2C8E0DE7D7F75029C,
	BannerLoadOptions_get_errorCallback_mFB13E6445519326E8DA9EADF963504B9EC8829D4,
	BannerLoadOptions_set_errorCallback_mC365EFE41F1A7974355B3E71AFCA92BB4D68B29F,
	BannerLoadOptions__ctor_m11C46F5B31C61DCCC3F11EAA79A8F100BB2927EA,
	LoadCallback__ctor_m5C6B156898E4A0BF7F63331E1BA003102D010E7D,
	LoadCallback_Invoke_mB7E5112ADB372535197BE17D07DBB0549972F2B1,
	LoadCallback_BeginInvoke_mBE67F6EE102F66C742C480C3EE04CEACC613CBB8,
	LoadCallback_EndInvoke_mBBE460F9CDC5EE9AFDD5BC1F0EDA5F6E4BD9B27D,
	ErrorCallback__ctor_m64E564F751D8613649A24FD6248CC0F15881B290,
	ErrorCallback_Invoke_mE37BDF37B126110B0389122400D95050C7CCF2DC,
	ErrorCallback_BeginInvoke_mFFCCDA11A8B57C19F874B2DAF32788B853EFF89A,
	ErrorCallback_EndInvoke_m2CCB7F607ABBB52FECE82E577D3F2654535E9EE6,
	BannerOptions_get_showCallback_m3B442270F8690D7E4EF1220C9679501E5924AA00,
	BannerOptions_set_showCallback_mD534AFCE62B60095EBC35BFAF2AAB18E1A00EF18,
	BannerOptions_get_hideCallback_m323343D8356C62E3AB02268F6DF2F3B4E3E8F83D,
	BannerOptions_set_hideCallback_mB317257D7CF7C6644EEE71D1FDA2A9AE60FCFFFC,
	BannerOptions_get_clickCallback_mC0A074CB4F0E32BA76CEF9D06C4E4B442810B7AB,
	BannerOptions_set_clickCallback_m1820691B6A579CC5E45AFE8495492AF5B1678B10,
	BannerOptions__ctor_m475BE772F884A0ED8AA86394A9DE5E90EC4B36CC,
	BannerCallback__ctor_m16A5562A17AF3F938A667EA5B012C3CB8FAF27DF,
	BannerCallback_Invoke_m5B9FE80ACEB47D7682C4086592C98EA517C1F047,
	BannerCallback_BeginInvoke_m358C6F3BFE6DD0930F5F8E9EEDDBE82E41F57AA4,
	BannerCallback_EndInvoke_m5CDBE59D8BF7BBA4FB90021DBA165946C646D8CB,
	Configuration_get_enabled_mB1B054D1A03505A4869B25EE3729E2A63C7CE7F4,
	Configuration_get_defaultPlacement_m4B8BB68526D93DFDF92BBEC3750EBAEC351A60E0,
	Configuration_get_placements_mF146B5494CA8F1922582EA35A59FD52FDBB457A6,
	Configuration__ctor_m252F492C6D21FEDFAC92F1EDEA03FE1FBFA9C44C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MetaData_get_category_mD83A7B88A5187E0CE29A26379205B193A8910FAC,
	MetaData_set_category_mC44964210205E2CA4F22879C9951D3192252536B,
	MetaData__ctor_mADC212484C7A336E5C005E8CDC761BA44F3473DF,
	MetaData_Set_m1E117B9B540A164E3E0E16D0F86E10A7D5F61D20,
	MetaData_Get_mD79A32153785D05BEF1FA97F20B138A88E18344B,
	MetaData_Values_mAA34EFAC1D98BEFEE81F756B24FAB1DC4C5CADEC,
	MetaData_ToJSON_m2C9C500EFA0377F8C17660D7D415BCA7E23AE4B6,
	AndroidInitializationListener__ctor_m390D6AB97AA27C1A174B3018E27A3ECCC1277136,
	AndroidInitializationListener_onInitializationComplete_mA7F23C70D5C18F0F401C824FB264D236B2CAE990,
	AndroidInitializationListener_onInitializationFailed_mA895BB86AC1F820FDAFF3237507B48A7DB7DBC35,
	AndroidLoadListener__ctor_mFEDFE065C4A48039F24D44266D614C59D9009557,
	AndroidLoadListener_onUnityAdsAdLoaded_m076BE77109B73FBE785616436DF7B1B616B957D1,
	AndroidLoadListener_onUnityAdsFailedToLoad_mED87CEFEEFDE6E74AEE5BECCEAEEED45B93DA908,
	AndroidShowListener__ctor_mA58C7BF0BA7F63A671DABABF4165302C0E712E29,
	AndroidShowListener_onUnityAdsShowFailure_mF860BEBD12F621D9404647310D58C5AB3BA40ACC,
	AndroidShowListener_onUnityAdsShowStart_m145CE17471FAA3F08D8EC19ECC2883585FF8D2A5,
	AndroidShowListener_onUnityAdsShowClick_mB195E707292A21577945EA98C73B1BF3DB60B4B7,
	AndroidShowListener_onUnityAdsShowComplete_mEE317B77C88A6F3517D2F8EA9386FA78E701ED20,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ShowOptions_get_resultCallback_mF55D6D3446642D4F59EDCEDF697411A4D9684CE2,
	ShowOptions_set_resultCallback_mBCD882830D968BC1D65AA0CCD8E57D577D158CEF,
	ShowOptions_get_gamerSid_mBF1D8EEA975EE847C7707416D153551D1F501284,
	ShowOptions_set_gamerSid_mCC249C4308EC9F4137359C2006ABC9D8FEDDBC1C,
	ShowOptions__ctor_m2E5353F6B1CFBF5F3C861F110851816148FF8629,
	ApplicationQuit_add_OnApplicationQuitEventHandler_m5BA122587548132F8998C86230E390E0A70CA554,
	ApplicationQuit_remove_OnApplicationQuitEventHandler_m3D8D434F1B90CFC3DB470A50266B062F9AB6EB08,
	ApplicationQuit_OnApplicationQuit_mC36553870FB7111FA72118A04F4B6E08F63D4102,
	ApplicationQuit__ctor_m6EE8BD1A05F13596893DB4DE1087F8735727CC6A,
	CoroutineExecutor_Update_m57A3B6869444E3857B714F8082FD864D97C47283,
	CoroutineExecutor__ctor_m9017C8A0007D2FDE70B49F0276FABD41B3053881,
	EnumUtilities_GetShowResultsFromCompletionState_mABD0D3CC79B8B33939F3A02C707F995A8A7D1D1C,
	NULL,
	NULL,
	NULL,
	NULL,
	Json_Deserialize_mBC5908FA40F6BD660F593A824C6CA7BED95E953D,
	Json_Serialize_mB750CC5CEA90A869583AA1215F9464A7F89DC986,
	Parser_IsWordBreak_m92D76D951FFDF12111E4B5A7BDF6CAD892303C7F,
	Parser__ctor_mA9E14C5AD9DFFDEA3CA07047C22162C5A861C1DB,
	Parser_Parse_mF946767D334740FDB518608297077778B5FDAB72,
	Parser_Dispose_mE368EE5AA939C1A14D9ABD914F56ABD0D5E7646D,
	Parser_ParseObject_m610E118B2F01BC43D3955AA03661423CDFF3F81B,
	Parser_ParseArray_m9313844911AB0BAABF715C0160884D582FD75E5C,
	Parser_ParseValue_m8AB8FB4B4261E8BD880D06D73C7E057FAE06961D,
	Parser_ParseByToken_mAE13997C473CA8948DEE24D82AF8CE35E3E6AE21,
	Parser_ParseString_mFBF054B310FDCCC2189EFB0FA3050F103072C3A2,
	Parser_ParseNumber_mEF76830818879BCD52D4F5C10616CA400E83BFBE,
	Parser_EatWhitespace_m26FD89BF05A8EAC8251AA2EDDE1823FE89A3C420,
	Parser_get_PeekChar_mC8BD88BD89E4BB7C4A802F5EE5699F34366AB104,
	Parser_get_NextChar_mAC16B4C5762A51EDFEB4D408B44E716F2676793C,
	Parser_get_NextWord_m2B11A27C774F3CA1B79C99C1CD2A578BB5DFA021,
	Parser_get_NextToken_m103C32FD7FEF6E4042BAB5C677542B484686F12C,
	Serializer__ctor_mFA3827F3C7C52833050A2ADE9624D88530E7CA0F,
	Serializer_Serialize_m95376D17EC08D37E9651166ECD71CFAD0739D145,
	Serializer_SerializeValue_mD7A69B1BB2EFCD26B508F2294000D61CA9522040,
	Serializer_SerializeObject_mD265453BE69C5CCEE6EB1BFE93BABB917C023E1C,
	Serializer_SerializeArray_m3B0D8184A2567D820C04D068E2407D9C779481A8,
	Serializer_SerializeString_mBE823537F71AB3CD68F0A37B83C16F2CE035E671,
	Serializer_SerializeOther_mF44D246AA44FAA80EFD3831A5280DCD0C94977D5,
	UnityLifecycleManager__ctor_mC028F74E5A0BF6351FB7C12779A4387FF8D7B13F,
	UnityLifecycleManager_Initialize_mF044C0FEA75BA482CDCDA11811FB3AE86BCBE78C,
	UnityLifecycleManager_StartCoroutine_m76BD162C15E4D98DE492242D032CF6C7F8DAE95F,
	UnityLifecycleManager_Post_mE7F8331C2A4486D2587FB87B40CC3C60364B44E1,
	UnityLifecycleManager_Dispose_mE81B52BB490DB4898DA231F1E37C03A2733E99D2,
	UnityLifecycleManager_SetOnApplicationQuitCallback_mA8166BF6F0FE51C1E631A973FA86795491864E15,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Purchase_onPurchasingCommand_m4BD315135425D7BACC579F03E21D0F5286665F43,
	Purchase_onGetPurchasingVersion_m0D90490EAAEA5702A978ECF58C93BD129C8877CC,
	Purchase_onGetProductCatalog_m1D748479ABAAFCBC1D2687A8E7AD2C728E1FC3AB,
	Purchase_onInitializePurchasing_m8C9275459E4230B25940336D30EB0142A6DD6578,
	Purchase_SendEvent_m6F38AFFCC3E92DA3F4E095903F077BDC6E25F878,
	Purchase_Initialize_mFD80DE82B064CCBEB6F8C91C4C297C7069F7ECE5,
	Purchase__ctor_m0DDAA09E7127F7E1DF77E62A6622055D9821080B,
	Purchasing_Initialize_mB01307AD43F5C2C0CE24427A261FA1D7ADA9FE62,
	Purchasing_InitiatePurchasingCommand_m68215657AA2A1DE4C28C39967975B5D55865B40B,
	Purchasing_GetPurchasingCatalog_m098F82AD49D95D3B81EDED0B1C96A95781C32DD9,
	Purchasing_GetPromoVersion_m7D0958D7F10D3431213477611D03C7240BCB6C90,
	Purchasing_SendEvent_mE7DA63FB35A5D3FAD21FE973D6EB717612B817FD,
	Purchasing__cctor_m1362565F616AF8A89583563AC935BED776F2709A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Platform_add_OnStart_m0E359896C499DA111FA6D7BBCB579370B6DC6818,
	Platform_remove_OnStart_mA5827B814FBFD3DD6FE528F8A51A86B793A55091,
	Platform_add_OnFinish_m077D1B927413A2D4E1AB207D1403FB8DE63BFEE1,
	Platform_remove_OnFinish_m02EF7DAD5880B09ED29A21B73158C5F6557F5E3C,
	Platform_get_Banner_m32E6640F032949FD021273065D8ACD8E787D8797,
	Platform_get_UnityLifecycleManager_m3277D76F465C3BD1480160A27C0910039D7596ED,
	Platform_get_NativePlatform_m59B0E3DA713B8EDB3DB0312CADAAE62E7589E24D,
	Platform_get_IsInitialized_m916A6B8BDCD47C10EB81C38966BADD4C594F0D73,
	Platform_get_IsShowing_m9CB1DE3B8D8A12530B2E07C062C65926928483FB,
	Platform_set_IsShowing_m0EDC8FA0575E9DFADC2DEA59E9A1A140C129EABA,
	Platform_get_Version_mF51946AC7BA222C082E3F749723DF58BF71062F9,
	Platform_get_DebugMode_mFB6A1AD18C0521D0DB1BD06B563CAE9DC5B0FC44,
	Platform_set_DebugMode_m4470F3109E718281ED0AA7872355126E89FDE602,
	Platform_get_Listeners_m766F8CE93F16E9F26050BCBE90B2FB091B5084F0,
	Platform__ctor_mB21B8BAFDDF845992AC21EA7DC4D8CAC28E560AC,
	Platform_Initialize_m034B9400C2D88F5FC92F3948E166F92B786787A5,
	Platform_Load_m614909C27C4E2EDBB0FD1F6F40AE9CB93579D223,
	Platform_Load_m6407AEB8653C57CFA86911B46E81765AEA31ACCC,
	Platform_Show_m5A18F9AFF5FB017CB067E5A6FFCA308BDE8A792B,
	Platform_Show_m1A91CAEBDFB6D251D865578D9084D232C3F8E0D6,
	Platform_AddListener_m9B5D2B74E7E5FF80EAF3C089B5A68863C505B3FE,
	Platform_RemoveListener_mBBBF242157AA72AF36AF14ABC9D5150F81772B0C,
	Platform_IsReady_mB39150ABBFEFB422F75250E04144E258CC609F73,
	Platform_GetPlacementState_m5C7B7C8320F7AA7B2EC4DA0CDC0718C60E41B02B,
	Platform_SetMetaData_m10EC61FECF881CC83AF4C6AA8A61F357CF2C7951,
	Platform_UnityAdsReady_m0072B251E469746C710D32B35DD6B7C1F9FD408B,
	Platform_UnityAdsDidError_m555871023730B8C771820952022945D5BBB4CBF5,
	Platform_UnityAdsDidStart_m01F88ABD0988E47705B26BAE0F77683EE57C75FB,
	Platform_UnityAdsDidFinish_m8F69FE8F0EDAAF40FE606ED0771ADF9709A53CB6,
	Platform_GetClonedHashSet_m66222BCAC39FD6C20D5AB9007AE80B9EFAFA9D95,
	Platform_U3CInitializeU3Eb__30_0_mDE420A6B013AD539E0EA6B8FEA62B89B4F4C3DAE,
	Platform_U3CInitializeU3Eb__30_1_m36DEE7079FD9D677115115F4E033774AD1C27C3B,
	U3CU3Ec__DisplayClass33_0__ctor_m1187FAB01F2614A4389FA984DC10BD54A338E00B,
	U3CU3Ec__DisplayClass33_0_U3CShowU3Eb__0_m66FA1D6F50F98B04BAFBB70A76C9DC144917B27C,
	U3CU3Ec__DisplayClass40_0__ctor_m992ECC5612F37563BEC9042E903C5D55AF66F842,
	U3CU3Ec__DisplayClass40_0_U3CUnityAdsReadyU3Eb__0_m5D9FCE7625E28193E42D2D5E6164DCF6A96722EE,
	U3CU3Ec__DisplayClass41_0__ctor_m64B6FBEA32AC10232A458AFCE600E5C1188A7955,
	U3CU3Ec__DisplayClass41_0_U3CUnityAdsDidErrorU3Eb__0_m601A08B3575F723406BFB9EAA5F7F86BB376F6FA,
	U3CU3Ec__DisplayClass42_0__ctor_m692817492B2CAD5D2352F01F1D33AA838DF87A22,
	U3CU3Ec__DisplayClass42_0_U3CUnityAdsDidStartU3Eb__0_mFA8724785A3DEE6D7F8613DD7C747A6288CB44F3,
	U3CU3Ec__DisplayClass43_0__ctor_m5D5F46F63A4BEC7B8470D68B55C6A3A21FE9E6F2,
	U3CU3Ec__DisplayClass43_0_U3CUnityAdsDidFinishU3Eb__0_mB85579E15AE8BAB99E8F8AFB46EFF2CE6BA8B41B,
	UnsupportedBanner_get_IsLoaded_mE65F2C64D685286044CDAF302E25ED51F917B462,
	UnsupportedBanner_SetupBanner_m3B67680B83A772F8AFDB76E5273FE56FED6436BE,
	UnsupportedBanner_Load_m660B71A6BA0CFA10857E0D2C55A06FB49361E64A,
	UnsupportedBanner_Show_m206C9C930CB43C80DEFDCFBC0054DCE4C8CEC285,
	UnsupportedBanner_Hide_mAAE6B3ED6A1908106E267844B1AC134DE727E5FF,
	UnsupportedBanner_SetPosition_m3E869559906446B30EF088B97273331BC3209682,
	UnsupportedBanner__ctor_m86ADC14A483A4805F76F94E750A3EF6CE28E52C7,
	UnsupportedPlatform_SetupPlatform_m06B4CEE7F10AA180B5944AF466667D6C54A9A52A,
	UnsupportedPlatform_Initialize_m8A2DA4302A24E0D168B672B3CF06102C96B3804F,
	UnsupportedPlatform_Load_m8FA1B6E44F1AB2E588630066EFF4B71311AD2311,
	UnsupportedPlatform_Show_m94E02923D066E0E1805BFA2048F4DAE213226BC7,
	UnsupportedPlatform_SetMetaData_mFC4DC0C68985337AC82BA740AD963E39A77A5D57,
	UnsupportedPlatform_GetDebugMode_mF8CFF327B872E632A70758C9334B471256C27630,
	UnsupportedPlatform_SetDebugMode_mE199E6F904AF3FE4FECD68686BA57641EB55248F,
	UnsupportedPlatform_GetVersion_mD6297F8A8B03FD67329F6B0B0A485530C7914DCC,
	UnsupportedPlatform_IsInitialized_m15CD3A14F0CF65F2BC515D70C3DADC8D1B7FC807,
	UnsupportedPlatform_IsReady_mFC68DA517BD9850A7C2672FDDE9DE1E0CA543833,
	UnsupportedPlatform_GetPlacementState_m6D9540107DFD6F22BBD973338E9A8FCA4B10931E,
	UnsupportedPlatform_GetDefaultPlacement_m4C98E4CA23734293DA07A424ACFEDB238867F390,
	UnsupportedPlatform__ctor_m1FE0C164063872EAF39905DA329C90CB977C2DEB,
	BannerPlaceholder_Awake_m1783DFD8FA2883CD29F01DCA393975C867E6D4F2,
	BannerPlaceholder_OnGUI_m08A5A45A5B550B2A37D4786E1A099EC2E8D80FAC,
	BannerPlaceholder_ShowBanner_mB8F269FC0B5DF7112CA0EDD3EF1DA477E3E1A267,
	BannerPlaceholder_HideBanner_mAD7A3396C3435CCA87F10D71833920946364E6D4,
	BannerPlaceholder_BackgroundTexture_mC1DD7A1B2E83AAC9DC633BD6E2024BF29C32800A,
	BannerPlaceholder_GetBannerRect_mBBBDD5FFE24400B0C37C7EF3D2A0278ABD13A9ED,
	BannerPlaceholder__ctor_m349970D7AF7E121F8232772574AADA517ACA754E,
	AndroidBanner_get_IsLoaded_m8CE03D99BDFD299D8EEDE5593F10013023EA21DB,
	AndroidBanner__ctor_m6D0C4E14B95B0C9284F6F001FDB31BA3C5D5E01C,
	AndroidBanner_SetupBanner_mA31F5D9E49562B13742CB300B9C5A49CFBFA189C,
	AndroidBanner_Load_mB2F18EB9FEE2B571AB059AABFB018D5B0AA3A5B6,
	AndroidBanner_Show_m9ADF7BF50973CC398A77EDB4CAE208348ED4B04F,
	AndroidBanner_Hide_mCA030BC63E5FF06F8D58AFD5647F16B22396F403,
	AndroidBanner_SetPosition_m327F21DD13057A81CBA490D36063E1129C3F2A32,
	AndroidBanner_onUnityBannerShow_m852EAD7BB7F1F3E97D699450C51B392409432349,
	AndroidBanner_onUnityBannerHide_m142E384D01222C2A36AF3E8BACA0CD2942A9663C,
	AndroidBanner_onUnityBannerLoaded_m7C6C0669FC1FC14A674128E65181CCF57F1F3D9D,
	AndroidBanner_onUnityBannerUnloaded_m9524179D96C969FBD587742509E88E59D51521B9,
	AndroidBanner_onUnityBannerClick_m538D7B3B599C9A795112A2A43F5DE26F778082CB,
	AndroidBanner_onUnityBannerError_m09BC37E8395F43264AF6933F372645854ED5A636,
	AndroidBanner_U3CHideU3Eb__13_0_m3957E5BAB32E9A56C5C478BD196E4C422735157C,
	AndroidBanner_U3CHideU3Eb__13_1_m6500FF48D73FC00ACAA6B8C55E90CFD101D459C3,
	AndroidBanner_U3ConUnityBannerLoadedU3Eb__17_1_m3936D1ABFFDBB0980AE5BA125BD669307A038984,
	AndroidBanner_U3ConUnityBannerLoadedU3Eb__17_0_m47738B03A2F733F83668FCEFE22F4B619669A714,
	AndroidBanner_U3ConUnityBannerClickU3Eb__19_0_m533279DEEFB0428E3E7EB332EF30FF28C9B24F09,
	U3CU3Ec__DisplayClass11_0__ctor_m61EECFBFF478610A3A83D1DD81DCA59996513941,
	U3CU3Ec__DisplayClass11_0_U3CLoadU3Eb__0_mE48A2C7608F0B60DC2A9C8846745D5922B889272,
	U3CU3Ec__DisplayClass12_0__ctor_mCD7F9F1F6313D7F785F95EF8F7C90EB85E122DBE,
	U3CU3Ec__DisplayClass12_0_U3CShowU3Eb__0_m834F6787AD0936F93D36DA9F6680109FCC125BBA,
	U3CU3Ec__DisplayClass12_0_U3CShowU3Eb__1_m30846CB3B0359F0D3E60B50ED3F7FADA1F9C2395,
	U3CU3Ec__DisplayClass20_0__ctor_mAC1BA867F70CA6C312DBBCC2726CA19DD6EB8990,
	U3CU3Ec__DisplayClass20_0_U3ConUnityBannerErrorU3Eb__0_mAB3AD08A70159A423E61CFFCD57D28FCBFB87524,
	AndroidPlatform__ctor_m76DC84F7585EF615BC4A661976709260551793BC,
	AndroidPlatform_SetupPlatform_m4008B3DF1C3D3772AD6AEF161CEADD77AA200DAB,
	AndroidPlatform_Initialize_m140A70BA00CCC852FA8DF65D4279E353C7357F24,
	AndroidPlatform_Load_m20195CECB14A60565930BB65528E61F45E096D76,
	AndroidPlatform_Show_m586367A6EBB44DBD0BC2D215444180AC7DCF0CF7,
	AndroidPlatform_SetMetaData_m27242C2C132416417F889FC436BEAA4E73E80939,
	AndroidPlatform_GetDebugMode_m7C28F41ECFD79733DE83A67463F152B4D4ED22AB,
	AndroidPlatform_SetDebugMode_m63485B1452D2C62E45E63CB55D0A19A2ED93E32D,
	AndroidPlatform_GetVersion_mBFFBB947E3763FA940F82DEC4621C19585BB1F96,
	AndroidPlatform_IsInitialized_m16CEC3F046F84AE9995EE0628C6CB3A7B95DD752,
	AndroidPlatform_IsReady_mCE702B3DC1BDA2BA826B9FBC91716443111D43C2,
	AndroidPlatform_RemoveListener_m9AA88B7424892BDCF1EFF825F3783F10C161D1F2,
	AndroidPlatform_GetPlacementState_m9DE9F3042377589C046C0E70BB64B96F1CB6D0E7,
	AndroidPlatform_GetDefaultPlacement_m7ABD068074E732D95E117AE6CEA9719546853AFA,
	AndroidPlatform_GetCurrentAndroidActivity_m4A5C5DD81F44160F72681B68F9A5586120108E2C,
	AndroidPlatform_UnityEngine_Advertisements_Purchasing_IPurchasingEventSender_SendPurchasingEvent_mF9D578F7C80462C92B1083F4C38C10591D3AB87E,
	BannerBundle_get_bannerView_m2A07888D66D072F8F43ABE06EA39197020DF4D5A,
	BannerBundle_get_bannerPlacementId_m20B1FE891D7C20179215F44ED6417456388ED960,
	BannerBundle__ctor_mB8E86C4736F19E581E4B5104CEC42B2B550BFB31,
	FinishEventArgs_get_placementId_mA8B3D5A682EEE6B014E376E2DD8C98B9E6E9E233,
	FinishEventArgs_get_showResult_m5700DEF03A63C7EAA2BAB5727F4A04CBF8C8C42D,
	FinishEventArgs__ctor_m8C6D99257E86B1479B9684C5AC883C0450D04A04,
	StartEventArgs_get_placementId_m8CDF99616359E073EBEAA383CE17204C08AE6D79,
	StartEventArgs__ctor_m98BE55E9D7876241E508BC0603098602E986DD6A,
};
static const int32_t s_InvokerIndices[364] = 
{
	2752,
	2745,
	2745,
	2745,
	2711,
	2737,
	2745,
	2709,
	2520,
	2283,
	2068,
	2745,
	2666,
	2709,
	2517,
	2752,
	2709,
	2709,
	2517,
	2517,
	2278,
	2709,
	2709,
	2709,
	2731,
	2594,
	2737,
	2745,
	2752,
	2709,
	2709,
	2517,
	2752,
	2709,
	2709,
	2517,
	2711,
	2706,
	2745,
	1592,
	1592,
	1559,
	1580,
	1580,
	1397,
	932,
	932,
	932,
	1397,
	1368,
	932,
	932,
	932,
	932,
	932,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1559,
	1379,
	1559,
	1379,
	1592,
	931,
	1592,
	735,
	1379,
	931,
	1379,
	482,
	1379,
	1559,
	1379,
	1559,
	1379,
	1559,
	1379,
	1592,
	931,
	1592,
	735,
	1379,
	1580,
	1559,
	1559,
	1379,
	1559,
	1580,
	1580,
	1397,
	932,
	932,
	1397,
	1368,
	932,
	932,
	932,
	932,
	932,
	1580,
	1379,
	932,
	932,
	1397,
	1368,
	1379,
	1379,
	1379,
	929,
	1592,
	887,
	1379,
	613,
	613,
	1379,
	1379,
	929,
	1559,
	1379,
	1379,
	932,
	1131,
	1559,
	1559,
	932,
	1592,
	932,
	932,
	1379,
	622,
	932,
	622,
	1379,
	1379,
	932,
	1379,
	399,
	932,
	932,
	1379,
	1580,
	1397,
	1559,
	1580,
	1243,
	1075,
	1559,
	1559,
	1379,
	1559,
	1379,
	1592,
	1379,
	1379,
	1592,
	1592,
	1592,
	1592,
	2590,
	-1,
	1131,
	1379,
	1379,
	2639,
	2639,
	2662,
	1379,
	2639,
	1592,
	1559,
	1559,
	1559,
	1127,
	1559,
	1559,
	1592,
	1546,
	1546,
	1559,
	1547,
	1592,
	2639,
	1379,
	1379,
	1379,
	1379,
	1379,
	1592,
	1592,
	1131,
	1379,
	1592,
	1379,
	1379,
	1379,
	1379,
	1592,
	1592,
	1592,
	1379,
	1379,
	1592,
	1592,
	1592,
	1379,
	1379,
	1592,
	2666,
	2666,
	2737,
	2737,
	2666,
	2752,
	1379,
	1379,
	1379,
	1379,
	1559,
	1559,
	1559,
	1580,
	1580,
	1559,
	1580,
	1397,
	1559,
	399,
	932,
	622,
	1592,
	1379,
	1379,
	1243,
	1075,
	1379,
	1379,
	1379,
	1379,
	929,
	1379,
	1379,
	1379,
	1379,
	1559,
	1559,
	1559,
	1580,
	1580,
	1397,
	1559,
	1580,
	1397,
	1559,
	622,
	399,
	1379,
	932,
	622,
	1592,
	1379,
	1379,
	1243,
	1075,
	1379,
	1379,
	1379,
	1379,
	929,
	2639,
	932,
	932,
	1592,
	932,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1580,
	1379,
	932,
	932,
	1397,
	1368,
	1592,
	1379,
	399,
	932,
	932,
	1379,
	1580,
	1397,
	1559,
	1580,
	1243,
	1075,
	1559,
	1592,
	1592,
	1592,
	887,
	1592,
	2138,
	2653,
	1592,
	1580,
	1592,
	1379,
	932,
	932,
	1397,
	1368,
	1379,
	1379,
	932,
	1379,
	1379,
	1379,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1592,
	1379,
	399,
	932,
	932,
	1379,
	1580,
	1397,
	1559,
	1580,
	1243,
	1592,
	1075,
	1559,
	2737,
	1379,
	1559,
	1559,
	932,
	1559,
	1547,
	929,
	1559,
	1379,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x060000A7, { 0, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[2] = 
{
	{ (Il2CppRGCTXDataType)1, 93 },
	{ (Il2CppRGCTXDataType)2, 93 },
};
extern const CustomAttributesCacheGenerator g_UnityEngine_Advertisements_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_Advertisements_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_Advertisements_CodeGenModule = 
{
	"UnityEngine.Advertisements.dll",
	364,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	2,
	s_rgctxValues,
	NULL,
	g_UnityEngine_Advertisements_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
