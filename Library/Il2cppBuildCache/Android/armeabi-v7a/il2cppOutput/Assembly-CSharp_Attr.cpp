﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CBoostHelicopterNowU3Ed__12_t0C37D0395174896A6E5A3706E86C72010B8D4768_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBoostHelicopterNowU3Ed__12_t120B7E3243DD041063D08B99AF5918819E8BA72E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBoostHelicopterNowU3Ed__12_tE2800D3B1FB7B0E8A590DE33366E6B3BF4B2D974_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBoostHelicopterNowU3Ed__12_tEB7AB33D399DA7AE43888DB96F8DAA6F18B2EB94_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CIsClimbingU3Ed__13_tC644B10DB02DA8C5D0589F7F40CAE7B33A0161A2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CIsClimbingU3Ed__14_t05F8E70221425349B4BBE445DE189BEF4698AC9E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CIsClimbingU3Ed__14_tC09D56759286ACB8E965E213553A8F190465063B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CIsClimbingU3Ed__14_tCB8320995774A93BA1088528A0CF69A513364A72_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CIsLevelFailedU3Ed__21_t99F7A1A7AC53B84C4087969ED4E8D0824AA06C9B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CMoveObjectU3Ed__34_t2EACAA28BCAB665BCBEF0F835037712D648BDC90_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CMoveObjectU3Ed__35_t50C7740B31CF95155E33E0757B3AD9F33683E6D7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CMoveObjectU3Ed__35_t8C90635DD28CBB7A85B947ED0AC579459C340A2F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CMoveObjectU3Ed__35_tDA1527E60714BAB33FB5F6D33DF5CD891AF3BF37_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnTriggerEnterU3Ed__11_t677F8B25E48451A2354A3EE7A708862BCA492708_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnTriggerEnterU3Ed__11_t7D3D7DBF022F6B334B59C160F44F85726BCCFF2B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnTriggerEnterU3Ed__11_t904EB5FF89899A024A6C18963357BD34EBCDA16A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnTriggerEnterU3Ed__11_tD832E0B5E21EE60EB7EE639B091415AA2A37570C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPlayToastU3Ed__44_tE9527CA78E775091AD4691E0A2C5F212C2AA847E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRestartLevellU3Ed__23_t9404402CF834E4FB4B8C2B286258480C1252A279_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartNextLevelU3Ed__27_t3813EED139F1C6383D0388371F447092CF4B61AF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTransformToRandomU3Ed__39_t22FBD7D18E312477264739FA52451E2465202093_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTransformToRandomU3Ed__39_t49AF10D930D8B25F8449CDFBEAE125B0A92A7840_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTransformToRandomU3Ed__39_t6A0F73153B2100DF513A4EB28E9307EFCCBF456F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTransformU3Ed__38_t8CC95BDD0F4FD49FC83285F7CEF8A93C3E6A1B8C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTransformU3Ed__38_tB809B2C3D179480B2C9A54CCE64435C10F528D02_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTransformU3Ed__38_tD5DE24B17D1D0A5F1FC0FE78E3872F10DFF2110B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CisGameCompletedU3Ed__25_tDADD3FD80F4A36560EF3A84C13B29DD66BDB3595_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CwaitU3Ed__8_t3902C28528005BABE7C375AF2E394292E6067C45_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CwaitU3Ed__8_t4CDB4A05B9DD4AAD7D14AE1D72498CAB758B6E70_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CwaitU3Ed__8_t53F34A2BCB132591FD143778B01D2B7B336DACD4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CwaitU3Ed__8_t5AD13EABC4D1FF4CB47B9ED66FF3489CD08BAFA0_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.SpaceAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.Scripting.PreserveAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2 (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void IAPDemo_t2F65C3A79FA62DA779CABC0DD4333AC5F5AE5BBA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x20\x49\x41\x50\x2F\x44\x65\x6D\x6F"), NULL);
	}
}
static void BrownAiBikeScript_tCCD4B4D66E5A0AB0BF9B2B7ECB651FEFF733EDC6_CustomAttributesCacheGenerator_bikeSpeed(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void BrownAiBoatScript_tFDDCD5A6CBE091A61C59220376FA7337F83551F6_CustomAttributesCacheGenerator_boatSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BrownAiCharacterScript_t4AABE7098B74BAC2C4C9425D0069B3EF862E2891_CustomAttributesCacheGenerator_BrownAiCharacterScript_OnTriggerEnter_m493718247AF0CF260CEB8A4C9DD1ED72F8BEBEFD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnTriggerEnterU3Ed__11_t677F8B25E48451A2354A3EE7A708862BCA492708_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COnTriggerEnterU3Ed__11_t677F8B25E48451A2354A3EE7A708862BCA492708_0_0_0_var), NULL);
	}
}
static void BrownAiCharacterScript_t4AABE7098B74BAC2C4C9425D0069B3EF862E2891_CustomAttributesCacheGenerator_BrownAiCharacterScript_IsClimbing_mE2168EA8FC2E7E70A1C69455C9FA9685BA147C76(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CIsClimbingU3Ed__14_t05F8E70221425349B4BBE445DE189BEF4698AC9E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CIsClimbingU3Ed__14_t05F8E70221425349B4BBE445DE189BEF4698AC9E_0_0_0_var), NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t677F8B25E48451A2354A3EE7A708862BCA492708_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t677F8B25E48451A2354A3EE7A708862BCA492708_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11__ctor_m58CE187FD101838CFCA77561782A057DBE065674(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t677F8B25E48451A2354A3EE7A708862BCA492708_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_IDisposable_Dispose_m1B1ED971F3313F0B1835963EC4B2C73FA501A814(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t677F8B25E48451A2354A3EE7A708862BCA492708_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA19AE93EDBD946686F0548C8A3C8CF793C75AB6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t677F8B25E48451A2354A3EE7A708862BCA492708_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_Reset_mDF0157FD691D1AA9DA10ED0CD2D171E2DAC63F6A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t677F8B25E48451A2354A3EE7A708862BCA492708_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_get_Current_m362197B045B5D2FE9811917DBDAE5ABF6166D8BE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_t05F8E70221425349B4BBE445DE189BEF4698AC9E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_t05F8E70221425349B4BBE445DE189BEF4698AC9E_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14__ctor_mF524A37C748D43169BC4BEE0CD4FA5DDFA04969A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_t05F8E70221425349B4BBE445DE189BEF4698AC9E_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_IDisposable_Dispose_m4DC1A0A554B0D891F7D55A5706870108D70CB307(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_t05F8E70221425349B4BBE445DE189BEF4698AC9E_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6AC86978016C16339DB835397D58344D4CE84CEA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_t05F8E70221425349B4BBE445DE189BEF4698AC9E_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_Reset_m3EDEF13B8E0F81E027857D03E2AE87CC2E650733(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_t05F8E70221425349B4BBE445DE189BEF4698AC9E_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_get_Current_mEA8103E3D058EAD5EA2236B4F85784722145ACC7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_characterIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_bikeIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_jeepIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_boatIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_tankIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_helicopterIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_gliderIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_rotationSpeed(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x65\x64\x73"), NULL);
	}
}
static void BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_BrownAiControllerScript_MoveObject_mE73BD22EC65468F1DB77EC6F56E369F3372B960C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CMoveObjectU3Ed__35_t50C7740B31CF95155E33E0757B3AD9F33683E6D7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CMoveObjectU3Ed__35_t50C7740B31CF95155E33E0757B3AD9F33683E6D7_0_0_0_var), NULL);
	}
}
static void BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_BrownAiControllerScript_Transform_m1B6BB668C02AF1A1B1D119A58FEEB654E580C844(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTransformU3Ed__38_t8CC95BDD0F4FD49FC83285F7CEF8A93C3E6A1B8C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTransformU3Ed__38_t8CC95BDD0F4FD49FC83285F7CEF8A93C3E6A1B8C_0_0_0_var), NULL);
	}
}
static void BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_BrownAiControllerScript_TransformToRandom_m235F4E738DF91B1007DBC556DE0015637C55A285(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTransformToRandomU3Ed__39_t49AF10D930D8B25F8449CDFBEAE125B0A92A7840_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTransformToRandomU3Ed__39_t49AF10D930D8B25F8449CDFBEAE125B0A92A7840_0_0_0_var), NULL);
	}
}
static void U3CMoveObjectU3Ed__35_t50C7740B31CF95155E33E0757B3AD9F33683E6D7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__35_t50C7740B31CF95155E33E0757B3AD9F33683E6D7_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35__ctor_m09BED1D81F93D8216B7F0FAEE530C1CBF188A422(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__35_t50C7740B31CF95155E33E0757B3AD9F33683E6D7_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_IDisposable_Dispose_mD48351D86E92E28331F2E598A87C699CDA727F75(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__35_t50C7740B31CF95155E33E0757B3AD9F33683E6D7_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD24BAE8A82B777887972D591C6FCF0BF84E28EE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__35_t50C7740B31CF95155E33E0757B3AD9F33683E6D7_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_Reset_m3AFFAEDB3F53A4EEE4966EA6E27C59B135C4AA95(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__35_t50C7740B31CF95155E33E0757B3AD9F33683E6D7_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_get_Current_m98A9BA1D1B9DEA461AA45606CD526DF70322DD08(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_t8CC95BDD0F4FD49FC83285F7CEF8A93C3E6A1B8C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_t8CC95BDD0F4FD49FC83285F7CEF8A93C3E6A1B8C_CustomAttributesCacheGenerator_U3CTransformU3Ed__38__ctor_m997EE86AA60658A13C21ED38BEC132A57A81EB01(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_t8CC95BDD0F4FD49FC83285F7CEF8A93C3E6A1B8C_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_IDisposable_Dispose_mB8A89A2B908D9F9289714BC710A4AD681841105A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_t8CC95BDD0F4FD49FC83285F7CEF8A93C3E6A1B8C_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC790BF887E6356F5D81DBAF95C2DDBE7636E02E6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_t8CC95BDD0F4FD49FC83285F7CEF8A93C3E6A1B8C_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_IEnumerator_Reset_mB23DA13FBE8C5E65C1F051D050FB12C855C291F2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_t8CC95BDD0F4FD49FC83285F7CEF8A93C3E6A1B8C_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_IEnumerator_get_Current_mEBD9114DE61869220216C93567201F39A0ACECB0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t49AF10D930D8B25F8449CDFBEAE125B0A92A7840_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t49AF10D930D8B25F8449CDFBEAE125B0A92A7840_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39__ctor_m551C7C45B492150BA04CEF5B3E2CCE43AF524DAF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t49AF10D930D8B25F8449CDFBEAE125B0A92A7840_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_IDisposable_Dispose_mB8D6EE430E1F60115A97625706C08E4175D9C266(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t49AF10D930D8B25F8449CDFBEAE125B0A92A7840_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE802F8C275160DBFE5A0E861778DB93AA30CA2A2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t49AF10D930D8B25F8449CDFBEAE125B0A92A7840_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_Reset_mF8829DE38EDE66FA018654CF651988DF3775B13D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t49AF10D930D8B25F8449CDFBEAE125B0A92A7840_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_get_Current_mBE8313C35CA232335D73F1F6529F45FC8E830ED2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BrownAiHelicopterScript_tB45E14166B167B5CEC303CE7B4223202D6AC7905_CustomAttributesCacheGenerator_helicopterSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BrownAiHelicopterScript_tB45E14166B167B5CEC303CE7B4223202D6AC7905_CustomAttributesCacheGenerator_BrownAiHelicopterScript_wait_m2F59CFA6957E47B9D0B98D2DB0CBC02F566206E1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CwaitU3Ed__8_t4CDB4A05B9DD4AAD7D14AE1D72498CAB758B6E70_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CwaitU3Ed__8_t4CDB4A05B9DD4AAD7D14AE1D72498CAB758B6E70_0_0_0_var), NULL);
	}
}
static void BrownAiHelicopterScript_tB45E14166B167B5CEC303CE7B4223202D6AC7905_CustomAttributesCacheGenerator_BrownAiHelicopterScript_BoostHelicopterNow_mA24205F67B91CA8E96E37EE86EF4FEED72487AF8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBoostHelicopterNowU3Ed__12_tE2800D3B1FB7B0E8A590DE33366E6B3BF4B2D974_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBoostHelicopterNowU3Ed__12_tE2800D3B1FB7B0E8A590DE33366E6B3BF4B2D974_0_0_0_var), NULL);
	}
}
static void U3CwaitU3Ed__8_t4CDB4A05B9DD4AAD7D14AE1D72498CAB758B6E70_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t4CDB4A05B9DD4AAD7D14AE1D72498CAB758B6E70_CustomAttributesCacheGenerator_U3CwaitU3Ed__8__ctor_m9CC191CFC8A05A0F2F712CB78E3D3C712D1B3BA8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t4CDB4A05B9DD4AAD7D14AE1D72498CAB758B6E70_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_IDisposable_Dispose_mB7ED5E1352F730692DA3719E632FE932074ECEEA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t4CDB4A05B9DD4AAD7D14AE1D72498CAB758B6E70_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD9CC63EC2CA3F08870E2F09F5419F073045356EB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t4CDB4A05B9DD4AAD7D14AE1D72498CAB758B6E70_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_IEnumerator_Reset_m8EDAC25117B4E697F2F433B11665FD810FB4FDB5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t4CDB4A05B9DD4AAD7D14AE1D72498CAB758B6E70_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_IEnumerator_get_Current_m2759321C2416D426BC60F3045E046A528C23E938(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_tE2800D3B1FB7B0E8A590DE33366E6B3BF4B2D974_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_tE2800D3B1FB7B0E8A590DE33366E6B3BF4B2D974_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12__ctor_m59B5CA3733B514DC5D43AB2BC40265EE4B6324CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_tE2800D3B1FB7B0E8A590DE33366E6B3BF4B2D974_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_IDisposable_Dispose_mEFD999D4EF533ABFE2E27B1A50696CD34CF077B3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_tE2800D3B1FB7B0E8A590DE33366E6B3BF4B2D974_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31180F90051ED150F895B5D6062E8C7EB35A6099(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_tE2800D3B1FB7B0E8A590DE33366E6B3BF4B2D974_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_Reset_m7F1A5716FF8139EDB930D8D75E2DA42121B3CA40(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_tE2800D3B1FB7B0E8A590DE33366E6B3BF4B2D974_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_get_Current_m4B7EE880FE971084FA8869BB14C0B17EE6A9665A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BrownAiTankScript_t98B71F50FB5CE7E5682814AE151AE912B340145E_CustomAttributesCacheGenerator_tankSpeed(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GreenAiBikeScript_t765F6BA8CC78691CEBB744D693B5A69B6F47DF0D_CustomAttributesCacheGenerator_bikeSpeed(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GreenAiBoatScript_t872CE0E58A77B76E622B3C74BB23A5F0A0934C4A_CustomAttributesCacheGenerator_boatSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GreenAiCharacterScript_tFB6FAD3CAFB282B7199451CDC805A155BBDB6683_CustomAttributesCacheGenerator_GreenAiCharacterScript_OnTriggerEnter_mE41DA5EA2AE137B60804896A6C5B1CAE142271E6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnTriggerEnterU3Ed__11_t904EB5FF89899A024A6C18963357BD34EBCDA16A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COnTriggerEnterU3Ed__11_t904EB5FF89899A024A6C18963357BD34EBCDA16A_0_0_0_var), NULL);
	}
}
static void GreenAiCharacterScript_tFB6FAD3CAFB282B7199451CDC805A155BBDB6683_CustomAttributesCacheGenerator_GreenAiCharacterScript_IsClimbing_m6104549DB6A64AC33BA7A9B683A4CE300389D87F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CIsClimbingU3Ed__14_tC09D56759286ACB8E965E213553A8F190465063B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CIsClimbingU3Ed__14_tC09D56759286ACB8E965E213553A8F190465063B_0_0_0_var), NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t904EB5FF89899A024A6C18963357BD34EBCDA16A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t904EB5FF89899A024A6C18963357BD34EBCDA16A_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11__ctor_m9A69DC7769006C0580046888DE33D5B3497E3158(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t904EB5FF89899A024A6C18963357BD34EBCDA16A_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_IDisposable_Dispose_m1FD5231A948EFABE2395EEDB9C08D376536BB94E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t904EB5FF89899A024A6C18963357BD34EBCDA16A_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m468266D865ED51B5686F8402CA721DCFD254D240(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t904EB5FF89899A024A6C18963357BD34EBCDA16A_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_Reset_mA18241CE3ADA61237D0B8ABE12D3EEF13C49C69C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t904EB5FF89899A024A6C18963357BD34EBCDA16A_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_get_Current_m97498923C25B468E44FBDA18864B3E10ADEB7FB2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_tC09D56759286ACB8E965E213553A8F190465063B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_tC09D56759286ACB8E965E213553A8F190465063B_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14__ctor_m101C21898DF05DB161D452C96753EEF525BD7066(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_tC09D56759286ACB8E965E213553A8F190465063B_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_IDisposable_Dispose_m6DBD346A64EE02ED6EDA1E3B242DDF77878C0065(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_tC09D56759286ACB8E965E213553A8F190465063B_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1470B7C3C6547F5A6DECCCD4E3811F7866E27ECC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_tC09D56759286ACB8E965E213553A8F190465063B_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_Reset_mCCC1079F77ABAA01075E34E30346DB874C61403E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_tC09D56759286ACB8E965E213553A8F190465063B_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_get_Current_m6BC85BF85A7A3BA1C81AD1A4D9EA1C7D25D7DECE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_characterIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_bikeIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_jeepIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_boatIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_tankIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_helicopterIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_gliderIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_rotationSpeed(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x65\x64\x73"), NULL);
	}
}
static void GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_GreenAiControllerScript_MoveObject_m428A17DBAFDD480889D2DC0910582B073B9BF276(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CMoveObjectU3Ed__35_t8C90635DD28CBB7A85B947ED0AC579459C340A2F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CMoveObjectU3Ed__35_t8C90635DD28CBB7A85B947ED0AC579459C340A2F_0_0_0_var), NULL);
	}
}
static void GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_GreenAiControllerScript_Transform_m0C755F0EE9BB07E4D98ACD902D5D7D18B73EA0AC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTransformU3Ed__38_tB809B2C3D179480B2C9A54CCE64435C10F528D02_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTransformU3Ed__38_tB809B2C3D179480B2C9A54CCE64435C10F528D02_0_0_0_var), NULL);
	}
}
static void GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_GreenAiControllerScript_TransformToRandom_mC3F7D234B52D8EB4EE29EBCE9DEB9F15F1A3F5DC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTransformToRandomU3Ed__39_t6A0F73153B2100DF513A4EB28E9307EFCCBF456F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTransformToRandomU3Ed__39_t6A0F73153B2100DF513A4EB28E9307EFCCBF456F_0_0_0_var), NULL);
	}
}
static void U3CMoveObjectU3Ed__35_t8C90635DD28CBB7A85B947ED0AC579459C340A2F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__35_t8C90635DD28CBB7A85B947ED0AC579459C340A2F_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35__ctor_m179E5FE2DF01B88D84D0D14765488F47DA4AB678(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__35_t8C90635DD28CBB7A85B947ED0AC579459C340A2F_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_IDisposable_Dispose_m9D27A724FD86ED921543813A4E7260E9A7F7B5D0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__35_t8C90635DD28CBB7A85B947ED0AC579459C340A2F_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD70A6C3669063054A3585BC89F8525F8A5223E7D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__35_t8C90635DD28CBB7A85B947ED0AC579459C340A2F_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_Reset_m817F8AD405448B9EDBC2F07BF7F5F83745689AD2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__35_t8C90635DD28CBB7A85B947ED0AC579459C340A2F_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_get_Current_m199D40D6301C2AAB59A577024977E61946E95F8D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_tB809B2C3D179480B2C9A54CCE64435C10F528D02_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_tB809B2C3D179480B2C9A54CCE64435C10F528D02_CustomAttributesCacheGenerator_U3CTransformU3Ed__38__ctor_mDA513F4551EA387CBE55ADCA55E2BA755F388B9D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_tB809B2C3D179480B2C9A54CCE64435C10F528D02_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_IDisposable_Dispose_m48F34B6CA28107A6D9F588D85396B9C016B27D72(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_tB809B2C3D179480B2C9A54CCE64435C10F528D02_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCAC7D326D7467468072FA99AC95AB8A772F0D6EE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_tB809B2C3D179480B2C9A54CCE64435C10F528D02_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_IEnumerator_Reset_mBE648FB20A3F7600472F2EEC5B8A27DA34001E66(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_tB809B2C3D179480B2C9A54CCE64435C10F528D02_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_IEnumerator_get_Current_m2D984DD67D92B18296EDFFE2E4629A37B611271B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t6A0F73153B2100DF513A4EB28E9307EFCCBF456F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t6A0F73153B2100DF513A4EB28E9307EFCCBF456F_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39__ctor_mA913800A026FDE676DCBEE781F12E7DB50359C3D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t6A0F73153B2100DF513A4EB28E9307EFCCBF456F_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_IDisposable_Dispose_m2BC9278A0B00FAAC4B9BDAF82C99D175095C258F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t6A0F73153B2100DF513A4EB28E9307EFCCBF456F_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m918D1A177676B7AADDB603964ECF77380C9450FB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t6A0F73153B2100DF513A4EB28E9307EFCCBF456F_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_Reset_m7FB7AABD45545A92964F18E329B23FF75F0EF2F7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t6A0F73153B2100DF513A4EB28E9307EFCCBF456F_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_get_Current_mB99F58C3386E898F1FA7C1FD7E8BC1C92D53D7CE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GreenAiHelicopterScript_tC681D9410CB1ECED4FD67F37A4E58A2DC586E135_CustomAttributesCacheGenerator_helicopterSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GreenAiHelicopterScript_tC681D9410CB1ECED4FD67F37A4E58A2DC586E135_CustomAttributesCacheGenerator_GreenAiHelicopterScript_wait_m0AF37D5CFAE7BB8285C0EEFB023914D5828D75A3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CwaitU3Ed__8_t5AD13EABC4D1FF4CB47B9ED66FF3489CD08BAFA0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CwaitU3Ed__8_t5AD13EABC4D1FF4CB47B9ED66FF3489CD08BAFA0_0_0_0_var), NULL);
	}
}
static void GreenAiHelicopterScript_tC681D9410CB1ECED4FD67F37A4E58A2DC586E135_CustomAttributesCacheGenerator_GreenAiHelicopterScript_BoostHelicopterNow_m2C697483CBC2445C941B05ED38FA730989961DDB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBoostHelicopterNowU3Ed__12_t0C37D0395174896A6E5A3706E86C72010B8D4768_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBoostHelicopterNowU3Ed__12_t0C37D0395174896A6E5A3706E86C72010B8D4768_0_0_0_var), NULL);
	}
}
static void U3CwaitU3Ed__8_t5AD13EABC4D1FF4CB47B9ED66FF3489CD08BAFA0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t5AD13EABC4D1FF4CB47B9ED66FF3489CD08BAFA0_CustomAttributesCacheGenerator_U3CwaitU3Ed__8__ctor_mDCC058A30FD6F11314464F59B1D944452DEDE31A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t5AD13EABC4D1FF4CB47B9ED66FF3489CD08BAFA0_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_IDisposable_Dispose_m32B292493C2C2097837B382D6F3BBD3FE5936571(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t5AD13EABC4D1FF4CB47B9ED66FF3489CD08BAFA0_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD11542748D2157BF3ECB8D0449FB8EBBF824C2C6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t5AD13EABC4D1FF4CB47B9ED66FF3489CD08BAFA0_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_IEnumerator_Reset_mD4370B65472947DCCF75489E73F2FCDA3C060708(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t5AD13EABC4D1FF4CB47B9ED66FF3489CD08BAFA0_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_IEnumerator_get_Current_mCDA0D7011D71B6E161157D399A915B705D0F4356(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_t0C37D0395174896A6E5A3706E86C72010B8D4768_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_t0C37D0395174896A6E5A3706E86C72010B8D4768_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12__ctor_mBBE717D794FC2AA7DD4F448CA792743B1729B381(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_t0C37D0395174896A6E5A3706E86C72010B8D4768_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_IDisposable_Dispose_m86EE5D0048CCD362408A8A627CA7C249E7B22704(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_t0C37D0395174896A6E5A3706E86C72010B8D4768_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m757362FC7C672367D082B76F985400626F3704D5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_t0C37D0395174896A6E5A3706E86C72010B8D4768_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_Reset_m80DE021A4A053E4CA424F5CE4BBD788881F70BD5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_t0C37D0395174896A6E5A3706E86C72010B8D4768_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_get_Current_mACFAF342ACBF07EC3B30B9176B0D4B5374E94A2E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GreenAiTankScript_tB772EDBC62546E97D06C735EF7E0C550236A81B0_CustomAttributesCacheGenerator_tankSpeed(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void AdsScript_t0B8CAA5F1B0A18DC4CE87A334C4CE021DCD63A06_CustomAttributesCacheGenerator_remoteKey(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x6D\x6F\x74\x65\x4B\x65\x79\x20\x66\x6F\x72\x20\x41\x6E\x61\x6C\x79\x74\x69\x63\x73"), NULL);
	}
}
static void AdsScript_t0B8CAA5F1B0A18DC4CE87A334C4CE021DCD63A06_CustomAttributesCacheGenerator_bannerID(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x4F\x4F\x47\x4C\x45\x20\x41\x44\x4D\x4F\x42"), NULL);
	}
}
static void AdsScript_t0B8CAA5F1B0A18DC4CE87A334C4CE021DCD63A06_CustomAttributesCacheGenerator_testMode(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x4E\x49\x54\x59"), NULL);
	}
}
static void AdsScript_t0B8CAA5F1B0A18DC4CE87A334C4CE021DCD63A06_CustomAttributesCacheGenerator_bannerPosition(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x73\x69\x74\x69\x6F\x6E\x20\x66\x6F\x72\x20\x42\x61\x6E\x6E\x65\x72"), NULL);
	}
}
static void CoinAnimationScript_tDD9E07CD4907784CAB40C5CC81FE1B03AB3391E6_CustomAttributesCacheGenerator_animatedCoinPrefab(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x20\x72\x65\x66\x65\x72\x65\x6E\x63\x65\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoinAnimationScript_tDD9E07CD4907784CAB40C5CC81FE1B03AB3391E6_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoinAnimationScript_tDD9E07CD4907784CAB40C5CC81FE1B03AB3391E6_CustomAttributesCacheGenerator_source(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoinAnimationScript_tDD9E07CD4907784CAB40C5CC81FE1B03AB3391E6_CustomAttributesCacheGenerator_maxCoins(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x76\x61\x69\x6C\x61\x62\x6C\x65\x20\x63\x6F\x69\x6E\x73\x20\x3A\x20\x28\x63\x6F\x69\x6E\x73\x20\x74\x6F\x20\x70\x6F\x6F\x6C\x29"), NULL);
	}
}
static void CoinAnimationScript_tDD9E07CD4907784CAB40C5CC81FE1B03AB3391E6_CustomAttributesCacheGenerator_minAnimDuration(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x69\x6D\x61\x74\x69\x6F\x6E\x20\x73\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[3];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.5f, 0.899999976f, NULL);
	}
}
static void CoinAnimationScript_tDD9E07CD4907784CAB40C5CC81FE1B03AB3391E6_CustomAttributesCacheGenerator_maxAnimDuration(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.899999976f, 2.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoinAnimationScript_tDD9E07CD4907784CAB40C5CC81FE1B03AB3391E6_CustomAttributesCacheGenerator_easeType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoinAnimationScript_tDD9E07CD4907784CAB40C5CC81FE1B03AB3391E6_CustomAttributesCacheGenerator_spread(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass17_0_t11873D890212501E7C830EC045721BB5816E989F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass19_0_t1E85ED0942E2697BB6FF4BBD8D617BB08C882E7E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_shapes(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_buttons(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_vector3s(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_IsLevelFailed_m70DE3501FE1BEEC60023C7A9F649FD8640D97E67(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CIsLevelFailedU3Ed__21_t99F7A1A7AC53B84C4087969ED4E8D0824AA06C9B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CIsLevelFailedU3Ed__21_t99F7A1A7AC53B84C4087969ED4E8D0824AA06C9B_0_0_0_var), NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_RestartLevell_m5AB99A05E7E5BDEBB8CA7F99D6CA514F1C3A5FD6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRestartLevellU3Ed__23_t9404402CF834E4FB4B8C2B286258480C1252A279_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRestartLevellU3Ed__23_t9404402CF834E4FB4B8C2B286258480C1252A279_0_0_0_var), NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_isGameCompleted_m5C2694B3FDB66DED81583ECC138D4BF54463CFFD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CisGameCompletedU3Ed__25_tDADD3FD80F4A36560EF3A84C13B29DD66BDB3595_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CisGameCompletedU3Ed__25_tDADD3FD80F4A36560EF3A84C13B29DD66BDB3595_0_0_0_var), NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_StartNextLevel_mD9B579F6AFA8F8346C0A922BDBC727F750C52E10(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartNextLevelU3Ed__27_t3813EED139F1C6383D0388371F447092CF4B61AF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartNextLevelU3Ed__27_t3813EED139F1C6383D0388371F447092CF4B61AF_0_0_0_var), NULL);
	}
}
static void U3CIsLevelFailedU3Ed__21_t99F7A1A7AC53B84C4087969ED4E8D0824AA06C9B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CIsLevelFailedU3Ed__21_t99F7A1A7AC53B84C4087969ED4E8D0824AA06C9B_CustomAttributesCacheGenerator_U3CIsLevelFailedU3Ed__21__ctor_mD10B2FD438BC1BEE23004BD942AB31491C0A124E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsLevelFailedU3Ed__21_t99F7A1A7AC53B84C4087969ED4E8D0824AA06C9B_CustomAttributesCacheGenerator_U3CIsLevelFailedU3Ed__21_System_IDisposable_Dispose_m076581E89A05A366FBAF808DE98241BDA23E05EB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsLevelFailedU3Ed__21_t99F7A1A7AC53B84C4087969ED4E8D0824AA06C9B_CustomAttributesCacheGenerator_U3CIsLevelFailedU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC589789B869BCDF5F0F5F602E0404335DBE4644E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsLevelFailedU3Ed__21_t99F7A1A7AC53B84C4087969ED4E8D0824AA06C9B_CustomAttributesCacheGenerator_U3CIsLevelFailedU3Ed__21_System_Collections_IEnumerator_Reset_m8A3ADC2BABE6D0B8A7B65EEE2CEBEF36F600C629(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsLevelFailedU3Ed__21_t99F7A1A7AC53B84C4087969ED4E8D0824AA06C9B_CustomAttributesCacheGenerator_U3CIsLevelFailedU3Ed__21_System_Collections_IEnumerator_get_Current_m5A479CAD77A871F46E639F24FA9DC80CED301DE6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRestartLevellU3Ed__23_t9404402CF834E4FB4B8C2B286258480C1252A279_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRestartLevellU3Ed__23_t9404402CF834E4FB4B8C2B286258480C1252A279_CustomAttributesCacheGenerator_U3CRestartLevellU3Ed__23__ctor_mD698104A3344AAD8AB7F375C1FCDFE48A90FEF8A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRestartLevellU3Ed__23_t9404402CF834E4FB4B8C2B286258480C1252A279_CustomAttributesCacheGenerator_U3CRestartLevellU3Ed__23_System_IDisposable_Dispose_m917325C36BA67A294D14EC471D182B556BCE62D7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRestartLevellU3Ed__23_t9404402CF834E4FB4B8C2B286258480C1252A279_CustomAttributesCacheGenerator_U3CRestartLevellU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE78F622C02552C6AFC3AC0A35FA99A639B581A06(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRestartLevellU3Ed__23_t9404402CF834E4FB4B8C2B286258480C1252A279_CustomAttributesCacheGenerator_U3CRestartLevellU3Ed__23_System_Collections_IEnumerator_Reset_m631238FEBA0FDA032E30FF7EEA31F50A1E220ADA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRestartLevellU3Ed__23_t9404402CF834E4FB4B8C2B286258480C1252A279_CustomAttributesCacheGenerator_U3CRestartLevellU3Ed__23_System_Collections_IEnumerator_get_Current_m509B652FC4478E31C51E1EADF6D16C8D8D1F3E68(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CisGameCompletedU3Ed__25_tDADD3FD80F4A36560EF3A84C13B29DD66BDB3595_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CisGameCompletedU3Ed__25_tDADD3FD80F4A36560EF3A84C13B29DD66BDB3595_CustomAttributesCacheGenerator_U3CisGameCompletedU3Ed__25__ctor_m2957E84768DAED7DE588D515A6A1CE2B39C026C7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CisGameCompletedU3Ed__25_tDADD3FD80F4A36560EF3A84C13B29DD66BDB3595_CustomAttributesCacheGenerator_U3CisGameCompletedU3Ed__25_System_IDisposable_Dispose_m3ADF2A8F3FA8F0BE8595C6CA0625BC752B5B852E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CisGameCompletedU3Ed__25_tDADD3FD80F4A36560EF3A84C13B29DD66BDB3595_CustomAttributesCacheGenerator_U3CisGameCompletedU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m42B4BF6B0DC26D917B0E47D89DCF7AD201804E77(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CisGameCompletedU3Ed__25_tDADD3FD80F4A36560EF3A84C13B29DD66BDB3595_CustomAttributesCacheGenerator_U3CisGameCompletedU3Ed__25_System_Collections_IEnumerator_Reset_m35439684D43C4B3E3217C106A4C1939FF2C02A97(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CisGameCompletedU3Ed__25_tDADD3FD80F4A36560EF3A84C13B29DD66BDB3595_CustomAttributesCacheGenerator_U3CisGameCompletedU3Ed__25_System_Collections_IEnumerator_get_Current_mA00BAAD4560CDC6CF6CC18BC9435689084F4D065(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartNextLevelU3Ed__27_t3813EED139F1C6383D0388371F447092CF4B61AF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartNextLevelU3Ed__27_t3813EED139F1C6383D0388371F447092CF4B61AF_CustomAttributesCacheGenerator_U3CStartNextLevelU3Ed__27__ctor_m84E1B3F4F8E539CC4A3741E7A8B6958EB8D0AEBC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartNextLevelU3Ed__27_t3813EED139F1C6383D0388371F447092CF4B61AF_CustomAttributesCacheGenerator_U3CStartNextLevelU3Ed__27_System_IDisposable_Dispose_m475BB8B13202250A345D0FADAD797D36B9E403EF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartNextLevelU3Ed__27_t3813EED139F1C6383D0388371F447092CF4B61AF_CustomAttributesCacheGenerator_U3CStartNextLevelU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEFE495A03A02293456740B5F11C350835D1396FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartNextLevelU3Ed__27_t3813EED139F1C6383D0388371F447092CF4B61AF_CustomAttributesCacheGenerator_U3CStartNextLevelU3Ed__27_System_Collections_IEnumerator_Reset_m720323159B46EA9B529D6511653DB54F79272B18(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartNextLevelU3Ed__27_t3813EED139F1C6383D0388371F447092CF4B61AF_CustomAttributesCacheGenerator_U3CStartNextLevelU3Ed__27_System_Collections_IEnumerator_get_Current_m5B09F71419B61AF21B156A7DF3D43E1995EBE375(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t0259FC3D9B75D94FC2AA58017A67F463CB0A525F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t4373CB59F6AD8FAFC444E432EBDBA2A692E8B217_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_toast(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x6F\x61\x73\x74"), NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_levelNumberText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x76\x65\x6C\x20\x4E\x75\x6D\x62\x65\x72\x20\x54\x65\x78\x74"), NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_coinText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x69\x6E\x73\x20\x54\x65\x78\x74"), NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_mainMenuPanel(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x20\x50\x61\x6E\x65\x6C\x73"), NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_one(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x70\x67\x72\x61\x64\x65\x20\x42\x75\x74\x74\x6F\x6E\x73"), NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_gameButtons(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x42\x75\x74\x74\x6F\x6E\x73"), NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_prevIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_UIManager_PlayToast_m033AE9F7CFA3D901AF314A312D49B4E6085D3195(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPlayToastU3Ed__44_tE9527CA78E775091AD4691E0A2C5F212C2AA847E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPlayToastU3Ed__44_tE9527CA78E775091AD4691E0A2C5F212C2AA847E_0_0_0_var), NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_UIManager_U3CAwakeU3Eb__31_0_m3122A16C76C0271B4114543368B4AE16D96F87C5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_UIManager_U3CAwakeU3Eb__31_1_mE40D9985291979A55391AD6F32851A8B125816EE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_UIManager_U3CAwakeU3Eb__31_2_m9D115D348778764D78362E208E83AE06A029371A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPlayToastU3Ed__44_tE9527CA78E775091AD4691E0A2C5F212C2AA847E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPlayToastU3Ed__44_tE9527CA78E775091AD4691E0A2C5F212C2AA847E_CustomAttributesCacheGenerator_U3CPlayToastU3Ed__44__ctor_mEAE19C120DB097EDD95DBE6FDE063A848F41EBA6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayToastU3Ed__44_tE9527CA78E775091AD4691E0A2C5F212C2AA847E_CustomAttributesCacheGenerator_U3CPlayToastU3Ed__44_System_IDisposable_Dispose_mF18FC39DAF7831611A1FF47716A054C35DE81982(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayToastU3Ed__44_tE9527CA78E775091AD4691E0A2C5F212C2AA847E_CustomAttributesCacheGenerator_U3CPlayToastU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE9752D8C7E3C66B17F947A16473FD29B271BA717(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayToastU3Ed__44_tE9527CA78E775091AD4691E0A2C5F212C2AA847E_CustomAttributesCacheGenerator_U3CPlayToastU3Ed__44_System_Collections_IEnumerator_Reset_m2349BD58837265E99891C68919BDBA1C7B12C32C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayToastU3Ed__44_tE9527CA78E775091AD4691E0A2C5F212C2AA847E_CustomAttributesCacheGenerator_U3CPlayToastU3Ed__44_System_Collections_IEnumerator_get_Current_m147D97967E8C7C1764873D48F08478FE3C16E7E6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PlayerBikeScript_tE0494C0D3389348356F38B40D4F1DAE3FDF875EE_CustomAttributesCacheGenerator_bikeSpeed(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void PlayerBoatScript_t768AEB5C4FABAF584B24A71206778CD986B6D136_CustomAttributesCacheGenerator_boatSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerCharacterScript_t37FDF6D7062F616CD3A0F50F67268935781D950E_CustomAttributesCacheGenerator_isClimbing(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void PlayerCharacterScript_t37FDF6D7062F616CD3A0F50F67268935781D950E_CustomAttributesCacheGenerator_PlayerCharacterScript_OnTriggerEnter_mDBE8BD73187B0A928C13B9E0275563B9B2569958(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnTriggerEnterU3Ed__11_t7D3D7DBF022F6B334B59C160F44F85726BCCFF2B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COnTriggerEnterU3Ed__11_t7D3D7DBF022F6B334B59C160F44F85726BCCFF2B_0_0_0_var), NULL);
	}
}
static void PlayerCharacterScript_t37FDF6D7062F616CD3A0F50F67268935781D950E_CustomAttributesCacheGenerator_PlayerCharacterScript_IsClimbing_m19886905495A4440976ACF0F2D25BA17011DDAD0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CIsClimbingU3Ed__13_tC644B10DB02DA8C5D0589F7F40CAE7B33A0161A2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CIsClimbingU3Ed__13_tC644B10DB02DA8C5D0589F7F40CAE7B33A0161A2_0_0_0_var), NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t7D3D7DBF022F6B334B59C160F44F85726BCCFF2B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t7D3D7DBF022F6B334B59C160F44F85726BCCFF2B_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11__ctor_m157690149B9EFC064D41233255CFF1D06F6E39EA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t7D3D7DBF022F6B334B59C160F44F85726BCCFF2B_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_IDisposable_Dispose_m3608C29B0A3E3E32B1042BB1B65824B7BCBEC6C1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t7D3D7DBF022F6B334B59C160F44F85726BCCFF2B_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2CB4290008F905A4825A8D09AC73523EED0D715B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t7D3D7DBF022F6B334B59C160F44F85726BCCFF2B_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_Reset_m980E27465640D7E4636D68BEDFDD1222A71C3454(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_t7D3D7DBF022F6B334B59C160F44F85726BCCFF2B_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_get_Current_m298E21F99DFA30907415C3D27D3AF0AD04F4B3FD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__13_tC644B10DB02DA8C5D0589F7F40CAE7B33A0161A2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__13_tC644B10DB02DA8C5D0589F7F40CAE7B33A0161A2_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__13__ctor_mE046B29894718B2CEE95FC9F23E6C957C3FFA998(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__13_tC644B10DB02DA8C5D0589F7F40CAE7B33A0161A2_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__13_System_IDisposable_Dispose_m367474145BDE9A57815C89B1370EE3DC4F317AD0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__13_tC644B10DB02DA8C5D0589F7F40CAE7B33A0161A2_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D8834D7DDF29F107743C566E8036B4323BB1688(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__13_tC644B10DB02DA8C5D0589F7F40CAE7B33A0161A2_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__13_System_Collections_IEnumerator_Reset_m271A0D38A671C486E12E68AD956D59A536B65DE7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__13_tC644B10DB02DA8C5D0589F7F40CAE7B33A0161A2_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__13_System_Collections_IEnumerator_get_Current_mA4A718648508AFFC8455A5D13E62BB8091F46067(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_characterIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_bikeIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_jeepIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_boatIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_tankIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_helicopterIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_gliderIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_rotationSpeed(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x65\x64\x73"), NULL);
	}
}
static void PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_PlayerControllerScript_MoveObject_mFDBF554082AA89295044CEA450F59F2184EF2FF4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CMoveObjectU3Ed__34_t2EACAA28BCAB665BCBEF0F835037712D648BDC90_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CMoveObjectU3Ed__34_t2EACAA28BCAB665BCBEF0F835037712D648BDC90_0_0_0_var), NULL);
	}
}
static void U3CMoveObjectU3Ed__34_t2EACAA28BCAB665BCBEF0F835037712D648BDC90_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__34_t2EACAA28BCAB665BCBEF0F835037712D648BDC90_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__34__ctor_m7679949E4579F0514F360B36E50FF224BD59B3DA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__34_t2EACAA28BCAB665BCBEF0F835037712D648BDC90_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__34_System_IDisposable_Dispose_mE95ED8C6FDC5F84ED1D757A53DA602E240778666(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__34_t2EACAA28BCAB665BCBEF0F835037712D648BDC90_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A7854A57AE804B9BB83BDE78DB37D0766FB65CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__34_t2EACAA28BCAB665BCBEF0F835037712D648BDC90_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__34_System_Collections_IEnumerator_Reset_m1752A1686309401EC13831FBF562A3FFAEBB69B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__34_t2EACAA28BCAB665BCBEF0F835037712D648BDC90_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__34_System_Collections_IEnumerator_get_Current_m7B5FC4B34965C4A5011095B7BAE82B2399652B2E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PlayerHelicopterScript_t9B7E46925E36AB47E790EA303E302417206DAEA1_CustomAttributesCacheGenerator_helicopterSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerHelicopterScript_t9B7E46925E36AB47E790EA303E302417206DAEA1_CustomAttributesCacheGenerator_PlayerHelicopterScript_wait_m2A2F63CFEB547FFAF4C6D64FB02D898B7244894E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CwaitU3Ed__8_t53F34A2BCB132591FD143778B01D2B7B336DACD4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CwaitU3Ed__8_t53F34A2BCB132591FD143778B01D2B7B336DACD4_0_0_0_var), NULL);
	}
}
static void PlayerHelicopterScript_t9B7E46925E36AB47E790EA303E302417206DAEA1_CustomAttributesCacheGenerator_PlayerHelicopterScript_BoostHelicopterNow_m394AF4AD515DAF71CD02DD1BD10B8904F6B9F68A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBoostHelicopterNowU3Ed__12_t120B7E3243DD041063D08B99AF5918819E8BA72E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBoostHelicopterNowU3Ed__12_t120B7E3243DD041063D08B99AF5918819E8BA72E_0_0_0_var), NULL);
	}
}
static void U3CwaitU3Ed__8_t53F34A2BCB132591FD143778B01D2B7B336DACD4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t53F34A2BCB132591FD143778B01D2B7B336DACD4_CustomAttributesCacheGenerator_U3CwaitU3Ed__8__ctor_m6D1BEC088C88A0A2E14F1560F02C07733ABBE2F9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t53F34A2BCB132591FD143778B01D2B7B336DACD4_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_IDisposable_Dispose_m78B42166C4DD382EFDFA89374A11B19BFE49A446(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t53F34A2BCB132591FD143778B01D2B7B336DACD4_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC4E14E44E3C1B5EA4BD55F528B43CB7B250DC8A7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t53F34A2BCB132591FD143778B01D2B7B336DACD4_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_IEnumerator_Reset_mFC68D47EBA84A0CB1C50D57E3A6ECF7C7C2EFB0F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t53F34A2BCB132591FD143778B01D2B7B336DACD4_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_IEnumerator_get_Current_mF4AA3F4158AEC974F5500B66AB2C5D0E7F560C0F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_t120B7E3243DD041063D08B99AF5918819E8BA72E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_t120B7E3243DD041063D08B99AF5918819E8BA72E_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12__ctor_mA348438E255D83BF6D31C4A904B1C9D0BFDF18BB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_t120B7E3243DD041063D08B99AF5918819E8BA72E_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_IDisposable_Dispose_m4C43CFFF5FF0DEC50CA5F4AF019157A2404DFA69(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_t120B7E3243DD041063D08B99AF5918819E8BA72E_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAA789A47FD4E85D770398F31F458D991E57D5822(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_t120B7E3243DD041063D08B99AF5918819E8BA72E_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_Reset_mEAD31E2B8E6B9460D8BC89E00A58DC568DFEDCE7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_t120B7E3243DD041063D08B99AF5918819E8BA72E_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_get_Current_m1E39FFAB0BA8AD5590084840D6C6B1147B388A3A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PlayerTankScript_tFF6F47993FD69BFAFAC36A93C13633A610633E46_CustomAttributesCacheGenerator_tankSpeed(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void WhiteAiBikeScript_tD531D71D3C0022F48ED2017A7CCE26BA5993358B_CustomAttributesCacheGenerator_bikeSpeed(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void WhiteAiBoatScript_t5E0AF80C46D534AC32CF139CD0D4DC3F38B322D2_CustomAttributesCacheGenerator_boatSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WhiteAiCharacterScript_t72246C2225D7E9E1B8C835DE0E4B680818E6AE66_CustomAttributesCacheGenerator_WhiteAiCharacterScript_OnTriggerEnter_mF98389C957252DAA658D0C6782844B50DD3BB48A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnTriggerEnterU3Ed__11_tD832E0B5E21EE60EB7EE639B091415AA2A37570C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COnTriggerEnterU3Ed__11_tD832E0B5E21EE60EB7EE639B091415AA2A37570C_0_0_0_var), NULL);
	}
}
static void WhiteAiCharacterScript_t72246C2225D7E9E1B8C835DE0E4B680818E6AE66_CustomAttributesCacheGenerator_WhiteAiCharacterScript_IsClimbing_m96BAB0B2C35DDFC80B654B636317E9C44D8D2C82(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CIsClimbingU3Ed__14_tCB8320995774A93BA1088528A0CF69A513364A72_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CIsClimbingU3Ed__14_tCB8320995774A93BA1088528A0CF69A513364A72_0_0_0_var), NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_tD832E0B5E21EE60EB7EE639B091415AA2A37570C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_tD832E0B5E21EE60EB7EE639B091415AA2A37570C_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11__ctor_mEBE528454705838D7F327CA5E247BA9D56E91EF7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_tD832E0B5E21EE60EB7EE639B091415AA2A37570C_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_IDisposable_Dispose_m516C2E127C3B3045FEEF4FF7AAA607FE145EDEF9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_tD832E0B5E21EE60EB7EE639B091415AA2A37570C_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3A149DD87BBDCDA6FB6654FCB0075EF9BA2BF827(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_tD832E0B5E21EE60EB7EE639B091415AA2A37570C_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_Reset_m9A6AF1050DBC028A87125D7D75181368217E7842(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnTriggerEnterU3Ed__11_tD832E0B5E21EE60EB7EE639B091415AA2A37570C_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_get_Current_m89517AA72A5C2553EA773B7BA5633B6E782E8B5D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_tCB8320995774A93BA1088528A0CF69A513364A72_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_tCB8320995774A93BA1088528A0CF69A513364A72_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14__ctor_mD50ABC879D4CF442FE82D0600C944384F089EE09(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_tCB8320995774A93BA1088528A0CF69A513364A72_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_IDisposable_Dispose_m24EADF4B39D6E9C8BB81DD85DF9C5DF33E513E0B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_tCB8320995774A93BA1088528A0CF69A513364A72_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD193DA26A09635216183F2B99C9518351610909D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_tCB8320995774A93BA1088528A0CF69A513364A72_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_Reset_m3001C0A5FCB20D56031F2BEAA18BBDED1E26C86C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIsClimbingU3Ed__14_tCB8320995774A93BA1088528A0CF69A513364A72_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_get_Current_m1F4082A4FC0430E5D727271AC49902284A2326E9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_characterIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_bikeIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_jeepIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_boatIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_tankIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_helicopterIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_gliderIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_rotationSpeed(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x65\x64\x73"), NULL);
	}
}
static void WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_WhiteAiControllerScript_MoveObject_mD0A4644E2AC3C736E82D453DD534CD123D126A18(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CMoveObjectU3Ed__35_tDA1527E60714BAB33FB5F6D33DF5CD891AF3BF37_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CMoveObjectU3Ed__35_tDA1527E60714BAB33FB5F6D33DF5CD891AF3BF37_0_0_0_var), NULL);
	}
}
static void WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_WhiteAiControllerScript_Transform_m6D5DF53567C35622727179CED636CC9F5432E595(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTransformU3Ed__38_tD5DE24B17D1D0A5F1FC0FE78E3872F10DFF2110B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTransformU3Ed__38_tD5DE24B17D1D0A5F1FC0FE78E3872F10DFF2110B_0_0_0_var), NULL);
	}
}
static void WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_WhiteAiControllerScript_TransformToRandom_mEE31E2CE3B2E990473C6C3FF5DA49ECABB3C8368(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTransformToRandomU3Ed__39_t22FBD7D18E312477264739FA52451E2465202093_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTransformToRandomU3Ed__39_t22FBD7D18E312477264739FA52451E2465202093_0_0_0_var), NULL);
	}
}
static void U3CMoveObjectU3Ed__35_tDA1527E60714BAB33FB5F6D33DF5CD891AF3BF37_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__35_tDA1527E60714BAB33FB5F6D33DF5CD891AF3BF37_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35__ctor_m46FAD7C53D13345B8A77D4EC983C9B26FB309376(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__35_tDA1527E60714BAB33FB5F6D33DF5CD891AF3BF37_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_IDisposable_Dispose_m17574B27BD597AD8546BC11CD6A58C8EB9040CBE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__35_tDA1527E60714BAB33FB5F6D33DF5CD891AF3BF37_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE77F08164CB742A73CC623896C303BE46F18789(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__35_tDA1527E60714BAB33FB5F6D33DF5CD891AF3BF37_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_Reset_m74279CE4296C2A6C033D3378BFB1E1B48CFF9FF0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveObjectU3Ed__35_tDA1527E60714BAB33FB5F6D33DF5CD891AF3BF37_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_get_Current_m159DA0631BCEEC3665E0265F971D1F9B27D6472C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_tD5DE24B17D1D0A5F1FC0FE78E3872F10DFF2110B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_tD5DE24B17D1D0A5F1FC0FE78E3872F10DFF2110B_CustomAttributesCacheGenerator_U3CTransformU3Ed__38__ctor_m9BD5E5BF43D39641AC0247391E4797A7319B1AD4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_tD5DE24B17D1D0A5F1FC0FE78E3872F10DFF2110B_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_IDisposable_Dispose_m0B2B7B2528B459935A3F95DFE3E91B482848C801(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_tD5DE24B17D1D0A5F1FC0FE78E3872F10DFF2110B_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB25722084BED3E54E2949574C55216D8F602E057(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_tD5DE24B17D1D0A5F1FC0FE78E3872F10DFF2110B_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_IEnumerator_Reset_mABC4575D59FB478E3D6019D8FFD280E2B7777ACE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformU3Ed__38_tD5DE24B17D1D0A5F1FC0FE78E3872F10DFF2110B_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_IEnumerator_get_Current_m6148983E37AABFC28BA9819C013473ABF88C5F77(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t22FBD7D18E312477264739FA52451E2465202093_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t22FBD7D18E312477264739FA52451E2465202093_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39__ctor_mE4E2E68B2E5D58BF723A15D1B39E372FC8B04C43(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t22FBD7D18E312477264739FA52451E2465202093_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_IDisposable_Dispose_m6194F8626FBFB3D09711A844BE285169751F003F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t22FBD7D18E312477264739FA52451E2465202093_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C6B2A3331459A3A3E8A7640B058F0B0C99BAD48(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t22FBD7D18E312477264739FA52451E2465202093_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_Reset_m80BF425047633FFBDC3BD6A67D0A4C77490717F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTransformToRandomU3Ed__39_t22FBD7D18E312477264739FA52451E2465202093_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_get_Current_mED3B378B7A0BC6D929D9A73DED17AC2D7CE5C3DB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WhiteAiHelicopterScript_tBAB805EB8B869DA41835FD46A6E627F428F3202E_CustomAttributesCacheGenerator_helicopterSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WhiteAiHelicopterScript_tBAB805EB8B869DA41835FD46A6E627F428F3202E_CustomAttributesCacheGenerator_WhiteAiHelicopterScript_wait_mED718A5D65ACB507B40E9AB7C3D5A527C94608F0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CwaitU3Ed__8_t3902C28528005BABE7C375AF2E394292E6067C45_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CwaitU3Ed__8_t3902C28528005BABE7C375AF2E394292E6067C45_0_0_0_var), NULL);
	}
}
static void WhiteAiHelicopterScript_tBAB805EB8B869DA41835FD46A6E627F428F3202E_CustomAttributesCacheGenerator_WhiteAiHelicopterScript_BoostHelicopterNow_mADA21CCA99D5687E6BC26F7BD3193889086B4AD8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBoostHelicopterNowU3Ed__12_tEB7AB33D399DA7AE43888DB96F8DAA6F18B2EB94_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBoostHelicopterNowU3Ed__12_tEB7AB33D399DA7AE43888DB96F8DAA6F18B2EB94_0_0_0_var), NULL);
	}
}
static void U3CwaitU3Ed__8_t3902C28528005BABE7C375AF2E394292E6067C45_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t3902C28528005BABE7C375AF2E394292E6067C45_CustomAttributesCacheGenerator_U3CwaitU3Ed__8__ctor_mCDB3D87DB2A64800169702361168C4CDC6B3D37C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t3902C28528005BABE7C375AF2E394292E6067C45_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_IDisposable_Dispose_mABACCB7EB2890077CA87A31B4FD0CF548274E171(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t3902C28528005BABE7C375AF2E394292E6067C45_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8272CCE65FF2619EBBA0D2C0A7BD4212C3125869(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t3902C28528005BABE7C375AF2E394292E6067C45_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_IEnumerator_Reset_mB2BE0C91E4E9A386047094591B5B86CDA8DB7810(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CwaitU3Ed__8_t3902C28528005BABE7C375AF2E394292E6067C45_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_IEnumerator_get_Current_mABCAEFC067279C876F020818FBBAEF9CBF999BD6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_tEB7AB33D399DA7AE43888DB96F8DAA6F18B2EB94_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_tEB7AB33D399DA7AE43888DB96F8DAA6F18B2EB94_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12__ctor_mEBCFF3C87A4DB90A3566DB58CC56588429F746EE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_tEB7AB33D399DA7AE43888DB96F8DAA6F18B2EB94_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_IDisposable_Dispose_mA987C60121DC0B5C6BEA47187B70D7D69BB47BB8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_tEB7AB33D399DA7AE43888DB96F8DAA6F18B2EB94_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2931D5D08309E702D72BC52D93601CC4D7308A8F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_tEB7AB33D399DA7AE43888DB96F8DAA6F18B2EB94_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_Reset_mFF46627F7E9356628562AF2AA430E389043A735B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBoostHelicopterNowU3Ed__12_tEB7AB33D399DA7AE43888DB96F8DAA6F18B2EB94_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_get_Current_mDD5107EF134E88023F3DD165A5D3CB19F0FFA802(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WhiteAiTankScript_t835FFEA659D5F876AB3ED681ECA8DB5AFEDE981C_CustomAttributesCacheGenerator_tankSpeed(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void DemoInventory_t5EBF9C31A38E9CD173ACFEAE5492FB11BA17F6AD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOColor_mA67D7B77174B479F8AE20F739A74BA7A2BBC9F0F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOFade_mEED04CEE3EE9D32B87478A3CFDB9CFB710307769(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOGradientColor_m47670ABD64AEDC972E8210CAF4C8A778B0B7676D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOBlendableColor_m51BBDB4A69B0560481736E6734EA2BF52DB1537D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t865A37B70235C08B0F32F1F40EAB16312D05FD50_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_t0A5F164BAD8ACAD20D458366B497C9B9C1974AE4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_tE81EA512AF1E002F436D3758BC5CE9D93CED44E4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_mF927A1EDCA91814D8A466A343E3223EEF4ED7478(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_m7067683474EE6A600294C2CCFD941403FA789A6E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m41638D05EB7678C523D0D39E320FAC80B8E48658(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_mF4B41443760536F3C399612AD0283A52937D95CC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFillAmount_mE4ED52B826A6F8035320E980F7C701B4047C4DBA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOGradientColor_mB4B8D1DEDA92464670EB8A6D985973477D51A4D5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFlexibleSize_mF0916C4E7A906202EFBAEA89CC2B55A9C3CF48B3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOMinSize_m54A6919625499AFD6F801C6EC9672965DBDC96E1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPreferredSize_mE26AFB2C606F095483EDED449FB8A5A80C08A8DE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_m15BC0FCF035E3DEEC3CFC30465EDE0FE6FD6D2EA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m86DB80308D92FC4DFD3E981F9E958FF2A207EA74(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOScale_mFA27F29E3DFB6B1CFCAC3CE73F06DF3737A3EB06(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos_m63999D434CE7E8F6BE66BBA92532DE9D98DC9F79(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPosX_m9B57E02E9902C7A3F1A8DD1021AF630C10478380(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPosY_m14CCA63E545D7C28AD1A76488D0084F55EE94CA8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3D_m6DEB7C4AE4CD2118BF83D268945340F3373DC7BB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DX_mA40303AC769280BDE74EE58BE5AD80BEA427CE08(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DY_mE6BE56AE421B959F22CBA6318BABC3CD367A7277(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DZ_m702F7D4FE9DC8E6116FB5E0DD478D2BB88ED1242(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorMax_mDD97F6A8B4468622E3808507CB6B89A8A109BDCE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorMin_m60C4775B1EEDBCF2CD3E96A8171C8A83FD7F400D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivot_mF623939EDCC152F664FECC8B71E321B0AA13F0A9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivotX_mC21C031A61BDB7C3A8087719F23CBC6D1D762D6B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivotY_m9922C28ADECA433362D3C6C816318CA00705996A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOSizeDelta_mC4FD07525E1CA5BBF577030D571CE0CCEFD4E6B7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPunchAnchorPos_m058E3DA140B9C497727F05AEAA296425E585B9EA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOShakeAnchorPos_m89D1C74E940B9434B2234FB49C031AD8FD1170AB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOShakeAnchorPos_m7818867062313A535AB2CD6F86267E2AB9357AC6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOJumpAnchorPos_m7046EE1C82AF0159CFAAA460E4C3D22797625480(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DONormalizedPos_m300F112FE2F67D4DC718083340C7FD27BC96F248(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOHorizontalNormalizedPos_mCB4BCD85B49C458BE871F890A86FEFE0D7F41B42(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOVerticalNormalizedPos_mC0269B4644816A0D5C09717CA92E4229CB31CDF1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOValue_mFDF45D60F52C82AACD8B50859A51B8F315A44F08(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_mFD36CA1EA683A0D2312EE87BCF3B0A924504E797(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOText_mC0932E45473278B2FFB1698954CC0EB0422C626F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_m4C5E9A68BFBEE876E1FF8D8E9A4BA77856F144C1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_m18196516A24780A550314E9327868D6D6F3EAA4E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_mC30D1F805FE976FB25EC361AB0ABA94413361B6F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t7D1FCC95A881539756647760D7E3BCAD117D9C2F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_t585165D7235045AD6EACEA96E3084E01A742C936_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_tD73E62A2224FE13C8F6B52FCE8BD1C5FD313B99F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_t9A0201E5801BCA86E98A5567791D120983EAD387_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_tB80886C18986402D46C539623ED89C069D383A01_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_t3101E53746ED12113974445EFE5F8ED7275D9845_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_tE55619A7933D4E2E6A3AF6BCBAB6B4C17A865A55_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_t7A8C490B052492D0F6162243E706C61D6E7EA629_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_t8BFF1858EDF6EB2283AE20F2D8CC662A1FA5B379_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_0_t5C701670B3C5449F73D82915BD4DA267A8B8E8F5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass12_0_t9C18A438FEAA8C5C1A925F5F28D6DF30A8D5B269_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_0_t27965A830C5CE83826CC357F50299FE28BE9CABB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass14_0_t532827BA234D3C2B9DA5E065866B8C8F605250F4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_0_t1E74F8DF7C76B80F85C66967590B72EF52627D1C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_0_t6FE99B74BA758AD9E3C5FA4B81F22E50E2878289_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass17_0_t161B9E0C9F3AF194B3F6E501D4E253148BFDFEB6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass18_0_t28369CB684CE937EF263102FBF87D2E7FC952FD0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass19_0_t7047CE2AF01FC75FD677DF4C1C7E4B32EC2452A5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass20_0_tB3B621D850CE15CA278C20DAAA2C3C0358206A21_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass21_0_t770F928128D9FC7ADA02EA054C711D60F30E16B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_0_t97D2AA4FE9552148F7E37D2DFED676A57DFB1351_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass23_0_t26F763CBFD1D4D4EDFFE9BD383DB924D3B6033E7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass24_0_tF09BD80EEE9ECC7296C10982CEDF0DBFD274B895_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass25_0_tA347965DD1CA3D351E3B581654CC8128EDFF3061_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass26_0_t93DB5B4DF0357D11E4A951B037CFD527BF3B56C8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass27_0_t4D4A1506D5AC9A010D11414FAD7E48C6A7FBA299_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass28_0_tE68276C76E7215C53283D3ADC6A731F1DF29E8C8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass29_0_t5555723CB03030A05216BCB8B27846D583B2D84D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass30_0_t94F73A3FFC30F6C2558392FF5F07AE2E096BF84F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass31_0_t805529EB975D0626EBC97ACAB3880EFC6AB317A3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass32_0_tA414D6A179D68B4F320ED1DB2C432127B75AB0E9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass33_0_t5CB8EE7F07D06E29D59588955B144762AF73E4F6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass34_0_t60E5325D834E607C2110DB690899FC3C7A44D407_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass35_0_t8938338C4A6E58BB25C48A1F318E0458FD3B8CC4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass37_0_tFAFEF1E41FDD4BAE035F7732E85AA44BBFD9DFD1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass38_0_t25800BA4D30D443A7F92C326FA62E4A0351DD2CA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass39_0_t51E4686289D218AD17F28EABBF58A0628BFD6B10_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOGradientColor_m7E9B0D004D4B3D602EBB852CA1755FE863C6B839(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOGradientColor_mA4E191E86F2EF81966259A40B2CDADE375437920(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForCompletion_m4BCEDFA5ED1ACF4BC1B3531C0C306A7AC553C1F2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForRewind_mD9F37D5AAD9B665EE07F083D026E65194A8E3E6E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForKill_m02BD7F3B018BB6B4ED4FCAF47B8DA435EC60BD6B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForElapsedLoops_m46D46788899029A20F73BE252F7427D853131404(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForPosition_m1771625E294B38C5D13DCA40846AEA881C1B23E6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForStart_mB4AAFE83B50AEF3A167F31C14B2F58794527F905(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOOffset_m09CA228EBED193314D3D9A41BBDC97393E1FC4D2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOTiling_m6A3CD06EC3030A93EC3E93DE678F34048D5C1141(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_tE5BFC2D888ECF087A82B3D73CECEAC3525EE4581_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_tACB95D3310A47E0C0B9E503E6931B7F7BAC03551_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_CustomAttributesCacheGenerator_DOTweenModuleUtils_Init_mFF2188F42FA7128FE1A9BBC2CB9E6351919C9D15(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_CustomAttributesCacheGenerator_DOTweenModuleUtils_Preserver_mBE8D1FE2AE913FBFFCCF43B5A615D941B3706F43(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void Physics_t379352FCCF26C01744720033D3784D7F57DE0D8F_CustomAttributesCacheGenerator_Physics_HasRigidbody_m3082D72A658CCDDF6BB1F3D4A3EFBD6397D6E862(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
static void Physics_t379352FCCF26C01744720033D3784D7F57DE0D8F_CustomAttributesCacheGenerator_Physics_CreateDOTweenPathTween_m94D41B65E37500E5ACCF6B6F3B5D53FB031C4245(CustomAttributesCache* cache)
{
	{
		PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 * tmp = (PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948 *)cache->attributes[0];
		PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[403] = 
{
	IAPDemo_t2F65C3A79FA62DA779CABC0DD4333AC5F5AE5BBA_CustomAttributesCacheGenerator,
	U3COnTriggerEnterU3Ed__11_t677F8B25E48451A2354A3EE7A708862BCA492708_CustomAttributesCacheGenerator,
	U3CIsClimbingU3Ed__14_t05F8E70221425349B4BBE445DE189BEF4698AC9E_CustomAttributesCacheGenerator,
	U3CMoveObjectU3Ed__35_t50C7740B31CF95155E33E0757B3AD9F33683E6D7_CustomAttributesCacheGenerator,
	U3CTransformU3Ed__38_t8CC95BDD0F4FD49FC83285F7CEF8A93C3E6A1B8C_CustomAttributesCacheGenerator,
	U3CTransformToRandomU3Ed__39_t49AF10D930D8B25F8449CDFBEAE125B0A92A7840_CustomAttributesCacheGenerator,
	U3CwaitU3Ed__8_t4CDB4A05B9DD4AAD7D14AE1D72498CAB758B6E70_CustomAttributesCacheGenerator,
	U3CBoostHelicopterNowU3Ed__12_tE2800D3B1FB7B0E8A590DE33366E6B3BF4B2D974_CustomAttributesCacheGenerator,
	U3COnTriggerEnterU3Ed__11_t904EB5FF89899A024A6C18963357BD34EBCDA16A_CustomAttributesCacheGenerator,
	U3CIsClimbingU3Ed__14_tC09D56759286ACB8E965E213553A8F190465063B_CustomAttributesCacheGenerator,
	U3CMoveObjectU3Ed__35_t8C90635DD28CBB7A85B947ED0AC579459C340A2F_CustomAttributesCacheGenerator,
	U3CTransformU3Ed__38_tB809B2C3D179480B2C9A54CCE64435C10F528D02_CustomAttributesCacheGenerator,
	U3CTransformToRandomU3Ed__39_t6A0F73153B2100DF513A4EB28E9307EFCCBF456F_CustomAttributesCacheGenerator,
	U3CwaitU3Ed__8_t5AD13EABC4D1FF4CB47B9ED66FF3489CD08BAFA0_CustomAttributesCacheGenerator,
	U3CBoostHelicopterNowU3Ed__12_t0C37D0395174896A6E5A3706E86C72010B8D4768_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass17_0_t11873D890212501E7C830EC045721BB5816E989F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass19_0_t1E85ED0942E2697BB6FF4BBD8D617BB08C882E7E_CustomAttributesCacheGenerator,
	U3CIsLevelFailedU3Ed__21_t99F7A1A7AC53B84C4087969ED4E8D0824AA06C9B_CustomAttributesCacheGenerator,
	U3CRestartLevellU3Ed__23_t9404402CF834E4FB4B8C2B286258480C1252A279_CustomAttributesCacheGenerator,
	U3CisGameCompletedU3Ed__25_tDADD3FD80F4A36560EF3A84C13B29DD66BDB3595_CustomAttributesCacheGenerator,
	U3CStartNextLevelU3Ed__27_t3813EED139F1C6383D0388371F447092CF4B61AF_CustomAttributesCacheGenerator,
	U3CU3Ec_t0259FC3D9B75D94FC2AA58017A67F463CB0A525F_CustomAttributesCacheGenerator,
	U3CU3Ec_t4373CB59F6AD8FAFC444E432EBDBA2A692E8B217_CustomAttributesCacheGenerator,
	U3CPlayToastU3Ed__44_tE9527CA78E775091AD4691E0A2C5F212C2AA847E_CustomAttributesCacheGenerator,
	U3COnTriggerEnterU3Ed__11_t7D3D7DBF022F6B334B59C160F44F85726BCCFF2B_CustomAttributesCacheGenerator,
	U3CIsClimbingU3Ed__13_tC644B10DB02DA8C5D0589F7F40CAE7B33A0161A2_CustomAttributesCacheGenerator,
	U3CMoveObjectU3Ed__34_t2EACAA28BCAB665BCBEF0F835037712D648BDC90_CustomAttributesCacheGenerator,
	U3CwaitU3Ed__8_t53F34A2BCB132591FD143778B01D2B7B336DACD4_CustomAttributesCacheGenerator,
	U3CBoostHelicopterNowU3Ed__12_t120B7E3243DD041063D08B99AF5918819E8BA72E_CustomAttributesCacheGenerator,
	U3COnTriggerEnterU3Ed__11_tD832E0B5E21EE60EB7EE639B091415AA2A37570C_CustomAttributesCacheGenerator,
	U3CIsClimbingU3Ed__14_tCB8320995774A93BA1088528A0CF69A513364A72_CustomAttributesCacheGenerator,
	U3CMoveObjectU3Ed__35_tDA1527E60714BAB33FB5F6D33DF5CD891AF3BF37_CustomAttributesCacheGenerator,
	U3CTransformU3Ed__38_tD5DE24B17D1D0A5F1FC0FE78E3872F10DFF2110B_CustomAttributesCacheGenerator,
	U3CTransformToRandomU3Ed__39_t22FBD7D18E312477264739FA52451E2465202093_CustomAttributesCacheGenerator,
	U3CwaitU3Ed__8_t3902C28528005BABE7C375AF2E394292E6067C45_CustomAttributesCacheGenerator,
	U3CBoostHelicopterNowU3Ed__12_tEB7AB33D399DA7AE43888DB96F8DAA6F18B2EB94_CustomAttributesCacheGenerator,
	DemoInventory_t5EBF9C31A38E9CD173ACFEAE5492FB11BA17F6AD_CustomAttributesCacheGenerator,
	DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t865A37B70235C08B0F32F1F40EAB16312D05FD50_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_t0A5F164BAD8ACAD20D458366B497C9B9C1974AE4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_tE81EA512AF1E002F436D3758BC5CE9D93CED44E4_CustomAttributesCacheGenerator,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t7D1FCC95A881539756647760D7E3BCAD117D9C2F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_t585165D7235045AD6EACEA96E3084E01A742C936_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_tD73E62A2224FE13C8F6B52FCE8BD1C5FD313B99F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_t9A0201E5801BCA86E98A5567791D120983EAD387_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t9CD30C09F13A8901FBD37CEEE628023A3AC88587_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_tB80886C18986402D46C539623ED89C069D383A01_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_t3101E53746ED12113974445EFE5F8ED7275D9845_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_tE55619A7933D4E2E6A3AF6BCBAB6B4C17A865A55_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_t7A8C490B052492D0F6162243E706C61D6E7EA629_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_t8BFF1858EDF6EB2283AE20F2D8CC662A1FA5B379_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_0_t5C701670B3C5449F73D82915BD4DA267A8B8E8F5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass12_0_t9C18A438FEAA8C5C1A925F5F28D6DF30A8D5B269_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_0_t27965A830C5CE83826CC357F50299FE28BE9CABB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass14_0_t532827BA234D3C2B9DA5E065866B8C8F605250F4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_0_t1E74F8DF7C76B80F85C66967590B72EF52627D1C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_0_t6FE99B74BA758AD9E3C5FA4B81F22E50E2878289_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass17_0_t161B9E0C9F3AF194B3F6E501D4E253148BFDFEB6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass18_0_t28369CB684CE937EF263102FBF87D2E7FC952FD0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass19_0_t7047CE2AF01FC75FD677DF4C1C7E4B32EC2452A5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass20_0_tB3B621D850CE15CA278C20DAAA2C3C0358206A21_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass21_0_t770F928128D9FC7ADA02EA054C711D60F30E16B9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_0_t97D2AA4FE9552148F7E37D2DFED676A57DFB1351_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass23_0_t26F763CBFD1D4D4EDFFE9BD383DB924D3B6033E7_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass24_0_tF09BD80EEE9ECC7296C10982CEDF0DBFD274B895_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass25_0_tA347965DD1CA3D351E3B581654CC8128EDFF3061_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass26_0_t93DB5B4DF0357D11E4A951B037CFD527BF3B56C8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass27_0_t4D4A1506D5AC9A010D11414FAD7E48C6A7FBA299_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass28_0_tE68276C76E7215C53283D3ADC6A731F1DF29E8C8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass29_0_t5555723CB03030A05216BCB8B27846D583B2D84D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass30_0_t94F73A3FFC30F6C2558392FF5F07AE2E096BF84F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass31_0_t805529EB975D0626EBC97ACAB3880EFC6AB317A3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass32_0_tA414D6A179D68B4F320ED1DB2C432127B75AB0E9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass33_0_t5CB8EE7F07D06E29D59588955B144762AF73E4F6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass34_0_t60E5325D834E607C2110DB690899FC3C7A44D407_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass35_0_t8938338C4A6E58BB25C48A1F318E0458FD3B8CC4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass36_0_tCE8E904A62CBD9F143B3D692CA532EFA7A6F777D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass37_0_tFAFEF1E41FDD4BAE035F7732E85AA44BBFD9DFD1_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass38_0_t25800BA4D30D443A7F92C326FA62E4A0351DD2CA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass39_0_t51E4686289D218AD17F28EABBF58A0628BFD6B10_CustomAttributesCacheGenerator,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_tE5BFC2D888ECF087A82B3D73CECEAC3525EE4581_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_tACB95D3310A47E0C0B9E503E6931B7F7BAC03551_CustomAttributesCacheGenerator,
	BrownAiBikeScript_tCCD4B4D66E5A0AB0BF9B2B7ECB651FEFF733EDC6_CustomAttributesCacheGenerator_bikeSpeed,
	BrownAiBoatScript_tFDDCD5A6CBE091A61C59220376FA7337F83551F6_CustomAttributesCacheGenerator_boatSpeed,
	BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_characterIndex,
	BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_bikeIndex,
	BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_jeepIndex,
	BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_boatIndex,
	BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_tankIndex,
	BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_helicopterIndex,
	BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_gliderIndex,
	BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_rotationSpeed,
	BrownAiHelicopterScript_tB45E14166B167B5CEC303CE7B4223202D6AC7905_CustomAttributesCacheGenerator_helicopterSpeed,
	BrownAiTankScript_t98B71F50FB5CE7E5682814AE151AE912B340145E_CustomAttributesCacheGenerator_tankSpeed,
	GreenAiBikeScript_t765F6BA8CC78691CEBB744D693B5A69B6F47DF0D_CustomAttributesCacheGenerator_bikeSpeed,
	GreenAiBoatScript_t872CE0E58A77B76E622B3C74BB23A5F0A0934C4A_CustomAttributesCacheGenerator_boatSpeed,
	GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_characterIndex,
	GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_bikeIndex,
	GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_jeepIndex,
	GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_boatIndex,
	GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_tankIndex,
	GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_helicopterIndex,
	GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_gliderIndex,
	GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_rotationSpeed,
	GreenAiHelicopterScript_tC681D9410CB1ECED4FD67F37A4E58A2DC586E135_CustomAttributesCacheGenerator_helicopterSpeed,
	GreenAiTankScript_tB772EDBC62546E97D06C735EF7E0C550236A81B0_CustomAttributesCacheGenerator_tankSpeed,
	AdsScript_t0B8CAA5F1B0A18DC4CE87A334C4CE021DCD63A06_CustomAttributesCacheGenerator_remoteKey,
	AdsScript_t0B8CAA5F1B0A18DC4CE87A334C4CE021DCD63A06_CustomAttributesCacheGenerator_bannerID,
	AdsScript_t0B8CAA5F1B0A18DC4CE87A334C4CE021DCD63A06_CustomAttributesCacheGenerator_testMode,
	AdsScript_t0B8CAA5F1B0A18DC4CE87A334C4CE021DCD63A06_CustomAttributesCacheGenerator_bannerPosition,
	CoinAnimationScript_tDD9E07CD4907784CAB40C5CC81FE1B03AB3391E6_CustomAttributesCacheGenerator_animatedCoinPrefab,
	CoinAnimationScript_tDD9E07CD4907784CAB40C5CC81FE1B03AB3391E6_CustomAttributesCacheGenerator_target,
	CoinAnimationScript_tDD9E07CD4907784CAB40C5CC81FE1B03AB3391E6_CustomAttributesCacheGenerator_source,
	CoinAnimationScript_tDD9E07CD4907784CAB40C5CC81FE1B03AB3391E6_CustomAttributesCacheGenerator_maxCoins,
	CoinAnimationScript_tDD9E07CD4907784CAB40C5CC81FE1B03AB3391E6_CustomAttributesCacheGenerator_minAnimDuration,
	CoinAnimationScript_tDD9E07CD4907784CAB40C5CC81FE1B03AB3391E6_CustomAttributesCacheGenerator_maxAnimDuration,
	CoinAnimationScript_tDD9E07CD4907784CAB40C5CC81FE1B03AB3391E6_CustomAttributesCacheGenerator_easeType,
	CoinAnimationScript_tDD9E07CD4907784CAB40C5CC81FE1B03AB3391E6_CustomAttributesCacheGenerator_spread,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_shapes,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_buttons,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_vector3s,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_toast,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_levelNumberText,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_coinText,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_mainMenuPanel,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_one,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_gameButtons,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_prevIndex,
	PlayerBikeScript_tE0494C0D3389348356F38B40D4F1DAE3FDF875EE_CustomAttributesCacheGenerator_bikeSpeed,
	PlayerBoatScript_t768AEB5C4FABAF584B24A71206778CD986B6D136_CustomAttributesCacheGenerator_boatSpeed,
	PlayerCharacterScript_t37FDF6D7062F616CD3A0F50F67268935781D950E_CustomAttributesCacheGenerator_isClimbing,
	PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_characterIndex,
	PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_bikeIndex,
	PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_jeepIndex,
	PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_boatIndex,
	PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_tankIndex,
	PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_helicopterIndex,
	PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_gliderIndex,
	PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_rotationSpeed,
	PlayerHelicopterScript_t9B7E46925E36AB47E790EA303E302417206DAEA1_CustomAttributesCacheGenerator_helicopterSpeed,
	PlayerTankScript_tFF6F47993FD69BFAFAC36A93C13633A610633E46_CustomAttributesCacheGenerator_tankSpeed,
	WhiteAiBikeScript_tD531D71D3C0022F48ED2017A7CCE26BA5993358B_CustomAttributesCacheGenerator_bikeSpeed,
	WhiteAiBoatScript_t5E0AF80C46D534AC32CF139CD0D4DC3F38B322D2_CustomAttributesCacheGenerator_boatSpeed,
	WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_characterIndex,
	WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_bikeIndex,
	WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_jeepIndex,
	WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_boatIndex,
	WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_tankIndex,
	WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_helicopterIndex,
	WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_gliderIndex,
	WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_rotationSpeed,
	WhiteAiHelicopterScript_tBAB805EB8B869DA41835FD46A6E627F428F3202E_CustomAttributesCacheGenerator_helicopterSpeed,
	WhiteAiTankScript_t835FFEA659D5F876AB3ED681ECA8DB5AFEDE981C_CustomAttributesCacheGenerator_tankSpeed,
	BrownAiCharacterScript_t4AABE7098B74BAC2C4C9425D0069B3EF862E2891_CustomAttributesCacheGenerator_BrownAiCharacterScript_OnTriggerEnter_m493718247AF0CF260CEB8A4C9DD1ED72F8BEBEFD,
	BrownAiCharacterScript_t4AABE7098B74BAC2C4C9425D0069B3EF862E2891_CustomAttributesCacheGenerator_BrownAiCharacterScript_IsClimbing_mE2168EA8FC2E7E70A1C69455C9FA9685BA147C76,
	U3COnTriggerEnterU3Ed__11_t677F8B25E48451A2354A3EE7A708862BCA492708_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11__ctor_m58CE187FD101838CFCA77561782A057DBE065674,
	U3COnTriggerEnterU3Ed__11_t677F8B25E48451A2354A3EE7A708862BCA492708_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_IDisposable_Dispose_m1B1ED971F3313F0B1835963EC4B2C73FA501A814,
	U3COnTriggerEnterU3Ed__11_t677F8B25E48451A2354A3EE7A708862BCA492708_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA19AE93EDBD946686F0548C8A3C8CF793C75AB6,
	U3COnTriggerEnterU3Ed__11_t677F8B25E48451A2354A3EE7A708862BCA492708_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_Reset_mDF0157FD691D1AA9DA10ED0CD2D171E2DAC63F6A,
	U3COnTriggerEnterU3Ed__11_t677F8B25E48451A2354A3EE7A708862BCA492708_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_get_Current_m362197B045B5D2FE9811917DBDAE5ABF6166D8BE,
	U3CIsClimbingU3Ed__14_t05F8E70221425349B4BBE445DE189BEF4698AC9E_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14__ctor_mF524A37C748D43169BC4BEE0CD4FA5DDFA04969A,
	U3CIsClimbingU3Ed__14_t05F8E70221425349B4BBE445DE189BEF4698AC9E_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_IDisposable_Dispose_m4DC1A0A554B0D891F7D55A5706870108D70CB307,
	U3CIsClimbingU3Ed__14_t05F8E70221425349B4BBE445DE189BEF4698AC9E_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6AC86978016C16339DB835397D58344D4CE84CEA,
	U3CIsClimbingU3Ed__14_t05F8E70221425349B4BBE445DE189BEF4698AC9E_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_Reset_m3EDEF13B8E0F81E027857D03E2AE87CC2E650733,
	U3CIsClimbingU3Ed__14_t05F8E70221425349B4BBE445DE189BEF4698AC9E_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_get_Current_mEA8103E3D058EAD5EA2236B4F85784722145ACC7,
	BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_BrownAiControllerScript_MoveObject_mE73BD22EC65468F1DB77EC6F56E369F3372B960C,
	BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_BrownAiControllerScript_Transform_m1B6BB668C02AF1A1B1D119A58FEEB654E580C844,
	BrownAiControllerScript_t907CCA18CE467EF770246DC1D9FEB1F46E425275_CustomAttributesCacheGenerator_BrownAiControllerScript_TransformToRandom_m235F4E738DF91B1007DBC556DE0015637C55A285,
	U3CMoveObjectU3Ed__35_t50C7740B31CF95155E33E0757B3AD9F33683E6D7_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35__ctor_m09BED1D81F93D8216B7F0FAEE530C1CBF188A422,
	U3CMoveObjectU3Ed__35_t50C7740B31CF95155E33E0757B3AD9F33683E6D7_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_IDisposable_Dispose_mD48351D86E92E28331F2E598A87C699CDA727F75,
	U3CMoveObjectU3Ed__35_t50C7740B31CF95155E33E0757B3AD9F33683E6D7_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD24BAE8A82B777887972D591C6FCF0BF84E28EE,
	U3CMoveObjectU3Ed__35_t50C7740B31CF95155E33E0757B3AD9F33683E6D7_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_Reset_m3AFFAEDB3F53A4EEE4966EA6E27C59B135C4AA95,
	U3CMoveObjectU3Ed__35_t50C7740B31CF95155E33E0757B3AD9F33683E6D7_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_get_Current_m98A9BA1D1B9DEA461AA45606CD526DF70322DD08,
	U3CTransformU3Ed__38_t8CC95BDD0F4FD49FC83285F7CEF8A93C3E6A1B8C_CustomAttributesCacheGenerator_U3CTransformU3Ed__38__ctor_m997EE86AA60658A13C21ED38BEC132A57A81EB01,
	U3CTransformU3Ed__38_t8CC95BDD0F4FD49FC83285F7CEF8A93C3E6A1B8C_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_IDisposable_Dispose_mB8A89A2B908D9F9289714BC710A4AD681841105A,
	U3CTransformU3Ed__38_t8CC95BDD0F4FD49FC83285F7CEF8A93C3E6A1B8C_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC790BF887E6356F5D81DBAF95C2DDBE7636E02E6,
	U3CTransformU3Ed__38_t8CC95BDD0F4FD49FC83285F7CEF8A93C3E6A1B8C_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_IEnumerator_Reset_mB23DA13FBE8C5E65C1F051D050FB12C855C291F2,
	U3CTransformU3Ed__38_t8CC95BDD0F4FD49FC83285F7CEF8A93C3E6A1B8C_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_IEnumerator_get_Current_mEBD9114DE61869220216C93567201F39A0ACECB0,
	U3CTransformToRandomU3Ed__39_t49AF10D930D8B25F8449CDFBEAE125B0A92A7840_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39__ctor_m551C7C45B492150BA04CEF5B3E2CCE43AF524DAF,
	U3CTransformToRandomU3Ed__39_t49AF10D930D8B25F8449CDFBEAE125B0A92A7840_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_IDisposable_Dispose_mB8D6EE430E1F60115A97625706C08E4175D9C266,
	U3CTransformToRandomU3Ed__39_t49AF10D930D8B25F8449CDFBEAE125B0A92A7840_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE802F8C275160DBFE5A0E861778DB93AA30CA2A2,
	U3CTransformToRandomU3Ed__39_t49AF10D930D8B25F8449CDFBEAE125B0A92A7840_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_Reset_mF8829DE38EDE66FA018654CF651988DF3775B13D,
	U3CTransformToRandomU3Ed__39_t49AF10D930D8B25F8449CDFBEAE125B0A92A7840_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_get_Current_mBE8313C35CA232335D73F1F6529F45FC8E830ED2,
	BrownAiHelicopterScript_tB45E14166B167B5CEC303CE7B4223202D6AC7905_CustomAttributesCacheGenerator_BrownAiHelicopterScript_wait_m2F59CFA6957E47B9D0B98D2DB0CBC02F566206E1,
	BrownAiHelicopterScript_tB45E14166B167B5CEC303CE7B4223202D6AC7905_CustomAttributesCacheGenerator_BrownAiHelicopterScript_BoostHelicopterNow_mA24205F67B91CA8E96E37EE86EF4FEED72487AF8,
	U3CwaitU3Ed__8_t4CDB4A05B9DD4AAD7D14AE1D72498CAB758B6E70_CustomAttributesCacheGenerator_U3CwaitU3Ed__8__ctor_m9CC191CFC8A05A0F2F712CB78E3D3C712D1B3BA8,
	U3CwaitU3Ed__8_t4CDB4A05B9DD4AAD7D14AE1D72498CAB758B6E70_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_IDisposable_Dispose_mB7ED5E1352F730692DA3719E632FE932074ECEEA,
	U3CwaitU3Ed__8_t4CDB4A05B9DD4AAD7D14AE1D72498CAB758B6E70_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD9CC63EC2CA3F08870E2F09F5419F073045356EB,
	U3CwaitU3Ed__8_t4CDB4A05B9DD4AAD7D14AE1D72498CAB758B6E70_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_IEnumerator_Reset_m8EDAC25117B4E697F2F433B11665FD810FB4FDB5,
	U3CwaitU3Ed__8_t4CDB4A05B9DD4AAD7D14AE1D72498CAB758B6E70_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_IEnumerator_get_Current_m2759321C2416D426BC60F3045E046A528C23E938,
	U3CBoostHelicopterNowU3Ed__12_tE2800D3B1FB7B0E8A590DE33366E6B3BF4B2D974_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12__ctor_m59B5CA3733B514DC5D43AB2BC40265EE4B6324CD,
	U3CBoostHelicopterNowU3Ed__12_tE2800D3B1FB7B0E8A590DE33366E6B3BF4B2D974_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_IDisposable_Dispose_mEFD999D4EF533ABFE2E27B1A50696CD34CF077B3,
	U3CBoostHelicopterNowU3Ed__12_tE2800D3B1FB7B0E8A590DE33366E6B3BF4B2D974_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31180F90051ED150F895B5D6062E8C7EB35A6099,
	U3CBoostHelicopterNowU3Ed__12_tE2800D3B1FB7B0E8A590DE33366E6B3BF4B2D974_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_Reset_m7F1A5716FF8139EDB930D8D75E2DA42121B3CA40,
	U3CBoostHelicopterNowU3Ed__12_tE2800D3B1FB7B0E8A590DE33366E6B3BF4B2D974_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_get_Current_m4B7EE880FE971084FA8869BB14C0B17EE6A9665A,
	GreenAiCharacterScript_tFB6FAD3CAFB282B7199451CDC805A155BBDB6683_CustomAttributesCacheGenerator_GreenAiCharacterScript_OnTriggerEnter_mE41DA5EA2AE137B60804896A6C5B1CAE142271E6,
	GreenAiCharacterScript_tFB6FAD3CAFB282B7199451CDC805A155BBDB6683_CustomAttributesCacheGenerator_GreenAiCharacterScript_IsClimbing_m6104549DB6A64AC33BA7A9B683A4CE300389D87F,
	U3COnTriggerEnterU3Ed__11_t904EB5FF89899A024A6C18963357BD34EBCDA16A_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11__ctor_m9A69DC7769006C0580046888DE33D5B3497E3158,
	U3COnTriggerEnterU3Ed__11_t904EB5FF89899A024A6C18963357BD34EBCDA16A_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_IDisposable_Dispose_m1FD5231A948EFABE2395EEDB9C08D376536BB94E,
	U3COnTriggerEnterU3Ed__11_t904EB5FF89899A024A6C18963357BD34EBCDA16A_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m468266D865ED51B5686F8402CA721DCFD254D240,
	U3COnTriggerEnterU3Ed__11_t904EB5FF89899A024A6C18963357BD34EBCDA16A_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_Reset_mA18241CE3ADA61237D0B8ABE12D3EEF13C49C69C,
	U3COnTriggerEnterU3Ed__11_t904EB5FF89899A024A6C18963357BD34EBCDA16A_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_get_Current_m97498923C25B468E44FBDA18864B3E10ADEB7FB2,
	U3CIsClimbingU3Ed__14_tC09D56759286ACB8E965E213553A8F190465063B_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14__ctor_m101C21898DF05DB161D452C96753EEF525BD7066,
	U3CIsClimbingU3Ed__14_tC09D56759286ACB8E965E213553A8F190465063B_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_IDisposable_Dispose_m6DBD346A64EE02ED6EDA1E3B242DDF77878C0065,
	U3CIsClimbingU3Ed__14_tC09D56759286ACB8E965E213553A8F190465063B_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1470B7C3C6547F5A6DECCCD4E3811F7866E27ECC,
	U3CIsClimbingU3Ed__14_tC09D56759286ACB8E965E213553A8F190465063B_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_Reset_mCCC1079F77ABAA01075E34E30346DB874C61403E,
	U3CIsClimbingU3Ed__14_tC09D56759286ACB8E965E213553A8F190465063B_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_get_Current_m6BC85BF85A7A3BA1C81AD1A4D9EA1C7D25D7DECE,
	GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_GreenAiControllerScript_MoveObject_m428A17DBAFDD480889D2DC0910582B073B9BF276,
	GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_GreenAiControllerScript_Transform_m0C755F0EE9BB07E4D98ACD902D5D7D18B73EA0AC,
	GreenAiControllerScript_t9F35E3DA9CBCA51A33A82895CE8047E8179ADD59_CustomAttributesCacheGenerator_GreenAiControllerScript_TransformToRandom_mC3F7D234B52D8EB4EE29EBCE9DEB9F15F1A3F5DC,
	U3CMoveObjectU3Ed__35_t8C90635DD28CBB7A85B947ED0AC579459C340A2F_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35__ctor_m179E5FE2DF01B88D84D0D14765488F47DA4AB678,
	U3CMoveObjectU3Ed__35_t8C90635DD28CBB7A85B947ED0AC579459C340A2F_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_IDisposable_Dispose_m9D27A724FD86ED921543813A4E7260E9A7F7B5D0,
	U3CMoveObjectU3Ed__35_t8C90635DD28CBB7A85B947ED0AC579459C340A2F_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD70A6C3669063054A3585BC89F8525F8A5223E7D,
	U3CMoveObjectU3Ed__35_t8C90635DD28CBB7A85B947ED0AC579459C340A2F_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_Reset_m817F8AD405448B9EDBC2F07BF7F5F83745689AD2,
	U3CMoveObjectU3Ed__35_t8C90635DD28CBB7A85B947ED0AC579459C340A2F_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_get_Current_m199D40D6301C2AAB59A577024977E61946E95F8D,
	U3CTransformU3Ed__38_tB809B2C3D179480B2C9A54CCE64435C10F528D02_CustomAttributesCacheGenerator_U3CTransformU3Ed__38__ctor_mDA513F4551EA387CBE55ADCA55E2BA755F388B9D,
	U3CTransformU3Ed__38_tB809B2C3D179480B2C9A54CCE64435C10F528D02_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_IDisposable_Dispose_m48F34B6CA28107A6D9F588D85396B9C016B27D72,
	U3CTransformU3Ed__38_tB809B2C3D179480B2C9A54CCE64435C10F528D02_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCAC7D326D7467468072FA99AC95AB8A772F0D6EE,
	U3CTransformU3Ed__38_tB809B2C3D179480B2C9A54CCE64435C10F528D02_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_IEnumerator_Reset_mBE648FB20A3F7600472F2EEC5B8A27DA34001E66,
	U3CTransformU3Ed__38_tB809B2C3D179480B2C9A54CCE64435C10F528D02_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_IEnumerator_get_Current_m2D984DD67D92B18296EDFFE2E4629A37B611271B,
	U3CTransformToRandomU3Ed__39_t6A0F73153B2100DF513A4EB28E9307EFCCBF456F_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39__ctor_mA913800A026FDE676DCBEE781F12E7DB50359C3D,
	U3CTransformToRandomU3Ed__39_t6A0F73153B2100DF513A4EB28E9307EFCCBF456F_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_IDisposable_Dispose_m2BC9278A0B00FAAC4B9BDAF82C99D175095C258F,
	U3CTransformToRandomU3Ed__39_t6A0F73153B2100DF513A4EB28E9307EFCCBF456F_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m918D1A177676B7AADDB603964ECF77380C9450FB,
	U3CTransformToRandomU3Ed__39_t6A0F73153B2100DF513A4EB28E9307EFCCBF456F_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_Reset_m7FB7AABD45545A92964F18E329B23FF75F0EF2F7,
	U3CTransformToRandomU3Ed__39_t6A0F73153B2100DF513A4EB28E9307EFCCBF456F_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_get_Current_mB99F58C3386E898F1FA7C1FD7E8BC1C92D53D7CE,
	GreenAiHelicopterScript_tC681D9410CB1ECED4FD67F37A4E58A2DC586E135_CustomAttributesCacheGenerator_GreenAiHelicopterScript_wait_m0AF37D5CFAE7BB8285C0EEFB023914D5828D75A3,
	GreenAiHelicopterScript_tC681D9410CB1ECED4FD67F37A4E58A2DC586E135_CustomAttributesCacheGenerator_GreenAiHelicopterScript_BoostHelicopterNow_m2C697483CBC2445C941B05ED38FA730989961DDB,
	U3CwaitU3Ed__8_t5AD13EABC4D1FF4CB47B9ED66FF3489CD08BAFA0_CustomAttributesCacheGenerator_U3CwaitU3Ed__8__ctor_mDCC058A30FD6F11314464F59B1D944452DEDE31A,
	U3CwaitU3Ed__8_t5AD13EABC4D1FF4CB47B9ED66FF3489CD08BAFA0_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_IDisposable_Dispose_m32B292493C2C2097837B382D6F3BBD3FE5936571,
	U3CwaitU3Ed__8_t5AD13EABC4D1FF4CB47B9ED66FF3489CD08BAFA0_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD11542748D2157BF3ECB8D0449FB8EBBF824C2C6,
	U3CwaitU3Ed__8_t5AD13EABC4D1FF4CB47B9ED66FF3489CD08BAFA0_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_IEnumerator_Reset_mD4370B65472947DCCF75489E73F2FCDA3C060708,
	U3CwaitU3Ed__8_t5AD13EABC4D1FF4CB47B9ED66FF3489CD08BAFA0_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_IEnumerator_get_Current_mCDA0D7011D71B6E161157D399A915B705D0F4356,
	U3CBoostHelicopterNowU3Ed__12_t0C37D0395174896A6E5A3706E86C72010B8D4768_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12__ctor_mBBE717D794FC2AA7DD4F448CA792743B1729B381,
	U3CBoostHelicopterNowU3Ed__12_t0C37D0395174896A6E5A3706E86C72010B8D4768_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_IDisposable_Dispose_m86EE5D0048CCD362408A8A627CA7C249E7B22704,
	U3CBoostHelicopterNowU3Ed__12_t0C37D0395174896A6E5A3706E86C72010B8D4768_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m757362FC7C672367D082B76F985400626F3704D5,
	U3CBoostHelicopterNowU3Ed__12_t0C37D0395174896A6E5A3706E86C72010B8D4768_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_Reset_m80DE021A4A053E4CA424F5CE4BBD788881F70BD5,
	U3CBoostHelicopterNowU3Ed__12_t0C37D0395174896A6E5A3706E86C72010B8D4768_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_get_Current_mACFAF342ACBF07EC3B30B9176B0D4B5374E94A2E,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_IsLevelFailed_m70DE3501FE1BEEC60023C7A9F649FD8640D97E67,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_RestartLevell_m5AB99A05E7E5BDEBB8CA7F99D6CA514F1C3A5FD6,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_isGameCompleted_m5C2694B3FDB66DED81583ECC138D4BF54463CFFD,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_GameManager_StartNextLevel_mD9B579F6AFA8F8346C0A922BDBC727F750C52E10,
	U3CIsLevelFailedU3Ed__21_t99F7A1A7AC53B84C4087969ED4E8D0824AA06C9B_CustomAttributesCacheGenerator_U3CIsLevelFailedU3Ed__21__ctor_mD10B2FD438BC1BEE23004BD942AB31491C0A124E,
	U3CIsLevelFailedU3Ed__21_t99F7A1A7AC53B84C4087969ED4E8D0824AA06C9B_CustomAttributesCacheGenerator_U3CIsLevelFailedU3Ed__21_System_IDisposable_Dispose_m076581E89A05A366FBAF808DE98241BDA23E05EB,
	U3CIsLevelFailedU3Ed__21_t99F7A1A7AC53B84C4087969ED4E8D0824AA06C9B_CustomAttributesCacheGenerator_U3CIsLevelFailedU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC589789B869BCDF5F0F5F602E0404335DBE4644E,
	U3CIsLevelFailedU3Ed__21_t99F7A1A7AC53B84C4087969ED4E8D0824AA06C9B_CustomAttributesCacheGenerator_U3CIsLevelFailedU3Ed__21_System_Collections_IEnumerator_Reset_m8A3ADC2BABE6D0B8A7B65EEE2CEBEF36F600C629,
	U3CIsLevelFailedU3Ed__21_t99F7A1A7AC53B84C4087969ED4E8D0824AA06C9B_CustomAttributesCacheGenerator_U3CIsLevelFailedU3Ed__21_System_Collections_IEnumerator_get_Current_m5A479CAD77A871F46E639F24FA9DC80CED301DE6,
	U3CRestartLevellU3Ed__23_t9404402CF834E4FB4B8C2B286258480C1252A279_CustomAttributesCacheGenerator_U3CRestartLevellU3Ed__23__ctor_mD698104A3344AAD8AB7F375C1FCDFE48A90FEF8A,
	U3CRestartLevellU3Ed__23_t9404402CF834E4FB4B8C2B286258480C1252A279_CustomAttributesCacheGenerator_U3CRestartLevellU3Ed__23_System_IDisposable_Dispose_m917325C36BA67A294D14EC471D182B556BCE62D7,
	U3CRestartLevellU3Ed__23_t9404402CF834E4FB4B8C2B286258480C1252A279_CustomAttributesCacheGenerator_U3CRestartLevellU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE78F622C02552C6AFC3AC0A35FA99A639B581A06,
	U3CRestartLevellU3Ed__23_t9404402CF834E4FB4B8C2B286258480C1252A279_CustomAttributesCacheGenerator_U3CRestartLevellU3Ed__23_System_Collections_IEnumerator_Reset_m631238FEBA0FDA032E30FF7EEA31F50A1E220ADA,
	U3CRestartLevellU3Ed__23_t9404402CF834E4FB4B8C2B286258480C1252A279_CustomAttributesCacheGenerator_U3CRestartLevellU3Ed__23_System_Collections_IEnumerator_get_Current_m509B652FC4478E31C51E1EADF6D16C8D8D1F3E68,
	U3CisGameCompletedU3Ed__25_tDADD3FD80F4A36560EF3A84C13B29DD66BDB3595_CustomAttributesCacheGenerator_U3CisGameCompletedU3Ed__25__ctor_m2957E84768DAED7DE588D515A6A1CE2B39C026C7,
	U3CisGameCompletedU3Ed__25_tDADD3FD80F4A36560EF3A84C13B29DD66BDB3595_CustomAttributesCacheGenerator_U3CisGameCompletedU3Ed__25_System_IDisposable_Dispose_m3ADF2A8F3FA8F0BE8595C6CA0625BC752B5B852E,
	U3CisGameCompletedU3Ed__25_tDADD3FD80F4A36560EF3A84C13B29DD66BDB3595_CustomAttributesCacheGenerator_U3CisGameCompletedU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m42B4BF6B0DC26D917B0E47D89DCF7AD201804E77,
	U3CisGameCompletedU3Ed__25_tDADD3FD80F4A36560EF3A84C13B29DD66BDB3595_CustomAttributesCacheGenerator_U3CisGameCompletedU3Ed__25_System_Collections_IEnumerator_Reset_m35439684D43C4B3E3217C106A4C1939FF2C02A97,
	U3CisGameCompletedU3Ed__25_tDADD3FD80F4A36560EF3A84C13B29DD66BDB3595_CustomAttributesCacheGenerator_U3CisGameCompletedU3Ed__25_System_Collections_IEnumerator_get_Current_mA00BAAD4560CDC6CF6CC18BC9435689084F4D065,
	U3CStartNextLevelU3Ed__27_t3813EED139F1C6383D0388371F447092CF4B61AF_CustomAttributesCacheGenerator_U3CStartNextLevelU3Ed__27__ctor_m84E1B3F4F8E539CC4A3741E7A8B6958EB8D0AEBC,
	U3CStartNextLevelU3Ed__27_t3813EED139F1C6383D0388371F447092CF4B61AF_CustomAttributesCacheGenerator_U3CStartNextLevelU3Ed__27_System_IDisposable_Dispose_m475BB8B13202250A345D0FADAD797D36B9E403EF,
	U3CStartNextLevelU3Ed__27_t3813EED139F1C6383D0388371F447092CF4B61AF_CustomAttributesCacheGenerator_U3CStartNextLevelU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEFE495A03A02293456740B5F11C350835D1396FF,
	U3CStartNextLevelU3Ed__27_t3813EED139F1C6383D0388371F447092CF4B61AF_CustomAttributesCacheGenerator_U3CStartNextLevelU3Ed__27_System_Collections_IEnumerator_Reset_m720323159B46EA9B529D6511653DB54F79272B18,
	U3CStartNextLevelU3Ed__27_t3813EED139F1C6383D0388371F447092CF4B61AF_CustomAttributesCacheGenerator_U3CStartNextLevelU3Ed__27_System_Collections_IEnumerator_get_Current_m5B09F71419B61AF21B156A7DF3D43E1995EBE375,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_UIManager_PlayToast_m033AE9F7CFA3D901AF314A312D49B4E6085D3195,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_UIManager_U3CAwakeU3Eb__31_0_m3122A16C76C0271B4114543368B4AE16D96F87C5,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_UIManager_U3CAwakeU3Eb__31_1_mE40D9985291979A55391AD6F32851A8B125816EE,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_UIManager_U3CAwakeU3Eb__31_2_m9D115D348778764D78362E208E83AE06A029371A,
	U3CPlayToastU3Ed__44_tE9527CA78E775091AD4691E0A2C5F212C2AA847E_CustomAttributesCacheGenerator_U3CPlayToastU3Ed__44__ctor_mEAE19C120DB097EDD95DBE6FDE063A848F41EBA6,
	U3CPlayToastU3Ed__44_tE9527CA78E775091AD4691E0A2C5F212C2AA847E_CustomAttributesCacheGenerator_U3CPlayToastU3Ed__44_System_IDisposable_Dispose_mF18FC39DAF7831611A1FF47716A054C35DE81982,
	U3CPlayToastU3Ed__44_tE9527CA78E775091AD4691E0A2C5F212C2AA847E_CustomAttributesCacheGenerator_U3CPlayToastU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE9752D8C7E3C66B17F947A16473FD29B271BA717,
	U3CPlayToastU3Ed__44_tE9527CA78E775091AD4691E0A2C5F212C2AA847E_CustomAttributesCacheGenerator_U3CPlayToastU3Ed__44_System_Collections_IEnumerator_Reset_m2349BD58837265E99891C68919BDBA1C7B12C32C,
	U3CPlayToastU3Ed__44_tE9527CA78E775091AD4691E0A2C5F212C2AA847E_CustomAttributesCacheGenerator_U3CPlayToastU3Ed__44_System_Collections_IEnumerator_get_Current_m147D97967E8C7C1764873D48F08478FE3C16E7E6,
	PlayerCharacterScript_t37FDF6D7062F616CD3A0F50F67268935781D950E_CustomAttributesCacheGenerator_PlayerCharacterScript_OnTriggerEnter_mDBE8BD73187B0A928C13B9E0275563B9B2569958,
	PlayerCharacterScript_t37FDF6D7062F616CD3A0F50F67268935781D950E_CustomAttributesCacheGenerator_PlayerCharacterScript_IsClimbing_m19886905495A4440976ACF0F2D25BA17011DDAD0,
	U3COnTriggerEnterU3Ed__11_t7D3D7DBF022F6B334B59C160F44F85726BCCFF2B_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11__ctor_m157690149B9EFC064D41233255CFF1D06F6E39EA,
	U3COnTriggerEnterU3Ed__11_t7D3D7DBF022F6B334B59C160F44F85726BCCFF2B_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_IDisposable_Dispose_m3608C29B0A3E3E32B1042BB1B65824B7BCBEC6C1,
	U3COnTriggerEnterU3Ed__11_t7D3D7DBF022F6B334B59C160F44F85726BCCFF2B_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2CB4290008F905A4825A8D09AC73523EED0D715B,
	U3COnTriggerEnterU3Ed__11_t7D3D7DBF022F6B334B59C160F44F85726BCCFF2B_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_Reset_m980E27465640D7E4636D68BEDFDD1222A71C3454,
	U3COnTriggerEnterU3Ed__11_t7D3D7DBF022F6B334B59C160F44F85726BCCFF2B_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_get_Current_m298E21F99DFA30907415C3D27D3AF0AD04F4B3FD,
	U3CIsClimbingU3Ed__13_tC644B10DB02DA8C5D0589F7F40CAE7B33A0161A2_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__13__ctor_mE046B29894718B2CEE95FC9F23E6C957C3FFA998,
	U3CIsClimbingU3Ed__13_tC644B10DB02DA8C5D0589F7F40CAE7B33A0161A2_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__13_System_IDisposable_Dispose_m367474145BDE9A57815C89B1370EE3DC4F317AD0,
	U3CIsClimbingU3Ed__13_tC644B10DB02DA8C5D0589F7F40CAE7B33A0161A2_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8D8834D7DDF29F107743C566E8036B4323BB1688,
	U3CIsClimbingU3Ed__13_tC644B10DB02DA8C5D0589F7F40CAE7B33A0161A2_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__13_System_Collections_IEnumerator_Reset_m271A0D38A671C486E12E68AD956D59A536B65DE7,
	U3CIsClimbingU3Ed__13_tC644B10DB02DA8C5D0589F7F40CAE7B33A0161A2_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__13_System_Collections_IEnumerator_get_Current_mA4A718648508AFFC8455A5D13E62BB8091F46067,
	PlayerControllerScript_t868EC711AD49AF708AB2D51C72A369BBC6782C4D_CustomAttributesCacheGenerator_PlayerControllerScript_MoveObject_mFDBF554082AA89295044CEA450F59F2184EF2FF4,
	U3CMoveObjectU3Ed__34_t2EACAA28BCAB665BCBEF0F835037712D648BDC90_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__34__ctor_m7679949E4579F0514F360B36E50FF224BD59B3DA,
	U3CMoveObjectU3Ed__34_t2EACAA28BCAB665BCBEF0F835037712D648BDC90_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__34_System_IDisposable_Dispose_mE95ED8C6FDC5F84ED1D757A53DA602E240778666,
	U3CMoveObjectU3Ed__34_t2EACAA28BCAB665BCBEF0F835037712D648BDC90_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0A7854A57AE804B9BB83BDE78DB37D0766FB65CD,
	U3CMoveObjectU3Ed__34_t2EACAA28BCAB665BCBEF0F835037712D648BDC90_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__34_System_Collections_IEnumerator_Reset_m1752A1686309401EC13831FBF562A3FFAEBB69B1,
	U3CMoveObjectU3Ed__34_t2EACAA28BCAB665BCBEF0F835037712D648BDC90_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__34_System_Collections_IEnumerator_get_Current_m7B5FC4B34965C4A5011095B7BAE82B2399652B2E,
	PlayerHelicopterScript_t9B7E46925E36AB47E790EA303E302417206DAEA1_CustomAttributesCacheGenerator_PlayerHelicopterScript_wait_m2A2F63CFEB547FFAF4C6D64FB02D898B7244894E,
	PlayerHelicopterScript_t9B7E46925E36AB47E790EA303E302417206DAEA1_CustomAttributesCacheGenerator_PlayerHelicopterScript_BoostHelicopterNow_m394AF4AD515DAF71CD02DD1BD10B8904F6B9F68A,
	U3CwaitU3Ed__8_t53F34A2BCB132591FD143778B01D2B7B336DACD4_CustomAttributesCacheGenerator_U3CwaitU3Ed__8__ctor_m6D1BEC088C88A0A2E14F1560F02C07733ABBE2F9,
	U3CwaitU3Ed__8_t53F34A2BCB132591FD143778B01D2B7B336DACD4_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_IDisposable_Dispose_m78B42166C4DD382EFDFA89374A11B19BFE49A446,
	U3CwaitU3Ed__8_t53F34A2BCB132591FD143778B01D2B7B336DACD4_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC4E14E44E3C1B5EA4BD55F528B43CB7B250DC8A7,
	U3CwaitU3Ed__8_t53F34A2BCB132591FD143778B01D2B7B336DACD4_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_IEnumerator_Reset_mFC68D47EBA84A0CB1C50D57E3A6ECF7C7C2EFB0F,
	U3CwaitU3Ed__8_t53F34A2BCB132591FD143778B01D2B7B336DACD4_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_IEnumerator_get_Current_mF4AA3F4158AEC974F5500B66AB2C5D0E7F560C0F,
	U3CBoostHelicopterNowU3Ed__12_t120B7E3243DD041063D08B99AF5918819E8BA72E_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12__ctor_mA348438E255D83BF6D31C4A904B1C9D0BFDF18BB,
	U3CBoostHelicopterNowU3Ed__12_t120B7E3243DD041063D08B99AF5918819E8BA72E_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_IDisposable_Dispose_m4C43CFFF5FF0DEC50CA5F4AF019157A2404DFA69,
	U3CBoostHelicopterNowU3Ed__12_t120B7E3243DD041063D08B99AF5918819E8BA72E_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAA789A47FD4E85D770398F31F458D991E57D5822,
	U3CBoostHelicopterNowU3Ed__12_t120B7E3243DD041063D08B99AF5918819E8BA72E_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_Reset_mEAD31E2B8E6B9460D8BC89E00A58DC568DFEDCE7,
	U3CBoostHelicopterNowU3Ed__12_t120B7E3243DD041063D08B99AF5918819E8BA72E_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_get_Current_m1E39FFAB0BA8AD5590084840D6C6B1147B388A3A,
	WhiteAiCharacterScript_t72246C2225D7E9E1B8C835DE0E4B680818E6AE66_CustomAttributesCacheGenerator_WhiteAiCharacterScript_OnTriggerEnter_mF98389C957252DAA658D0C6782844B50DD3BB48A,
	WhiteAiCharacterScript_t72246C2225D7E9E1B8C835DE0E4B680818E6AE66_CustomAttributesCacheGenerator_WhiteAiCharacterScript_IsClimbing_m96BAB0B2C35DDFC80B654B636317E9C44D8D2C82,
	U3COnTriggerEnterU3Ed__11_tD832E0B5E21EE60EB7EE639B091415AA2A37570C_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11__ctor_mEBE528454705838D7F327CA5E247BA9D56E91EF7,
	U3COnTriggerEnterU3Ed__11_tD832E0B5E21EE60EB7EE639B091415AA2A37570C_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_IDisposable_Dispose_m516C2E127C3B3045FEEF4FF7AAA607FE145EDEF9,
	U3COnTriggerEnterU3Ed__11_tD832E0B5E21EE60EB7EE639B091415AA2A37570C_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3A149DD87BBDCDA6FB6654FCB0075EF9BA2BF827,
	U3COnTriggerEnterU3Ed__11_tD832E0B5E21EE60EB7EE639B091415AA2A37570C_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_Reset_m9A6AF1050DBC028A87125D7D75181368217E7842,
	U3COnTriggerEnterU3Ed__11_tD832E0B5E21EE60EB7EE639B091415AA2A37570C_CustomAttributesCacheGenerator_U3COnTriggerEnterU3Ed__11_System_Collections_IEnumerator_get_Current_m89517AA72A5C2553EA773B7BA5633B6E782E8B5D,
	U3CIsClimbingU3Ed__14_tCB8320995774A93BA1088528A0CF69A513364A72_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14__ctor_mD50ABC879D4CF442FE82D0600C944384F089EE09,
	U3CIsClimbingU3Ed__14_tCB8320995774A93BA1088528A0CF69A513364A72_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_IDisposable_Dispose_m24EADF4B39D6E9C8BB81DD85DF9C5DF33E513E0B,
	U3CIsClimbingU3Ed__14_tCB8320995774A93BA1088528A0CF69A513364A72_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD193DA26A09635216183F2B99C9518351610909D,
	U3CIsClimbingU3Ed__14_tCB8320995774A93BA1088528A0CF69A513364A72_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_Reset_m3001C0A5FCB20D56031F2BEAA18BBDED1E26C86C,
	U3CIsClimbingU3Ed__14_tCB8320995774A93BA1088528A0CF69A513364A72_CustomAttributesCacheGenerator_U3CIsClimbingU3Ed__14_System_Collections_IEnumerator_get_Current_m1F4082A4FC0430E5D727271AC49902284A2326E9,
	WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_WhiteAiControllerScript_MoveObject_mD0A4644E2AC3C736E82D453DD534CD123D126A18,
	WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_WhiteAiControllerScript_Transform_m6D5DF53567C35622727179CED636CC9F5432E595,
	WhiteAiControllerScript_t49D6932C8DF203DBEE6865A7EC6AC8D6949815E5_CustomAttributesCacheGenerator_WhiteAiControllerScript_TransformToRandom_mEE31E2CE3B2E990473C6C3FF5DA49ECABB3C8368,
	U3CMoveObjectU3Ed__35_tDA1527E60714BAB33FB5F6D33DF5CD891AF3BF37_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35__ctor_m46FAD7C53D13345B8A77D4EC983C9B26FB309376,
	U3CMoveObjectU3Ed__35_tDA1527E60714BAB33FB5F6D33DF5CD891AF3BF37_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_IDisposable_Dispose_m17574B27BD597AD8546BC11CD6A58C8EB9040CBE,
	U3CMoveObjectU3Ed__35_tDA1527E60714BAB33FB5F6D33DF5CD891AF3BF37_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE77F08164CB742A73CC623896C303BE46F18789,
	U3CMoveObjectU3Ed__35_tDA1527E60714BAB33FB5F6D33DF5CD891AF3BF37_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_Reset_m74279CE4296C2A6C033D3378BFB1E1B48CFF9FF0,
	U3CMoveObjectU3Ed__35_tDA1527E60714BAB33FB5F6D33DF5CD891AF3BF37_CustomAttributesCacheGenerator_U3CMoveObjectU3Ed__35_System_Collections_IEnumerator_get_Current_m159DA0631BCEEC3665E0265F971D1F9B27D6472C,
	U3CTransformU3Ed__38_tD5DE24B17D1D0A5F1FC0FE78E3872F10DFF2110B_CustomAttributesCacheGenerator_U3CTransformU3Ed__38__ctor_m9BD5E5BF43D39641AC0247391E4797A7319B1AD4,
	U3CTransformU3Ed__38_tD5DE24B17D1D0A5F1FC0FE78E3872F10DFF2110B_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_IDisposable_Dispose_m0B2B7B2528B459935A3F95DFE3E91B482848C801,
	U3CTransformU3Ed__38_tD5DE24B17D1D0A5F1FC0FE78E3872F10DFF2110B_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB25722084BED3E54E2949574C55216D8F602E057,
	U3CTransformU3Ed__38_tD5DE24B17D1D0A5F1FC0FE78E3872F10DFF2110B_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_IEnumerator_Reset_mABC4575D59FB478E3D6019D8FFD280E2B7777ACE,
	U3CTransformU3Ed__38_tD5DE24B17D1D0A5F1FC0FE78E3872F10DFF2110B_CustomAttributesCacheGenerator_U3CTransformU3Ed__38_System_Collections_IEnumerator_get_Current_m6148983E37AABFC28BA9819C013473ABF88C5F77,
	U3CTransformToRandomU3Ed__39_t22FBD7D18E312477264739FA52451E2465202093_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39__ctor_mE4E2E68B2E5D58BF723A15D1B39E372FC8B04C43,
	U3CTransformToRandomU3Ed__39_t22FBD7D18E312477264739FA52451E2465202093_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_IDisposable_Dispose_m6194F8626FBFB3D09711A844BE285169751F003F,
	U3CTransformToRandomU3Ed__39_t22FBD7D18E312477264739FA52451E2465202093_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C6B2A3331459A3A3E8A7640B058F0B0C99BAD48,
	U3CTransformToRandomU3Ed__39_t22FBD7D18E312477264739FA52451E2465202093_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_Reset_m80BF425047633FFBDC3BD6A67D0A4C77490717F0,
	U3CTransformToRandomU3Ed__39_t22FBD7D18E312477264739FA52451E2465202093_CustomAttributesCacheGenerator_U3CTransformToRandomU3Ed__39_System_Collections_IEnumerator_get_Current_mED3B378B7A0BC6D929D9A73DED17AC2D7CE5C3DB,
	WhiteAiHelicopterScript_tBAB805EB8B869DA41835FD46A6E627F428F3202E_CustomAttributesCacheGenerator_WhiteAiHelicopterScript_wait_mED718A5D65ACB507B40E9AB7C3D5A527C94608F0,
	WhiteAiHelicopterScript_tBAB805EB8B869DA41835FD46A6E627F428F3202E_CustomAttributesCacheGenerator_WhiteAiHelicopterScript_BoostHelicopterNow_mADA21CCA99D5687E6BC26F7BD3193889086B4AD8,
	U3CwaitU3Ed__8_t3902C28528005BABE7C375AF2E394292E6067C45_CustomAttributesCacheGenerator_U3CwaitU3Ed__8__ctor_mCDB3D87DB2A64800169702361168C4CDC6B3D37C,
	U3CwaitU3Ed__8_t3902C28528005BABE7C375AF2E394292E6067C45_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_IDisposable_Dispose_mABACCB7EB2890077CA87A31B4FD0CF548274E171,
	U3CwaitU3Ed__8_t3902C28528005BABE7C375AF2E394292E6067C45_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8272CCE65FF2619EBBA0D2C0A7BD4212C3125869,
	U3CwaitU3Ed__8_t3902C28528005BABE7C375AF2E394292E6067C45_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_IEnumerator_Reset_mB2BE0C91E4E9A386047094591B5B86CDA8DB7810,
	U3CwaitU3Ed__8_t3902C28528005BABE7C375AF2E394292E6067C45_CustomAttributesCacheGenerator_U3CwaitU3Ed__8_System_Collections_IEnumerator_get_Current_mABCAEFC067279C876F020818FBBAEF9CBF999BD6,
	U3CBoostHelicopterNowU3Ed__12_tEB7AB33D399DA7AE43888DB96F8DAA6F18B2EB94_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12__ctor_mEBCFF3C87A4DB90A3566DB58CC56588429F746EE,
	U3CBoostHelicopterNowU3Ed__12_tEB7AB33D399DA7AE43888DB96F8DAA6F18B2EB94_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_IDisposable_Dispose_mA987C60121DC0B5C6BEA47187B70D7D69BB47BB8,
	U3CBoostHelicopterNowU3Ed__12_tEB7AB33D399DA7AE43888DB96F8DAA6F18B2EB94_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2931D5D08309E702D72BC52D93601CC4D7308A8F,
	U3CBoostHelicopterNowU3Ed__12_tEB7AB33D399DA7AE43888DB96F8DAA6F18B2EB94_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_Reset_mFF46627F7E9356628562AF2AA430E389043A735B,
	U3CBoostHelicopterNowU3Ed__12_tEB7AB33D399DA7AE43888DB96F8DAA6F18B2EB94_CustomAttributesCacheGenerator_U3CBoostHelicopterNowU3Ed__12_System_Collections_IEnumerator_get_Current_mDD5107EF134E88023F3DD165A5D3CB19F0FFA802,
	DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOColor_mA67D7B77174B479F8AE20F739A74BA7A2BBC9F0F,
	DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOFade_mEED04CEE3EE9D32B87478A3CFDB9CFB710307769,
	DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOGradientColor_m47670ABD64AEDC972E8210CAF4C8A778B0B7676D,
	DOTweenModuleSprite_t13F23C4EBAB3A379FF429E99421267128B73C247_CustomAttributesCacheGenerator_DOTweenModuleSprite_DOBlendableColor_m51BBDB4A69B0560481736E6734EA2BF52DB1537D,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_mF927A1EDCA91814D8A466A343E3223EEF4ED7478,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_m7067683474EE6A600294C2CCFD941403FA789A6E,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m41638D05EB7678C523D0D39E320FAC80B8E48658,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_mF4B41443760536F3C399612AD0283A52937D95CC,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_mE66C8B7D137E0DDE54AAA96F551811FB8BDB8754,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFillAmount_mE4ED52B826A6F8035320E980F7C701B4047C4DBA,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOGradientColor_mB4B8D1DEDA92464670EB8A6D985973477D51A4D5,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFlexibleSize_mF0916C4E7A906202EFBAEA89CC2B55A9C3CF48B3,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOMinSize_m54A6919625499AFD6F801C6EC9672965DBDC96E1,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPreferredSize_mE26AFB2C606F095483EDED449FB8A5A80C08A8DE,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_m15BC0FCF035E3DEEC3CFC30465EDE0FE6FD6D2EA,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m86DB80308D92FC4DFD3E981F9E958FF2A207EA74,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOScale_mFA27F29E3DFB6B1CFCAC3CE73F06DF3737A3EB06,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos_m63999D434CE7E8F6BE66BBA92532DE9D98DC9F79,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPosX_m9B57E02E9902C7A3F1A8DD1021AF630C10478380,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPosY_m14CCA63E545D7C28AD1A76488D0084F55EE94CA8,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3D_m6DEB7C4AE4CD2118BF83D268945340F3373DC7BB,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DX_mA40303AC769280BDE74EE58BE5AD80BEA427CE08,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DY_mE6BE56AE421B959F22CBA6318BABC3CD367A7277,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorPos3DZ_m702F7D4FE9DC8E6116FB5E0DD478D2BB88ED1242,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorMax_mDD97F6A8B4468622E3808507CB6B89A8A109BDCE,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOAnchorMin_m60C4775B1EEDBCF2CD3E96A8171C8A83FD7F400D,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivot_mF623939EDCC152F664FECC8B71E321B0AA13F0A9,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivotX_mC21C031A61BDB7C3A8087719F23CBC6D1D762D6B,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPivotY_m9922C28ADECA433362D3C6C816318CA00705996A,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOSizeDelta_mC4FD07525E1CA5BBF577030D571CE0CCEFD4E6B7,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOPunchAnchorPos_m058E3DA140B9C497727F05AEAA296425E585B9EA,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOShakeAnchorPos_m89D1C74E940B9434B2234FB49C031AD8FD1170AB,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOShakeAnchorPos_m7818867062313A535AB2CD6F86267E2AB9357AC6,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOJumpAnchorPos_m7046EE1C82AF0159CFAAA460E4C3D22797625480,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DONormalizedPos_m300F112FE2F67D4DC718083340C7FD27BC96F248,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOHorizontalNormalizedPos_mCB4BCD85B49C458BE871F890A86FEFE0D7F41B42,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOVerticalNormalizedPos_mC0269B4644816A0D5C09717CA92E4229CB31CDF1,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOValue_mFDF45D60F52C82AACD8B50859A51B8F315A44F08,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOColor_mFD36CA1EA683A0D2312EE87BCF3B0A924504E797,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOFade_m21F7CCBB732ABBD63BFD893D1A8D9837532A70D1,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOText_mC0932E45473278B2FFB1698954CC0EB0422C626F,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_m4C5E9A68BFBEE876E1FF8D8E9A4BA77856F144C1,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_m18196516A24780A550314E9327868D6D6F3EAA4E,
	DOTweenModuleUI_t3F1B2A064C1503EBADAF7AF43507802DC556417B_CustomAttributesCacheGenerator_DOTweenModuleUI_DOBlendableColor_mC30D1F805FE976FB25EC361AB0ABA94413361B6F,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOGradientColor_m7E9B0D004D4B3D602EBB852CA1755FE863C6B839,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOGradientColor_mA4E191E86F2EF81966259A40B2CDADE375437920,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForCompletion_m4BCEDFA5ED1ACF4BC1B3531C0C306A7AC553C1F2,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForRewind_mD9F37D5AAD9B665EE07F083D026E65194A8E3E6E,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForKill_m02BD7F3B018BB6B4ED4FCAF47B8DA435EC60BD6B,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForElapsedLoops_m46D46788899029A20F73BE252F7427D853131404,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForPosition_m1771625E294B38C5D13DCA40846AEA881C1B23E6,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_WaitForStart_mB4AAFE83B50AEF3A167F31C14B2F58794527F905,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOOffset_m09CA228EBED193314D3D9A41BBDC97393E1FC4D2,
	DOTweenModuleUnityVersion_tE9A51A0ECE5E1305EF2B335613C03596C6C30CA5_CustomAttributesCacheGenerator_DOTweenModuleUnityVersion_DOTiling_m6A3CD06EC3030A93EC3E93DE678F34048D5C1141,
	DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_CustomAttributesCacheGenerator_DOTweenModuleUtils_Init_mFF2188F42FA7128FE1A9BBC2CB9E6351919C9D15,
	DOTweenModuleUtils_t1924C98C4EB46411DA5B22524EB14EFDC0203EA0_CustomAttributesCacheGenerator_DOTweenModuleUtils_Preserver_mBE8D1FE2AE913FBFFCCF43B5A615D941B3706F43,
	Physics_t379352FCCF26C01744720033D3784D7F57DE0D8F_CustomAttributesCacheGenerator_Physics_HasRigidbody_m3082D72A658CCDDF6BB1F3D4A3EFBD6397D6E862,
	Physics_t379352FCCF26C01744720033D3784D7F57DE0D8F_CustomAttributesCacheGenerator_Physics_CreateDOTweenPathTween_m94D41B65E37500E5ACCF6B6F3B5D53FB031C4245,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
